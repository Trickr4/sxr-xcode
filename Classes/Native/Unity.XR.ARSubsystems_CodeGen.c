﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* XRCpuImage_OnAsyncConversionComplete_mE1632D9BCA9BB444DBB3283CBFE5567609FF98D0_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.XR.ARSubsystems.XRAnchor UnityEngine.XR.ARSubsystems.XRAnchor::get_defaultValue()
extern void XRAnchor_get_defaultValue_m457A914338467F05B7928AF1657C2447DDD38B96 ();
// 0x00000002 System.Void UnityEngine.XR.ARSubsystems.XRAnchor::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr)
extern void XRAnchor__ctor_m590E9A5E27E9C84C8C2AD35012271FE9C10A005E_AdjustorThunk ();
// 0x00000003 System.Void UnityEngine.XR.ARSubsystems.XRAnchor::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr,System.Guid)
extern void XRAnchor__ctor_m49CDF2A63F1027B771BC697C7A0CDB742C9DE739_AdjustorThunk ();
// 0x00000004 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRAnchor::get_trackableId()
extern void XRAnchor_get_trackableId_m7BD89E3F1664C126D09D8DD141EA18E8A9933711_AdjustorThunk ();
// 0x00000005 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRAnchor::get_pose()
extern void XRAnchor_get_pose_m7CA50F0FCB9FE7A6FB60C6AFD33B62AF4BE0CB1A_AdjustorThunk ();
// 0x00000006 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRAnchor::get_trackingState()
extern void XRAnchor_get_trackingState_m6C73E20FCB9A2E33666BD07816D04099D61EF3EF_AdjustorThunk ();
// 0x00000007 System.IntPtr UnityEngine.XR.ARSubsystems.XRAnchor::get_nativePtr()
extern void XRAnchor_get_nativePtr_m19C61EBCF7D12C860A76C60CAFF2E7B0FBDFF137_AdjustorThunk ();
// 0x00000008 System.Guid UnityEngine.XR.ARSubsystems.XRAnchor::get_sessionId()
extern void XRAnchor_get_sessionId_m3545F898B9A294B7D70434F25F439A1891D29FEE_AdjustorThunk ();
// 0x00000009 System.Int32 UnityEngine.XR.ARSubsystems.XRAnchor::GetHashCode()
extern void XRAnchor_GetHashCode_m593C577C80CA85C9354B112F494F4934E17EA369_AdjustorThunk ();
// 0x0000000A System.Boolean UnityEngine.XR.ARSubsystems.XRAnchor::Equals(UnityEngine.XR.ARSubsystems.XRAnchor)
extern void XRAnchor_Equals_m89DED9F057036C85F1987E32A651A9D59D60AAD7_AdjustorThunk ();
// 0x0000000B System.Boolean UnityEngine.XR.ARSubsystems.XRAnchor::Equals(System.Object)
extern void XRAnchor_Equals_m0EFEDC085E8C3080D6868D1540B933426F72A4A2_AdjustorThunk ();
// 0x0000000C System.Boolean UnityEngine.XR.ARSubsystems.XRAnchor::op_Equality(UnityEngine.XR.ARSubsystems.XRAnchor,UnityEngine.XR.ARSubsystems.XRAnchor)
extern void XRAnchor_op_Equality_m21E2BB8C91610B5AF27BC05A58705E74F1498800 ();
// 0x0000000D System.Boolean UnityEngine.XR.ARSubsystems.XRAnchor::op_Inequality(UnityEngine.XR.ARSubsystems.XRAnchor,UnityEngine.XR.ARSubsystems.XRAnchor)
extern void XRAnchor_op_Inequality_m01B9F6BCF82A24457752761541A482983357982E ();
// 0x0000000E System.Void UnityEngine.XR.ARSubsystems.XRAnchor::.cctor()
extern void XRAnchor__cctor_mC3E7FD4E2A9B8EF79EC58A30B00CF8DC481CB3FA ();
// 0x0000000F System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::.ctor()
extern void XRAnchorSubsystem__ctor_mB1BE6B896975D4F735B5CBBB3F30B7E350435A16 ();
// 0x00000010 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::OnStart()
extern void XRAnchorSubsystem_OnStart_m120BD231AFCC0B894C3B3BFF9A7343F0B5F53BB7 ();
// 0x00000011 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::OnStop()
extern void XRAnchorSubsystem_OnStop_m60282819F70508F3788AB414E64174B07C9DA935 ();
// 0x00000012 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::OnDestroyed()
extern void XRAnchorSubsystem_OnDestroyed_m4D13D1C9010C7CECAECDF8BE3342D06782CDE172 ();
// 0x00000013 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRAnchor> UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRAnchorSubsystem_GetChanges_mA9F45640A583B5E123F075733A25637AC4444D25 ();
// 0x00000014 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::TryAddAnchor(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void XRAnchorSubsystem_TryAddAnchor_m9F1AF4816CB4E057B7865296416B5F82AB40E381 ();
// 0x00000015 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::TryAttachAnchor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void XRAnchorSubsystem_TryAttachAnchor_m1D49B360C92C7448A369A63A28DE509DDFC8051C ();
// 0x00000016 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::TryRemoveAnchor(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XRAnchorSubsystem_TryRemoveAnchor_m95FFE9DECD2CA03F5B9C4C2B1DA58891725C43EE ();
// 0x00000017 UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider UnityEngine.XR.ARSubsystems.XRAnchorSubsystem::CreateProvider()
// 0x00000018 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor::get_supportsTrackableAttachments()
extern void XRAnchorSubsystemDescriptor_get_supportsTrackableAttachments_mC606DA1BE171F5FDFE670DD382FADA7A9E457C2A ();
// 0x00000019 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor::set_supportsTrackableAttachments(System.Boolean)
extern void XRAnchorSubsystemDescriptor_set_supportsTrackableAttachments_m6D69CEBBEAEFE86FAC483223DC138F53D926C21E ();
// 0x0000001A System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo)
extern void XRAnchorSubsystemDescriptor_Create_m1E770B2B67D3E34E14C84C952F4EFF171B020F1A ();
// 0x0000001B System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo)
extern void XRAnchorSubsystemDescriptor__ctor_m8EE0138294476EBE73B28511776747A3E16021F6 ();
// 0x0000001C UnityEngine.XR.ARSubsystems.XRReferencePoint UnityEngine.XR.ARSubsystems.XRReferencePoint::get_defaultValue()
extern void XRReferencePoint_get_defaultValue_mCCFAF4140E24AC2FDF1C8D19043E57B6BFEAC0AD ();
// 0x0000001D System.Void UnityEngine.XR.ARSubsystems.XRReferencePoint::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr)
extern void XRReferencePoint__ctor_mF823D8DC414C0B110A0C29F28668C1F3DA2B2997_AdjustorThunk ();
// 0x0000001E System.Void UnityEngine.XR.ARSubsystems.XRReferencePoint::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr,System.Guid)
extern void XRReferencePoint__ctor_m816965A70CDD2827DE0808A49B135E411E8532BB_AdjustorThunk ();
// 0x0000001F UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRReferencePoint::get_trackableId()
extern void XRReferencePoint_get_trackableId_m6D53542802F2444CE58861B8868274F9A8296D88_AdjustorThunk ();
// 0x00000020 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRReferencePoint::get_pose()
extern void XRReferencePoint_get_pose_mA4320629B8C7AE23D97FCD8E2C5FB9C9FB6AED9C_AdjustorThunk ();
// 0x00000021 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRReferencePoint::get_trackingState()
extern void XRReferencePoint_get_trackingState_mBA0DB4050B734039D22D0ECF69CD6E8896DF52B8_AdjustorThunk ();
// 0x00000022 System.IntPtr UnityEngine.XR.ARSubsystems.XRReferencePoint::get_nativePtr()
extern void XRReferencePoint_get_nativePtr_mE9EC85AD0E4976145CB0EDC4A74AA5BB076C5789_AdjustorThunk ();
// 0x00000023 System.Guid UnityEngine.XR.ARSubsystems.XRReferencePoint::get_sessionId()
extern void XRReferencePoint_get_sessionId_m5DCAF1725B8A29481940252D80634C99A3C2F0D0_AdjustorThunk ();
// 0x00000024 System.Int32 UnityEngine.XR.ARSubsystems.XRReferencePoint::GetHashCode()
extern void XRReferencePoint_GetHashCode_mD7BC968C92D3CC25E7D06502570A94B104F9E32C_AdjustorThunk ();
// 0x00000025 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePoint::Equals(UnityEngine.XR.ARSubsystems.XRReferencePoint)
extern void XRReferencePoint_Equals_mA58F0C1C266D740037A7D6700857A5E739160AF8_AdjustorThunk ();
// 0x00000026 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePoint::Equals(System.Object)
extern void XRReferencePoint_Equals_mD22BFD6609737E5CC6A31D2C1B519CD5207C89BC_AdjustorThunk ();
// 0x00000027 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePoint::op_Equality(UnityEngine.XR.ARSubsystems.XRReferencePoint,UnityEngine.XR.ARSubsystems.XRReferencePoint)
extern void XRReferencePoint_op_Equality_mBE34F72FA0469B0D2B97620A8C7F53C01E85A8BE ();
// 0x00000028 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePoint::op_Inequality(UnityEngine.XR.ARSubsystems.XRReferencePoint,UnityEngine.XR.ARSubsystems.XRReferencePoint)
extern void XRReferencePoint_op_Inequality_m675724084220C79C2F9A35E8D8462DD13146DDD0 ();
// 0x00000029 System.Void UnityEngine.XR.ARSubsystems.XRReferencePoint::.cctor()
extern void XRReferencePoint__cctor_m3A7635582CA05369AD6537861329B761A106DB82 ();
// 0x0000002A System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::.ctor()
extern void XRReferencePointSubsystem__ctor_mD93381DE24CA18A7BA022014E77BEBA9AA2CD6E9 ();
// 0x0000002B System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::OnStart()
extern void XRReferencePointSubsystem_OnStart_m60B74FB6CD375125B7623A7D0D8D4B7602B53AD8 ();
// 0x0000002C System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::OnStop()
extern void XRReferencePointSubsystem_OnStop_mB170405D1F702B112F844EF70F71B5AA54C345B3 ();
// 0x0000002D System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::OnDestroyed()
extern void XRReferencePointSubsystem_OnDestroyed_mCC9C916A1FA37AA028305374BF13E962E2260260 ();
// 0x0000002E UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint> UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRReferencePointSubsystem_GetChanges_mE0EC8049CED1EA604A751066DB97430E803BE487 ();
// 0x0000002F System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::TryAddReferencePoint(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRReferencePoint&)
extern void XRReferencePointSubsystem_TryAddReferencePoint_m55C922BC7F9943136A05B7E883D044CFBD5E4B87 ();
// 0x00000030 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::TryAttachReferencePoint(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRReferencePoint&)
extern void XRReferencePointSubsystem_TryAttachReferencePoint_mFC09929BC0AF19465D22E30685A9C213BE434B8E ();
// 0x00000031 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::TryRemoveReferencePoint(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XRReferencePointSubsystem_TryRemoveReferencePoint_m3F404A6F9DD63129EE44497554CC59CE8396DC22 ();
// 0x00000032 UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_Provider UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::CreateProvider()
// 0x00000033 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor::get_supportsTrackableAttachments()
extern void XRReferencePointSubsystemDescriptor_get_supportsTrackableAttachments_mA94E2928B96D7F5CC69B8D225F8FADDCDC80922D ();
// 0x00000034 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor::set_supportsTrackableAttachments(System.Boolean)
extern void XRReferencePointSubsystemDescriptor_set_supportsTrackableAttachments_mA4F7709D4C170D414F862490ABBF0090DE46A8AB ();
// 0x00000035 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo)
extern void XRReferencePointSubsystemDescriptor_Create_m4E9D9DF5FCE2FE3F8672653BC733F87D3A4327D0 ();
// 0x00000036 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo)
extern void XRReferencePointSubsystemDescriptor__ctor_mB5AD6FF2521FF148612D7662DF93BD6CA68069B2 ();
// 0x00000037 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraConfiguration::get_width()
extern void XRCameraConfiguration_get_width_m8ECF3F57F94FC3F97FC1FE9CAAE4D1DCAD39F067_AdjustorThunk ();
// 0x00000038 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraConfiguration::get_height()
extern void XRCameraConfiguration_get_height_m92B6D4553AE4900BDF258E6F224ECACD06E30C40_AdjustorThunk ();
// 0x00000039 UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCameraConfiguration::get_resolution()
extern void XRCameraConfiguration_get_resolution_mDED625C9D21911EF0D05C49DCBC589FE7915C2B2_AdjustorThunk ();
// 0x0000003A System.Nullable`1<System.Int32> UnityEngine.XR.ARSubsystems.XRCameraConfiguration::get_framerate()
extern void XRCameraConfiguration_get_framerate_m76E136DC6045EB254C009C56A3BC667D7EBB3C77_AdjustorThunk ();
// 0x0000003B System.IntPtr UnityEngine.XR.ARSubsystems.XRCameraConfiguration::get_nativeConfigurationHandle()
extern void XRCameraConfiguration_get_nativeConfigurationHandle_mEB607EED8AC9829EDAE9D969F066062FB6B2C300_AdjustorThunk ();
// 0x0000003C System.Void UnityEngine.XR.ARSubsystems.XRCameraConfiguration::.ctor(System.IntPtr,UnityEngine.Vector2Int,System.Int32)
extern void XRCameraConfiguration__ctor_mC618794BE9A0EEE69A332A6FD53A8F39997FE372_AdjustorThunk ();
// 0x0000003D System.Void UnityEngine.XR.ARSubsystems.XRCameraConfiguration::.ctor(System.IntPtr,UnityEngine.Vector2Int)
extern void XRCameraConfiguration__ctor_m4BC0ECCE825EEBD919A474A5D1E0E68A318A2237_AdjustorThunk ();
// 0x0000003E System.String UnityEngine.XR.ARSubsystems.XRCameraConfiguration::ToString()
extern void XRCameraConfiguration_ToString_m702D1FD8278A19B797B0DDA0C37C08D4F41D4535_AdjustorThunk ();
// 0x0000003F System.Int32 UnityEngine.XR.ARSubsystems.XRCameraConfiguration::GetHashCode()
extern void XRCameraConfiguration_GetHashCode_m05CDBC4B6384E712DF61BB1E387014DE4FEDA04C_AdjustorThunk ();
// 0x00000040 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraConfiguration::Equals(System.Object)
extern void XRCameraConfiguration_Equals_m63FEF3901B78AE9D56217FE49666AC7BCECD4C38_AdjustorThunk ();
// 0x00000041 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraConfiguration::Equals(UnityEngine.XR.ARSubsystems.XRCameraConfiguration)
extern void XRCameraConfiguration_Equals_mB4BD44CDAA94CFC33E88A0205EAEF098220F1E97_AdjustorThunk ();
// 0x00000042 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraConfiguration::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraConfiguration,UnityEngine.XR.ARSubsystems.XRCameraConfiguration)
extern void XRCameraConfiguration_op_Equality_mD226534E63EB64C9AE11264609C249004FC843DA ();
// 0x00000043 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraConfiguration::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraConfiguration,UnityEngine.XR.ARSubsystems.XRCameraConfiguration)
extern void XRCameraConfiguration_op_Inequality_mFCEDF0D1D7723413124B15E044F9B542400BB3FD ();
// 0x00000044 System.Int64 UnityEngine.XR.ARSubsystems.XRCameraFrame::get_timestampNs()
extern void XRCameraFrame_get_timestampNs_m0FAE10EDEEDF94C0892E8800E8CB693F64B83B14_AdjustorThunk ();
// 0x00000045 System.Single UnityEngine.XR.ARSubsystems.XRCameraFrame::get_averageBrightness()
extern void XRCameraFrame_get_averageBrightness_m153B54A25E5013B090D737BB3BC2DCF300C88E92_AdjustorThunk ();
// 0x00000046 System.Single UnityEngine.XR.ARSubsystems.XRCameraFrame::get_averageColorTemperature()
extern void XRCameraFrame_get_averageColorTemperature_m18EFBA25B4D6580D16CE859CECD1A0767CFBA006_AdjustorThunk ();
// 0x00000047 UnityEngine.Color UnityEngine.XR.ARSubsystems.XRCameraFrame::get_colorCorrection()
extern void XRCameraFrame_get_colorCorrection_m21EA821139C87463A369E9D308D3794A398D7A2C_AdjustorThunk ();
// 0x00000048 UnityEngine.Matrix4x4 UnityEngine.XR.ARSubsystems.XRCameraFrame::get_projectionMatrix()
extern void XRCameraFrame_get_projectionMatrix_m1AFF19568E4F29BDC16B943B34A8C988CA8382A1_AdjustorThunk ();
// 0x00000049 UnityEngine.Matrix4x4 UnityEngine.XR.ARSubsystems.XRCameraFrame::get_displayMatrix()
extern void XRCameraFrame_get_displayMatrix_mE7C0D233A017D72EA36CC4879F49BC872D49B2A5_AdjustorThunk ();
// 0x0000004A UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRCameraFrame::get_trackingState()
extern void XRCameraFrame_get_trackingState_mB45FB220A1EDE8A180217EC6F710A9651E61682A_AdjustorThunk ();
// 0x0000004B System.IntPtr UnityEngine.XR.ARSubsystems.XRCameraFrame::get_nativePtr()
extern void XRCameraFrame_get_nativePtr_mA8681A34CB2D48614EBF236A61AD0649C6D11E7E_AdjustorThunk ();
// 0x0000004C UnityEngine.XR.ARSubsystems.XRCameraFrameProperties UnityEngine.XR.ARSubsystems.XRCameraFrame::get_properties()
extern void XRCameraFrame_get_properties_mE2BF04FF350BC7B1FF375CB5C4E703211133DEF5_AdjustorThunk ();
// 0x0000004D System.Single UnityEngine.XR.ARSubsystems.XRCameraFrame::get_averageIntensityInLumens()
extern void XRCameraFrame_get_averageIntensityInLumens_mD12108393415DB6E8CF9E8F4787B5BEFC526D460_AdjustorThunk ();
// 0x0000004E System.Double UnityEngine.XR.ARSubsystems.XRCameraFrame::get_exposureDuration()
extern void XRCameraFrame_get_exposureDuration_mDEE7FB820A5F0D72393A15B920C0491CB6949070_AdjustorThunk ();
// 0x0000004F System.Single UnityEngine.XR.ARSubsystems.XRCameraFrame::get_exposureOffset()
extern void XRCameraFrame_get_exposureOffset_m3CECB3069D57AA892A6D5B9FD9B78CBDC6A712F0_AdjustorThunk ();
// 0x00000050 System.Single UnityEngine.XR.ARSubsystems.XRCameraFrame::get_mainLightIntensityLumens()
extern void XRCameraFrame_get_mainLightIntensityLumens_m6D3A8EE7DB14B2E02CE989F62818D3CF07286ABF_AdjustorThunk ();
// 0x00000051 UnityEngine.Color UnityEngine.XR.ARSubsystems.XRCameraFrame::get_mainLightColor()
extern void XRCameraFrame_get_mainLightColor_mEE9EEAC1AC653DCEFD91EB190B762F427B2EB698_AdjustorThunk ();
// 0x00000052 UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.XRCameraFrame::get_mainLightDirection()
extern void XRCameraFrame_get_mainLightDirection_mCB3295F9A3FE677D53B8AF5C71DF76A3174B4072_AdjustorThunk ();
// 0x00000053 UnityEngine.Rendering.SphericalHarmonicsL2 UnityEngine.XR.ARSubsystems.XRCameraFrame::get_ambientSphericalHarmonics()
extern void XRCameraFrame_get_ambientSphericalHarmonics_m34B37F0029843DB7846400F943C84157AFB7F043_AdjustorThunk ();
// 0x00000054 UnityEngine.XR.ARSubsystems.XRTextureDescriptor UnityEngine.XR.ARSubsystems.XRCameraFrame::get_cameraGrain()
extern void XRCameraFrame_get_cameraGrain_m72F990B7526591FE40C70083779B0B4F1CE14E35_AdjustorThunk ();
// 0x00000055 System.Single UnityEngine.XR.ARSubsystems.XRCameraFrame::get_noiseIntensity()
extern void XRCameraFrame_get_noiseIntensity_mD42D8F6D6374209752F61E8A63DD98D6CA5CAEC2_AdjustorThunk ();
// 0x00000056 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasTimestamp()
extern void XRCameraFrame_get_hasTimestamp_m08EAA6466145202B9E9E612895F452F35CB6F673_AdjustorThunk ();
// 0x00000057 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasAverageBrightness()
extern void XRCameraFrame_get_hasAverageBrightness_m08A69E2E7D6D477EB44CBBB282F82476149DA292_AdjustorThunk ();
// 0x00000058 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasAverageColorTemperature()
extern void XRCameraFrame_get_hasAverageColorTemperature_m434C9B70523C3ED04A3BD3C5DCCE25199387168F_AdjustorThunk ();
// 0x00000059 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasColorCorrection()
extern void XRCameraFrame_get_hasColorCorrection_mE00653B8C09496E3FD15089827105AB65B2C108F_AdjustorThunk ();
// 0x0000005A System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasProjectionMatrix()
extern void XRCameraFrame_get_hasProjectionMatrix_m614664027EA1D284BE2C5B5FD5ED2D84E88BB39A_AdjustorThunk ();
// 0x0000005B System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasDisplayMatrix()
extern void XRCameraFrame_get_hasDisplayMatrix_m545C335B1B2849139A11439858E4FEE82741CCA9_AdjustorThunk ();
// 0x0000005C System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasAverageIntensityInLumens()
extern void XRCameraFrame_get_hasAverageIntensityInLumens_mFFFFCBBC3BBB69F1C1F7FA47DBECF72D3F988813_AdjustorThunk ();
// 0x0000005D System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasExposureDuration()
extern void XRCameraFrame_get_hasExposureDuration_m8ACE9F1BCE652BE74152A7641B6AE15C94BAAE56_AdjustorThunk ();
// 0x0000005E System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasExposureOffset()
extern void XRCameraFrame_get_hasExposureOffset_m1D09B799890FF7214EA2F3C881E4B2673E43992A_AdjustorThunk ();
// 0x0000005F System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasMainLightIntensityLumens()
extern void XRCameraFrame_get_hasMainLightIntensityLumens_m28BBB2A9DDA43CD6EB2D872F045E1AA41CB2B016_AdjustorThunk ();
// 0x00000060 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasMainLightColor()
extern void XRCameraFrame_get_hasMainLightColor_mA219350CE0AC141BD931021A46A9862A9340F7C5_AdjustorThunk ();
// 0x00000061 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasMainLightDirection()
extern void XRCameraFrame_get_hasMainLightDirection_m985FBD9C1BFBAF5C97B67BBFDB25FDA8B5231280_AdjustorThunk ();
// 0x00000062 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasAmbientSphericalHarmonics()
extern void XRCameraFrame_get_hasAmbientSphericalHarmonics_m21AB7CA0BC5E522208735F1D670053518D407D08_AdjustorThunk ();
// 0x00000063 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasCameraGrain()
extern void XRCameraFrame_get_hasCameraGrain_m2AB571C0D95AEE9F87E609BF40F44A005AD73B20_AdjustorThunk ();
// 0x00000064 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::get_hasNoiseIntensity()
extern void XRCameraFrame_get_hasNoiseIntensity_m0959F24C2CE910180B80489C8B43C211EE6E9ED7_AdjustorThunk ();
// 0x00000065 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::TryGetTimestamp(System.Int64&)
extern void XRCameraFrame_TryGetTimestamp_m0930188029B9A820EE23C68EE9FA3A45BFD76928_AdjustorThunk ();
// 0x00000066 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::TryGetAverageBrightness(System.Single&)
extern void XRCameraFrame_TryGetAverageBrightness_mC1F49F26D2531DE89DAC1529E7D8C1506ED78C1C_AdjustorThunk ();
// 0x00000067 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::TryGetAverageColorTemperature(System.Single&)
extern void XRCameraFrame_TryGetAverageColorTemperature_m0A7E61255CE92478AB82FE5F013ED04089C857F2_AdjustorThunk ();
// 0x00000068 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::TryGetProjectionMatrix(UnityEngine.Matrix4x4&)
extern void XRCameraFrame_TryGetProjectionMatrix_mA846AEFFB2CDA29D49756C5396A4F6FB17EFF5A3_AdjustorThunk ();
// 0x00000069 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::TryGetDisplayMatrix(UnityEngine.Matrix4x4&)
extern void XRCameraFrame_TryGetDisplayMatrix_m3634FB652629917956DB5EA25F37E53F84BF9E4F_AdjustorThunk ();
// 0x0000006A System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::TryGetAverageIntensityInLumens(System.Single&)
extern void XRCameraFrame_TryGetAverageIntensityInLumens_m6F0AA92D0F6E0FD01D2B2F342B8F080B11C424D0_AdjustorThunk ();
// 0x0000006B System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::Equals(UnityEngine.XR.ARSubsystems.XRCameraFrame)
extern void XRCameraFrame_Equals_m5795BA83EB6809C67D23D58FA2D9BCF8F7664EA8_AdjustorThunk ();
// 0x0000006C System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::Equals(System.Object)
extern void XRCameraFrame_Equals_m72CE1B12ABF7FAB123A2898E4DCBFFB5EE088777_AdjustorThunk ();
// 0x0000006D System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraFrame,UnityEngine.XR.ARSubsystems.XRCameraFrame)
extern void XRCameraFrame_op_Equality_m39912ADC47C063E96C2E98E7FF5F9E16E1BE7069 ();
// 0x0000006E System.Boolean UnityEngine.XR.ARSubsystems.XRCameraFrame::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraFrame,UnityEngine.XR.ARSubsystems.XRCameraFrame)
extern void XRCameraFrame_op_Inequality_m03D8D007CD75133E461110920866206D695740D3 ();
// 0x0000006F System.Int32 UnityEngine.XR.ARSubsystems.XRCameraFrame::GetHashCode()
extern void XRCameraFrame_GetHashCode_mB1F696089EB4E7E021E3383006178C023928D124_AdjustorThunk ();
// 0x00000070 System.String UnityEngine.XR.ARSubsystems.XRCameraFrame::ToString()
extern void XRCameraFrame_ToString_mBFD4510AB5312CCAF0C115734AF91E8BF6C480AC_AdjustorThunk ();
// 0x00000071 UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::get_focalLength()
extern void XRCameraIntrinsics_get_focalLength_m9D090B0B207598F353860CB5735B85B78827C93F_AdjustorThunk ();
// 0x00000072 UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::get_principalPoint()
extern void XRCameraIntrinsics_get_principalPoint_mC66F07CA90FAA4A8A94BDF2A62641196C9DD6DEC_AdjustorThunk ();
// 0x00000073 UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::get_resolution()
extern void XRCameraIntrinsics_get_resolution_m6572536639CC3A6F1A1E1DE50971522B2933355D_AdjustorThunk ();
// 0x00000074 System.Void UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::.ctor(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2Int)
extern void XRCameraIntrinsics__ctor_m40F0632FD31F48C9FFB073ED2C5516D94D172633_AdjustorThunk ();
// 0x00000075 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::Equals(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics)
extern void XRCameraIntrinsics_Equals_mF8BFEADD1C696A2DC28CCA8F07620B0376967EB1_AdjustorThunk ();
// 0x00000076 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::Equals(System.Object)
extern void XRCameraIntrinsics_Equals_m9BE2EE15CDEC43C62816925AF33D02974592CFA2_AdjustorThunk ();
// 0x00000077 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics,UnityEngine.XR.ARSubsystems.XRCameraIntrinsics)
extern void XRCameraIntrinsics_op_Equality_mB198DA468A2DEB3F6D497DA72851ECF39740874D ();
// 0x00000078 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics,UnityEngine.XR.ARSubsystems.XRCameraIntrinsics)
extern void XRCameraIntrinsics_op_Inequality_mB74D8A6DCDE30C21E121941C206767C8507E6817 ();
// 0x00000079 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::GetHashCode()
extern void XRCameraIntrinsics_GetHashCode_m3A63BB112EB34BF80692DEF316C220C129041F26_AdjustorThunk ();
// 0x0000007A System.String UnityEngine.XR.ARSubsystems.XRCameraIntrinsics::ToString()
extern void XRCameraIntrinsics_ToString_mDFEFA74E34AB9AAAC4221614F3BBBFD19D454E37_AdjustorThunk ();
// 0x0000007B System.Single UnityEngine.XR.ARSubsystems.XRCameraParams::get_zNear()
extern void XRCameraParams_get_zNear_mD3785EC9C402C69AAFAF24AB16BA946963509F04_AdjustorThunk ();
// 0x0000007C System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_zNear(System.Single)
extern void XRCameraParams_set_zNear_m6DD4FC25349D4D0805EB2BE360CBE4954EADB951_AdjustorThunk ();
// 0x0000007D System.Single UnityEngine.XR.ARSubsystems.XRCameraParams::get_zFar()
extern void XRCameraParams_get_zFar_m91AE073EEFBFA27403EF2D07A00AB814A0E9E767_AdjustorThunk ();
// 0x0000007E System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_zFar(System.Single)
extern void XRCameraParams_set_zFar_mC5FDDD40DD4FAF1E73C8A45042E14810233A06C8_AdjustorThunk ();
// 0x0000007F System.Single UnityEngine.XR.ARSubsystems.XRCameraParams::get_screenWidth()
extern void XRCameraParams_get_screenWidth_m284D5554D15C661CB76315320E728900F234B8FC_AdjustorThunk ();
// 0x00000080 System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_screenWidth(System.Single)
extern void XRCameraParams_set_screenWidth_m9047176614CF36F4D0D7D7B509FF8748BB87FC50_AdjustorThunk ();
// 0x00000081 System.Single UnityEngine.XR.ARSubsystems.XRCameraParams::get_screenHeight()
extern void XRCameraParams_get_screenHeight_mBAE5A3EEA77FD635ABCA56D62B80270356F350A4_AdjustorThunk ();
// 0x00000082 System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_screenHeight(System.Single)
extern void XRCameraParams_set_screenHeight_m16D3F7A4907E471B3433D965A96E7E74B3956D3B_AdjustorThunk ();
// 0x00000083 UnityEngine.ScreenOrientation UnityEngine.XR.ARSubsystems.XRCameraParams::get_screenOrientation()
extern void XRCameraParams_get_screenOrientation_mF47570D9A01E6E868BD0A77E89942B47B9A5A86B_AdjustorThunk ();
// 0x00000084 System.Void UnityEngine.XR.ARSubsystems.XRCameraParams::set_screenOrientation(UnityEngine.ScreenOrientation)
extern void XRCameraParams_set_screenOrientation_m3539BE3608BE3DFCA6890DF6F6C38DE34D244CB6_AdjustorThunk ();
// 0x00000085 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraParams::Equals(UnityEngine.XR.ARSubsystems.XRCameraParams)
extern void XRCameraParams_Equals_m13F7C4A8684FAE1B19033C4D4F173A203BD31B6C_AdjustorThunk ();
// 0x00000086 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraParams::Equals(System.Object)
extern void XRCameraParams_Equals_m5C141227483E4A8477B429232F18168069A2A871_AdjustorThunk ();
// 0x00000087 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraParams::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraParams)
extern void XRCameraParams_op_Equality_m5631553B4B12F4A44972CD37522F1AE449FFF2C5 ();
// 0x00000088 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraParams::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraParams)
extern void XRCameraParams_op_Inequality_m3CBEF158E25B5B1B51E946BC72EFBD1CBEA20072 ();
// 0x00000089 System.Int32 UnityEngine.XR.ARSubsystems.XRCameraParams::GetHashCode()
extern void XRCameraParams_GetHashCode_m4B13187F7D2BDEF05AEE01B1C08BF7BE743450D3_AdjustorThunk ();
// 0x0000008A System.String UnityEngine.XR.ARSubsystems.XRCameraParams::ToString()
extern void XRCameraParams_ToString_mDF369BABC469E9F0FD2F675CA51762D43B658A95_AdjustorThunk ();
// 0x0000008B System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::.ctor()
extern void XRCameraSubsystem__ctor_m494821143D51BD2028BDC82ED7FCFACAD928B92D ();
// 0x0000008C UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_currentCamera()
extern void XRCameraSubsystem_get_currentCamera_m282FFC69B0ADD4A2E95D7B7BEA183B642264140C ();
// 0x0000008D UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_requestedCamera()
extern void XRCameraSubsystem_get_requestedCamera_mDEEADE48A7A0CCC1ADAA9EB8E71C9A5B6D16A1C4 ();
// 0x0000008E System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::set_requestedCamera(UnityEngine.XR.ARSubsystems.Feature)
extern void XRCameraSubsystem_set_requestedCamera_m65F790F389D3F872FD4C667C07E77645F009BD02 ();
// 0x0000008F System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_autoFocusEnabled()
extern void XRCameraSubsystem_get_autoFocusEnabled_m2169E884DF44269DD6EAB5EDA16E199E3BE6DD1D ();
// 0x00000090 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_autoFocusRequested()
extern void XRCameraSubsystem_get_autoFocusRequested_mB8C597786A8E139BDF3C8C4C2E37BAE48D281423 ();
// 0x00000091 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::set_autoFocusRequested(System.Boolean)
extern void XRCameraSubsystem_set_autoFocusRequested_m9FE78AF905C22EF72760868A91FD164CE83DC29B ();
// 0x00000092 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_currentLightEstimation()
extern void XRCameraSubsystem_get_currentLightEstimation_mB75AD8B5DCDF179B3339D982D5B5D4EDA9DF4826 ();
// 0x00000093 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_requestedLightEstimation()
extern void XRCameraSubsystem_get_requestedLightEstimation_mB9F4B02D8C27819A36B09B7FD57E70854942E0C8 ();
// 0x00000094 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::set_requestedLightEstimation(UnityEngine.XR.ARSubsystems.Feature)
extern void XRCameraSubsystem_set_requestedLightEstimation_m0D5FC93E6EC1921CB83EAF535C6AA1EC5C4EA694 ();
// 0x00000095 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::OnStart()
extern void XRCameraSubsystem_OnStart_m20BD1C2BFE2BD80BEDC645782953875B923D1D2E ();
// 0x00000096 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::OnStop()
extern void XRCameraSubsystem_OnStop_m72C30EE6BA570645B8338043A45B8F1815E04833 ();
// 0x00000097 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::OnDestroyed()
extern void XRCameraSubsystem_OnDestroyed_mC2BAABF53993142A81A6F010D91C022EE15D1954 ();
// 0x00000098 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTextureDescriptor> UnityEngine.XR.ARSubsystems.XRCameraSubsystem::GetTextureDescriptors(Unity.Collections.Allocator)
extern void XRCameraSubsystem_GetTextureDescriptors_mD7CFEDA2DDB138789A1E96CE71672F9C12FE4D21 ();
// 0x00000099 UnityEngine.Material UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_cameraMaterial()
extern void XRCameraSubsystem_get_cameraMaterial_m5B8CE90E2D4F8AF83D5D8134B1CB57D38065EBF5 ();
// 0x0000009A System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::OnBeforeBackgroundRender(System.Int32)
extern void XRCameraSubsystem_OnBeforeBackgroundRender_m6D6CBD308D7E538974A268019054AAE46BFC2FBC ();
// 0x0000009B System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryGetIntrinsics(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics&)
extern void XRCameraSubsystem_TryGetIntrinsics_m7002E115FF03D6FD7131F11918579C79613CD3B6 ();
// 0x0000009C Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARSubsystems.XRCameraSubsystem::GetConfigurations(Unity.Collections.Allocator)
extern void XRCameraSubsystem_GetConfigurations_m89E371017BA4612ECF7B4D472A0F71D290738BFA ();
// 0x0000009D System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_currentConfiguration()
extern void XRCameraSubsystem_get_currentConfiguration_m3AB42FB62192A5C598C668FB84BEE5B1D371278D ();
// 0x0000009E System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::set_currentConfiguration(System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration>)
extern void XRCameraSubsystem_set_currentConfiguration_m7B434D9F5A4E5A324D74E5ECF655937F746CA166 ();
// 0x0000009F System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_invertCulling()
extern void XRCameraSubsystem_get_invertCulling_mEA7FB5A83FC369DCB02B408585E21CDD6005899D ();
// 0x000000A0 UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider UnityEngine.XR.ARSubsystems.XRCameraSubsystem::CreateProvider()
// 0x000000A1 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryGetLatestFrame(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraFrame&)
extern void XRCameraSubsystem_TryGetLatestFrame_m89FA346C38CE80B4D7069CB8C8C6F4952537B771 ();
// 0x000000A2 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::get_permissionGranted()
extern void XRCameraSubsystem_get_permissionGranted_m47E72ACE1D8264D73CCEE9341DCE766C3038056C ();
// 0x000000A3 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem::GetMaterialKeywords(System.Collections.Generic.List`1<System.String>&,System.Collections.Generic.List`1<System.String>&)
extern void XRCameraSubsystem_GetMaterialKeywords_mA71C43409E4766D3350FC61C2403A896F461F8EF ();
// 0x000000A4 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryGetLatestImage(UnityEngine.XR.ARSubsystems.XRCpuImage&)
extern void XRCameraSubsystem_TryGetLatestImage_m6AD5F6C38BFF2E73745A37F5868055D6F79750F2 ();
// 0x000000A5 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::TryAcquireLatestCpuImage(UnityEngine.XR.ARSubsystems.XRCpuImage&)
extern void XRCameraSubsystem_TryAcquireLatestCpuImage_mDE6925F131B1FA9E0A87CBB3D06B8D8245003EDC ();
// 0x000000A6 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::Register(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystem_Register_m703AA08482A5B9AFE8DCCCC33CF7D65BA4B56508 ();
// 0x000000A7 System.String UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_id()
extern void XRCameraSubsystemCinfo_get_id_m1DB8669B5D86333CEB72F4B933CCA95503921A5F_AdjustorThunk ();
// 0x000000A8 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_id(System.String)
extern void XRCameraSubsystemCinfo_set_id_m559DBD38CBD75958E02AF1F62D676E431F661520_AdjustorThunk ();
// 0x000000A9 System.Type UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_implementationType()
extern void XRCameraSubsystemCinfo_get_implementationType_m6240AB836CC4FDF45C40F018932181137A9F4EFC_AdjustorThunk ();
// 0x000000AA System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_implementationType(System.Type)
extern void XRCameraSubsystemCinfo_set_implementationType_m49F9345BCD43251FFDC4DD68E1C006AFB98E3634_AdjustorThunk ();
// 0x000000AB System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsAverageBrightness()
extern void XRCameraSubsystemCinfo_get_supportsAverageBrightness_m435847F6BEF656D51B758CE3A345ED340348D19C_AdjustorThunk ();
// 0x000000AC System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsAverageBrightness(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsAverageBrightness_m2F3DA568B12203F175FEAF777DA8753394F4780B_AdjustorThunk ();
// 0x000000AD System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsAverageColorTemperature()
extern void XRCameraSubsystemCinfo_get_supportsAverageColorTemperature_mEB9C8EA9865557B03879002D49702DABC898FFEA_AdjustorThunk ();
// 0x000000AE System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsAverageColorTemperature(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsAverageColorTemperature_m697272008DF9F4DA4287992BF6968433A46FD977_AdjustorThunk ();
// 0x000000AF System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsColorCorrection()
extern void XRCameraSubsystemCinfo_get_supportsColorCorrection_m750008A02F360F9B08CFA09A56041CC9F322D4D7_AdjustorThunk ();
// 0x000000B0 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsColorCorrection(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsColorCorrection_m021F469C4BE1CF91484A2C483102FED9D650EB49_AdjustorThunk ();
// 0x000000B1 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsDisplayMatrix()
extern void XRCameraSubsystemCinfo_get_supportsDisplayMatrix_m4FB943DF5BAD950FC1E8F207C43E1423CE8DACB4_AdjustorThunk ();
// 0x000000B2 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsDisplayMatrix(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsDisplayMatrix_m15E696085A5B470BC086F59B6EB2B4ED67E6BC6E_AdjustorThunk ();
// 0x000000B3 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsProjectionMatrix()
extern void XRCameraSubsystemCinfo_get_supportsProjectionMatrix_m6264020FBBB0BE11F965A3806FE976C477A85029_AdjustorThunk ();
// 0x000000B4 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsProjectionMatrix(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsProjectionMatrix_m392F1AA0BE94C4F9BE41B4B501373769BC32BBE3_AdjustorThunk ();
// 0x000000B5 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsTimestamp()
extern void XRCameraSubsystemCinfo_get_supportsTimestamp_mA412069D5F8BE727C5697AE3DECB293EEDD5C2CC_AdjustorThunk ();
// 0x000000B6 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsTimestamp(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsTimestamp_mC0811CD8D662F79DA2C8144617A3C4B3F57475CD_AdjustorThunk ();
// 0x000000B7 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsCameraConfigurations()
extern void XRCameraSubsystemCinfo_get_supportsCameraConfigurations_mC9E1DDCB24429931986DF787B70AE1B26155C968_AdjustorThunk ();
// 0x000000B8 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsCameraConfigurations(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsCameraConfigurations_mE52728D3658A16E910A5290A1F13A3B49716A86E_AdjustorThunk ();
// 0x000000B9 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsCameraImage()
extern void XRCameraSubsystemCinfo_get_supportsCameraImage_m0F0C52A74FE18C235A3A45A3112A41049CFF21CB_AdjustorThunk ();
// 0x000000BA System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsCameraImage(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsCameraImage_mAB3557E5539DCFAEE89775A5A7A768EF9AFC18B2_AdjustorThunk ();
// 0x000000BB System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsAverageIntensityInLumens()
extern void XRCameraSubsystemCinfo_get_supportsAverageIntensityInLumens_mAA76CDE43EF6429034235FA62E9EA906C017DABA_AdjustorThunk ();
// 0x000000BC System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsAverageIntensityInLumens(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsAverageIntensityInLumens_m4FFC89CEF155CC9CAEE5B26B7B511EFB56401A77_AdjustorThunk ();
// 0x000000BD System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsFaceTrackingAmbientIntensityLightEstimation()
extern void XRCameraSubsystemCinfo_get_supportsFaceTrackingAmbientIntensityLightEstimation_m23AA159827223BD1F7C74A9F277EA3A11EA884A6_AdjustorThunk ();
// 0x000000BE System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsFaceTrackingAmbientIntensityLightEstimation(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsFaceTrackingAmbientIntensityLightEstimation_m20E48DF12ECC198A544B85B4E6E827D5C44B0F8F_AdjustorThunk ();
// 0x000000BF System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsFaceTrackingHDRLightEstimation()
extern void XRCameraSubsystemCinfo_get_supportsFaceTrackingHDRLightEstimation_m097C130B703C858853BDA995D3E92003DFFAC98B_AdjustorThunk ();
// 0x000000C0 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsFaceTrackingHDRLightEstimation(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsFaceTrackingHDRLightEstimation_m88310BAC5F76E8BB7E43421B136782D26CDF52B9_AdjustorThunk ();
// 0x000000C1 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsWorldTrackingAmbientIntensityLightEstimation()
extern void XRCameraSubsystemCinfo_get_supportsWorldTrackingAmbientIntensityLightEstimation_m8AC57089BDA75C88D09190067A03184659811A32_AdjustorThunk ();
// 0x000000C2 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsWorldTrackingAmbientIntensityLightEstimation(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsWorldTrackingAmbientIntensityLightEstimation_mB569E126D3D68FB5FFD6A9FBB8BD8A5B9EA2C68F_AdjustorThunk ();
// 0x000000C3 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsWorldTrackingHDRLightEstimation()
extern void XRCameraSubsystemCinfo_get_supportsWorldTrackingHDRLightEstimation_mC47D81A504E58D9835032082113BA08900121E10_AdjustorThunk ();
// 0x000000C4 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsWorldTrackingHDRLightEstimation(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsWorldTrackingHDRLightEstimation_mBBFB21875B3BE98CADDC0945FD09162AB5A4D4D3_AdjustorThunk ();
// 0x000000C5 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsFocusModes()
extern void XRCameraSubsystemCinfo_get_supportsFocusModes_m16EB0068BED9439C2054345D4AF8C377FA3AE165_AdjustorThunk ();
// 0x000000C6 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsFocusModes(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsFocusModes_m799DEE56E25682F95D399627085512849CB183F0_AdjustorThunk ();
// 0x000000C7 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::get_supportsCameraGrain()
extern void XRCameraSubsystemCinfo_get_supportsCameraGrain_m824EC72A0311C4F7ABA41EBF3F1D512256364074_AdjustorThunk ();
// 0x000000C8 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::set_supportsCameraGrain(System.Boolean)
extern void XRCameraSubsystemCinfo_set_supportsCameraGrain_m7A9DDA176272DAE0056462DA008F808C69FFB382_AdjustorThunk ();
// 0x000000C9 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::Equals(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemCinfo_Equals_mFE87F930B521AF5DE45B11505D902F7D54132D7E_AdjustorThunk ();
// 0x000000CA System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::Equals(System.Object)
extern void XRCameraSubsystemCinfo_Equals_mC1C672F751A190D738D675E92E08A9BD51CDFD17_AdjustorThunk ();
// 0x000000CB System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo,UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemCinfo_op_Equality_m35BEFC61F3D89C60D502DE1D7EDFF866CDCF03DA ();
// 0x000000CC System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo,UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemCinfo_op_Inequality_mF793DEE0C5451A47EF9BC048CF55C19B691C6AAA ();
// 0x000000CD System.Int32 UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo::GetHashCode()
extern void XRCameraSubsystemCinfo_GetHashCode_mD73BDB766ED1A08FD01FE5B5DC4659F5C6AA46D2_AdjustorThunk ();
// 0x000000CE System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemDescriptor__ctor_m3CEE22F997A70853919BFA5D77F4E42B641FA5A8 ();
// 0x000000CF System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsAverageBrightness()
extern void XRCameraSubsystemDescriptor_get_supportsAverageBrightness_m982955584D39B97CD69E4E72FA65FAD92D6CCA82 ();
// 0x000000D0 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsAverageBrightness(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsAverageBrightness_m035A437122E761A414CAEB39CBBB811B2BE5E5A1 ();
// 0x000000D1 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsAverageColorTemperature()
extern void XRCameraSubsystemDescriptor_get_supportsAverageColorTemperature_mF1DC55FBCDA326B859182DDA33EE3832B11151D6 ();
// 0x000000D2 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsAverageColorTemperature(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsAverageColorTemperature_mC2E6DAE5DEA38CC15FB9BF2BC8D0D74D6EAF074F ();
// 0x000000D3 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsDisplayMatrix()
extern void XRCameraSubsystemDescriptor_get_supportsDisplayMatrix_m9AC0B144D6812FC25D67F4F1B13EFF53428B6462 ();
// 0x000000D4 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsDisplayMatrix(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsDisplayMatrix_m6AA3AD2B2169BEFEC0E25AF133954C105ACF7A57 ();
// 0x000000D5 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsProjectionMatrix()
extern void XRCameraSubsystemDescriptor_get_supportsProjectionMatrix_m52F7C1EDC42DAC2863A6F01538CFEE89DD6FA668 ();
// 0x000000D6 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsProjectionMatrix(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsProjectionMatrix_mF08048B92AAC06A3E1AD27FAAE706307E8330B13 ();
// 0x000000D7 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsTimestamp()
extern void XRCameraSubsystemDescriptor_get_supportsTimestamp_m78C4A64340FC54929EC0D23F34F7893F11999370 ();
// 0x000000D8 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsTimestamp(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsTimestamp_mCB9A7611C018D10F589A5614C639D43BD68EEAC1 ();
// 0x000000D9 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsCameraConfigurations()
extern void XRCameraSubsystemDescriptor_get_supportsCameraConfigurations_m878013E1778D4D8ED8E20D61EFA80B0676A7688B ();
// 0x000000DA System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsCameraConfigurations(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsCameraConfigurations_mD83859D14421E469C44251BD9BB485DE452F69DA ();
// 0x000000DB System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsCameraImage()
extern void XRCameraSubsystemDescriptor_get_supportsCameraImage_mFA18FCE5C49B17187AD3ACA02435903D255A0093 ();
// 0x000000DC System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsCameraImage(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsCameraImage_mA00EF890A0FA5562BAF9FF4C7D576DC6A587C444 ();
// 0x000000DD System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsAverageIntensityInLumens()
extern void XRCameraSubsystemDescriptor_get_supportsAverageIntensityInLumens_mE38B4C4D2479E91D44C522A3CAA4C617ABCE2309 ();
// 0x000000DE System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsAverageIntensityInLumens(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsAverageIntensityInLumens_mA3B28B90284C27EE9FADB0C32FBA590EAC33A6AB ();
// 0x000000DF System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsFocusModes()
extern void XRCameraSubsystemDescriptor_get_supportsFocusModes_m65F7A70A6965CEB2D4B8D42FE35A443B04562F9B ();
// 0x000000E0 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsFocusModes(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsFocusModes_mC0614C2F8149C084D9ECCDC6E240BAAE6913734E ();
// 0x000000E1 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsFaceTrackingAmbientIntensityLightEstimation()
extern void XRCameraSubsystemDescriptor_get_supportsFaceTrackingAmbientIntensityLightEstimation_m448DBB0CC704A29454243ED6022701F2FFB7D19F ();
// 0x000000E2 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsFaceTrackingAmbientIntensityLightEstimation(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsFaceTrackingAmbientIntensityLightEstimation_m4F005F802752C51D36461E3E80220FF4B0777463 ();
// 0x000000E3 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsFaceTrackingHDRLightEstimation()
extern void XRCameraSubsystemDescriptor_get_supportsFaceTrackingHDRLightEstimation_mC0E15E39B702A849E7AB91F3B2A5BC950B1C0C8C ();
// 0x000000E4 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsFaceTrackingHDRLightEstimation(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsFaceTrackingHDRLightEstimation_m7328E20EC1237AED5871608A27021AB566E28898 ();
// 0x000000E5 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsWorldTrackingAmbientIntensityLightEstimation()
extern void XRCameraSubsystemDescriptor_get_supportsWorldTrackingAmbientIntensityLightEstimation_m917A561ABD1E474A48D8784B361360D930042778 ();
// 0x000000E6 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsWorldTrackingAmbientIntensityLightEstimation(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsWorldTrackingAmbientIntensityLightEstimation_m0867CE9108F76FBEB341DEE537717BD609860A41 ();
// 0x000000E7 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsWorldTrackingHDRLightEstimation()
extern void XRCameraSubsystemDescriptor_get_supportsWorldTrackingHDRLightEstimation_m4E113282BC0476FA0673F95D741C390F4076E4A8 ();
// 0x000000E8 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsWorldTrackingHDRLightEstimation(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsWorldTrackingHDRLightEstimation_m9146770DDEEF2023163D25C87784FD44AE1FCF44 ();
// 0x000000E9 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::get_supportsCameraGrain()
extern void XRCameraSubsystemDescriptor_get_supportsCameraGrain_m384B5321E12E7E7AE907D1665C75017A3911E5F3 ();
// 0x000000EA System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::set_supportsCameraGrain(System.Boolean)
extern void XRCameraSubsystemDescriptor_set_supportsCameraGrain_m830D2705CB8327CF8E9C881BFEDA1E547871A7BA ();
// 0x000000EB UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRCameraSubsystemCinfo)
extern void XRCameraSubsystemDescriptor_Create_mD1CD2F7DDCCF8702EEDE082BAFCAFAC8ECE3DEA3 ();
// 0x000000EC UnityEngine.XR.ARSubsystems.ConfigurationDescriptor UnityEngine.XR.ARSubsystems.Configuration::get_descriptor()
extern void Configuration_get_descriptor_m8E1A59E0CDBA65733F1E89153C4CAEDBDE4BF5CD_AdjustorThunk ();
// 0x000000ED System.Void UnityEngine.XR.ARSubsystems.Configuration::set_descriptor(UnityEngine.XR.ARSubsystems.ConfigurationDescriptor)
extern void Configuration_set_descriptor_m3D755E0F4483B99A7038B27DEE762780C92033BF_AdjustorThunk ();
// 0x000000EE UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.Configuration::get_features()
extern void Configuration_get_features_m8DB48A18EE1E9DDC73A6FAAE1F671621E4D9C3FF_AdjustorThunk ();
// 0x000000EF System.Void UnityEngine.XR.ARSubsystems.Configuration::set_features(UnityEngine.XR.ARSubsystems.Feature)
extern void Configuration_set_features_m6355432613E049A1E2316EE8B95BE7DFD08C564E_AdjustorThunk ();
// 0x000000F0 System.Void UnityEngine.XR.ARSubsystems.Configuration::.ctor(UnityEngine.XR.ARSubsystems.ConfigurationDescriptor,UnityEngine.XR.ARSubsystems.Feature)
extern void Configuration__ctor_m6DCC3415FCA98D74490598E6B333220002F83B9F_AdjustorThunk ();
// 0x000000F1 System.Int32 UnityEngine.XR.ARSubsystems.Configuration::GetHashCode()
extern void Configuration_GetHashCode_mC1034C38DC7D77D3314C85529794198AE7414D28_AdjustorThunk ();
// 0x000000F2 System.Boolean UnityEngine.XR.ARSubsystems.Configuration::Equals(UnityEngine.XR.ARSubsystems.Configuration)
extern void Configuration_Equals_mDDA107F00E66E0E4E80051E78A69FAAABC440311_AdjustorThunk ();
// 0x000000F3 System.Boolean UnityEngine.XR.ARSubsystems.Configuration::Equals(System.Object)
extern void Configuration_Equals_m9D01331F4C4217610EBF65953C40A621A696F6FC_AdjustorThunk ();
// 0x000000F4 System.Boolean UnityEngine.XR.ARSubsystems.Configuration::op_Equality(UnityEngine.XR.ARSubsystems.Configuration,UnityEngine.XR.ARSubsystems.Configuration)
extern void Configuration_op_Equality_m56BB883CDDC773E9417AB7E77C70570D54AA4F6D ();
// 0x000000F5 System.Boolean UnityEngine.XR.ARSubsystems.Configuration::op_Inequality(UnityEngine.XR.ARSubsystems.Configuration,UnityEngine.XR.ARSubsystems.Configuration)
extern void Configuration_op_Inequality_mBC68393FE19F9726E36CD002D51A29D049D03546 ();
// 0x000000F6 UnityEngine.XR.ARSubsystems.Configuration UnityEngine.XR.ARSubsystems.ConfigurationChooser::ChooseConfiguration(Unity.Collections.NativeSlice`1<UnityEngine.XR.ARSubsystems.ConfigurationDescriptor>,UnityEngine.XR.ARSubsystems.Feature)
// 0x000000F7 System.Void UnityEngine.XR.ARSubsystems.ConfigurationChooser::.ctor()
extern void ConfigurationChooser__ctor_m293981265D9F9F94392ED1EDAB6AB40C633CDEB5 ();
// 0x000000F8 System.IntPtr UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::get_identifier()
extern void ConfigurationDescriptor_get_identifier_m8C2119C732D9203F795339D70278341A09717114_AdjustorThunk ();
// 0x000000F9 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::get_capabilities()
extern void ConfigurationDescriptor_get_capabilities_m1C841BC2128C90CBE3966A3885AA3444BFBCF87F_AdjustorThunk ();
// 0x000000FA System.Int32 UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::get_rank()
extern void ConfigurationDescriptor_get_rank_m16DB30BCB6D532A72D4DC437289651C11B72DAA7_AdjustorThunk ();
// 0x000000FB System.Void UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::.ctor(System.IntPtr,UnityEngine.XR.ARSubsystems.Feature,System.Int32)
extern void ConfigurationDescriptor__ctor_m9FEDA4546BDA2EE188D8C44D4FD5EBC8BF23B244_AdjustorThunk ();
// 0x000000FC System.String UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::HexString(System.IntPtr)
extern void ConfigurationDescriptor_HexString_mD1D5021B1B8160927E4D52057DD8BB1F3A405573_AdjustorThunk ();
// 0x000000FD System.String UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::ToString()
extern void ConfigurationDescriptor_ToString_m8C9B3F12F1B3998DE1CA034FB04A699013D437AA_AdjustorThunk ();
// 0x000000FE System.Int32 UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::GetHashCode()
extern void ConfigurationDescriptor_GetHashCode_m5568ACF729A487DD99C555182524736ADA2E8F07_AdjustorThunk ();
// 0x000000FF System.Boolean UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::Equals(UnityEngine.XR.ARSubsystems.ConfigurationDescriptor)
extern void ConfigurationDescriptor_Equals_m20A0629D5EB248930415E499619C746E246CCC0D_AdjustorThunk ();
// 0x00000100 System.Boolean UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::Equals(System.Object)
extern void ConfigurationDescriptor_Equals_m63E4470800DF34C13B14CCD8879A45D03C8E28C4_AdjustorThunk ();
// 0x00000101 System.Boolean UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::op_Equality(UnityEngine.XR.ARSubsystems.ConfigurationDescriptor,UnityEngine.XR.ARSubsystems.ConfigurationDescriptor)
extern void ConfigurationDescriptor_op_Equality_m3DD670C71C323823087D717EE530978642956637 ();
// 0x00000102 System.Boolean UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::op_Inequality(UnityEngine.XR.ARSubsystems.ConfigurationDescriptor,UnityEngine.XR.ARSubsystems.ConfigurationDescriptor)
extern void ConfigurationDescriptor_op_Inequality_m54778CDE499216C9034DF6BA616C5583D348300C ();
// 0x00000103 UnityEngine.XR.ARSubsystems.Configuration UnityEngine.XR.ARSubsystems.DefaultConfigurationChooser::ChooseConfiguration(Unity.Collections.NativeSlice`1<UnityEngine.XR.ARSubsystems.ConfigurationDescriptor>,UnityEngine.XR.ARSubsystems.Feature)
extern void DefaultConfigurationChooser_ChooseConfiguration_m5C0D67889CBF390B4BC805FACCB19082487A105D ();
// 0x00000104 System.Void UnityEngine.XR.ARSubsystems.DefaultConfigurationChooser::.ctor()
extern void DefaultConfigurationChooser__ctor_mA2DD6EAD824E737D3FA2F722E023ADEFEA79C268 ();
// 0x00000105 System.Boolean UnityEngine.XR.ARSubsystems.FeatureExtensions::Any(UnityEngine.XR.ARSubsystems.Feature,UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_Any_m9A551424EE459226756825C4204862B7DD1FFE5D ();
// 0x00000106 System.Boolean UnityEngine.XR.ARSubsystems.FeatureExtensions::All(UnityEngine.XR.ARSubsystems.Feature,UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_All_m28EA1884048677E672FCB7DB0333FAB23F00A556 ();
// 0x00000107 System.Boolean UnityEngine.XR.ARSubsystems.FeatureExtensions::None(UnityEngine.XR.ARSubsystems.Feature,UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_None_m6C758CA7C15EF85929E49C2955B1E0302EA4939E ();
// 0x00000108 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.FeatureExtensions::Union(UnityEngine.XR.ARSubsystems.Feature,UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_Union_mEEFBCF0FA10823B7A65625E4F4DE4784D12B4EB1 ();
// 0x00000109 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.FeatureExtensions::Intersection(UnityEngine.XR.ARSubsystems.Feature,UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_Intersection_m79C38D66FB2BD6E622A3E6F5A7B838E7EC61F0A9 ();
// 0x0000010A UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.FeatureExtensions::SetDifference(UnityEngine.XR.ARSubsystems.Feature,UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_SetDifference_mE8B2C6885F24E4E29A52F51A57AF8F57B3F5E980 ();
// 0x0000010B UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.FeatureExtensions::SymmetricDifference(UnityEngine.XR.ARSubsystems.Feature,UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_SymmetricDifference_m664AA718B00BFEF9108230DF90DFD269D42919D5 ();
// 0x0000010C UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.FeatureExtensions::SetEnabled(UnityEngine.XR.ARSubsystems.Feature,UnityEngine.XR.ARSubsystems.Feature,System.Boolean)
extern void FeatureExtensions_SetEnabled_m8BB3223239E1259F0057CEBF2CBD23192860FF06 ();
// 0x0000010D UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.FeatureExtensions::Cameras(UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_Cameras_m1BECB29268A0BF510D6C311CBF5CB54254E03A15 ();
// 0x0000010E UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.FeatureExtensions::TrackingModes(UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_TrackingModes_mDE0FC3B938A8A3F22854D58DCAD264915C064843 ();
// 0x0000010F UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.FeatureExtensions::LightEstimation(UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_LightEstimation_m9CD1E28D53E5DED2E4E4E3FDBF72369972217877 ();
// 0x00000110 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.FeatureExtensions::WithoutCameraOrTracking(UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_WithoutCameraOrTracking_m30F447B3A3619A4D5ACE7B2CC168EE9DE8713668 ();
// 0x00000111 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.FeatureExtensions::LowestBit(UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_LowestBit_mB1DB2BC2E74BD3F8C262F504FF6CCDF35F20F151 ();
// 0x00000112 System.String UnityEngine.XR.ARSubsystems.FeatureExtensions::ToStringList(UnityEngine.XR.ARSubsystems.Feature,System.String)
extern void FeatureExtensions_ToStringList_mE85CF7C790356F522E273D136328B745F0C4F149 ();
// 0x00000113 System.Int32 UnityEngine.XR.ARSubsystems.FeatureExtensions::Count(UnityEngine.XR.ARSubsystems.Feature)
extern void FeatureExtensions_Count_m65FE7DAEFA72D88B6BD55B6EA93D59975EF90DEB ();
// 0x00000114 UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCpuImage::get_dimensions()
extern void XRCpuImage_get_dimensions_m21142AD3EFE33129CBBD8ACC604D1398121FB0CE_AdjustorThunk ();
// 0x00000115 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::set_dimensions(UnityEngine.Vector2Int)
extern void XRCpuImage_set_dimensions_m992E0A3FE1FE423D86A046987F251FD7D3D63E89_AdjustorThunk ();
// 0x00000116 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage::get_width()
extern void XRCpuImage_get_width_mB2BFCC0E20A9C86B6BD2AAE14B80EF44EA4327C7_AdjustorThunk ();
// 0x00000117 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage::get_height()
extern void XRCpuImage_get_height_m5D71C1DCDCC58501BBB0568B66E9259C60700AA7_AdjustorThunk ();
// 0x00000118 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage::get_planeCount()
extern void XRCpuImage_get_planeCount_mB545D3D7E24A0E186EFD1992A749B01A04AB096D_AdjustorThunk ();
// 0x00000119 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::set_planeCount(System.Int32)
extern void XRCpuImage_set_planeCount_m5737E3E280F5F49311540F98C9BB025716DCD977_AdjustorThunk ();
// 0x0000011A UnityEngine.XR.ARSubsystems.XRCpuImage_Format UnityEngine.XR.ARSubsystems.XRCpuImage::get_format()
extern void XRCpuImage_get_format_mD0A34A79B5F05E264452DCE9901DD2F43F527880_AdjustorThunk ();
// 0x0000011B System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::set_format(UnityEngine.XR.ARSubsystems.XRCpuImage_Format)
extern void XRCpuImage_set_format_m8629082CD757705A7BD147BADAB20A85562E7AB0_AdjustorThunk ();
// 0x0000011C System.Double UnityEngine.XR.ARSubsystems.XRCpuImage::get_timestamp()
extern void XRCpuImage_get_timestamp_m8BD98D270345A7FB70D7AC06A4D9C6BB2A170923_AdjustorThunk ();
// 0x0000011D System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::set_timestamp(System.Double)
extern void XRCpuImage_set_timestamp_m8F93F34F608F2A1A095EBC756D619B6F304208B9_AdjustorThunk ();
// 0x0000011E System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage::get_valid()
extern void XRCpuImage_get_valid_m74F130657887888264BB9A93150F39CB5077DDBB_AdjustorThunk ();
// 0x0000011F System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::.ctor(UnityEngine.XR.ARSubsystems.XRCpuImage_Api,UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo)
extern void XRCpuImage__ctor_mA38DE21566F85C757C12AF8CD750EE8F10C1AEFB_AdjustorThunk ();
// 0x00000120 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage::FormatSupported(UnityEngine.TextureFormat)
extern void XRCpuImage_FormatSupported_m3CD1E19616F7FAD3F2E2901EC26E0E3E33CD09BA_AdjustorThunk ();
// 0x00000121 UnityEngine.XR.ARSubsystems.XRCpuImage_Plane UnityEngine.XR.ARSubsystems.XRCpuImage::GetPlane(System.Int32)
extern void XRCpuImage_GetPlane_mCF396EE57D114577EF10F97D7425EE950D60D8E8_AdjustorThunk ();
// 0x00000122 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage::GetConvertedDataSize(UnityEngine.Vector2Int,UnityEngine.TextureFormat)
extern void XRCpuImage_GetConvertedDataSize_m467C94A6671214C85E47BF77B8BC82FF2F0790D2_AdjustorThunk ();
// 0x00000123 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage::GetConvertedDataSize(UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams)
extern void XRCpuImage_GetConvertedDataSize_m5A64E2913C09E6120C9358D80EBFA6AA2B57BB5B_AdjustorThunk ();
// 0x00000124 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::Convert(UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams,System.IntPtr,System.Int32)
extern void XRCpuImage_Convert_m440FC298D000D4ABED5CDBA38FB22A5CD0BB61F5_AdjustorThunk ();
// 0x00000125 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::Convert(UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams,Unity.Collections.NativeSlice`1<System.Byte>)
extern void XRCpuImage_Convert_mD5EF9358113B8C9543795F7568FC0F9FAE7F8435_AdjustorThunk ();
// 0x00000126 UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion UnityEngine.XR.ARSubsystems.XRCpuImage::ConvertAsync(UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams)
extern void XRCpuImage_ConvertAsync_m1486B5A2C9147AD0F0143A1A00CE24E308109F15_AdjustorThunk ();
// 0x00000127 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::ConvertAsync(UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams,System.Action`3<UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversionStatus,UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams,Unity.Collections.NativeArray`1<System.Byte>>)
extern void XRCpuImage_ConvertAsync_m7804259655BF42F77E739615633C728527E3E97D_AdjustorThunk ();
// 0x00000128 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::OnAsyncConversionComplete(UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversionStatus,UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams,System.IntPtr,System.Int32,System.IntPtr)
extern void XRCpuImage_OnAsyncConversionComplete_mE1632D9BCA9BB444DBB3283CBFE5567609FF98D0 ();
// 0x00000129 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::ValidateNativeHandleAndThrow()
extern void XRCpuImage_ValidateNativeHandleAndThrow_mCCE2D922A560FEA868503AC78BBA9C10B7FC8F25_AdjustorThunk ();
// 0x0000012A System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::ValidateConversionParamsAndThrow(UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams)
extern void XRCpuImage_ValidateConversionParamsAndThrow_m910D903A78C38AE0FE83278C9C8143ACF29D4351_AdjustorThunk ();
// 0x0000012B System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::Dispose()
extern void XRCpuImage_Dispose_m2E0EDC3DCC4EC7820D895586CD406593AFB70E0B_AdjustorThunk ();
// 0x0000012C System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage::GetHashCode()
extern void XRCpuImage_GetHashCode_mC3A63D5C67E64A34967C0D766BF07F32A946C4C2_AdjustorThunk ();
// 0x0000012D System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage::Equals(System.Object)
extern void XRCpuImage_Equals_m4AE6685AE4EE997352BC72A2D2B2F704502C0D20_AdjustorThunk ();
// 0x0000012E System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage::Equals(UnityEngine.XR.ARSubsystems.XRCpuImage)
extern void XRCpuImage_Equals_mA327280B4F60BF824A15F67F2FF0AD608DD5A07A_AdjustorThunk ();
// 0x0000012F System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage::op_Equality(UnityEngine.XR.ARSubsystems.XRCpuImage,UnityEngine.XR.ARSubsystems.XRCpuImage)
extern void XRCpuImage_op_Equality_m2804851877FA4BF36BE91DFFEB11B74A9332F3E0 ();
// 0x00000130 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage::op_Inequality(UnityEngine.XR.ARSubsystems.XRCpuImage,UnityEngine.XR.ARSubsystems.XRCpuImage)
extern void XRCpuImage_op_Inequality_m447487CD14D74E28B660B0C0F0C0870E8D0A9DC1 ();
// 0x00000131 System.String UnityEngine.XR.ARSubsystems.XRCpuImage::ToString()
extern void XRCpuImage_ToString_mCC4B5407E5B32F13D6F563B4012EDBABBF1548F0_AdjustorThunk ();
// 0x00000132 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage::.cctor()
extern void XRCpuImage__cctor_mC7990C4BC0490F405725B4287151E407D36B83F4 ();
// 0x00000133 UnityEngine.TextureFormat UnityEngine.XR.ARSubsystems.XRCpuImageFormatExtensions::AsTextureFormat(UnityEngine.XR.ARSubsystems.XRCpuImage_Format)
extern void XRCpuImageFormatExtensions_AsTextureFormat_m99960EBA0D4425DD05F8A50ED43D1474008C522A ();
// 0x00000134 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem::.ctor()
extern void XRDepthSubsystem__ctor_m1374F0D0EDEA230229CC5ADFF30647B3D2D100D3 ();
// 0x00000135 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem::OnStart()
extern void XRDepthSubsystem_OnStart_m0C7E81B5E231E3909F8494F8A9706F37B4B78FDC ();
// 0x00000136 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem::OnDestroyed()
extern void XRDepthSubsystem_OnDestroyed_m3AF3ED89CA1E5D4E00F9681600BBA028E5A62429 ();
// 0x00000137 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem::OnStop()
extern void XRDepthSubsystem_OnStop_m8FD775CD57E1FF5809A58B0960E3117A76C5C4A7 ();
// 0x00000138 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud> UnityEngine.XR.ARSubsystems.XRDepthSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRDepthSubsystem_GetChanges_m7B42781E43AFE126FBE6DFE2C8A3AF76DC53EEB3 ();
// 0x00000139 UnityEngine.XR.ARSubsystems.XRPointCloudData UnityEngine.XR.ARSubsystems.XRDepthSubsystem::GetPointCloudData(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator)
extern void XRDepthSubsystem_GetPointCloudData_m49BEF4047DEED6FC3E885AF893387ED347971BB8 ();
// 0x0000013A UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider UnityEngine.XR.ARSubsystems.XRDepthSubsystem::CreateProvider()
// 0x0000013B System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo)
extern void XRDepthSubsystemDescriptor__ctor_m350CD434BA44A06555D7522D9511963FA95437A5 ();
// 0x0000013C System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::get_supportsFeaturePoints()
extern void XRDepthSubsystemDescriptor_get_supportsFeaturePoints_m85CDF3E45EEAFC5B98302006B1782376C929B6C6 ();
// 0x0000013D System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::set_supportsFeaturePoints(System.Boolean)
extern void XRDepthSubsystemDescriptor_set_supportsFeaturePoints_m372ABA744A1FC28D1ACAACEB5890BB1D156A515F ();
// 0x0000013E System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::get_supportsUniqueIds()
extern void XRDepthSubsystemDescriptor_get_supportsUniqueIds_m3C75A183E8BAA7D6019A3E0941D43BA337C19049 ();
// 0x0000013F System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::set_supportsUniqueIds(System.Boolean)
extern void XRDepthSubsystemDescriptor_set_supportsUniqueIds_m4E7BCEF119F81892B7AA739EEBC6072A15E67957 ();
// 0x00000140 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::get_supportsConfidence()
extern void XRDepthSubsystemDescriptor_get_supportsConfidence_m8FCFA9A0F8537516D883BEEB63D045373DADC92E ();
// 0x00000141 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::set_supportsConfidence(System.Boolean)
extern void XRDepthSubsystemDescriptor_set_supportsConfidence_m513D6A5FCE31FD37260DDE662A08100F81FAC3EE ();
// 0x00000142 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor::RegisterDescriptor(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo)
extern void XRDepthSubsystemDescriptor_RegisterDescriptor_m53237B806014993075B559C44D9A743DB447154F ();
// 0x00000143 UnityEngine.XR.ARSubsystems.XRPointCloud UnityEngine.XR.ARSubsystems.XRPointCloud::get_defaultValue()
extern void XRPointCloud_get_defaultValue_m71EFAD95365CFFB007E85B39F6CCEB2182FCEEDC ();
// 0x00000144 System.Void UnityEngine.XR.ARSubsystems.XRPointCloud::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr)
extern void XRPointCloud__ctor_m0F992F42C621E29D49C0B27DD514B62FA5A7A655_AdjustorThunk ();
// 0x00000145 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRPointCloud::get_trackableId()
extern void XRPointCloud_get_trackableId_mA394197EAD026665FC02A1118CBBB46FF6873EF1_AdjustorThunk ();
// 0x00000146 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRPointCloud::get_pose()
extern void XRPointCloud_get_pose_m09C2DF1AD7F1220B547BD2EBCCA6E35F85A87EB0_AdjustorThunk ();
// 0x00000147 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRPointCloud::get_trackingState()
extern void XRPointCloud_get_trackingState_m0CE633649849B59E4AEA7875F62F0B34CA61FC96_AdjustorThunk ();
// 0x00000148 System.IntPtr UnityEngine.XR.ARSubsystems.XRPointCloud::get_nativePtr()
extern void XRPointCloud_get_nativePtr_m313F72EB3D0E3A439691D4A4AF84A61EE08FE371_AdjustorThunk ();
// 0x00000149 System.Int32 UnityEngine.XR.ARSubsystems.XRPointCloud::GetHashCode()
extern void XRPointCloud_GetHashCode_m171B58B8F5EB316F2E7746BFF30205A9724B11F7_AdjustorThunk ();
// 0x0000014A System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloud::Equals(UnityEngine.XR.ARSubsystems.XRPointCloud)
extern void XRPointCloud_Equals_m38B177BD481DFAAA66F4B66BE336A98AA4C4DCC6_AdjustorThunk ();
// 0x0000014B System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloud::Equals(System.Object)
extern void XRPointCloud_Equals_m01C5EBB7AC6017B014186EC74AC1CD637A7D56E9_AdjustorThunk ();
// 0x0000014C System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloud::op_Equality(UnityEngine.XR.ARSubsystems.XRPointCloud,UnityEngine.XR.ARSubsystems.XRPointCloud)
extern void XRPointCloud_op_Equality_m0AD50ADAE436F5A4D5DC68D67E0CCEF37E2783C0 ();
// 0x0000014D System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloud::op_Inequality(UnityEngine.XR.ARSubsystems.XRPointCloud,UnityEngine.XR.ARSubsystems.XRPointCloud)
extern void XRPointCloud_op_Inequality_mD012C95636F2CFABAB524548362726BD2A190A48 ();
// 0x0000014E System.Void UnityEngine.XR.ARSubsystems.XRPointCloud::.cctor()
extern void XRPointCloud__cctor_mF51225F68978906B6C48B574FB42B90511C76130 ();
// 0x0000014F Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARSubsystems.XRPointCloudData::get_positions()
extern void XRPointCloudData_get_positions_m2BDA572054D639DB35E9FDA3D15AEF3B7B39D40C_AdjustorThunk ();
// 0x00000150 System.Void UnityEngine.XR.ARSubsystems.XRPointCloudData::set_positions(Unity.Collections.NativeArray`1<UnityEngine.Vector3>)
extern void XRPointCloudData_set_positions_m78BB0E1E2A5860DAC6F60D6C9A6A37544FF9880E_AdjustorThunk ();
// 0x00000151 Unity.Collections.NativeArray`1<System.Single> UnityEngine.XR.ARSubsystems.XRPointCloudData::get_confidenceValues()
extern void XRPointCloudData_get_confidenceValues_m156073A1640F58477DBCBAC6BDA05C3BF866ACE6_AdjustorThunk ();
// 0x00000152 System.Void UnityEngine.XR.ARSubsystems.XRPointCloudData::set_confidenceValues(Unity.Collections.NativeArray`1<System.Single>)
extern void XRPointCloudData_set_confidenceValues_mC837DE6B63BF8CBD8F2480C1A4ED3247AABCB861_AdjustorThunk ();
// 0x00000153 Unity.Collections.NativeArray`1<System.UInt64> UnityEngine.XR.ARSubsystems.XRPointCloudData::get_identifiers()
extern void XRPointCloudData_get_identifiers_m6AA5FE2F151300371A1F5A8310A49A0D4A35BD23_AdjustorThunk ();
// 0x00000154 System.Void UnityEngine.XR.ARSubsystems.XRPointCloudData::set_identifiers(Unity.Collections.NativeArray`1<System.UInt64>)
extern void XRPointCloudData_set_identifiers_m550B2B8C6EF821D5BBD47C066FF6C961EF0CA562_AdjustorThunk ();
// 0x00000155 System.Void UnityEngine.XR.ARSubsystems.XRPointCloudData::Dispose()
extern void XRPointCloudData_Dispose_mDF78595F088472E60327A1D366AA787C68A3EDE3_AdjustorThunk ();
// 0x00000156 System.Int32 UnityEngine.XR.ARSubsystems.XRPointCloudData::GetHashCode()
extern void XRPointCloudData_GetHashCode_mA67A28CD8661AAE597D4466135AF1750D2569409_AdjustorThunk ();
// 0x00000157 System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloudData::Equals(System.Object)
extern void XRPointCloudData_Equals_mC870B135A9697D1A2AFB40892E70D7D403590E2A_AdjustorThunk ();
// 0x00000158 System.String UnityEngine.XR.ARSubsystems.XRPointCloudData::ToString()
extern void XRPointCloudData_ToString_m3790E45AE87D2C6F63D664EF736F530B7A4FCB4D_AdjustorThunk ();
// 0x00000159 System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloudData::Equals(UnityEngine.XR.ARSubsystems.XRPointCloudData)
extern void XRPointCloudData_Equals_mDE5097D689526E5461CBBC48C36E6221F71F7598_AdjustorThunk ();
// 0x0000015A System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloudData::op_Equality(UnityEngine.XR.ARSubsystems.XRPointCloudData,UnityEngine.XR.ARSubsystems.XRPointCloudData)
extern void XRPointCloudData_op_Equality_m96CE37D4B2F213B20AE0352C787DBE13BE339ECE ();
// 0x0000015B System.Boolean UnityEngine.XR.ARSubsystems.XRPointCloudData::op_Inequality(UnityEngine.XR.ARSubsystems.XRPointCloudData,UnityEngine.XR.ARSubsystems.XRPointCloudData)
extern void XRPointCloudData_op_Inequality_mB515ACB6DD311FB5AF3CA608B7078581AFEDD25B ();
// 0x0000015C UnityEngine.XR.ARSubsystems.XREnvironmentProbe UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_defaultValue()
extern void XREnvironmentProbe_get_defaultValue_m14B351BF8F54FCFCDA803FA3C29D1590BC0148E4 ();
// 0x0000015D UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_trackableId()
extern void XREnvironmentProbe_get_trackableId_m2F7F8DCE954C099E60807742B6A4B27DA2F30085_AdjustorThunk ();
// 0x0000015E System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_trackableId(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XREnvironmentProbe_set_trackableId_m02C924524F96E6A0A434C30A08F251F9AF407453_AdjustorThunk ();
// 0x0000015F UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_scale()
extern void XREnvironmentProbe_get_scale_m2346CDBC2FEF9CC85761BF11829E10D2992E0004_AdjustorThunk ();
// 0x00000160 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_scale(UnityEngine.Vector3)
extern void XREnvironmentProbe_set_scale_m122476D077C3E65524BDF5A662B6D4FC01FC1954_AdjustorThunk ();
// 0x00000161 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_pose()
extern void XREnvironmentProbe_get_pose_m2CF6BF7E554B1225E99947B620D2C029499E7996_AdjustorThunk ();
// 0x00000162 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_pose(UnityEngine.Pose)
extern void XREnvironmentProbe_set_pose_m3BE983E4E465A1DC6D4E829788A0FE217250BE28_AdjustorThunk ();
// 0x00000163 UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_size()
extern void XREnvironmentProbe_get_size_mCFA1F9B8C8BE0138585D2BE4C2059E0153133C11_AdjustorThunk ();
// 0x00000164 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_size(UnityEngine.Vector3)
extern void XREnvironmentProbe_set_size_m0F087E604DCEC21E2CBFCC6BB1AE557C684B4428_AdjustorThunk ();
// 0x00000165 UnityEngine.XR.ARSubsystems.XRTextureDescriptor UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_textureDescriptor()
extern void XREnvironmentProbe_get_textureDescriptor_mA926C0701B8D774E5C0A1F51DC54546CB71ECFFA_AdjustorThunk ();
// 0x00000166 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_textureDescriptor(UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void XREnvironmentProbe_set_textureDescriptor_m104A7E7448BDBA6BB45B540AE3DDA8D37150BF82_AdjustorThunk ();
// 0x00000167 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_trackingState()
extern void XREnvironmentProbe_get_trackingState_m48BAAE58BF028382D00D5F49BA1F6023E53B0AF9_AdjustorThunk ();
// 0x00000168 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_trackingState(UnityEngine.XR.ARSubsystems.TrackingState)
extern void XREnvironmentProbe_set_trackingState_m1E2728289637E522527BC24ED51E97CAAB7E7E4C_AdjustorThunk ();
// 0x00000169 System.IntPtr UnityEngine.XR.ARSubsystems.XREnvironmentProbe::get_nativePtr()
extern void XREnvironmentProbe_get_nativePtr_m3F1EB67BA31BFA57D741EC97267A851EA376E8D5_AdjustorThunk ();
// 0x0000016A System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::set_nativePtr(System.IntPtr)
extern void XREnvironmentProbe_set_nativePtr_mE1643DBD64E1DF09A9B62F7DFD5342C90F00E010_AdjustorThunk ();
// 0x0000016B System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbe::Equals(UnityEngine.XR.ARSubsystems.XREnvironmentProbe)
extern void XREnvironmentProbe_Equals_mD54F2132909E56F471109994AB0D41C708BE8C91_AdjustorThunk ();
// 0x0000016C System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbe::Equals(System.Object)
extern void XREnvironmentProbe_Equals_mF133292903D42FC122E0F88D73FE918A3F0D6722_AdjustorThunk ();
// 0x0000016D System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbe::op_Equality(UnityEngine.XR.ARSubsystems.XREnvironmentProbe,UnityEngine.XR.ARSubsystems.XREnvironmentProbe)
extern void XREnvironmentProbe_op_Equality_mB488DFEB2BC8DB08F90D3B88DE96A6C274745779 ();
// 0x0000016E System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbe::op_Inequality(UnityEngine.XR.ARSubsystems.XREnvironmentProbe,UnityEngine.XR.ARSubsystems.XREnvironmentProbe)
extern void XREnvironmentProbe_op_Inequality_mC044BE07ABC3F68B8BE3A59DAF9D86F6FB2F7C87 ();
// 0x0000016F System.Int32 UnityEngine.XR.ARSubsystems.XREnvironmentProbe::GetHashCode()
extern void XREnvironmentProbe_GetHashCode_m0171139D9AB03DE5465347053AEF443E482DEA83_AdjustorThunk ();
// 0x00000170 System.String UnityEngine.XR.ARSubsystems.XREnvironmentProbe::ToString()
extern void XREnvironmentProbe_ToString_m6F495FF5C04959B29C49C6E45DCD15FD3E6F153F_AdjustorThunk ();
// 0x00000171 System.String UnityEngine.XR.ARSubsystems.XREnvironmentProbe::ToString(System.String)
extern void XREnvironmentProbe_ToString_mAADD3CBE24607D1FDE563A639744E7A6657EF7DB_AdjustorThunk ();
// 0x00000172 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbe::.cctor()
extern void XREnvironmentProbe__cctor_m833E25DAF76FD20D0DDC939A61A75E4457896AF9 ();
// 0x00000173 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::.ctor()
extern void XREnvironmentProbeSubsystem__ctor_mD5858B70AAE45F8F825E8C0D6E980C8DCD8A7E33 ();
// 0x00000174 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::get_automaticPlacementRequested()
extern void XREnvironmentProbeSubsystem_get_automaticPlacementRequested_mCC6780B1B0DE13EBC0460D8471D744D3E1FF3059 ();
// 0x00000175 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::set_automaticPlacementRequested(System.Boolean)
extern void XREnvironmentProbeSubsystem_set_automaticPlacementRequested_mD346F82B7985B210050CA290587B387CC1AF1494 ();
// 0x00000176 UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::get_subsystemDescriptor()
extern void XREnvironmentProbeSubsystem_get_subsystemDescriptor_mB3F44E9126C4222121677B889A82A8765515528E ();
// 0x00000177 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::get_automaticPlacementEnabled()
extern void XREnvironmentProbeSubsystem_get_automaticPlacementEnabled_m8652E1BE78F0049A3821862C791F899F24A13834 ();
// 0x00000178 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::get_environmentTextureHDRRequested()
extern void XREnvironmentProbeSubsystem_get_environmentTextureHDRRequested_mC65FBA739FBDA6B4111308DEE949DE2317AFA36B ();
// 0x00000179 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::set_environmentTextureHDRRequested(System.Boolean)
extern void XREnvironmentProbeSubsystem_set_environmentTextureHDRRequested_m21BF6D56B29E88354A9263C2FC88A2D05B59C873 ();
// 0x0000017A System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::get_environmentTextureHDREnabled()
extern void XREnvironmentProbeSubsystem_get_environmentTextureHDREnabled_mEA9B5F383052BC8DDD43E168A0228FB31ABC4318 ();
// 0x0000017B UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbe> UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XREnvironmentProbeSubsystem_GetChanges_mA100F4697822F10AAAD8506683629996239BD7C5 ();
// 0x0000017C System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::OnStart()
extern void XREnvironmentProbeSubsystem_OnStart_m6043B9D1DE6190686DA46A5D4DFD284C4E144166 ();
// 0x0000017D System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::OnStop()
extern void XREnvironmentProbeSubsystem_OnStop_mEC24AA84C449BD8D8EA6DBDA7598A2375D2C012C ();
// 0x0000017E System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::OnDestroyed()
extern void XREnvironmentProbeSubsystem_OnDestroyed_m2D6E9339398E952F73B8C6E951386B05D648E3BA ();
// 0x0000017F System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::TryAddEnvironmentProbe(UnityEngine.Pose,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.XR.ARSubsystems.XREnvironmentProbe&)
extern void XREnvironmentProbeSubsystem_TryAddEnvironmentProbe_m20C670C360A17973331846037D1C075351EB399B ();
// 0x00000180 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::RemoveEnvironmentProbe(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XREnvironmentProbeSubsystem_RemoveEnvironmentProbe_m402226077D7D17C3CD67CCD60CAEC91697AB9FB9 ();
// 0x00000181 UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::CreateProvider()
// 0x00000182 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem::Register(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystem_Register_m0701E0648B078B75B6DC47AED942AEA83B986E11 ();
// 0x00000183 System.String UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_id()
extern void XREnvironmentProbeSubsystemCinfo_get_id_m35112EDFEC749952B475247292216B73C159BBDF_AdjustorThunk ();
// 0x00000184 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_id(System.String)
extern void XREnvironmentProbeSubsystemCinfo_set_id_mC04469DE2280532DA98230C42D293A40E73870EE_AdjustorThunk ();
// 0x00000185 System.Type UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_implementationType()
extern void XREnvironmentProbeSubsystemCinfo_get_implementationType_m4092117AA12E3175B4DBCA30FC7E035E785058E0_AdjustorThunk ();
// 0x00000186 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_implementationType(System.Type)
extern void XREnvironmentProbeSubsystemCinfo_set_implementationType_mC5DFD8692500C7513DE3AC48BDE90AB5CE9D1AF9_AdjustorThunk ();
// 0x00000187 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsManualPlacement()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsManualPlacement_m9F8E25BF94BD0D089D9BFE4EFE4F4F5349EE8F00_AdjustorThunk ();
// 0x00000188 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsManualPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsManualPlacement_m82BD1C3FA4706E2EC81E6B8CF2D9804FA75BD391_AdjustorThunk ();
// 0x00000189 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsRemovalOfManual()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfManual_mEF312990150F85B0CF01436677E5F795BF9BD61E_AdjustorThunk ();
// 0x0000018A System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsRemovalOfManual(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfManual_m99273CA85107749AF9914F38B1B944660E5C0860_AdjustorThunk ();
// 0x0000018B System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsAutomaticPlacement()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsAutomaticPlacement_m65A5F29D5CB748401E6F039C82332012BA2EC61A_AdjustorThunk ();
// 0x0000018C System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsAutomaticPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsAutomaticPlacement_mCF993333DFF9094801678045C101EAC59D6A0A55_AdjustorThunk ();
// 0x0000018D System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsRemovalOfAutomatic()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfAutomatic_mE0EA584480EB5DA00FF26D3129926FD60FD36CD9_AdjustorThunk ();
// 0x0000018E System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsRemovalOfAutomatic(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfAutomatic_mB19AC789CAA508C4AB003E575CDD69692B7974D8_AdjustorThunk ();
// 0x0000018F System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsEnvironmentTexture()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsEnvironmentTexture_m4B973F04E1F4698C0C001A88463A1C1DE9CD0C95_AdjustorThunk ();
// 0x00000190 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsEnvironmentTexture(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsEnvironmentTexture_m6DCBFAB057A1C98762332AB4E661C056ECD574FC_AdjustorThunk ();
// 0x00000191 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::get_supportsEnvironmentTextureHDR()
extern void XREnvironmentProbeSubsystemCinfo_get_supportsEnvironmentTextureHDR_m79C96A18C168AE457BD4E1F996F63BAD23DC8149_AdjustorThunk ();
// 0x00000192 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::set_supportsEnvironmentTextureHDR(System.Boolean)
extern void XREnvironmentProbeSubsystemCinfo_set_supportsEnvironmentTextureHDR_m37EB940E817F6792E0085944543D9D41F7BF0058_AdjustorThunk ();
// 0x00000193 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::Equals(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemCinfo_Equals_m5515D71C2B24DE3277CCB1CFE7B900F6FB867679_AdjustorThunk ();
// 0x00000194 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::Equals(System.Object)
extern void XREnvironmentProbeSubsystemCinfo_Equals_m9054174CEA9A5167D68DE43F4861EC5EF9F797AA_AdjustorThunk ();
// 0x00000195 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::op_Equality(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemCinfo_op_Equality_m069BD1A34F40D78B3A2C189DF0C6DBF9F02524E1 ();
// 0x00000196 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemCinfo_op_Inequality_mD521BAED92A48BD4F24DD2693A3075D8B0DF3B3F ();
// 0x00000197 System.Int32 UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo::GetHashCode()
extern void XREnvironmentProbeSubsystemCinfo_GetHashCode_m455133A5C39919DE0737C8B2EFD39B29331B08D0_AdjustorThunk ();
// 0x00000198 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemDescriptor__ctor_mB4A6964DAD6E27CDA6E2FD747C31F45FAFCBFDBE ();
// 0x00000199 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsManualPlacement()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsManualPlacement_m5AD7DF2F4755AF9D77E0543BB886EFBA3B9F819D ();
// 0x0000019A System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsManualPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsManualPlacement_m5F6FA0792E3DDDF3F050BB7359BC4A5C0990B624 ();
// 0x0000019B System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsRemovalOfManual()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsRemovalOfManual_m5E310D3807CEA59342DC63BFC92C236EBEE9B050 ();
// 0x0000019C System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsRemovalOfManual(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsRemovalOfManual_mF8D9A8DABBD292211E67EFCA1060D6F37B70D329 ();
// 0x0000019D System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsAutomaticPlacement()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsAutomaticPlacement_mE81C20404E1B5E438640209D2AEAD344B960E83A ();
// 0x0000019E System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsAutomaticPlacement(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsAutomaticPlacement_m0F393A7ADFA798824A9D0EDB586D879BDE79B117 ();
// 0x0000019F System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsRemovalOfAutomatic()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsRemovalOfAutomatic_mF2E0D44C620A28668F575A2FFDB85DD0DFA33A40 ();
// 0x000001A0 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsRemovalOfAutomatic(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsRemovalOfAutomatic_mB92E4298805A5F9640139D53BFBF72CC28B006BE ();
// 0x000001A1 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsEnvironmentTexture()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsEnvironmentTexture_m012FA38EC2000019C5DA5359C048AF6F645FD6BA ();
// 0x000001A2 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsEnvironmentTexture(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsEnvironmentTexture_mC3622C706E9C8AFA8FB574B8096568C6484D61A9 ();
// 0x000001A3 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::get_supportsEnvironmentTextureHDR()
extern void XREnvironmentProbeSubsystemDescriptor_get_supportsEnvironmentTextureHDR_m5028FEAD3D411E08D66884DA61AE117126FECC30 ();
// 0x000001A4 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::set_supportsEnvironmentTextureHDR(System.Boolean)
extern void XREnvironmentProbeSubsystemDescriptor_set_supportsEnvironmentTextureHDR_m77896178EBFF4F9BA49E428611E6AFBD0CFE09A7 ();
// 0x000001A5 UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemCinfo)
extern void XREnvironmentProbeSubsystemDescriptor_Create_mB7EBDC47BFF343F6B7B621B5D5E5EE9600EF767E ();
// 0x000001A6 UnityEngine.XR.ARSubsystems.XRFace UnityEngine.XR.ARSubsystems.XRFace::get_defaultValue()
extern void XRFace_get_defaultValue_m10A49DFCC1786C0E8F3244200F1AC9696C16AD34 ();
// 0x000001A7 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRFace::get_trackableId()
extern void XRFace_get_trackableId_mC7AA3B622C6B16A9E8B5A3BEA524C7ED54A6188D_AdjustorThunk ();
// 0x000001A8 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRFace::get_pose()
extern void XRFace_get_pose_m3792AF11CBB24361529B7291ED46B9DD2970AC54_AdjustorThunk ();
// 0x000001A9 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRFace::get_trackingState()
extern void XRFace_get_trackingState_m29CA0D89354ACC65EF8A18C09201BCBB3F732E3B_AdjustorThunk ();
// 0x000001AA System.IntPtr UnityEngine.XR.ARSubsystems.XRFace::get_nativePtr()
extern void XRFace_get_nativePtr_mAB11BBF883F193C7A8224F78D8AC3F814E5CFF24_AdjustorThunk ();
// 0x000001AB UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRFace::get_leftEyePose()
extern void XRFace_get_leftEyePose_mB6508142768ACD1B9C5EA05224DEF9E690C7F0F1_AdjustorThunk ();
// 0x000001AC UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRFace::get_rightEyePose()
extern void XRFace_get_rightEyePose_m8A78A727975AA070F197E566C8135B2CA45E4996_AdjustorThunk ();
// 0x000001AD UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.XRFace::get_fixationPoint()
extern void XRFace_get_fixationPoint_mBC16DB0D6E29A8DCAEB022097B502398BF106405_AdjustorThunk ();
// 0x000001AE System.Boolean UnityEngine.XR.ARSubsystems.XRFace::Equals(System.Object)
extern void XRFace_Equals_m9070A1CAB9F7539BB46EF54457FF63D77F49AB2F_AdjustorThunk ();
// 0x000001AF System.Int32 UnityEngine.XR.ARSubsystems.XRFace::GetHashCode()
extern void XRFace_GetHashCode_m4BD8F265F67DE7593A58B15EBEBCC826788C0C16_AdjustorThunk ();
// 0x000001B0 System.Boolean UnityEngine.XR.ARSubsystems.XRFace::op_Equality(UnityEngine.XR.ARSubsystems.XRFace,UnityEngine.XR.ARSubsystems.XRFace)
extern void XRFace_op_Equality_m869853751F92BFF53F76B82C59DA0E3EB34ACEC3 ();
// 0x000001B1 System.Boolean UnityEngine.XR.ARSubsystems.XRFace::op_Inequality(UnityEngine.XR.ARSubsystems.XRFace,UnityEngine.XR.ARSubsystems.XRFace)
extern void XRFace_op_Inequality_mD49925C1F7492D95AF42C8C6F79C78556520035B ();
// 0x000001B2 System.Boolean UnityEngine.XR.ARSubsystems.XRFace::Equals(UnityEngine.XR.ARSubsystems.XRFace)
extern void XRFace_Equals_mFD60266E20097CC26C9C7C24F81A56B40468424F_AdjustorThunk ();
// 0x000001B3 System.Void UnityEngine.XR.ARSubsystems.XRFace::.cctor()
extern void XRFace__cctor_m75F3E710F4D3A5809652FBB5237C2FB241EB8D89 ();
// 0x000001B4 System.Void UnityEngine.XR.ARSubsystems.XRFaceMesh::Resize(System.Int32,System.Int32,UnityEngine.XR.ARSubsystems.XRFaceMesh_Attributes,Unity.Collections.Allocator)
extern void XRFaceMesh_Resize_m278CBD449E198430D20CCC37897C7F254A94D65E_AdjustorThunk ();
// 0x000001B5 Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARSubsystems.XRFaceMesh::get_vertices()
extern void XRFaceMesh_get_vertices_mD6CE3C180851CA10DE10A7B6C7BF0817E73609E9_AdjustorThunk ();
// 0x000001B6 Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARSubsystems.XRFaceMesh::get_normals()
extern void XRFaceMesh_get_normals_m8CF3D0395943001F6A681154DF7A75FA045CE6AE_AdjustorThunk ();
// 0x000001B7 Unity.Collections.NativeArray`1<System.Int32> UnityEngine.XR.ARSubsystems.XRFaceMesh::get_indices()
extern void XRFaceMesh_get_indices_m4355D06541511C7724AA543FB0D66BE69F261F11_AdjustorThunk ();
// 0x000001B8 Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARSubsystems.XRFaceMesh::get_uvs()
extern void XRFaceMesh_get_uvs_mECB939F9E262D22AED47311D6985116FC0CEEA9B_AdjustorThunk ();
// 0x000001B9 System.Void UnityEngine.XR.ARSubsystems.XRFaceMesh::Dispose()
extern void XRFaceMesh_Dispose_m99786F0191BAD0D7E369911BEC69528BF8EEEDBC_AdjustorThunk ();
// 0x000001BA System.Int32 UnityEngine.XR.ARSubsystems.XRFaceMesh::GetHashCode()
extern void XRFaceMesh_GetHashCode_m1DAD63B36571737E153BF2B79C78E7C70C0E7CDA_AdjustorThunk ();
// 0x000001BB System.Boolean UnityEngine.XR.ARSubsystems.XRFaceMesh::Equals(System.Object)
extern void XRFaceMesh_Equals_m2D832F4D965FF78E5B5AEB89D8DD66A622F473E4_AdjustorThunk ();
// 0x000001BC System.String UnityEngine.XR.ARSubsystems.XRFaceMesh::ToString()
extern void XRFaceMesh_ToString_m037407F8735C9CF13EEA1CC1C9F216CA930F3ED4_AdjustorThunk ();
// 0x000001BD System.Boolean UnityEngine.XR.ARSubsystems.XRFaceMesh::Equals(UnityEngine.XR.ARSubsystems.XRFaceMesh)
extern void XRFaceMesh_Equals_m4560A69CCA817DD121CF283E87330937C98B233A_AdjustorThunk ();
// 0x000001BE System.Boolean UnityEngine.XR.ARSubsystems.XRFaceMesh::op_Equality(UnityEngine.XR.ARSubsystems.XRFaceMesh,UnityEngine.XR.ARSubsystems.XRFaceMesh)
extern void XRFaceMesh_op_Equality_m26FDDF515E062E95ED2BE3A9E0059567A01B24F3 ();
// 0x000001BF System.Boolean UnityEngine.XR.ARSubsystems.XRFaceMesh::op_Inequality(UnityEngine.XR.ARSubsystems.XRFaceMesh,UnityEngine.XR.ARSubsystems.XRFaceMesh)
extern void XRFaceMesh_op_Inequality_m2EE5ED9174FB59B8B721D52E57B94B96AEA3A516 ();
// 0x000001C0 System.Void UnityEngine.XR.ARSubsystems.XRFaceMesh::Resize(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<T>&,System.Boolean)
// 0x000001C1 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::.ctor()
extern void XRFaceSubsystem__ctor_m848121C59971872F188E502DC2228B2F888C3B39 ();
// 0x000001C2 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::OnStart()
extern void XRFaceSubsystem_OnStart_m156F96C39D26A1F4787FD202FBC7B75F668249C1 ();
// 0x000001C3 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::OnDestroyed()
extern void XRFaceSubsystem_OnDestroyed_m6A339572B90043E82921A77EF62497005DA78151 ();
// 0x000001C4 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::OnStop()
extern void XRFaceSubsystem_OnStop_mF9AE8B1760B8EB324DCC41AC2BDF2A6CA5237080 ();
// 0x000001C5 System.Int32 UnityEngine.XR.ARSubsystems.XRFaceSubsystem::get_requestedMaximumFaceCount()
extern void XRFaceSubsystem_get_requestedMaximumFaceCount_mCDBADE73C31C3FCECA03C16974C0BA626AF5A678 ();
// 0x000001C6 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::set_requestedMaximumFaceCount(System.Int32)
extern void XRFaceSubsystem_set_requestedMaximumFaceCount_mF74DBC2F6E1532AFA44B006A9B2E593DB041C80D ();
// 0x000001C7 System.Int32 UnityEngine.XR.ARSubsystems.XRFaceSubsystem::get_currentMaximumFaceCount()
extern void XRFaceSubsystem_get_currentMaximumFaceCount_m7C73145C5EB8CFED720755F1DE40C55A60BA2896 ();
// 0x000001C8 System.Int32 UnityEngine.XR.ARSubsystems.XRFaceSubsystem::get_supportedFaceCount()
extern void XRFaceSubsystem_get_supportedFaceCount_m98AC6844E4919284379E0B7D17DE393D2DEAC182 ();
// 0x000001C9 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace> UnityEngine.XR.ARSubsystems.XRFaceSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRFaceSubsystem_GetChanges_m7A0FF036C2DF93EBD2B39EB65AA87335601C9AA7 ();
// 0x000001CA System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem::GetFaceMesh(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,UnityEngine.XR.ARSubsystems.XRFaceMesh&)
extern void XRFaceSubsystem_GetFaceMesh_m347A4FE63D6AB092FCBA7EB94542F9B3DBFDFA52 ();
// 0x000001CB UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider UnityEngine.XR.ARSubsystems.XRFaceSubsystem::CreateProvider()
// 0x000001CC System.String UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_id()
extern void FaceSubsystemParams_get_id_mAEEA8292B68FD9CDC00CDB3F54650CE9B8DFFD55_AdjustorThunk ();
// 0x000001CD System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_id(System.String)
extern void FaceSubsystemParams_set_id_m3DB203D7778C049F34D33B8B621CA63C26C50173_AdjustorThunk ();
// 0x000001CE System.Type UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_subsystemImplementationType()
extern void FaceSubsystemParams_get_subsystemImplementationType_m07845AB046188D52ADBE505597D94AEE5443BBC6_AdjustorThunk ();
// 0x000001CF System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_subsystemImplementationType(System.Type)
extern void FaceSubsystemParams_set_subsystemImplementationType_mEB4B5EA266E818DA6DFA71FCD85E00C22F7D36F4_AdjustorThunk ();
// 0x000001D0 System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_supportsFacePose()
extern void FaceSubsystemParams_get_supportsFacePose_m1F7E0D0F6964B4DF7C97155FC8EE013FFFA4BC6B_AdjustorThunk ();
// 0x000001D1 System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_supportsFacePose(System.Boolean)
extern void FaceSubsystemParams_set_supportsFacePose_mAEC183F6C4DEB3146FD8A336540851FB500295D5_AdjustorThunk ();
// 0x000001D2 System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_supportsFaceMeshVerticesAndIndices()
extern void FaceSubsystemParams_get_supportsFaceMeshVerticesAndIndices_m18D1BB926039B3D8BD2541FF395632C24023E22E_AdjustorThunk ();
// 0x000001D3 System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_supportsFaceMeshVerticesAndIndices(System.Boolean)
extern void FaceSubsystemParams_set_supportsFaceMeshVerticesAndIndices_m508C74CE5B60402069FE4A2E6FA373531A139D78_AdjustorThunk ();
// 0x000001D4 System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_supportsFaceMeshUVs()
extern void FaceSubsystemParams_get_supportsFaceMeshUVs_m35C7FCE40E7C59CBF05291DF7266A12A6D75A217_AdjustorThunk ();
// 0x000001D5 System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_supportsFaceMeshUVs(System.Boolean)
extern void FaceSubsystemParams_set_supportsFaceMeshUVs_mC4CC736CE3F53B8C04F3399DFCAA1727CF4928A9_AdjustorThunk ();
// 0x000001D6 System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_supportsFaceMeshNormals()
extern void FaceSubsystemParams_get_supportsFaceMeshNormals_mDDB3AB2B685A51D5D36C9A5D5C403F0F4C8DFEA5_AdjustorThunk ();
// 0x000001D7 System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_supportsFaceMeshNormals(System.Boolean)
extern void FaceSubsystemParams_set_supportsFaceMeshNormals_m100AA9A06836AF23CE936AA2BDA6403E584E4B3C_AdjustorThunk ();
// 0x000001D8 System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::get_supportsEyeTracking()
extern void FaceSubsystemParams_get_supportsEyeTracking_m141415010676F58F488D1CB30163E16A275C177D_AdjustorThunk ();
// 0x000001D9 System.Void UnityEngine.XR.ARSubsystems.FaceSubsystemParams::set_supportsEyeTracking(System.Boolean)
extern void FaceSubsystemParams_set_supportsEyeTracking_m06BFB3B0F58946DD993C9AEA99D3B617F15AFBB6_AdjustorThunk ();
// 0x000001DA System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::Equals(UnityEngine.XR.ARSubsystems.FaceSubsystemParams)
extern void FaceSubsystemParams_Equals_m1C189CDCD693681575267133354C41E5F4CA158C_AdjustorThunk ();
// 0x000001DB System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::Equals(System.Object)
extern void FaceSubsystemParams_Equals_m319C11F36441455D498041CCAC4762F4D7393F03_AdjustorThunk ();
// 0x000001DC System.Int32 UnityEngine.XR.ARSubsystems.FaceSubsystemParams::GetHashCode()
extern void FaceSubsystemParams_GetHashCode_mEDB28F47A58BE6CC8307751C6D73BE5C5801DE77_AdjustorThunk ();
// 0x000001DD System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::op_Equality(UnityEngine.XR.ARSubsystems.FaceSubsystemParams,UnityEngine.XR.ARSubsystems.FaceSubsystemParams)
extern void FaceSubsystemParams_op_Equality_mCA3B5779A790E6B0846E36A6FA895A1450F5957C ();
// 0x000001DE System.Boolean UnityEngine.XR.ARSubsystems.FaceSubsystemParams::op_Inequality(UnityEngine.XR.ARSubsystems.FaceSubsystemParams,UnityEngine.XR.ARSubsystems.FaceSubsystemParams)
extern void FaceSubsystemParams_op_Inequality_m75983FBFBAE50E14DEB5E01BA48CA9E9B2B018E6 ();
// 0x000001DF System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.FaceSubsystemParams)
extern void XRFaceSubsystemDescriptor__ctor_mBD859B2AB42BFCD523BB02010B0048D03C4FEB3D ();
// 0x000001E0 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::get_supportsFacePose()
extern void XRFaceSubsystemDescriptor_get_supportsFacePose_m68F807E50DD826B46C3E13D44E426ED7430F42A3 ();
// 0x000001E1 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::get_supportsFaceMeshVerticesAndIndices()
extern void XRFaceSubsystemDescriptor_get_supportsFaceMeshVerticesAndIndices_m70FD2CA774BC71CA1CBE56FFF48FBC157A34E444 ();
// 0x000001E2 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::get_supportsFaceMeshUVs()
extern void XRFaceSubsystemDescriptor_get_supportsFaceMeshUVs_m29254875989386F1B592D9C20164E0CE7AFC96CB ();
// 0x000001E3 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::get_supportsFaceMeshNormals()
extern void XRFaceSubsystemDescriptor_get_supportsFaceMeshNormals_mBD374FCF46BEA94D10073D70780AA8104A018CE0 ();
// 0x000001E4 System.Boolean UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::get_supportsEyeTracking()
extern void XRFaceSubsystemDescriptor_get_supportsEyeTracking_m25F30879E205470D26A93B900CFE3B6EA33EB618 ();
// 0x000001E5 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.FaceSubsystemParams)
extern void XRFaceSubsystemDescriptor_Create_m68C75BA860D9E4EB66A232D86C3BFB2B435FEE74 ();
// 0x000001E6 System.Guid UnityEngine.XR.ARSubsystems.GuidUtil::Compose(System.UInt64,System.UInt64)
extern void GuidUtil_Compose_mF0A2DF31C9F5E45DC7786601C82B926546B021D4 ();
// 0x000001E7 System.Int32 UnityEngine.XR.ARSubsystems.HashCode::Combine(System.Int32,System.Int32)
extern void HashCode_Combine_m59AE2A453CEA5122839942FAD800C7E591DCC83D ();
// 0x000001E8 System.Int32 UnityEngine.XR.ARSubsystems.HashCode::ReferenceHash(System.Object)
extern void HashCode_ReferenceHash_mBCF52B8AB75E5AF77B211169F8A9029E3A4AE89D ();
// 0x000001E9 System.Int32 UnityEngine.XR.ARSubsystems.HashCode::Combine(System.Int32,System.Int32,System.Int32)
extern void HashCode_Combine_m8B2DF6BE8079E68942F69FD736B474339C199588 ();
// 0x000001EA System.Int32 UnityEngine.XR.ARSubsystems.HashCode::Combine(System.Int32,System.Int32,System.Int32,System.Int32)
extern void HashCode_Combine_m43F67900AD70583B21B1477145A5490CB3FD6DB2 ();
// 0x000001EB System.Int32 UnityEngine.XR.ARSubsystems.HashCode::Combine(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void HashCode_Combine_m7C576EEF662F6F46F0D8E59F304B21A3DDA3FFF5 ();
// 0x000001EC System.Int32 UnityEngine.XR.ARSubsystems.HashCode::Combine(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void HashCode_Combine_m015E8EDB3690FB78DCEE2E810749C9D37C10AD5E ();
// 0x000001ED System.Int32 UnityEngine.XR.ARSubsystems.HashCode::Combine(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void HashCode_Combine_mD5158D70E4D3C96633A68F4B1C417A357CA570FF ();
// 0x000001EE System.Int32 UnityEngine.XR.ARSubsystems.HashCode::Combine(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void HashCode_Combine_m9476B5F623B72DB1FBC776B0E37B79F052DFB8F1 ();
// 0x000001EF UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRHumanBody::get_trackableId()
extern void XRHumanBody_get_trackableId_m6932327AA835FDFFA3A8AC2C11C45E2491E998AA_AdjustorThunk ();
// 0x000001F0 System.Void UnityEngine.XR.ARSubsystems.XRHumanBody::set_trackableId(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XRHumanBody_set_trackableId_m9D91D37BF9C59410D9D15B428F009E448CE2E12A_AdjustorThunk ();
// 0x000001F1 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRHumanBody::get_pose()
extern void XRHumanBody_get_pose_m3E48843E383A32DF5ED22BFD89FB52C9C7AD1E5B_AdjustorThunk ();
// 0x000001F2 System.Void UnityEngine.XR.ARSubsystems.XRHumanBody::set_pose(UnityEngine.Pose)
extern void XRHumanBody_set_pose_m95CBE9AE820F6CA2C3AA06F8201B226BABF189D8_AdjustorThunk ();
// 0x000001F3 System.Single UnityEngine.XR.ARSubsystems.XRHumanBody::get_estimatedHeightScaleFactor()
extern void XRHumanBody_get_estimatedHeightScaleFactor_m42EC00C0BA5064C9F02DF146FBF06AF12F987E20_AdjustorThunk ();
// 0x000001F4 System.Void UnityEngine.XR.ARSubsystems.XRHumanBody::set_estimatedHeightScaleFactor(System.Single)
extern void XRHumanBody_set_estimatedHeightScaleFactor_m5CBA4FE2D7F35F03F6D5A2521F71F5760E4E47A2_AdjustorThunk ();
// 0x000001F5 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRHumanBody::get_trackingState()
extern void XRHumanBody_get_trackingState_m313FDD56C02437850F6BD0B1C4F735F1B06AC3CA_AdjustorThunk ();
// 0x000001F6 System.Void UnityEngine.XR.ARSubsystems.XRHumanBody::set_trackingState(UnityEngine.XR.ARSubsystems.TrackingState)
extern void XRHumanBody_set_trackingState_mC53B5E2436E49F115A3AF0BE79175C7FB8E6F496_AdjustorThunk ();
// 0x000001F7 System.IntPtr UnityEngine.XR.ARSubsystems.XRHumanBody::get_nativePtr()
extern void XRHumanBody_get_nativePtr_m322717DC0CFB9C7E2AFB064BCC2A233E99B713D8_AdjustorThunk ();
// 0x000001F8 System.Void UnityEngine.XR.ARSubsystems.XRHumanBody::set_nativePtr(System.IntPtr)
extern void XRHumanBody_set_nativePtr_m73E59C4802F71BD054295CB53587C50D28926B12_AdjustorThunk ();
// 0x000001F9 UnityEngine.XR.ARSubsystems.XRHumanBody UnityEngine.XR.ARSubsystems.XRHumanBody::get_defaultValue()
extern void XRHumanBody_get_defaultValue_m93BC6C94804B68DB3D5C99631A36E0BACACEF20E ();
// 0x000001FA System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBody::Equals(UnityEngine.XR.ARSubsystems.XRHumanBody)
extern void XRHumanBody_Equals_mCD4170D2041DF3892DD5185DBE2F78F154CA4F5E_AdjustorThunk ();
// 0x000001FB System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBody::Equals(System.Object)
extern void XRHumanBody_Equals_m0F6EB11CB87E2809A62F29438C47424676EB9601_AdjustorThunk ();
// 0x000001FC System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBody::op_Equality(UnityEngine.XR.ARSubsystems.XRHumanBody,UnityEngine.XR.ARSubsystems.XRHumanBody)
extern void XRHumanBody_op_Equality_mA345FE946AC61C9EEF4F959AFF0B7D0F2E0E38EA ();
// 0x000001FD System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBody::op_Inequality(UnityEngine.XR.ARSubsystems.XRHumanBody,UnityEngine.XR.ARSubsystems.XRHumanBody)
extern void XRHumanBody_op_Inequality_m5ECB9E3AA85696BA74A8BDDB2F9B152818C4DC43 ();
// 0x000001FE System.Int32 UnityEngine.XR.ARSubsystems.XRHumanBody::GetHashCode()
extern void XRHumanBody_GetHashCode_m88DD5A5B057659E907F3715926F9CFDC9B1F2E27_AdjustorThunk ();
// 0x000001FF System.Void UnityEngine.XR.ARSubsystems.XRHumanBody::.cctor()
extern void XRHumanBody__cctor_m8F0780BA6E9EADBA6D4C5250FAF95EBC80F67ED9 ();
// 0x00000200 System.Int32 UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::get_index()
extern void XRHumanBodyJoint_get_index_m2FF98E6DAD602BF81A289D8617423DABA5C9E1BE_AdjustorThunk ();
// 0x00000201 System.Int32 UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::get_parentIndex()
extern void XRHumanBodyJoint_get_parentIndex_m44F1F92C071C6D0C398826086906EBE196D10E5E_AdjustorThunk ();
// 0x00000202 UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::get_localScale()
extern void XRHumanBodyJoint_get_localScale_m8C6B243F80A342FAE843FEC72E0551CA35F94CEC_AdjustorThunk ();
// 0x00000203 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::get_localPose()
extern void XRHumanBodyJoint_get_localPose_m99D151B828994BCC94134CF7C1F99D197D701924_AdjustorThunk ();
// 0x00000204 UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::get_anchorScale()
extern void XRHumanBodyJoint_get_anchorScale_m93774112A48834FF193F64C20A932144AF5F29A4_AdjustorThunk ();
// 0x00000205 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::get_anchorPose()
extern void XRHumanBodyJoint_get_anchorPose_m68590EE4ECF7790861446409AC93D29728955491_AdjustorThunk ();
// 0x00000206 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::get_tracked()
extern void XRHumanBodyJoint_get_tracked_mE08747C37E23CE4041B1508F80CBAC759D1E463A_AdjustorThunk ();
// 0x00000207 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::.ctor(System.Int32,System.Int32,UnityEngine.Vector3,UnityEngine.Pose,UnityEngine.Vector3,UnityEngine.Pose,System.Boolean)
extern void XRHumanBodyJoint__ctor_m7DD8554A3DB1E94EAFF47584054D2156B9A8AC75_AdjustorThunk ();
// 0x00000208 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::Equals(UnityEngine.XR.ARSubsystems.XRHumanBodyJoint)
extern void XRHumanBodyJoint_Equals_m5B9B34C9174921998E796152C84C3B50DAD8403E_AdjustorThunk ();
// 0x00000209 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::Equals(System.Object)
extern void XRHumanBodyJoint_Equals_m300514EDD81E0CB83FFECCA87BD4F46283F72262_AdjustorThunk ();
// 0x0000020A System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::op_Equality(UnityEngine.XR.ARSubsystems.XRHumanBodyJoint,UnityEngine.XR.ARSubsystems.XRHumanBodyJoint)
extern void XRHumanBodyJoint_op_Equality_mCFE279F68500F5560EAFBAC78221137E30968A9B ();
// 0x0000020B System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::op_Inequality(UnityEngine.XR.ARSubsystems.XRHumanBodyJoint,UnityEngine.XR.ARSubsystems.XRHumanBodyJoint)
extern void XRHumanBodyJoint_op_Inequality_m9DF662438BD450D7AA85E4314EB114292EF3707F ();
// 0x0000020C System.Int32 UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::GetHashCode()
extern void XRHumanBodyJoint_GetHashCode_mACD20EC1EB6727E1CCF55520D5C1B3A89D1AFDFB_AdjustorThunk ();
// 0x0000020D System.String UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::ToString()
extern void XRHumanBodyJoint_ToString_mBABBE34A2903E956E30E312B54D77EFF25377D8B_AdjustorThunk ();
// 0x0000020E System.String UnityEngine.XR.ARSubsystems.XRHumanBodyJoint::ToString(System.String)
extern void XRHumanBodyJoint_ToString_m6BFD692ABEAA12E3E5A646857A1E820B2F3ADC11_AdjustorThunk ();
// 0x0000020F System.Int32 UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint::get_index()
extern void XRHumanBodyPose2DJoint_get_index_mD9FDCC24C68D17796A05B98EFF9CADF0246FD7EC_AdjustorThunk ();
// 0x00000210 System.Int32 UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint::get_parentIndex()
extern void XRHumanBodyPose2DJoint_get_parentIndex_m0C4A19B0FB8293184E647A0A8EC03A7022EF9FEA_AdjustorThunk ();
// 0x00000211 UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint::get_position()
extern void XRHumanBodyPose2DJoint_get_position_m0BCAA6689F1121EDFD38D334B926D4F54ECE605E_AdjustorThunk ();
// 0x00000212 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint::get_tracked()
extern void XRHumanBodyPose2DJoint_get_tracked_m111E89A5838439234E91BB8628DA8311B51FAB85_AdjustorThunk ();
// 0x00000213 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint::.ctor(System.Int32,System.Int32,UnityEngine.Vector2,System.Boolean)
extern void XRHumanBodyPose2DJoint__ctor_mD27CC086E2B589B0E2BFD7B95DDD331E73E83A5B_AdjustorThunk ();
// 0x00000214 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint::Equals(UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint)
extern void XRHumanBodyPose2DJoint_Equals_mEC56CED7627342B791C31D08FB59EECE6383B8A5_AdjustorThunk ();
// 0x00000215 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint::Equals(System.Object)
extern void XRHumanBodyPose2DJoint_Equals_m24B4310DA6326E8106C760B34B2FE1BABF3F0363_AdjustorThunk ();
// 0x00000216 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint::op_Equality(UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint,UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint)
extern void XRHumanBodyPose2DJoint_op_Equality_m10919647DEBA6C480E7CDD365619606690127006 ();
// 0x00000217 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint::op_Inequality(UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint,UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint)
extern void XRHumanBodyPose2DJoint_op_Inequality_m401A52CF6A7AD6C8A2FC7E57E4A586E1E9BB0BAC ();
// 0x00000218 System.Int32 UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint::GetHashCode()
extern void XRHumanBodyPose2DJoint_GetHashCode_m567FCBE39D79FE81B59DA5C379FAAFCBB0349C21_AdjustorThunk ();
// 0x00000219 System.String UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint::ToString()
extern void XRHumanBodyPose2DJoint_ToString_mE0D6AB26ECEE026021EB07111142AAC530A917B4_AdjustorThunk ();
// 0x0000021A System.String UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint::ToString(System.String)
extern void XRHumanBodyPose2DJoint_ToString_m401CCCD51037D42EEA6B6E79EE4621E716ECF529_AdjustorThunk ();
// 0x0000021B System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::get_pose2DRequested()
extern void XRHumanBodySubsystem_get_pose2DRequested_mDB41813111380F9D8195A1F5FE8B0DE55F06098B ();
// 0x0000021C System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::set_pose2DRequested(System.Boolean)
extern void XRHumanBodySubsystem_set_pose2DRequested_m3BF999A23600C191381ECDD6EB66373FDA46CD86 ();
// 0x0000021D System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::get_pose2DEnabled()
extern void XRHumanBodySubsystem_get_pose2DEnabled_m893C824E4AE179F04F45C754DD64404087A3E78B ();
// 0x0000021E System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::get_pose3DRequested()
extern void XRHumanBodySubsystem_get_pose3DRequested_mC73DEF74952ABB92741874CFA27AA7C1B7A9CB28 ();
// 0x0000021F System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::set_pose3DRequested(System.Boolean)
extern void XRHumanBodySubsystem_set_pose3DRequested_mB0DA2A9AAAD7297597F7817D7855F31F967D69EE ();
// 0x00000220 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::get_pose3DEnabled()
extern void XRHumanBodySubsystem_get_pose3DEnabled_m2EAD056DCD50B75F4E87A3AAED98ABA2AA403C7F ();
// 0x00000221 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::get_pose3DScaleEstimationRequested()
extern void XRHumanBodySubsystem_get_pose3DScaleEstimationRequested_mB109E27892E4EE7D82C20C31CD006FF75CEDE4E2 ();
// 0x00000222 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::set_pose3DScaleEstimationRequested(System.Boolean)
extern void XRHumanBodySubsystem_set_pose3DScaleEstimationRequested_m2CB0FC8D225000B64C84708A3C63E2931FE59D09 ();
// 0x00000223 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::get_pose3DScaleEstimationEnabled()
extern void XRHumanBodySubsystem_get_pose3DScaleEstimationEnabled_mEFB4713A2B4E308CD8B0A37D77A7D49482DC7E01 ();
// 0x00000224 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::.ctor()
extern void XRHumanBodySubsystem__ctor_m2AC16B13B82E955D9601CBDFE93D6316FEE22497 ();
// 0x00000225 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::OnStart()
extern void XRHumanBodySubsystem_OnStart_m54AAE040A476EE3857350E562E63DCE74D623D37 ();
// 0x00000226 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::OnStop()
extern void XRHumanBodySubsystem_OnStop_mC566D8BD9E4A61C4CBBC604681E56771211DCC5D ();
// 0x00000227 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::OnDestroyed()
extern void XRHumanBodySubsystem_OnDestroyed_m89C8064BB65A1BBE3E72FEE8C92393022A7D547A ();
// 0x00000228 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody> UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRHumanBodySubsystem_GetChanges_mDD19CA22B2DCD5F318F24A80232FF251716B67C8 ();
// 0x00000229 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::GetSkeleton(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBodyJoint>&)
extern void XRHumanBodySubsystem_GetSkeleton_mB6924CE63569B2151553227515A51F241C3581C1 ();
// 0x0000022A Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint> UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::GetHumanBodyPose2DJoints(Unity.Collections.Allocator)
extern void XRHumanBodySubsystem_GetHumanBodyPose2DJoints_m2CC03CB9773A7992B394080F3D97D80A3287F96C ();
// 0x0000022B UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::CreateProvider()
// 0x0000022C System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem::Register(UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo)
extern void XRHumanBodySubsystem_Register_m7B79583DD1CBF6AD5ABECC95B63E001E4FE82342 ();
// 0x0000022D System.String UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::get_id()
extern void XRHumanBodySubsystemCinfo_get_id_m2351F8702B67461C381E99A2BEBB44E6E70C656C_AdjustorThunk ();
// 0x0000022E System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::set_id(System.String)
extern void XRHumanBodySubsystemCinfo_set_id_m2F5A213DA264A18243B72F18268A963780C599A0_AdjustorThunk ();
// 0x0000022F System.Type UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::get_implementationType()
extern void XRHumanBodySubsystemCinfo_get_implementationType_m801524FD9F24E9A4E4DE6A442313F957F17B43F3_AdjustorThunk ();
// 0x00000230 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::set_implementationType(System.Type)
extern void XRHumanBodySubsystemCinfo_set_implementationType_mF7AEC0C8173DD69CB5C99EDB5C3771B1C8416BE3_AdjustorThunk ();
// 0x00000231 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::get_supportsHumanBody2D()
extern void XRHumanBodySubsystemCinfo_get_supportsHumanBody2D_m3A1954180F31774465457140AED37F85D5530AEC_AdjustorThunk ();
// 0x00000232 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::set_supportsHumanBody2D(System.Boolean)
extern void XRHumanBodySubsystemCinfo_set_supportsHumanBody2D_m0FB82F19C615B7912E7D07B9B7BE06FF9E8661E5_AdjustorThunk ();
// 0x00000233 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::get_supportsHumanBody3D()
extern void XRHumanBodySubsystemCinfo_get_supportsHumanBody3D_m4C2A07E0A796F98705E3E7B5187F4E24EA06744F_AdjustorThunk ();
// 0x00000234 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::set_supportsHumanBody3D(System.Boolean)
extern void XRHumanBodySubsystemCinfo_set_supportsHumanBody3D_mB9FE42BA975ABA23027E454E52E89B0CE44C814F_AdjustorThunk ();
// 0x00000235 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::get_supportsHumanBody3DScaleEstimation()
extern void XRHumanBodySubsystemCinfo_get_supportsHumanBody3DScaleEstimation_mBBA1279F9F6108A7C64F4C2828C9522585EA22E7_AdjustorThunk ();
// 0x00000236 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::set_supportsHumanBody3DScaleEstimation(System.Boolean)
extern void XRHumanBodySubsystemCinfo_set_supportsHumanBody3DScaleEstimation_mE16BC101154FE97C200A79196CACD9176246928A_AdjustorThunk ();
// 0x00000237 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::Equals(UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo)
extern void XRHumanBodySubsystemCinfo_Equals_m57792C7AE10CE3DA0E80743AFF2DA808D308839B_AdjustorThunk ();
// 0x00000238 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::Equals(System.Object)
extern void XRHumanBodySubsystemCinfo_Equals_m491D4271ACD9CF310EC5E9B6886F68B1A0E24D47_AdjustorThunk ();
// 0x00000239 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo,UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo)
extern void XRHumanBodySubsystemCinfo_op_Equality_mD23BCE60F13DECC8FF29F4A7E60963A367BA5B46 ();
// 0x0000023A System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo,UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo)
extern void XRHumanBodySubsystemCinfo_op_Inequality_mB943F987D91BFF9760FA1AC4CD47EC78F745F663 ();
// 0x0000023B System.Int32 UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo::GetHashCode()
extern void XRHumanBodySubsystemCinfo_GetHashCode_m7B445703AA6EA9C3CE69B45494F5443A5EA4C7C1_AdjustorThunk ();
// 0x0000023C System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo)
extern void XRHumanBodySubsystemDescriptor__ctor_m93C7493D80D6F74451A6CB6ECEFEF0EF3CC4A781 ();
// 0x0000023D System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor::get_supportsHumanBody2D()
extern void XRHumanBodySubsystemDescriptor_get_supportsHumanBody2D_mC77D7E93FC0A45CB78FADA03690D74EFE6A220B0 ();
// 0x0000023E System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor::set_supportsHumanBody2D(System.Boolean)
extern void XRHumanBodySubsystemDescriptor_set_supportsHumanBody2D_m3547E6D95ADA18366B57CCAB05D1F69A47F18457 ();
// 0x0000023F System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor::get_supportsHumanBody3D()
extern void XRHumanBodySubsystemDescriptor_get_supportsHumanBody3D_m1B38E725F577DF2B1A7113C21757A27C0FD3FCC1 ();
// 0x00000240 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor::set_supportsHumanBody3D(System.Boolean)
extern void XRHumanBodySubsystemDescriptor_set_supportsHumanBody3D_m29383BA5B2AE8244ED5E2504887909AAFD61F5E0 ();
// 0x00000241 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor::get_supportsHumanBody3DScaleEstimation()
extern void XRHumanBodySubsystemDescriptor_get_supportsHumanBody3DScaleEstimation_m2A321A79DC0F16972CC81576BC0B62F31CF9F395 ();
// 0x00000242 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor::set_supportsHumanBody3DScaleEstimation(System.Boolean)
extern void XRHumanBodySubsystemDescriptor_set_supportsHumanBody3DScaleEstimation_m658F556331D0FF423A9CD3765DC477A494091B9C ();
// 0x00000243 UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemCinfo)
extern void XRHumanBodySubsystemDescriptor_Create_m2E42215B4BA4999BAEBEE9C1CF30615B3936C4EF ();
// 0x00000244 System.Int32 UnityEngine.XR.ARSubsystems.IReferenceImageLibrary::get_count()
// 0x00000245 UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARSubsystems.IReferenceImageLibrary::get_Item(System.Int32)
// 0x00000246 Unity.Jobs.JobHandle UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::ScheduleAddImageJobImpl(Unity.Collections.NativeSlice`1<System.Byte>,UnityEngine.Vector2Int,UnityEngine.TextureFormat,UnityEngine.XR.ARSubsystems.XRReferenceImage,Unity.Jobs.JobHandle)
// 0x00000247 Unity.Jobs.JobHandle UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::ScheduleAddImageJob(Unity.Collections.NativeSlice`1<System.Byte>,UnityEngine.Vector2Int,UnityEngine.TextureFormat,UnityEngine.XR.ARSubsystems.XRReferenceImage,Unity.Jobs.JobHandle)
extern void MutableRuntimeReferenceImageLibrary_ScheduleAddImageJob_mA55DD943A67604A04A45611E1C508C45212178E3 ();
// 0x00000248 System.Int32 UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::get_supportedTextureFormatCount()
// 0x00000249 UnityEngine.TextureFormat UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::GetSupportedTextureFormatAt(System.Int32)
extern void MutableRuntimeReferenceImageLibrary_GetSupportedTextureFormatAt_mD0BD6E3C3B979C5946BB4E2B8F30B57511602DF5 ();
// 0x0000024A UnityEngine.TextureFormat UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::GetSupportedTextureFormatAtImpl(System.Int32)
// 0x0000024B System.Boolean UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::IsTextureFormatSupported(UnityEngine.TextureFormat)
extern void MutableRuntimeReferenceImageLibrary_IsTextureFormatSupported_m196AF8CC46F8D19600F1D02E5EC7F2F73DA745AA ();
// 0x0000024C UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::GetEnumerator()
extern void MutableRuntimeReferenceImageLibrary_GetEnumerator_mF0AD39CFBE88F86EC1F8D6D905C8AAB4E22F6349 ();
// 0x0000024D UnityEngine.XR.ARSubsystems.SerializableGuid UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::GenerateNewGuid()
extern void MutableRuntimeReferenceImageLibrary_GenerateNewGuid_m9E2A885618C9EDD289C6891D181B6BF8002C6732 ();
// 0x0000024E System.Void UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary::.ctor()
extern void MutableRuntimeReferenceImageLibrary__ctor_m6C12CDC2B47B9C510CC95F349BF1403A256B41D5 ();
// 0x0000024F UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary::get_Item(System.Int32)
extern void RuntimeReferenceImageLibrary_get_Item_mFC00AA9544BAF5762E53450DB26CE3B950E135F2 ();
// 0x00000250 System.Int32 UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary::get_count()
// 0x00000251 UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary::GetReferenceImageAt(System.Int32)
// 0x00000252 System.Void UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary::.ctor()
extern void RuntimeReferenceImageLibrary__ctor_m5D95CE2FFF53CB65745F1273F6C24A804C4DF5E9 ();
// 0x00000253 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::.ctor()
extern void XRImageTrackingSubsystem__ctor_m32B2EFB936DCAB4D5B7DC15C0A993C1F1736A4EF ();
// 0x00000254 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::OnStart()
extern void XRImageTrackingSubsystem_OnStart_m171D82E548A07407640CF28589ACAA4EE6066209 ();
// 0x00000255 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::OnStop()
extern void XRImageTrackingSubsystem_OnStop_m297891ECD05100E676DDD27FAEA4388DBF2C4521 ();
// 0x00000256 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::OnDestroyed()
extern void XRImageTrackingSubsystem_OnDestroyed_mDBA0F73C00C6790E6DDF2E060F5E26CE470F18BC ();
// 0x00000257 UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::get_imageLibrary()
extern void XRImageTrackingSubsystem_get_imageLibrary_mCFF108F9559826539F0EEC73BA0092F7544537B8 ();
// 0x00000258 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::set_imageLibrary(UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary)
extern void XRImageTrackingSubsystem_set_imageLibrary_m02F82B9AA311E96B2EFEF1808E18E881832008EE ();
// 0x00000259 UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::CreateRuntimeLibrary(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
extern void XRImageTrackingSubsystem_CreateRuntimeLibrary_m26FE1D40056DEED2BE3D7A090B375184076792AE ();
// 0x0000025A UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage> UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRImageTrackingSubsystem_GetChanges_m84DB25AC8DB44AE84050A755C823960BABC2CFA0 ();
// 0x0000025B System.Int32 UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::get_requestedMaxNumberOfMovingImages()
extern void XRImageTrackingSubsystem_get_requestedMaxNumberOfMovingImages_mA7FD5D9BF9D702AF6A97F8BA75189403012612BB ();
// 0x0000025C System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::set_requestedMaxNumberOfMovingImages(System.Int32)
extern void XRImageTrackingSubsystem_set_requestedMaxNumberOfMovingImages_mFFF6F4BA02B92B7A68E324C78746DA41E5C93F3B ();
// 0x0000025D System.Int32 UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::get_currentMaxNumberOfMovingImages()
extern void XRImageTrackingSubsystem_get_currentMaxNumberOfMovingImages_m337416CE69762F2494BA01E4ECED7826809F6FE7 ();
// 0x0000025E UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::CreateProvider()
// 0x0000025F System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::get_supportsMovingImages()
extern void XRImageTrackingSubsystemDescriptor_get_supportsMovingImages_m7B4BB427E6EBD7D250ED4F8C523DB9E5B9EFD293 ();
// 0x00000260 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::set_supportsMovingImages(System.Boolean)
extern void XRImageTrackingSubsystemDescriptor_set_supportsMovingImages_m49B9418C9FD793F56A7137D7F5F15835493D2676 ();
// 0x00000261 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::get_requiresPhysicalImageDimensions()
extern void XRImageTrackingSubsystemDescriptor_get_requiresPhysicalImageDimensions_mA8297F529194125C38FA13B46C83E04C0B786599 ();
// 0x00000262 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::set_requiresPhysicalImageDimensions(System.Boolean)
extern void XRImageTrackingSubsystemDescriptor_set_requiresPhysicalImageDimensions_m2FA9112497A113B4D27FA66EBE1FD506260BB763 ();
// 0x00000263 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::get_supportsMutableLibrary()
extern void XRImageTrackingSubsystemDescriptor_get_supportsMutableLibrary_m5533B99BFE6C3887A3308628413326C791F28212 ();
// 0x00000264 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::set_supportsMutableLibrary(System.Boolean)
extern void XRImageTrackingSubsystemDescriptor_set_supportsMutableLibrary_mB25E82BDAFDB47FE8DBEA218309593AD51401E3F ();
// 0x00000265 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo)
extern void XRImageTrackingSubsystemDescriptor_Create_mADB63232DFEE95AA44F52496B7289C8469E118B9 ();
// 0x00000266 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo)
extern void XRImageTrackingSubsystemDescriptor__ctor_m5D86FBB12C3EA031B9DEC71C6A2CC48F4A176711 ();
// 0x00000267 System.Void UnityEngine.XR.ARSubsystems.XRReferenceImage::.ctor(UnityEngine.XR.ARSubsystems.SerializableGuid,UnityEngine.XR.ARSubsystems.SerializableGuid,System.Nullable`1<UnityEngine.Vector2>,System.String,UnityEngine.Texture2D)
extern void XRReferenceImage__ctor_m6B1ECFC5354FC9FDC73635BF5E693DA33DE02A4B_AdjustorThunk ();
// 0x00000268 System.Guid UnityEngine.XR.ARSubsystems.XRReferenceImage::get_guid()
extern void XRReferenceImage_get_guid_mEFF96705B63F80C7C38125D170F7E62B784AEED2_AdjustorThunk ();
// 0x00000269 System.Guid UnityEngine.XR.ARSubsystems.XRReferenceImage::get_textureGuid()
extern void XRReferenceImage_get_textureGuid_m818FF686F0FAB8AE85630A62E3FA74C2C81C2AC0_AdjustorThunk ();
// 0x0000026A System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::get_specifySize()
extern void XRReferenceImage_get_specifySize_m5792CAE7DC7ADB8591E770B1F32D71BCCE9F0597_AdjustorThunk ();
// 0x0000026B UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRReferenceImage::get_size()
extern void XRReferenceImage_get_size_m29A6DA526141F214BE2949524305EFE91C07FA32_AdjustorThunk ();
// 0x0000026C System.Single UnityEngine.XR.ARSubsystems.XRReferenceImage::get_width()
extern void XRReferenceImage_get_width_m52157ED5E292633BA8BA0CFE7F97A756B6985DB8_AdjustorThunk ();
// 0x0000026D System.Single UnityEngine.XR.ARSubsystems.XRReferenceImage::get_height()
extern void XRReferenceImage_get_height_mA55A287CAA626B1817465DA78D946D3EB82E0D8D_AdjustorThunk ();
// 0x0000026E System.String UnityEngine.XR.ARSubsystems.XRReferenceImage::get_name()
extern void XRReferenceImage_get_name_m6DD83ECED755444645A14816918747E2EEAE92A9_AdjustorThunk ();
// 0x0000026F UnityEngine.Texture2D UnityEngine.XR.ARSubsystems.XRReferenceImage::get_texture()
extern void XRReferenceImage_get_texture_m97887B57DD747DCE051484D1C97F1240B673FE16_AdjustorThunk ();
// 0x00000270 System.String UnityEngine.XR.ARSubsystems.XRReferenceImage::ToString()
extern void XRReferenceImage_ToString_m37745D8B95903B29F917CE6E0A141E79E6F4B937_AdjustorThunk ();
// 0x00000271 System.Int32 UnityEngine.XR.ARSubsystems.XRReferenceImage::GetHashCode()
extern void XRReferenceImage_GetHashCode_mFBF4D8E0A33B3EFDCB6D6E5A944AB4CF52AAB334_AdjustorThunk ();
// 0x00000272 System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::Equals(System.Object)
extern void XRReferenceImage_Equals_m90DF560ECB15F4363E40AF3C3A2F82F5E0FD147D_AdjustorThunk ();
// 0x00000273 System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::Equals(UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void XRReferenceImage_Equals_m5FBBB7CFF96645894AA08221795ABC2A98E4DEF5_AdjustorThunk ();
// 0x00000274 System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::op_Equality(UnityEngine.XR.ARSubsystems.XRReferenceImage,UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void XRReferenceImage_op_Equality_mD0355E3DAD36502F8CF26DDDCCBFB5440D41A171 ();
// 0x00000275 System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::op_Inequality(UnityEngine.XR.ARSubsystems.XRReferenceImage,UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void XRReferenceImage_op_Inequality_mDDDE4B2F7C0270E4FE4837E693B36C72892C4297 ();
// 0x00000276 System.Int32 UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::get_count()
extern void XRReferenceImageLibrary_get_count_m031E8D7C22B586EE44B0256912F688867E9DEB84 ();
// 0x00000277 System.Collections.Generic.List`1_Enumerator<UnityEngine.XR.ARSubsystems.XRReferenceImage> UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::GetEnumerator()
extern void XRReferenceImageLibrary_GetEnumerator_m7F1F01CC0BD4EF373F5B3766D2D1476901C4DCFD ();
// 0x00000278 UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::get_Item(System.Int32)
extern void XRReferenceImageLibrary_get_Item_m9FE52A96359701129EC85E3AFB382E08E7E3B799 ();
// 0x00000279 System.Int32 UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::indexOf(UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void XRReferenceImageLibrary_indexOf_m6E722FBAD970A7FB4C9415EDF6F46BAA25B89116 ();
// 0x0000027A System.Guid UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::get_guid()
extern void XRReferenceImageLibrary_get_guid_mAE3BC056A0B6817FD14E09D150B561CB468EFCDC ();
// 0x0000027B System.Void UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary::.ctor()
extern void XRReferenceImageLibrary__ctor_m783C31BF895E269078A6F82966CE008024A5450D ();
// 0x0000027C System.Void UnityEngine.XR.ARSubsystems.XRTrackedImage::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,System.Guid,UnityEngine.Pose,UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr)
extern void XRTrackedImage__ctor_mF31D86D7A523FD9EE7F4166A9ABB04272E93436B_AdjustorThunk ();
// 0x0000027D UnityEngine.XR.ARSubsystems.XRTrackedImage UnityEngine.XR.ARSubsystems.XRTrackedImage::get_defaultValue()
extern void XRTrackedImage_get_defaultValue_mC27C0C8BAC99DFBD1900C92FBA0D4940D86468EE ();
// 0x0000027E UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRTrackedImage::get_trackableId()
extern void XRTrackedImage_get_trackableId_m6EB6DBACC95E5EE2AFEE3CE421F4C123F32E9CB8_AdjustorThunk ();
// 0x0000027F System.Guid UnityEngine.XR.ARSubsystems.XRTrackedImage::get_sourceImageId()
extern void XRTrackedImage_get_sourceImageId_m402089FA779BB9821B50B23F79579466D895939B_AdjustorThunk ();
// 0x00000280 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRTrackedImage::get_pose()
extern void XRTrackedImage_get_pose_m0566E087CA2DC99DF749E80277510C61DCF13186_AdjustorThunk ();
// 0x00000281 UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRTrackedImage::get_size()
extern void XRTrackedImage_get_size_m746034D0E2FD28C9E48A90965E4FCD9137988906_AdjustorThunk ();
// 0x00000282 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRTrackedImage::get_trackingState()
extern void XRTrackedImage_get_trackingState_mA7177B042E8F9F9B584582970BC5FF0377CE94DB_AdjustorThunk ();
// 0x00000283 System.IntPtr UnityEngine.XR.ARSubsystems.XRTrackedImage::get_nativePtr()
extern void XRTrackedImage_get_nativePtr_mB44BA43B02762B89091D56F254221F0741808629_AdjustorThunk ();
// 0x00000284 System.Int32 UnityEngine.XR.ARSubsystems.XRTrackedImage::GetHashCode()
extern void XRTrackedImage_GetHashCode_mC1A5AB6C756498852952CB1B9F4F69D1177A02A6_AdjustorThunk ();
// 0x00000285 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedImage::Equals(UnityEngine.XR.ARSubsystems.XRTrackedImage)
extern void XRTrackedImage_Equals_m12A588942242306FC770FD88421D00750F22A141_AdjustorThunk ();
// 0x00000286 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedImage::Equals(System.Object)
extern void XRTrackedImage_Equals_m7C7F0B2FC7A6818276C2BC763CF0465333453B9C_AdjustorThunk ();
// 0x00000287 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedImage::op_Equality(UnityEngine.XR.ARSubsystems.XRTrackedImage,UnityEngine.XR.ARSubsystems.XRTrackedImage)
extern void XRTrackedImage_op_Equality_m8C52E2C73BB01445DA64A954189A25E1C6B162AA ();
// 0x00000288 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedImage::op_Inequality(UnityEngine.XR.ARSubsystems.XRTrackedImage,UnityEngine.XR.ARSubsystems.XRTrackedImage)
extern void XRTrackedImage_op_Inequality_mDC56A7B7605F26C5D1D049FE1D68D4463155F847 ();
// 0x00000289 System.Void UnityEngine.XR.ARSubsystems.XRTrackedImage::.cctor()
extern void XRTrackedImage__cctor_m4D42652FA025B44DA4EEAF27F15B77E11DAF4614 ();
// 0x0000028A Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.NativeCopyUtility::PtrToNativeArrayWithDefault(T,System.Void*,System.Int32,System.Int32,Unity.Collections.Allocator)
// 0x0000028B System.Void UnityEngine.XR.ARSubsystems.NativeCopyUtility::FillArrayWithValue(Unity.Collections.NativeArray`1<T>,T)
// 0x0000028C Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.NativeCopyUtility::CreateArrayFilledWithValue(T,System.Int32,Unity.Collections.Allocator)
// 0x0000028D UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem_Provider UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem::CreateProvider()
// 0x0000028E System.Void UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem::.ctor()
extern void XRObjectTrackingSubsystem__ctor_m76F90C34C8D522DB4C6F2943BF5B74076DFC464A ();
// 0x0000028F System.Void UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem::OnStart()
extern void XRObjectTrackingSubsystem_OnStart_m2CE4CCEBC38F4E78EFDCC5E5E1A42B31BC1877DA ();
// 0x00000290 UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem::get_library()
extern void XRObjectTrackingSubsystem_get_library_m475F269C1D4EEA78B1415CE53B705EEC59973DBD ();
// 0x00000291 System.Void UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem::set_library(UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary)
extern void XRObjectTrackingSubsystem_set_library_mE75E8CCF9DF2E39914DF5BE5D26F2A07C01637EF ();
// 0x00000292 System.Void UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem::OnDestroyed()
extern void XRObjectTrackingSubsystem_OnDestroyed_m4EE74EF86BC5E6596F0BD15C2750F93A04355069 ();
// 0x00000293 System.Void UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem::OnStop()
extern void XRObjectTrackingSubsystem_OnStop_m05729B1B5EBBF4F8338DC755819BA21D7F1DFB16 ();
// 0x00000294 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject> UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRObjectTrackingSubsystem_GetChanges_m8FCC51540E8197A8B79F50CC2E9B665B7D3FF205 ();
// 0x00000295 System.Void UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem::Register(System.String,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities)
// 0x00000296 UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor::get_capabilities()
extern void XRObjectTrackingSubsystemDescriptor_get_capabilities_m76851EA2A5BC61D9B216AC74F2E95BF26748BD77 ();
// 0x00000297 System.Void UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor::set_capabilities(UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities)
extern void XRObjectTrackingSubsystemDescriptor_set_capabilities_m5F8A4A904B5B3FCB93E512CD5C03261672EE9462 ();
// 0x00000298 System.Void UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor::.ctor(System.String,System.Type,System.Type,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities)
extern void XRObjectTrackingSubsystemDescriptor__ctor_m20F2C0E95938A87ABE2AD12DE31D4C70D1C5EC12 ();
// 0x00000299 System.String UnityEngine.XR.ARSubsystems.XRReferenceObject::get_name()
extern void XRReferenceObject_get_name_mB9D4C5BF34D4FF064180412CCE6585867BA98718_AdjustorThunk ();
// 0x0000029A System.Guid UnityEngine.XR.ARSubsystems.XRReferenceObject::get_guid()
extern void XRReferenceObject_get_guid_mA7BD0F3F54EABC39D19355113087CD4DFF94BE57_AdjustorThunk ();
// 0x0000029B T UnityEngine.XR.ARSubsystems.XRReferenceObject::FindEntry()
// 0x0000029C UnityEngine.XR.ARSubsystems.XRReferenceObjectEntry UnityEngine.XR.ARSubsystems.XRReferenceObject::FindEntry(System.Type)
extern void XRReferenceObject_FindEntry_mBCEBCEF4265B7D210FFA15179493BF8BDBB70C94_AdjustorThunk ();
// 0x0000029D System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceObject::Equals(UnityEngine.XR.ARSubsystems.XRReferenceObject)
extern void XRReferenceObject_Equals_m504798221B2E8A72005FD241A5C5E2A063FF37A2_AdjustorThunk ();
// 0x0000029E System.Int32 UnityEngine.XR.ARSubsystems.XRReferenceObject::GetHashCode()
extern void XRReferenceObject_GetHashCode_mE918BE08147EE066AF8CC5971E4F5A9221881678_AdjustorThunk ();
// 0x0000029F System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceObject::Equals(System.Object)
extern void XRReferenceObject_Equals_mC83DEBBA89CBF9EF334B79634C9CA099B166DBC9_AdjustorThunk ();
// 0x000002A0 System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceObject::op_Equality(UnityEngine.XR.ARSubsystems.XRReferenceObject,UnityEngine.XR.ARSubsystems.XRReferenceObject)
extern void XRReferenceObject_op_Equality_mA352611067E59B117AB607BAE3CD182706C462D0 ();
// 0x000002A1 System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceObject::op_Inequality(UnityEngine.XR.ARSubsystems.XRReferenceObject,UnityEngine.XR.ARSubsystems.XRReferenceObject)
extern void XRReferenceObject_op_Inequality_m14C3E4A6DDD3BD64A168898642FAC1741D542155 ();
// 0x000002A2 System.Void UnityEngine.XR.ARSubsystems.XRReferenceObjectEntry::.ctor()
extern void XRReferenceObjectEntry__ctor_mB617B601F6FA34FA4DAB5E9AE50925FCBFB1B6FE ();
// 0x000002A3 System.Int32 UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary::get_count()
extern void XRReferenceObjectLibrary_get_count_mAB2D34091CFC65B152FD3FD7095480FF5D6CC99C ();
// 0x000002A4 System.Collections.Generic.List`1_Enumerator<UnityEngine.XR.ARSubsystems.XRReferenceObject> UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary::GetEnumerator()
extern void XRReferenceObjectLibrary_GetEnumerator_m7ECEFBA77B4669ADD3EDCE0841E4FF9C79C3312D ();
// 0x000002A5 UnityEngine.XR.ARSubsystems.XRReferenceObject UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary::get_Item(System.Int32)
extern void XRReferenceObjectLibrary_get_Item_m09F2CB2A14D830F37053B8B6E02E9659F3527996 ();
// 0x000002A6 System.Guid UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary::get_guid()
extern void XRReferenceObjectLibrary_get_guid_mC10138A0A17DF18E104F14079063BA1C12A3DF87 ();
// 0x000002A7 System.Int32 UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary::indexOf(UnityEngine.XR.ARSubsystems.XRReferenceObject)
extern void XRReferenceObjectLibrary_indexOf_mBF56521C68737F5FBBA77684AD76A78E19C5B1F0 ();
// 0x000002A8 System.Void UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary::.ctor()
extern void XRReferenceObjectLibrary__ctor_mA11977E1ED613E7E7EF79C3F2494E243F619C442 ();
// 0x000002A9 UnityEngine.XR.ARSubsystems.XRTrackedObject UnityEngine.XR.ARSubsystems.XRTrackedObject::get_defaultValue()
extern void XRTrackedObject_get_defaultValue_m4623361129019EE5722A95C580171705EA1F3901 ();
// 0x000002AA UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRTrackedObject::get_trackableId()
extern void XRTrackedObject_get_trackableId_mB720981791DE599B20879640517A33BE2FE2D84D_AdjustorThunk ();
// 0x000002AB UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRTrackedObject::get_pose()
extern void XRTrackedObject_get_pose_mF865EAF61AE8767D6A0CCF59494A51F2D670F603_AdjustorThunk ();
// 0x000002AC UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRTrackedObject::get_trackingState()
extern void XRTrackedObject_get_trackingState_m0BD1D36132D7B57151A4CAE07B94238B2AEF3DED_AdjustorThunk ();
// 0x000002AD System.IntPtr UnityEngine.XR.ARSubsystems.XRTrackedObject::get_nativePtr()
extern void XRTrackedObject_get_nativePtr_mD654B09F24E79E99FA2A6B1A95C4EAFDF09C639F_AdjustorThunk ();
// 0x000002AE System.Guid UnityEngine.XR.ARSubsystems.XRTrackedObject::get_referenceObjectGuid()
extern void XRTrackedObject_get_referenceObjectGuid_m09514BB6AD9782AF342076F85BB28631C458BDC8_AdjustorThunk ();
// 0x000002AF System.Void UnityEngine.XR.ARSubsystems.XRTrackedObject::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr,System.Guid)
extern void XRTrackedObject__ctor_m81B6436D0E3BA4E73E1445074972DB81E3D27275_AdjustorThunk ();
// 0x000002B0 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedObject::Equals(System.Object)
extern void XRTrackedObject_Equals_mF0CA07E970C48D514F2B9BBEC0FE44F46429C524_AdjustorThunk ();
// 0x000002B1 System.Int32 UnityEngine.XR.ARSubsystems.XRTrackedObject::GetHashCode()
extern void XRTrackedObject_GetHashCode_m2F1509AA89026BB34BFFE2C07529AAB3B5B0A429_AdjustorThunk ();
// 0x000002B2 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedObject::op_Equality(UnityEngine.XR.ARSubsystems.XRTrackedObject,UnityEngine.XR.ARSubsystems.XRTrackedObject)
extern void XRTrackedObject_op_Equality_m06220676F2AB319883E5895019E7010622DE9583 ();
// 0x000002B3 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedObject::op_Inequality(UnityEngine.XR.ARSubsystems.XRTrackedObject,UnityEngine.XR.ARSubsystems.XRTrackedObject)
extern void XRTrackedObject_op_Inequality_m05EF7C266FC336DCCA28A984954021CE67818E40 ();
// 0x000002B4 System.Boolean UnityEngine.XR.ARSubsystems.XRTrackedObject::Equals(UnityEngine.XR.ARSubsystems.XRTrackedObject)
extern void XRTrackedObject_Equals_m925ED652F271F772E282C3621290411A259CBEEE_AdjustorThunk ();
// 0x000002B5 System.Void UnityEngine.XR.ARSubsystems.XRTrackedObject::.cctor()
extern void XRTrackedObject__cctor_mF6797A036790C2B6133B8B8A44C64B49FDBFF296 ();
// 0x000002B6 System.Boolean UnityEngine.XR.ARSubsystems.SegmentationDepthModeExtension::Enabled(UnityEngine.XR.ARSubsystems.HumanSegmentationDepthMode)
extern void SegmentationDepthModeExtension_Enabled_mC45FEDDFD11308BA5CFB5C2240A88635895E9187 ();
// 0x000002B7 System.Boolean UnityEngine.XR.ARSubsystems.SegmentationStencilModeExtension::Enabled(UnityEngine.XR.ARSubsystems.HumanSegmentationStencilMode)
extern void SegmentationStencilModeExtension_Enabled_mA5776A4254B52494C3EE2307E50B98CFE3AD84E9 ();
// 0x000002B8 UnityEngine.XR.ARSubsystems.HumanSegmentationStencilMode UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::get_requestedHumanStencilMode()
extern void XROcclusionSubsystem_get_requestedHumanStencilMode_mAD9070FFC7B5D0AFD32F66D1E19781000EA4FBE5 ();
// 0x000002B9 System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::set_requestedHumanStencilMode(UnityEngine.XR.ARSubsystems.HumanSegmentationStencilMode)
extern void XROcclusionSubsystem_set_requestedHumanStencilMode_m78A1E8C64FA3FE165307CB4F01D79A78BE28D240 ();
// 0x000002BA UnityEngine.XR.ARSubsystems.HumanSegmentationStencilMode UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::get_currentHumanStencilMode()
extern void XROcclusionSubsystem_get_currentHumanStencilMode_m9849AFC9E475F1FAD6E72C470ADB280BD07B6D7C ();
// 0x000002BB UnityEngine.XR.ARSubsystems.HumanSegmentationDepthMode UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::get_requestedHumanDepthMode()
extern void XROcclusionSubsystem_get_requestedHumanDepthMode_mFCE0752D4D52962C5661FE062875E2651B553749 ();
// 0x000002BC System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::set_requestedHumanDepthMode(UnityEngine.XR.ARSubsystems.HumanSegmentationDepthMode)
extern void XROcclusionSubsystem_set_requestedHumanDepthMode_mB6EF9CA558CACAF18BEF9C5B314BAE5E3E8C96AD ();
// 0x000002BD UnityEngine.XR.ARSubsystems.HumanSegmentationDepthMode UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::get_currentHumanDepthMode()
extern void XROcclusionSubsystem_get_currentHumanDepthMode_mB0AEA01C806ABCECFEE120CE1C55266069F9AE93 ();
// 0x000002BE System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::.ctor()
extern void XROcclusionSubsystem__ctor_m45621B004D781EE9D13D4019637E8F40066F3635 ();
// 0x000002BF System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::OnStart()
extern void XROcclusionSubsystem_OnStart_mA0B7A6DA08D403017F81E20C618C480619243AFB ();
// 0x000002C0 System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::OnStop()
extern void XROcclusionSubsystem_OnStop_m44A69643260CD3780DC4FA320EB9FD504BF63D67 ();
// 0x000002C1 System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::OnDestroyed()
extern void XROcclusionSubsystem_OnDestroyed_mBACD7ABA75543E8CE322990B7A824AD231E42F30 ();
// 0x000002C2 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::TryGetHumanStencil(UnityEngine.XR.ARSubsystems.XRTextureDescriptor&)
extern void XROcclusionSubsystem_TryGetHumanStencil_mD77A6EC8BAABEEBCB9A7A94595D8100BF25653EF ();
// 0x000002C3 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::TryAcquireHumanStencilCpuImage(UnityEngine.XR.ARSubsystems.XRCpuImage&)
extern void XROcclusionSubsystem_TryAcquireHumanStencilCpuImage_m43ADAED8657329954D85AA2B27052D09F5A79578 ();
// 0x000002C4 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::TryAcquireHumanDepthCpuImage(UnityEngine.XR.ARSubsystems.XRCpuImage&)
extern void XROcclusionSubsystem_TryAcquireHumanDepthCpuImage_mFA2D132F84E9EC2E1EA7DFD9948BAC4ED57F8175 ();
// 0x000002C5 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::TryGetHumanDepth(UnityEngine.XR.ARSubsystems.XRTextureDescriptor&)
extern void XROcclusionSubsystem_TryGetHumanDepth_mCD193E94A35335AA62E4A59E45DDF2FE1C32BC17 ();
// 0x000002C6 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTextureDescriptor> UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::GetTextureDescriptors(Unity.Collections.Allocator)
extern void XROcclusionSubsystem_GetTextureDescriptors_mCA503BE6370F557B008A9B00BA3A2AB52CB91321 ();
// 0x000002C7 System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::GetMaterialKeywords(System.Collections.Generic.List`1<System.String>&,System.Collections.Generic.List`1<System.String>&)
extern void XROcclusionSubsystem_GetMaterialKeywords_mDD300FD3B92701C39A5B55737737A23C9687AB4B ();
// 0x000002C8 UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::CreateProvider()
// 0x000002C9 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystem::Register(UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo)
extern void XROcclusionSubsystem_Register_mB185B88C9B33B36B35798EBB7BAA81298C32653F ();
// 0x000002CA System.String UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::get_id()
extern void XROcclusionSubsystemCinfo_get_id_m96A0EF17FED72227D3ED1136F891B8AD462A22C3_AdjustorThunk ();
// 0x000002CB System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::set_id(System.String)
extern void XROcclusionSubsystemCinfo_set_id_mA0AA0AA870676C64796A472C2A628497172F0458_AdjustorThunk ();
// 0x000002CC System.Type UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::get_implementationType()
extern void XROcclusionSubsystemCinfo_get_implementationType_m375A67B86D9AED5EA754A97869FB74624164313E_AdjustorThunk ();
// 0x000002CD System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::set_implementationType(System.Type)
extern void XROcclusionSubsystemCinfo_set_implementationType_m9E73CDE4355BF6B44269FCC147E95E2BB9AB4FC4_AdjustorThunk ();
// 0x000002CE System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::get_supportsHumanSegmentationStencilImage()
extern void XROcclusionSubsystemCinfo_get_supportsHumanSegmentationStencilImage_m04B46A2EC45A5664843ED06C3A797CA984170E6C_AdjustorThunk ();
// 0x000002CF System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::set_supportsHumanSegmentationStencilImage(System.Boolean)
extern void XROcclusionSubsystemCinfo_set_supportsHumanSegmentationStencilImage_mC57AB832FA71CD2607744CB7595C4CB75D3C8618_AdjustorThunk ();
// 0x000002D0 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::get_supportsHumanSegmentationDepthImage()
extern void XROcclusionSubsystemCinfo_get_supportsHumanSegmentationDepthImage_m4111410B7452236F3C676F152C49A1E50D6C59AF_AdjustorThunk ();
// 0x000002D1 System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::set_supportsHumanSegmentationDepthImage(System.Boolean)
extern void XROcclusionSubsystemCinfo_set_supportsHumanSegmentationDepthImage_mC6477C08869651350B069C0F82A15498FF3EEFDD_AdjustorThunk ();
// 0x000002D2 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::Equals(UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo)
extern void XROcclusionSubsystemCinfo_Equals_mB73AB4777C08BBB6A84453DB250F0A3C7743439D_AdjustorThunk ();
// 0x000002D3 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::Equals(System.Object)
extern void XROcclusionSubsystemCinfo_Equals_m6F0BDC6E02CACCACA97BCBA852EF23B4E4639BC3_AdjustorThunk ();
// 0x000002D4 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::op_Equality(UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo,UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo)
extern void XROcclusionSubsystemCinfo_op_Equality_m95B41F2A31BB59670EBD254BDFAF32846959FEB4 ();
// 0x000002D5 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo,UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo)
extern void XROcclusionSubsystemCinfo_op_Inequality_mEA92EBF18577F8EB4C2101B3D9AF0F52CAF94AAC ();
// 0x000002D6 System.Int32 UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo::GetHashCode()
extern void XROcclusionSubsystemCinfo_GetHashCode_mBBD1B91B4917F2B46FE0D174B10EBFD54F264F52_AdjustorThunk ();
// 0x000002D7 System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo)
extern void XROcclusionSubsystemDescriptor__ctor_mC66EEC804A37FC13398BF862C35B2210A956AB5C ();
// 0x000002D8 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor::get_supportsHumanSegmentationStencilImage()
extern void XROcclusionSubsystemDescriptor_get_supportsHumanSegmentationStencilImage_m2ABDF70773231A0A0EDA5357739B7254AA531708 ();
// 0x000002D9 System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor::set_supportsHumanSegmentationStencilImage(System.Boolean)
extern void XROcclusionSubsystemDescriptor_set_supportsHumanSegmentationStencilImage_m8BD2C081B31FC942AE2EC96770FCCDBCAF04226F ();
// 0x000002DA System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor::get_supportsHumanSegmentationDepthImage()
extern void XROcclusionSubsystemDescriptor_get_supportsHumanSegmentationDepthImage_mCA9CABD2BFD1ACC9012903F7858C3B438E3D1EDD ();
// 0x000002DB System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor::set_supportsHumanSegmentationDepthImage(System.Boolean)
extern void XROcclusionSubsystemDescriptor_set_supportsHumanSegmentationDepthImage_mD53709C1291F48491604CBD4C6571621BD924122 ();
// 0x000002DC UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XROcclusionSubsystemCinfo)
extern void XROcclusionSubsystemDescriptor_Create_mE8F6EB04566638A7E69EC8F42305163B90A3DA85 ();
// 0x000002DD System.Void UnityEngine.XR.ARSubsystems.XRParticipant::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr,System.Guid)
extern void XRParticipant__ctor_mAA44F8E88C7CD88220CCFC72140293C6A87C3936_AdjustorThunk ();
// 0x000002DE UnityEngine.XR.ARSubsystems.XRParticipant UnityEngine.XR.ARSubsystems.XRParticipant::get_defaultParticipant()
extern void XRParticipant_get_defaultParticipant_m1ACE083807AB7AC1D3C5C6B008B21D6235E0C90D ();
// 0x000002DF UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRParticipant::get_trackableId()
extern void XRParticipant_get_trackableId_mAF0DAE2613E96C830102678EA49DA306402C7700_AdjustorThunk ();
// 0x000002E0 UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRParticipant::get_pose()
extern void XRParticipant_get_pose_m9FDF90F628DF1FC812226F06F196A113644C1717_AdjustorThunk ();
// 0x000002E1 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRParticipant::get_trackingState()
extern void XRParticipant_get_trackingState_m759EEC47B61486F19F9312FBBD6B29DD2F0C46FB_AdjustorThunk ();
// 0x000002E2 System.IntPtr UnityEngine.XR.ARSubsystems.XRParticipant::get_nativePtr()
extern void XRParticipant_get_nativePtr_m564BDF8BE2F0111A797DC8444537B79017E67CFF_AdjustorThunk ();
// 0x000002E3 System.Guid UnityEngine.XR.ARSubsystems.XRParticipant::get_sessionId()
extern void XRParticipant_get_sessionId_m5938006D21673E09D6CFCA25D59838C96D6F4104_AdjustorThunk ();
// 0x000002E4 System.Int32 UnityEngine.XR.ARSubsystems.XRParticipant::GetHashCode()
extern void XRParticipant_GetHashCode_mC57DBF0AB32B41BEFF5BACDF6491A31922C1BECF_AdjustorThunk ();
// 0x000002E5 System.Boolean UnityEngine.XR.ARSubsystems.XRParticipant::Equals(UnityEngine.XR.ARSubsystems.XRParticipant)
extern void XRParticipant_Equals_mDD39066DDA04071F6A1BE956D8C0C8CCF4FC2F2E_AdjustorThunk ();
// 0x000002E6 System.Boolean UnityEngine.XR.ARSubsystems.XRParticipant::Equals(System.Object)
extern void XRParticipant_Equals_mDA735125F2F48F7EAE29E9F89DA73AF80D667E3B_AdjustorThunk ();
// 0x000002E7 System.Boolean UnityEngine.XR.ARSubsystems.XRParticipant::op_Equality(UnityEngine.XR.ARSubsystems.XRParticipant,UnityEngine.XR.ARSubsystems.XRParticipant)
extern void XRParticipant_op_Equality_m487121B72916A790BBC6650532F5E87DC42C10E6 ();
// 0x000002E8 System.Boolean UnityEngine.XR.ARSubsystems.XRParticipant::op_Inequality(UnityEngine.XR.ARSubsystems.XRParticipant,UnityEngine.XR.ARSubsystems.XRParticipant)
extern void XRParticipant_op_Inequality_mAAC7AF1F23488D31EA335A672F26577305E7E777 ();
// 0x000002E9 System.Void UnityEngine.XR.ARSubsystems.XRParticipant::.cctor()
extern void XRParticipant__cctor_m08A417AEC969464B03C91EB03652D93A5F5A620C ();
// 0x000002EA System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem::.ctor()
extern void XRParticipantSubsystem__ctor_m2A0AF9EDA5FA38728983C7B80CD1528333744028 ();
// 0x000002EB System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem::OnStart()
extern void XRParticipantSubsystem_OnStart_m418621832F927A7D135F1A859DEC11844D859BE8 ();
// 0x000002EC System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem::OnStop()
extern void XRParticipantSubsystem_OnStop_m7D9596D15D2D034F3E3B3C828EB39BDA8B3A0312 ();
// 0x000002ED System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem::OnDestroyed()
extern void XRParticipantSubsystem_OnDestroyed_m1FC0F22A84096B22BEC61922444455428F2AA480 ();
// 0x000002EE UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRParticipant> UnityEngine.XR.ARSubsystems.XRParticipantSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRParticipantSubsystem_GetChanges_mAFC1419DAF4C1E3FEDD4B14C2561C4413AFA47BA ();
// 0x000002EF UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider UnityEngine.XR.ARSubsystems.XRParticipantSubsystem::CreateProvider()
// 0x000002F0 UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor_Capabilities UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor::get_capabilities()
extern void XRParticipantSubsystemDescriptor_get_capabilities_mC77B52797D0A552DE7073B23C3BB84682B700B21 ();
// 0x000002F1 System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor::set_capabilities(UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor_Capabilities)
extern void XRParticipantSubsystemDescriptor_set_capabilities_m59EB7EA3C05BEE15CAB691BCCA3B3DD7B2D5BDA6 ();
// 0x000002F2 System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor::Register(System.String,UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor_Capabilities)
// 0x000002F3 System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor::.ctor(System.String,System.Type,System.Type,UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor_Capabilities)
extern void XRParticipantSubsystemDescriptor__ctor_m4D10C6AEDF54E4B11618837A7989EE5A696A4368 ();
// 0x000002F4 UnityEngine.XR.ARSubsystems.BoundedPlane UnityEngine.XR.ARSubsystems.BoundedPlane::get_defaultValue()
extern void BoundedPlane_get_defaultValue_mD9C5DCC9919CFB735B2D62B8F4BEF1DAEBA37E89 ();
// 0x000002F5 System.Void UnityEngine.XR.ARSubsystems.BoundedPlane::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.PlaneAlignment,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr,UnityEngine.XR.ARSubsystems.PlaneClassification)
extern void BoundedPlane__ctor_m6669034B2D75285B18BB5F4AB225FFF405E12896_AdjustorThunk ();
// 0x000002F6 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.BoundedPlane::get_trackableId()
extern void BoundedPlane_get_trackableId_mD2A3DCDA61898E637ACAE0A0F4A832931378071A_AdjustorThunk ();
// 0x000002F7 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.BoundedPlane::get_subsumedById()
extern void BoundedPlane_get_subsumedById_m77895AF3F9E056F4816CB9177209586E98967889_AdjustorThunk ();
// 0x000002F8 UnityEngine.Pose UnityEngine.XR.ARSubsystems.BoundedPlane::get_pose()
extern void BoundedPlane_get_pose_m30B084D1F307CB46894BE4FAD448788001A0F0DF_AdjustorThunk ();
// 0x000002F9 UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::get_center()
extern void BoundedPlane_get_center_m1AB8ED2CBDC8F4A85358F98A8DD832436818505F_AdjustorThunk ();
// 0x000002FA UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::get_extents()
extern void BoundedPlane_get_extents_mC0F005CBFDA55C68EF9CC52FBC8C0FC07854CBBB_AdjustorThunk ();
// 0x000002FB UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::get_size()
extern void BoundedPlane_get_size_mC49BFA58CD7CE92C2470C82F30EA6F8A4F38B854_AdjustorThunk ();
// 0x000002FC UnityEngine.XR.ARSubsystems.PlaneAlignment UnityEngine.XR.ARSubsystems.BoundedPlane::get_alignment()
extern void BoundedPlane_get_alignment_m85C46D81426435F0B9399ABAF80AC5342A2F7137_AdjustorThunk ();
// 0x000002FD UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.BoundedPlane::get_trackingState()
extern void BoundedPlane_get_trackingState_m9B6467E433564ABE4BED9328F7B23AB05905B008_AdjustorThunk ();
// 0x000002FE System.IntPtr UnityEngine.XR.ARSubsystems.BoundedPlane::get_nativePtr()
extern void BoundedPlane_get_nativePtr_m715E5D9C20481ED26B239C0865C9A934EB114E8F_AdjustorThunk ();
// 0x000002FF UnityEngine.XR.ARSubsystems.PlaneClassification UnityEngine.XR.ARSubsystems.BoundedPlane::get_classification()
extern void BoundedPlane_get_classification_mBC7152460D4441EE38BE0A9ACC26F31AC810C373_AdjustorThunk ();
// 0x00000300 System.Single UnityEngine.XR.ARSubsystems.BoundedPlane::get_width()
extern void BoundedPlane_get_width_m0A50EE1F2895796227CD25F82B867DBEDE310E0A_AdjustorThunk ();
// 0x00000301 System.Single UnityEngine.XR.ARSubsystems.BoundedPlane::get_height()
extern void BoundedPlane_get_height_m142AC62B93F60D6C445DFAB1380EE4CDDE852DC7_AdjustorThunk ();
// 0x00000302 UnityEngine.Vector3 UnityEngine.XR.ARSubsystems.BoundedPlane::get_normal()
extern void BoundedPlane_get_normal_m1DBB621B1447071A5C7C5F2966A90459B9481078_AdjustorThunk ();
// 0x00000303 UnityEngine.Plane UnityEngine.XR.ARSubsystems.BoundedPlane::get_plane()
extern void BoundedPlane_get_plane_mB634B619F93280612D0D395F8BD42B8533CEA787_AdjustorThunk ();
// 0x00000304 System.Void UnityEngine.XR.ARSubsystems.BoundedPlane::GetCorners(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void BoundedPlane_GetCorners_mC7C70E4A661E0AC9DCD3E53C11C3CFE6885A7D91_AdjustorThunk ();
// 0x00000305 System.String UnityEngine.XR.ARSubsystems.BoundedPlane::ToString()
extern void BoundedPlane_ToString_mC00A85440C02CCFFD015A56BC18154E709DF4646_AdjustorThunk ();
// 0x00000306 System.Boolean UnityEngine.XR.ARSubsystems.BoundedPlane::Equals(System.Object)
extern void BoundedPlane_Equals_mE1F35325F340F3CCC6662E73296FFDF8B0436C4D_AdjustorThunk ();
// 0x00000307 System.Int32 UnityEngine.XR.ARSubsystems.BoundedPlane::GetHashCode()
extern void BoundedPlane_GetHashCode_mE003D802E745B7D4891C72388C0E80E9F8FA45DD_AdjustorThunk ();
// 0x00000308 System.Boolean UnityEngine.XR.ARSubsystems.BoundedPlane::op_Equality(UnityEngine.XR.ARSubsystems.BoundedPlane,UnityEngine.XR.ARSubsystems.BoundedPlane)
extern void BoundedPlane_op_Equality_mF251CF6307424910EBF752DC6BECC935C8B5BE34 ();
// 0x00000309 System.Boolean UnityEngine.XR.ARSubsystems.BoundedPlane::op_Inequality(UnityEngine.XR.ARSubsystems.BoundedPlane,UnityEngine.XR.ARSubsystems.BoundedPlane)
extern void BoundedPlane_op_Inequality_m6BEAF4DF01C450C1C4F1491967959CA3223114EE ();
// 0x0000030A System.Boolean UnityEngine.XR.ARSubsystems.BoundedPlane::Equals(UnityEngine.XR.ARSubsystems.BoundedPlane)
extern void BoundedPlane_Equals_m45A1269EAC68DE7B82FDC42D04073236B2FD333C_AdjustorThunk ();
// 0x0000030B System.Void UnityEngine.XR.ARSubsystems.BoundedPlane::.cctor()
extern void BoundedPlane__cctor_m86C83D139BFE0A6444E9D93CCFD7EC5749BED3E4 ();
// 0x0000030C System.Boolean UnityEngine.XR.ARSubsystems.PlaneAlignmentExtensions::IsHorizontal(UnityEngine.XR.ARSubsystems.PlaneAlignment)
extern void PlaneAlignmentExtensions_IsHorizontal_m1FE7018A4BEB30AC79E56AD6E4EB7F1F306343D9 ();
// 0x0000030D System.Boolean UnityEngine.XR.ARSubsystems.PlaneAlignmentExtensions::IsVertical(UnityEngine.XR.ARSubsystems.PlaneAlignment)
extern void PlaneAlignmentExtensions_IsVertical_mBE82213A0D92EAC23478A636A344498733536717 ();
// 0x0000030E System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::.ctor()
extern void XRPlaneSubsystem__ctor_mD446FA374A919AF2EDD1A1D7C47913E621CBF809 ();
// 0x0000030F System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::OnStart()
extern void XRPlaneSubsystem_OnStart_mE7DE5789EA25815D532CAC0E888F4018DFA8DDC7 ();
// 0x00000310 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::OnDestroyed()
extern void XRPlaneSubsystem_OnDestroyed_m092E6A86005959CBA29F6F8328250B3A87D78058 ();
// 0x00000311 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::OnStop()
extern void XRPlaneSubsystem_OnStop_m9E81F5B23F8D0AE818DA72270A43927E5597291C ();
// 0x00000312 UnityEngine.XR.ARSubsystems.PlaneDetectionMode UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::get_requestedPlaneDetectionMode()
extern void XRPlaneSubsystem_get_requestedPlaneDetectionMode_mB79EA6EDEA868D978E61B63D6D2C2ECA342E957A ();
// 0x00000313 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::set_requestedPlaneDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void XRPlaneSubsystem_set_requestedPlaneDetectionMode_m31D455F45A2E087A0BEA96AB76B4F3F8FC77E8F9 ();
// 0x00000314 UnityEngine.XR.ARSubsystems.PlaneDetectionMode UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::get_currentPlaneDetectionMode()
extern void XRPlaneSubsystem_get_currentPlaneDetectionMode_mAFE5F30973EC4F786ED19B179C0DCE45272F1316 ();
// 0x00000315 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.BoundedPlane> UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRPlaneSubsystem_GetChanges_m0487B4AE994BA3CE0DD7D9FA365856F4C9F5710B ();
// 0x00000316 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::GetBoundary(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<UnityEngine.Vector2>&)
extern void XRPlaneSubsystem_GetBoundary_m8E8F373A0147BF4BB2E51B8EB12CBD85244A598F ();
// 0x00000317 UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::CreateProvider()
// 0x00000318 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::CreateOrResizeNativeArrayIfNecessary(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<T>&)
// 0x00000319 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::get_supportsHorizontalPlaneDetection()
extern void XRPlaneSubsystemDescriptor_get_supportsHorizontalPlaneDetection_mCE1A453A7A8232F5F853C3DE97D050E1FBF1EF4B ();
// 0x0000031A System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsHorizontalPlaneDetection(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsHorizontalPlaneDetection_m9461E5C2D8FFB4D448B3D67D86466AD37F9D4701 ();
// 0x0000031B System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::get_supportsVerticalPlaneDetection()
extern void XRPlaneSubsystemDescriptor_get_supportsVerticalPlaneDetection_m40C0375B4213105E598B98B1FA139209924B8787 ();
// 0x0000031C System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsVerticalPlaneDetection(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsVerticalPlaneDetection_m8A4D26E382738EAE8C04FE1A25A0C6F361A94868 ();
// 0x0000031D System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::get_supportsArbitraryPlaneDetection()
extern void XRPlaneSubsystemDescriptor_get_supportsArbitraryPlaneDetection_m167EFA5CEDBF49EB1221D7E4FD594183EBF38A92 ();
// 0x0000031E System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsArbitraryPlaneDetection(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsArbitraryPlaneDetection_m33CAB48781AA7EF4032C40320B761A2F192432C1 ();
// 0x0000031F System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::get_supportsBoundaryVertices()
extern void XRPlaneSubsystemDescriptor_get_supportsBoundaryVertices_m566A87DC259E6BFF79DA388547F9A7AC73A71849 ();
// 0x00000320 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsBoundaryVertices(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsBoundaryVertices_mD484E051C4FA3399B9BAF6E47260D58806AB4993 ();
// 0x00000321 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::get_supportsClassification()
extern void XRPlaneSubsystemDescriptor_get_supportsClassification_mB068F96C31ACDA86254500C653F34FF21BD78219 ();
// 0x00000322 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::set_supportsClassification(System.Boolean)
extern void XRPlaneSubsystemDescriptor_set_supportsClassification_m51D7D13704B7109545D8555E299E5F12525B0143 ();
// 0x00000323 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::Create(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo)
extern void XRPlaneSubsystemDescriptor_Create_m3C5EB6950F78DFA4DEF405243ED67F0CBE2110CA ();
// 0x00000324 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo)
extern void XRPlaneSubsystemDescriptor__ctor_mF240C9165052F15A60A6055E0F880629D3B8B5F3 ();
// 0x00000325 System.Boolean UnityEngine.XR.ARSubsystems.Promise`1::get_keepWaiting()
// 0x00000326 T UnityEngine.XR.ARSubsystems.Promise`1::get_result()
// 0x00000327 System.Void UnityEngine.XR.ARSubsystems.Promise`1::set_result(T)
// 0x00000328 UnityEngine.XR.ARSubsystems.Promise`1<T> UnityEngine.XR.ARSubsystems.Promise`1::CreateResolvedPromise(T)
// 0x00000329 System.Void UnityEngine.XR.ARSubsystems.Promise`1::Resolve(T)
// 0x0000032A System.Void UnityEngine.XR.ARSubsystems.Promise`1::OnKeepWaiting()
// 0x0000032B System.Void UnityEngine.XR.ARSubsystems.Promise`1::.ctor()
// 0x0000032C UnityEngine.XR.ARSubsystems.XRRaycast UnityEngine.XR.ARSubsystems.XRRaycast::get_defaultValue()
extern void XRRaycast_get_defaultValue_mD62060B6A68161E3B3A2F929E772C1B681306CC0 ();
// 0x0000032D UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRRaycast::get_trackableId()
extern void XRRaycast_get_trackableId_m6DBE200F60327FBBD8C1852FD50F5881AFDEE90B_AdjustorThunk ();
// 0x0000032E UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRRaycast::get_pose()
extern void XRRaycast_get_pose_m6EAC1A67DCD90871104B13EE918B1F19C9B8083A_AdjustorThunk ();
// 0x0000032F UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRRaycast::get_trackingState()
extern void XRRaycast_get_trackingState_m78D3C1216CFEC8374CC3B84540DDF6B9FD94ECAB_AdjustorThunk ();
// 0x00000330 System.IntPtr UnityEngine.XR.ARSubsystems.XRRaycast::get_nativePtr()
extern void XRRaycast_get_nativePtr_m2BF1942CDEE019895049665F903277F290B436DA_AdjustorThunk ();
// 0x00000331 System.Single UnityEngine.XR.ARSubsystems.XRRaycast::get_distance()
extern void XRRaycast_get_distance_m2AFA9CBDBDA989D5EA183DC81EE867910960616B_AdjustorThunk ();
// 0x00000332 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRRaycast::get_hitTrackableId()
extern void XRRaycast_get_hitTrackableId_m4E477515193C2CE62EF964D6E26E1BE6DB48F5F3_AdjustorThunk ();
// 0x00000333 System.Void UnityEngine.XR.ARSubsystems.XRRaycast::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr,System.Single,UnityEngine.XR.ARSubsystems.TrackableId)
extern void XRRaycast__ctor_m7D756FF576B0D1C307B2DE2807129ED2176EEBE7_AdjustorThunk ();
// 0x00000334 System.Int32 UnityEngine.XR.ARSubsystems.XRRaycast::GetHashCode()
extern void XRRaycast_GetHashCode_m24DCE228B4EA497D7F9CC62D27271B1A72156C35_AdjustorThunk ();
// 0x00000335 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycast::Equals(System.Object)
extern void XRRaycast_Equals_mD29FED6CD5F7C75D4005856BE7FE35107F84A81D_AdjustorThunk ();
// 0x00000336 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycast::Equals(UnityEngine.XR.ARSubsystems.XRRaycast)
extern void XRRaycast_Equals_mBB5E88F0CE73CBE2F93483A95A810FFB9575F495_AdjustorThunk ();
// 0x00000337 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycast::op_Equality(UnityEngine.XR.ARSubsystems.XRRaycast,UnityEngine.XR.ARSubsystems.XRRaycast)
extern void XRRaycast_op_Equality_m078A221E3D496F3A19339788052561E0ECFC7EF5 ();
// 0x00000338 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycast::op_Inequality(UnityEngine.XR.ARSubsystems.XRRaycast,UnityEngine.XR.ARSubsystems.XRRaycast)
extern void XRRaycast_op_Inequality_m3A2D9FD239BE87C78BD26004A802272654CC35F5 ();
// 0x00000339 System.Void UnityEngine.XR.ARSubsystems.XRRaycast::.cctor()
extern void XRRaycast__cctor_mE6BA22D64A43ECC5E386DB7ADC0461CAA3E0AD9B ();
// 0x0000033A UnityEngine.XR.ARSubsystems.XRRaycastHit UnityEngine.XR.ARSubsystems.XRRaycastHit::get_defaultValue()
extern void XRRaycastHit_get_defaultValue_m17AEBDAC971A56C3FC4C7C4E2E14ECC357658DFA ();
// 0x0000033B UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRRaycastHit::get_trackableId()
extern void XRRaycastHit_get_trackableId_mAECCB1BE08FB0B5A48CB27D955250FE2068492CF_AdjustorThunk ();
// 0x0000033C System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::set_trackableId(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XRRaycastHit_set_trackableId_mD5381CB555237421AA3A1A4F42BDBA66C2CEE77F_AdjustorThunk ();
// 0x0000033D UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRRaycastHit::get_pose()
extern void XRRaycastHit_get_pose_mE0B0A754E818C6FF3675A41CA95185A3E608C8C3_AdjustorThunk ();
// 0x0000033E System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::set_pose(UnityEngine.Pose)
extern void XRRaycastHit_set_pose_m3E6F13DE1371303DD66CD9D9E8B86500C24C5516_AdjustorThunk ();
// 0x0000033F System.Single UnityEngine.XR.ARSubsystems.XRRaycastHit::get_distance()
extern void XRRaycastHit_get_distance_mC748DE6ED96B0C735DCA4AD320FA0BF522246D19_AdjustorThunk ();
// 0x00000340 System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::set_distance(System.Single)
extern void XRRaycastHit_set_distance_m53218D1A8CBD8F632F988C439D5F98633A050815_AdjustorThunk ();
// 0x00000341 UnityEngine.XR.ARSubsystems.TrackableType UnityEngine.XR.ARSubsystems.XRRaycastHit::get_hitType()
extern void XRRaycastHit_get_hitType_m52BBF5DBDE1B3E7E01571EE029F68EB29E240DA6_AdjustorThunk ();
// 0x00000342 System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::set_hitType(UnityEngine.XR.ARSubsystems.TrackableType)
extern void XRRaycastHit_set_hitType_m776B39B9226EB310D47FB6A10BA78844AEC4EE58_AdjustorThunk ();
// 0x00000343 System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,System.Single,UnityEngine.XR.ARSubsystems.TrackableType)
extern void XRRaycastHit__ctor_m522E98C4B6FD85F8386911C5BC497DE4968E3961_AdjustorThunk ();
// 0x00000344 System.Int32 UnityEngine.XR.ARSubsystems.XRRaycastHit::GetHashCode()
extern void XRRaycastHit_GetHashCode_m74F67C2F858CF9669399A90DC761E0E763C0827F_AdjustorThunk ();
// 0x00000345 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastHit::Equals(System.Object)
extern void XRRaycastHit_Equals_m0A24B5C58B6CA930CDD05F6F17F54FD60DA10DE5_AdjustorThunk ();
// 0x00000346 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastHit::Equals(UnityEngine.XR.ARSubsystems.XRRaycastHit)
extern void XRRaycastHit_Equals_m2E3F746F63AC5ED95DF5E79AB43C2DE8A8E42E60_AdjustorThunk ();
// 0x00000347 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastHit::op_Equality(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.XR.ARSubsystems.XRRaycastHit)
extern void XRRaycastHit_op_Equality_m1543C9C16776653F7665569024203E1F54305FC7 ();
// 0x00000348 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastHit::op_Inequality(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.XR.ARSubsystems.XRRaycastHit)
extern void XRRaycastHit_op_Inequality_m5533730BFA8AD45F27384E867981EF82A0AB0862 ();
// 0x00000349 System.Void UnityEngine.XR.ARSubsystems.XRRaycastHit::.cctor()
extern void XRRaycastHit__cctor_m304374EB65F3AE9EFA5D8418B9CF3CE8A90B752B ();
// 0x0000034A System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::.ctor()
extern void XRRaycastSubsystem__ctor_mE8BD2BFB3AFD44403F3A663CA5D5AAC707419506 ();
// 0x0000034B System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::OnStart()
extern void XRRaycastSubsystem_OnStart_m7B2A704BD7A5EA9FAF5E80D67C83AC90A2B7E0E7 ();
// 0x0000034C System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::OnStop()
extern void XRRaycastSubsystem_OnStop_mB922B7F9B65FF7EC41D3042D161476F5E491573F ();
// 0x0000034D System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::OnDestroyed()
extern void XRRaycastSubsystem_OnDestroyed_mD476DFCB51E552D3BB2C841CAB96B532D9D77BCC ();
// 0x0000034E UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRRaycast> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRRaycastSubsystem_GetChanges_mE85AF5735862EDDABEF7C2F955D2B4C1CB1A80DA ();
// 0x0000034F System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::TryAddRaycast(UnityEngine.Vector2,System.Single,UnityEngine.XR.ARSubsystems.XRRaycast&)
extern void XRRaycastSubsystem_TryAddRaycast_m04F51D27F5AFFA539549FA123B7D41A63E3A71AC ();
// 0x00000350 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::TryAddRaycast(UnityEngine.Ray,System.Single,UnityEngine.XR.ARSubsystems.XRRaycast&)
extern void XRRaycastSubsystem_TryAddRaycast_mFA57974A0D00FC2A6F752468BE4FF8A9A78BB828 ();
// 0x00000351 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::RemoveRaycast(UnityEngine.XR.ARSubsystems.TrackableId)
extern void XRRaycastSubsystem_RemoveRaycast_m979A4F09D73F43CD9D405D9595673CA5DFBE2F2D ();
// 0x00000352 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::Raycast(UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void XRRaycastSubsystem_Raycast_mD6335AB75E7AD15295138215F593EAB71754E6FA ();
// 0x00000353 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::Raycast(UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void XRRaycastSubsystem_Raycast_m46598C4ACA7D6AC6B6DA53A92ED1349F327EC6BF ();
// 0x00000354 UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::CreateProvider()
// 0x00000355 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::get_supportsViewportBasedRaycast()
extern void XRRaycastSubsystemDescriptor_get_supportsViewportBasedRaycast_mD71BE0D71A6B3B48DDAB480114F930293C40DF26 ();
// 0x00000356 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::set_supportsViewportBasedRaycast(System.Boolean)
extern void XRRaycastSubsystemDescriptor_set_supportsViewportBasedRaycast_m7A6EBE60F40966D0314C378B1441C7DB41C0720D ();
// 0x00000357 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::get_supportsWorldBasedRaycast()
extern void XRRaycastSubsystemDescriptor_get_supportsWorldBasedRaycast_m7CDCA8DFD75903B7169A59254A31EBCB1D1962BD ();
// 0x00000358 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::set_supportsWorldBasedRaycast(System.Boolean)
extern void XRRaycastSubsystemDescriptor_set_supportsWorldBasedRaycast_m23E91C1C3684B0FE2AF37D3BE2A79B0D88BFC7B3 ();
// 0x00000359 UnityEngine.XR.ARSubsystems.TrackableType UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::get_supportedTrackableTypes()
extern void XRRaycastSubsystemDescriptor_get_supportedTrackableTypes_m02F17127CFA033A9D6D84C7F0D53D0BA3FE379C4 ();
// 0x0000035A System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::set_supportedTrackableTypes(UnityEngine.XR.ARSubsystems.TrackableType)
extern void XRRaycastSubsystemDescriptor_set_supportedTrackableTypes_mA429421E574C9261CFC271AC43A521E43B990DCD ();
// 0x0000035B System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::get_supportsTrackedRaycasts()
extern void XRRaycastSubsystemDescriptor_get_supportsTrackedRaycasts_mCFC70DB9283F92245EB14474CD4E996005E252FC ();
// 0x0000035C System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::set_supportsTrackedRaycasts(System.Boolean)
extern void XRRaycastSubsystemDescriptor_set_supportsTrackedRaycasts_m5945AF718E03677402F4007F25462DC1BC631524 ();
// 0x0000035D System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::RegisterDescriptor(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo)
extern void XRRaycastSubsystemDescriptor_RegisterDescriptor_mFA32B9879B902AA46943CF8809094299062A41DB ();
// 0x0000035E System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo)
extern void XRRaycastSubsystemDescriptor__ctor_m449AFD6137C639FB21F17C456B20BC954875BC9E ();
// 0x0000035F System.Void UnityEngine.XR.ARSubsystems.ScopedProfiler::.ctor(System.String)
extern void ScopedProfiler__ctor_mC6576AB1ED762DB2335436C4C63121FE04BBF264_AdjustorThunk ();
// 0x00000360 System.Void UnityEngine.XR.ARSubsystems.ScopedProfiler::.ctor(System.String,UnityEngine.Object)
extern void ScopedProfiler__ctor_m0EFC86CF601B63CCAC679AF8DE9BE02A9A2635AB_AdjustorThunk ();
// 0x00000361 System.Void UnityEngine.XR.ARSubsystems.ScopedProfiler::Dispose()
extern void ScopedProfiler_Dispose_m9330643F81D6C1961371A3D1436A53EFCB232887_AdjustorThunk ();
// 0x00000362 System.Void UnityEngine.XR.ARSubsystems.SerializableGuid::.ctor(System.UInt64,System.UInt64)
extern void SerializableGuid__ctor_m67FFC2270F5BF1783DE5E4C4F85A214315DE46BB_AdjustorThunk ();
// 0x00000363 UnityEngine.XR.ARSubsystems.SerializableGuid UnityEngine.XR.ARSubsystems.SerializableGuid::get_empty()
extern void SerializableGuid_get_empty_mCDC698E4D3EE9F3B311588C6FC1EE7CC9E820892 ();
// 0x00000364 System.Guid UnityEngine.XR.ARSubsystems.SerializableGuid::get_guid()
extern void SerializableGuid_get_guid_mDD1F60EF61B262769627D5F48F8840285E1986A0_AdjustorThunk ();
// 0x00000365 System.Int32 UnityEngine.XR.ARSubsystems.SerializableGuid::GetHashCode()
extern void SerializableGuid_GetHashCode_m0C64440E27DBCFCF12B8E0E0FF92AB5B15344BC0_AdjustorThunk ();
// 0x00000366 System.Boolean UnityEngine.XR.ARSubsystems.SerializableGuid::Equals(System.Object)
extern void SerializableGuid_Equals_m6D1C338E1D6985165503EBB3B369A7A7A58442D9_AdjustorThunk ();
// 0x00000367 System.String UnityEngine.XR.ARSubsystems.SerializableGuid::ToString()
extern void SerializableGuid_ToString_m4D5204E4E13A13718A1450AABFC59A192DDEF80D_AdjustorThunk ();
// 0x00000368 System.String UnityEngine.XR.ARSubsystems.SerializableGuid::ToString(System.String)
extern void SerializableGuid_ToString_m2B1D0D590302829D2CB6A8419F715D79AEBF0780_AdjustorThunk ();
// 0x00000369 System.String UnityEngine.XR.ARSubsystems.SerializableGuid::ToString(System.String,System.IFormatProvider)
extern void SerializableGuid_ToString_m0B633F3089883432FF2F63F82D49F0DF86ABF77F_AdjustorThunk ();
// 0x0000036A System.Boolean UnityEngine.XR.ARSubsystems.SerializableGuid::Equals(UnityEngine.XR.ARSubsystems.SerializableGuid)
extern void SerializableGuid_Equals_m22017F6AF109B89F27E01D9E99014B0E95D6649E_AdjustorThunk ();
// 0x0000036B System.Boolean UnityEngine.XR.ARSubsystems.SerializableGuid::op_Equality(UnityEngine.XR.ARSubsystems.SerializableGuid,UnityEngine.XR.ARSubsystems.SerializableGuid)
extern void SerializableGuid_op_Equality_m0324F1D0AF69531DE06866CD1DD802E9C3CD40CB ();
// 0x0000036C System.Boolean UnityEngine.XR.ARSubsystems.SerializableGuid::op_Inequality(UnityEngine.XR.ARSubsystems.SerializableGuid,UnityEngine.XR.ARSubsystems.SerializableGuid)
extern void SerializableGuid_op_Inequality_m262FFAE2823E3735F070BA7C168A505365B64B52 ();
// 0x0000036D System.Void UnityEngine.XR.ARSubsystems.SerializableGuid::.cctor()
extern void SerializableGuid__cctor_m7370C5A2AF171A415261C9CB3A55593963A6A721 ();
// 0x0000036E System.Boolean UnityEngine.XR.ARSubsystems.SessionAvailabilityExtensions::IsSupported(UnityEngine.XR.ARSubsystems.SessionAvailability)
extern void SessionAvailabilityExtensions_IsSupported_mA0E371DBCB2DB69E066EA4DCADC76721AAD04ABA ();
// 0x0000036F System.Boolean UnityEngine.XR.ARSubsystems.SessionAvailabilityExtensions::IsInstalled(UnityEngine.XR.ARSubsystems.SessionAvailability)
extern void SessionAvailabilityExtensions_IsInstalled_m30AE248D1BBF1C669EE32FE5976A35C16152DC9C ();
// 0x00000370 System.IntPtr UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_nativePtr()
extern void XRSessionSubsystem_get_nativePtr_m0F00EE85A23E2FBE08AE83393F4C7DC97C22366B ();
// 0x00000371 System.Guid UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_sessionId()
extern void XRSessionSubsystem_get_sessionId_m830EF72639051E3486DA85FE6E4EF1C9AD3481E1 ();
// 0x00000372 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionAvailability> UnityEngine.XR.ARSubsystems.XRSessionSubsystem::GetAvailabilityAsync()
extern void XRSessionSubsystem_GetAvailabilityAsync_mE1444BD33C0A1EAD4982FC0AE64D1251635487ED ();
// 0x00000373 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionInstallationStatus> UnityEngine.XR.ARSubsystems.XRSessionSubsystem::InstallAsync()
extern void XRSessionSubsystem_InstallAsync_m35E08EF7130491F2E498C990109FA7323A2ABCCC ();
// 0x00000374 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::.ctor()
extern void XRSessionSubsystem__ctor_m2817E6FDD974187708CFD270DE4C6D3132774648 ();
// 0x00000375 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnStart()
extern void XRSessionSubsystem_OnStart_mCE65AF851F73EEE76E058AD66DA6E3E355C3FFCB ();
// 0x00000376 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::Reset()
extern void XRSessionSubsystem_Reset_mA6596EEA8C670E2561986B54BB34E0F15A0D5836 ();
// 0x00000377 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnStop()
extern void XRSessionSubsystem_OnStop_mC995FD70B27E089A98FEE62DAFB6642521F90F33 ();
// 0x00000378 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnDestroyed()
extern void XRSessionSubsystem_OnDestroyed_mE818CF27CDBBA6E22713A186B50C7BED4A77CBCA ();
// 0x00000379 System.Nullable`1<UnityEngine.XR.ARSubsystems.Configuration> UnityEngine.XR.ARSubsystems.XRSessionSubsystem::DetermineConfiguration(UnityEngine.XR.ARSubsystems.Feature)
extern void XRSessionSubsystem_DetermineConfiguration_mF4235A223A27B21F222E70E23FCD211F606F91AB ();
// 0x0000037A System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void XRSessionSubsystem_Update_m168E0641976ED5CD2084BD32692044A2D361B945 ();
// 0x0000037B System.Nullable`1<UnityEngine.XR.ARSubsystems.Configuration> UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_currentConfiguration()
extern void XRSessionSubsystem_get_currentConfiguration_mD51168E864094B29123D44E2284844EFD08F9218 ();
// 0x0000037C System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::set_currentConfiguration(System.Nullable`1<UnityEngine.XR.ARSubsystems.Configuration>)
extern void XRSessionSubsystem_set_currentConfiguration_m173B5C54D28FA42528D57FC6C36665712239704E ();
// 0x0000037D UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_requestedFeatures()
extern void XRSessionSubsystem_get_requestedFeatures_m918BFAED674C970093F4D15BA4425E39AEF2E72B ();
// 0x0000037E Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.ConfigurationDescriptor> UnityEngine.XR.ARSubsystems.XRSessionSubsystem::GetConfigurationDescriptors(Unity.Collections.Allocator)
extern void XRSessionSubsystem_GetConfigurationDescriptors_mD9F32AE5997EFAA780BCA50998D79494673C7207 ();
// 0x0000037F System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnApplicationPause()
extern void XRSessionSubsystem_OnApplicationPause_mE53A32290C253F808E5B14A11B7917286E86B08A ();
// 0x00000380 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::OnApplicationResume()
extern void XRSessionSubsystem_OnApplicationResume_m4103D0866A4152C9A52E52A306D85EBAD8405F2F ();
// 0x00000381 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_trackingState()
extern void XRSessionSubsystem_get_trackingState_m6CEDC16CB9B224A0302A83BC2C22FC4C0905EB30 ();
// 0x00000382 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_requestedTrackingMode()
extern void XRSessionSubsystem_get_requestedTrackingMode_m5C0B4035A44E91732BAB0BA1CD8D6B84ED0A3DC4 ();
// 0x00000383 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::set_requestedTrackingMode(UnityEngine.XR.ARSubsystems.Feature)
extern void XRSessionSubsystem_set_requestedTrackingMode_mE6950872428B9C4FD3A11BF3C2B8273345644339 ();
// 0x00000384 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_currentTrackingMode()
extern void XRSessionSubsystem_get_currentTrackingMode_m7707AA9874ED16C9E8908EB50B270F5067EB0CD2 ();
// 0x00000385 UnityEngine.XR.ARSubsystems.ConfigurationChooser UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_configurationChooser()
extern void XRSessionSubsystem_get_configurationChooser_mEBF33BD6BF7567261EC97270477B88BC20E00E0A ();
// 0x00000386 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::set_configurationChooser(UnityEngine.XR.ARSubsystems.ConfigurationChooser)
extern void XRSessionSubsystem_set_configurationChooser_m3BED2362E1E27943DF11DF7474A02CDB137050CF ();
// 0x00000387 UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_notTrackingReason()
extern void XRSessionSubsystem_get_notTrackingReason_m2425113BCCDD44CEF92AA9A045C002CAF981B6D7 ();
// 0x00000388 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_matchFrameRateEnabled()
extern void XRSessionSubsystem_get_matchFrameRateEnabled_mD95D36035F4CF3A1606234CC4092B78CD7DF6671 ();
// 0x00000389 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_matchFrameRateRequested()
extern void XRSessionSubsystem_get_matchFrameRateRequested_m55D71F6FB285EC98FB7FEFF73D2B17738E6FD251 ();
// 0x0000038A System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem::set_matchFrameRateRequested(System.Boolean)
extern void XRSessionSubsystem_set_matchFrameRateRequested_m6AEBB720BEB2954B383907B5191DEFFD03606824 ();
// 0x0000038B System.Int32 UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_frameRate()
extern void XRSessionSubsystem_get_frameRate_m9C029A08839E039C3459DF5CE4A5E5CFAD3DC7DE ();
// 0x0000038C UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider UnityEngine.XR.ARSubsystems.XRSessionSubsystem::CreateProvider()
// 0x0000038D UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor UnityEngine.XR.ARSubsystems.XRSessionSubsystem::get_subsystemDescriptor()
extern void XRSessionSubsystem_get_subsystemDescriptor_mEEA810642D61C1E226C8A8C4F6133FF085B3DB35 ();
// 0x0000038E System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::get_supportsInstall()
extern void XRSessionSubsystemDescriptor_get_supportsInstall_m2AA89682007FE1D8BB811FD152DE326FF7BB5A99 ();
// 0x0000038F System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::set_supportsInstall(System.Boolean)
extern void XRSessionSubsystemDescriptor_set_supportsInstall_m010EE3F0CB4B143A90B93C1F10F063FB12546920 ();
// 0x00000390 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::get_supportsMatchFrameRate()
extern void XRSessionSubsystemDescriptor_get_supportsMatchFrameRate_m66DA7D5EE88322AF2EE5FC3B1BF8203115C2CA8F ();
// 0x00000391 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::set_supportsMatchFrameRate(System.Boolean)
extern void XRSessionSubsystemDescriptor_set_supportsMatchFrameRate_mC2B0189D51BF3B64026D01DD6A088052C5D74BFC ();
// 0x00000392 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::RegisterDescriptor(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo)
extern void XRSessionSubsystemDescriptor_RegisterDescriptor_m7A9F84E8A57323CDB5DC415BA05E72D6A72025E4 ();
// 0x00000393 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor::.ctor(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo)
extern void XRSessionSubsystemDescriptor__ctor_mF2A65C6A814FB2D22D5ED1608E5EFD5B0CD9A6E2 ();
// 0x00000394 UnityEngine.ScreenOrientation UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::get_screenOrientation()
extern void XRSessionUpdateParams_get_screenOrientation_m111C145EA6A683F025DF48C6EA355E37D8974183_AdjustorThunk ();
// 0x00000395 System.Void UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::set_screenOrientation(UnityEngine.ScreenOrientation)
extern void XRSessionUpdateParams_set_screenOrientation_m7C20FD52988E0F21604700B5CDA93FBA63DD28C6_AdjustorThunk ();
// 0x00000396 UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::get_screenDimensions()
extern void XRSessionUpdateParams_get_screenDimensions_m61A9722E272D6292B9C7C093BF7792FB007BF21E_AdjustorThunk ();
// 0x00000397 System.Void UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::set_screenDimensions(UnityEngine.Vector2Int)
extern void XRSessionUpdateParams_set_screenDimensions_m41570268847916BA02DD2427BDDB08B3D466A905_AdjustorThunk ();
// 0x00000398 System.Int32 UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::GetHashCode()
extern void XRSessionUpdateParams_GetHashCode_m3E0C208F41FAC84F879A073F85FB9DC0F1C09520_AdjustorThunk ();
// 0x00000399 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::Equals(System.Object)
extern void XRSessionUpdateParams_Equals_m415AB0E24C9CF0C013872ED16C571B65DACF24B1_AdjustorThunk ();
// 0x0000039A System.String UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::ToString()
extern void XRSessionUpdateParams_ToString_m7150FEAE08C59544392C3D47B3CB5AC318B82F4A_AdjustorThunk ();
// 0x0000039B System.Boolean UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::Equals(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void XRSessionUpdateParams_Equals_mFE8BAF000FDC02612C5D563960EB974E510DEAB3_AdjustorThunk ();
// 0x0000039C System.Boolean UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::op_Equality(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams,UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void XRSessionUpdateParams_op_Equality_mEB57CF7E4D66886BF9EE3FF1BBF7D1B73E63608B ();
// 0x0000039D System.Boolean UnityEngine.XR.ARSubsystems.XRSessionUpdateParams::op_Inequality(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams,UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void XRSessionUpdateParams_op_Inequality_mD00004C24603E13FC7D4F0239F812446EE21FF75 ();
// 0x0000039E UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.TrackableId::get_invalidId()
extern void TrackableId_get_invalidId_mBE9FA1EC8F2EC1575C1B31666EA928A3382DF1CD ();
// 0x0000039F System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::get_subId1()
extern void TrackableId_get_subId1_mF453A72AB194301098CEE0A9CED682524CFB30BD_AdjustorThunk ();
// 0x000003A0 System.Void UnityEngine.XR.ARSubsystems.TrackableId::set_subId1(System.UInt64)
extern void TrackableId_set_subId1_m43A33DBEA409BEF994296301506511538AFD96DB_AdjustorThunk ();
// 0x000003A1 System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::get_subId2()
extern void TrackableId_get_subId2_m2738106507454E9F70ADF4E3A74DB37BAD0E912F_AdjustorThunk ();
// 0x000003A2 System.Void UnityEngine.XR.ARSubsystems.TrackableId::set_subId2(System.UInt64)
extern void TrackableId_set_subId2_m0C75B44985527D97E859E98B0512FD3BE65FB539_AdjustorThunk ();
// 0x000003A3 System.Void UnityEngine.XR.ARSubsystems.TrackableId::.ctor(System.UInt64,System.UInt64)
extern void TrackableId__ctor_m497D3C74C918FDE476EA168A431DAE4E135E88B4_AdjustorThunk ();
// 0x000003A4 System.Void UnityEngine.XR.ARSubsystems.TrackableId::.ctor(System.String)
extern void TrackableId__ctor_m9DD30A8FE5DB5798D50C6F121E8FFD324F4DE4D6_AdjustorThunk ();
// 0x000003A5 System.String UnityEngine.XR.ARSubsystems.TrackableId::ToString()
extern void TrackableId_ToString_mBA49191865E57697F4279D2781B182590726A215_AdjustorThunk ();
// 0x000003A6 System.Int32 UnityEngine.XR.ARSubsystems.TrackableId::GetHashCode()
extern void TrackableId_GetHashCode_m89E7236D11700A1FAF335918CA65CDEB1BF4D973_AdjustorThunk ();
// 0x000003A7 System.Boolean UnityEngine.XR.ARSubsystems.TrackableId::Equals(System.Object)
extern void TrackableId_Equals_mBB92F3933E2215399757A70A3704E580DD32406C_AdjustorThunk ();
// 0x000003A8 System.Boolean UnityEngine.XR.ARSubsystems.TrackableId::Equals(UnityEngine.XR.ARSubsystems.TrackableId)
extern void TrackableId_Equals_mCE458E0FDCDD6E339FCC1926EE88EB7B3D45F943_AdjustorThunk ();
// 0x000003A9 System.Boolean UnityEngine.XR.ARSubsystems.TrackableId::op_Equality(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.XR.ARSubsystems.TrackableId)
extern void TrackableId_op_Equality_m3171D96F331BC92756A7B171C85CD627442E7873 ();
// 0x000003AA System.Boolean UnityEngine.XR.ARSubsystems.TrackableId::op_Inequality(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.XR.ARSubsystems.TrackableId)
extern void TrackableId_op_Inequality_mE90FE883749A9959B2B3F85FB12456646ACE6B93 ();
// 0x000003AB System.Void UnityEngine.XR.ARSubsystems.TrackableId::.cctor()
extern void TrackableId__cctor_m4EBC45AC6693CBEFB8B222F0EE6D8DAEC841264E ();
// 0x000003AC UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.ITrackable::get_trackableId()
// 0x000003AD UnityEngine.Pose UnityEngine.XR.ARSubsystems.ITrackable::get_pose()
// 0x000003AE UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.ITrackable::get_trackingState()
// 0x000003AF System.IntPtr UnityEngine.XR.ARSubsystems.ITrackable::get_nativePtr()
// 0x000003B0 Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::get_added()
// 0x000003B1 Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::get_updated()
// 0x000003B2 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1::get_removed()
// 0x000003B3 System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1::get_isCreated()
// 0x000003B4 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::set_isCreated(System.Boolean)
// 0x000003B5 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
// 0x000003B6 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator,T)
// 0x000003B7 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
// 0x000003B8 UnityEngine.XR.ARSubsystems.TrackableChanges`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::CopyFrom(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>,Unity.Collections.Allocator)
// 0x000003B9 System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::Dispose()
// 0x000003BA System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
// 0x000003BB UnityEngine.XR.ARSubsystems.TrackableChanges`1<TTrackable> UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::GetChanges(Unity.Collections.Allocator)
// 0x000003BC System.Void UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::.ctor()
// 0x000003BD System.Void UnityEngine.XR.ARSubsystems.ValidationUtility`1::ValidateAndThrow(UnityEngine.XR.ARSubsystems.TrackableChanges`1<T>)
// 0x000003BE System.Void UnityEngine.XR.ARSubsystems.ValidationUtility`1::ValidateAndDisposeIfThrown(UnityEngine.XR.ARSubsystems.TrackableChanges`1<T>)
// 0x000003BF System.Void UnityEngine.XR.ARSubsystems.ValidationUtility`1::AddToSetAndThrowIfDuplicate(UnityEngine.XR.ARSubsystems.TrackableId,System.Boolean,System.String)
// 0x000003C0 System.Void UnityEngine.XR.ARSubsystems.ValidationUtility`1::.ctor()
// 0x000003C1 System.Void UnityEngine.XR.ARSubsystems.ValidationUtility`1::.cctor()
// 0x000003C2 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImageAsyncConversionStatusExtensions::IsDone(UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversionStatus)
extern void XRCpuImageAsyncConversionStatusExtensions_IsDone_mBCD5B446791EE8D22AC506686C479B584AF6F6AA ();
// 0x000003C3 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImageAsyncConversionStatusExtensions::IsError(UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversionStatus)
extern void XRCpuImageAsyncConversionStatusExtensions_IsError_mC4BC17F0DC4216F3D3537C92958FBAA2C980AE14 ();
// 0x000003C4 System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::OnStart()
// 0x000003C5 System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::OnStop()
// 0x000003C6 System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::OnDestroyed()
// 0x000003C7 System.Boolean UnityEngine.XR.ARSubsystems.XRSubsystem`1::get_running()
// 0x000003C8 System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::OnDestroy()
// 0x000003C9 System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::Start()
// 0x000003CA System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::Stop()
// 0x000003CB System.Void UnityEngine.XR.ARSubsystems.XRSubsystem`1::.ctor()
// 0x000003CC System.IntPtr UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_nativeTexture()
extern void XRTextureDescriptor_get_nativeTexture_m83CAA03353C203B7D38618C1963C715F052081F8_AdjustorThunk ();
// 0x000003CD System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_nativeTexture(System.IntPtr)
extern void XRTextureDescriptor_set_nativeTexture_mEF92A3E263840B8F428C314323C20A11F896D907_AdjustorThunk ();
// 0x000003CE System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_width()
extern void XRTextureDescriptor_get_width_m158B2CEE4A0F56DF263BB642F5E4A3C3CF339E0B_AdjustorThunk ();
// 0x000003CF System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_width(System.Int32)
extern void XRTextureDescriptor_set_width_m59E159F83238991BAD9838C5835A07E44F6A163E_AdjustorThunk ();
// 0x000003D0 System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_height()
extern void XRTextureDescriptor_get_height_mCE50370000BCF4A70B95344A0731A771401C0894_AdjustorThunk ();
// 0x000003D1 System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_height(System.Int32)
extern void XRTextureDescriptor_set_height_mE690E293BE1FE8009052CC87FA454FB79DE9DF0E_AdjustorThunk ();
// 0x000003D2 System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_mipmapCount()
extern void XRTextureDescriptor_get_mipmapCount_m491B149D8BBF148B2030214818E237A28D9B6CC4_AdjustorThunk ();
// 0x000003D3 System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_mipmapCount(System.Int32)
extern void XRTextureDescriptor_set_mipmapCount_m8CC98FD1B188CA92DE7C1C430BF71E11E7AD7858_AdjustorThunk ();
// 0x000003D4 UnityEngine.TextureFormat UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_format()
extern void XRTextureDescriptor_get_format_mA2DA22DC1DEBCAD27A9C69F3374D614DF1C3FA2B_AdjustorThunk ();
// 0x000003D5 System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_format(UnityEngine.TextureFormat)
extern void XRTextureDescriptor_set_format_m2BEDFB4C31E590B2C2AAE7145AEAE714491E0EA6_AdjustorThunk ();
// 0x000003D6 System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_propertyNameId()
extern void XRTextureDescriptor_get_propertyNameId_mA3A29036B96A64D1C4F147678E60E2BFCAAAAFF0_AdjustorThunk ();
// 0x000003D7 System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_propertyNameId(System.Int32)
extern void XRTextureDescriptor_set_propertyNameId_m87654C29B3CEFA71D22E9F1323058334E8338B4F_AdjustorThunk ();
// 0x000003D8 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_valid()
extern void XRTextureDescriptor_get_valid_mF060565C5E24FDF97771F6FDA3235562DF01977B_AdjustorThunk ();
// 0x000003D9 System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_depth()
extern void XRTextureDescriptor_get_depth_m753CFA3697D1A98ABFA8331BDA0F37C8D156ABA9_AdjustorThunk ();
// 0x000003DA System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_depth(System.Int32)
extern void XRTextureDescriptor_set_depth_mB7F0D2390736CBDF0325186F9D3DFD1831C067DF_AdjustorThunk ();
// 0x000003DB UnityEngine.Rendering.TextureDimension UnityEngine.XR.ARSubsystems.XRTextureDescriptor::get_dimension()
extern void XRTextureDescriptor_get_dimension_m580C5254C35EE0208427909D7DA2CED82BF8835F_AdjustorThunk ();
// 0x000003DC System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::set_dimension(UnityEngine.Rendering.TextureDimension)
extern void XRTextureDescriptor_set_dimension_mA4C6202E8028775EEE873185090FC8FB0847F371_AdjustorThunk ();
// 0x000003DD System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::hasIdenticalTextureMetadata(UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void XRTextureDescriptor_hasIdenticalTextureMetadata_mD9C2A76A8B680BB7B2742F82235E40977CD098AE_AdjustorThunk ();
// 0x000003DE System.Void UnityEngine.XR.ARSubsystems.XRTextureDescriptor::Reset()
extern void XRTextureDescriptor_Reset_m64A787FBD1F11161369A72A7D61763DDF8D74EBC_AdjustorThunk ();
// 0x000003DF System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::Equals(UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void XRTextureDescriptor_Equals_m124C4B8E0370717E0714FB2D28493A77034C6E38_AdjustorThunk ();
// 0x000003E0 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::Equals(System.Object)
extern void XRTextureDescriptor_Equals_m8D2E1A6303E60A653F70870CBD04845414F6A0A5_AdjustorThunk ();
// 0x000003E1 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::op_Equality(UnityEngine.XR.ARSubsystems.XRTextureDescriptor,UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void XRTextureDescriptor_op_Equality_mBEE6E663B93B3648626DAACC5D0AD1F2C0B76847 ();
// 0x000003E2 System.Boolean UnityEngine.XR.ARSubsystems.XRTextureDescriptor::op_Inequality(UnityEngine.XR.ARSubsystems.XRTextureDescriptor,UnityEngine.XR.ARSubsystems.XRTextureDescriptor)
extern void XRTextureDescriptor_op_Inequality_m1373FCEBF131F54B4A61398DE7034853861E9EAE ();
// 0x000003E3 System.Int32 UnityEngine.XR.ARSubsystems.XRTextureDescriptor::GetHashCode()
extern void XRTextureDescriptor_GetHashCode_mE61628A57D74C31744B57EBFBE8E8EDFA673B65F_AdjustorThunk ();
// 0x000003E4 System.String UnityEngine.XR.ARSubsystems.XRTextureDescriptor::ToString()
extern void XRTextureDescriptor_ToString_mA7C17125D54876E04397C7022031B6A346CA9A7F_AdjustorThunk ();
// 0x000003E5 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::Start()
extern void Provider_Start_mF4586E88E223BF0A1F8B875E37ED8E956025F7DC ();
// 0x000003E6 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::Stop()
extern void Provider_Stop_m7670689E746816D6B1D5D3AA7D79CD83AF463D20 ();
// 0x000003E7 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::Destroy()
extern void Provider_Destroy_m67293D02BB308F9F682704C9674ED6BF0F9106CD ();
// 0x000003E8 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRAnchor> UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRAnchor,Unity.Collections.Allocator)
// 0x000003E9 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::TryAddAnchor(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void Provider_TryAddAnchor_mF724411302D13377F72BAD790C4D6841A3B8D94C ();
// 0x000003EA System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::TryAttachAnchor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void Provider_TryAttachAnchor_m82C5EF065E0549465D43C8768478DCED4DAB432A ();
// 0x000003EB System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::TryRemoveAnchor(UnityEngine.XR.ARSubsystems.TrackableId)
extern void Provider_TryRemoveAnchor_m3E507AA8C15B4B13787D2FECC6D22E6442A52D20 ();
// 0x000003EC System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystem_Provider::.ctor()
extern void Provider__ctor_mF658D6D83E2D1075FAEAF7AF0AABEE1A3B8CDD87 ();
// 0x000003ED System.String UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::get_id()
extern void Cinfo_get_id_mAB9FA0AEB8F01DFBEFD37A79A0A76523FDC5EA97_AdjustorThunk ();
// 0x000003EE System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::set_id(System.String)
extern void Cinfo_set_id_m07E3333D64F89961070832339B11E5BCAA3923E1_AdjustorThunk ();
// 0x000003EF System.Type UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_mDD1BA48FC9C3C388B5DF95EA240F84A4862AB497_AdjustorThunk ();
// 0x000003F0 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_m8222C7E7310436CA41B9C74D28C4C487B1D02DAD_AdjustorThunk ();
// 0x000003F1 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::get_supportsTrackableAttachments()
extern void Cinfo_get_supportsTrackableAttachments_m266173570E6C947C78F79960EE9C7C7E120048B5_AdjustorThunk ();
// 0x000003F2 System.Void UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::set_supportsTrackableAttachments(System.Boolean)
extern void Cinfo_set_supportsTrackableAttachments_mF43F0A8FF39724C929A1AE638719068470F01160_AdjustorThunk ();
// 0x000003F3 System.Int32 UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m384C61B0C35F73A23C9D5B627245E6F7B3ACB610_AdjustorThunk ();
// 0x000003F4 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m670F61358AE4F7A8BD0CD8A1DFC52800519D75E5_AdjustorThunk ();
// 0x000003F5 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_m1C2F4C09DC5A8A145F507FA0383E9BB9536515FD_AdjustorThunk ();
// 0x000003F6 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_mF2CE3F3A33DDD2B27855774E43DC9076775BC98C ();
// 0x000003F7 System.Boolean UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_m31844DA84D4A263CA3B6BA5965DF214511E50B95 ();
// 0x000003F8 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_Provider::Start()
extern void Provider_Start_m5E039CE52C6D7873CAE86F45CC8CCDDD10CFA738 ();
// 0x000003F9 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_Provider::Stop()
extern void Provider_Stop_m00D52CC50FC71C24B3BFC370A592A4BDC3A2E805 ();
// 0x000003FA System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_Provider::Destroy()
extern void Provider_Destroy_m181F91A509C877EEDAA0CDBCA51A03123BFA5DEF ();
// 0x000003FB UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint> UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRReferencePoint,Unity.Collections.Allocator)
// 0x000003FC System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_Provider::TryAddReferencePoint(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRReferencePoint&)
extern void Provider_TryAddReferencePoint_m24758900098738D227C197B63C5BB1A4D6E48599 ();
// 0x000003FD System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_Provider::TryAttachReferencePoint(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRReferencePoint&)
extern void Provider_TryAttachReferencePoint_m220B48C6D25FCB8C834B21CB58D2C45C87212180 ();
// 0x000003FE System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_Provider::TryRemoveReferencePoint(UnityEngine.XR.ARSubsystems.TrackableId)
extern void Provider_TryRemoveReferencePoint_m1DE094DA1EBC860FB65C6B200F2CA79940239A80 ();
// 0x000003FF System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_Provider::.ctor()
extern void Provider__ctor_mCAD1B3B1509E0232266D304B2CAB429D16C3359E ();
// 0x00000400 System.String UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo::get_id()
extern void Cinfo_get_id_mB1E35C0B52EEAA8EB934C4D3F02465CF8A752015_AdjustorThunk ();
// 0x00000401 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo::set_id(System.String)
extern void Cinfo_set_id_m9CAE75E21B0DAE38A8619D1B04D17EDEC81E97D7_AdjustorThunk ();
// 0x00000402 System.Type UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_m764697AF3D79BDFA6010287A8B542F9323693096_AdjustorThunk ();
// 0x00000403 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_m9D5112215C6766E6561E42DA858B7F3D72F0005E_AdjustorThunk ();
// 0x00000404 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo::get_supportsTrackableAttachments()
extern void Cinfo_get_supportsTrackableAttachments_m041B030B1BD0114D8FCE9A9F804CFF5984FB07BD_AdjustorThunk ();
// 0x00000405 System.Void UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo::set_supportsTrackableAttachments(System.Boolean)
extern void Cinfo_set_supportsTrackableAttachments_mC0A061EDA609485B0E83FC7857E8573C93F38FD7_AdjustorThunk ();
// 0x00000406 System.Int32 UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_mA1002FD6C9DDB0C39442B7692A7CDB5562C61086_AdjustorThunk ();
// 0x00000407 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_mC210DE78D45CD980BF7B5A186241CFC5CC243D2E_AdjustorThunk ();
// 0x00000408 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_m7D9C657E628D2DFC390587FBE0BDFEB1D5CDC92D_AdjustorThunk ();
// 0x00000409 System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_mE1F2D9B59D8CE1464461320ED675AF909582610A ();
// 0x0000040A System.Boolean UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_m434589D57A438F397CD363A5E2CB491095725A53 ();
// 0x0000040B UnityEngine.XR.ARSubsystems.XRCpuImage_Api UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_cpuImageApi()
extern void Provider_get_cpuImageApi_m20E5F2315BA1B77750BCFC6AB528084A0FA02987 ();
// 0x0000040C UnityEngine.Material UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_cameraMaterial()
extern void Provider_get_cameraMaterial_m82824265BAB7CF990BA7F189A7DBAC583B06D7C4 ();
// 0x0000040D System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_permissionGranted()
extern void Provider_get_permissionGranted_mA6995C1A907115576EE47F20298835116FDC8D37 ();
// 0x0000040E System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_invertCulling()
extern void Provider_get_invertCulling_m4D45573FD037E1BD3A4E5B90B8B4226AC0302A02 ();
// 0x0000040F UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_currentCamera()
extern void Provider_get_currentCamera_m299A8E1A074CB725165D1A0691D723EFECAD6D07 ();
// 0x00000410 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_requestedCamera()
extern void Provider_get_requestedCamera_m14F1AFE7227DA3B140BF1F0018CF16A79B2735B0 ();
// 0x00000411 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::set_requestedCamera(UnityEngine.XR.ARSubsystems.Feature)
extern void Provider_set_requestedCamera_m5C2C333F32E753D08C2FE311BE46E936646DB351 ();
// 0x00000412 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::Start()
extern void Provider_Start_m3D11C46FF20C01CFCFE6EDAA7E3343BB76CA60A1 ();
// 0x00000413 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::Stop()
extern void Provider_Stop_m496FD02A62F7B5C2690BFB8883150846011EE681 ();
// 0x00000414 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::Destroy()
extern void Provider_Destroy_m1B756AB089B69831AE9FDA27C06464D90640D3B7 ();
// 0x00000415 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::TryGetFrame(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraFrame&)
extern void Provider_TryGetFrame_mA84121E1F987E6A61FA76E4730F66546CE8D4185 ();
// 0x00000416 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_autoFocusEnabled()
extern void Provider_get_autoFocusEnabled_m3947CAE5A0D98C752EDB0257F1B3CD79896132A3 ();
// 0x00000417 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_autoFocusRequested()
extern void Provider_get_autoFocusRequested_m7B38D80EDC1E28E0E5C14F2B6157D9B399ECCC17 ();
// 0x00000418 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::set_autoFocusRequested(System.Boolean)
extern void Provider_set_autoFocusRequested_m2C390A2C14C33F75C8E650EA23A522A20686CCC7 ();
// 0x00000419 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_currentLightEstimation()
extern void Provider_get_currentLightEstimation_mFC6A9C54C1A59510DF8CB6991A5D8BFC735EC642 ();
// 0x0000041A UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_requestedLightEstimation()
extern void Provider_get_requestedLightEstimation_mA711479359B827B01C37AB9306A0AFB786E12D48 ();
// 0x0000041B System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::set_requestedLightEstimation(UnityEngine.XR.ARSubsystems.Feature)
extern void Provider_set_requestedLightEstimation_m6CC82728BFE90FE5939780452447E3BB7BA552E4 ();
// 0x0000041C System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::TryGetIntrinsics(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics&)
extern void Provider_TryGetIntrinsics_m881C1076A92682F37AB6D83C9FF420DACBFCB1ED ();
// 0x0000041D Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::GetConfigurations(UnityEngine.XR.ARSubsystems.XRCameraConfiguration,Unity.Collections.Allocator)
extern void Provider_GetConfigurations_m9FD5BBC0B7C6270E7371BC88EC5EB0B38CBEACE2 ();
// 0x0000041E System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::get_currentConfiguration()
extern void Provider_get_currentConfiguration_m2A1DE1BFD6EA2F2F5E4130AA941F91FCB5672185 ();
// 0x0000041F System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::set_currentConfiguration(System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration>)
extern void Provider_set_currentConfiguration_m5BA7EA99BF9DA46C010D6F62BEAD46B4BC0B1F3B ();
// 0x00000420 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTextureDescriptor> UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::GetTextureDescriptors(UnityEngine.XR.ARSubsystems.XRTextureDescriptor,Unity.Collections.Allocator)
extern void Provider_GetTextureDescriptors_mFC6F6C5A3B399ACAD7A8E78A93E73ABA8197F4D3 ();
// 0x00000421 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::GetMaterialKeywords(System.Collections.Generic.List`1<System.String>&,System.Collections.Generic.List`1<System.String>&)
extern void Provider_GetMaterialKeywords_m5BD9801B59F20AE0090BB12574A4CB4E69E466A6 ();
// 0x00000422 System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::TryAcquireLatestCpuImage(UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo&)
extern void Provider_TryAcquireLatestCpuImage_mAADAAE7BD5FFDB7A3132232990CAEFCEE7834B59 ();
// 0x00000423 UnityEngine.Material UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::CreateCameraMaterial(System.String)
extern void Provider_CreateCameraMaterial_m62FBA6BFEA66D3EE8265E8C53A66AF4C403E14B4 ();
// 0x00000424 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::OnBeforeBackgroundRender(System.Int32)
extern void Provider_OnBeforeBackgroundRender_mBF9C737F3E774D8D51DEFF2E11F33A5DD88395A7 ();
// 0x00000425 System.Void UnityEngine.XR.ARSubsystems.XRCameraSubsystem_Provider::.ctor()
extern void Provider__ctor_mCC7EE094D3B2DA61C91D5E78E8E62F1F58DD47FE ();
// 0x00000426 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Api::TryGetPlane(System.Int32,System.Int32,UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo&)
extern void Api_TryGetPlane_m9D64816098EE052F9D610E1EF0DE7F77CB8E02BE ();
// 0x00000427 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Api::TryGetConvertedDataSize(System.Int32,UnityEngine.Vector2Int,UnityEngine.TextureFormat,System.Int32&)
extern void Api_TryGetConvertedDataSize_mC457255D5B5BB9201905A929880F80842DF8E2BB ();
// 0x00000428 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Api::TryConvert(System.Int32,UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams,System.IntPtr,System.Int32)
extern void Api_TryConvert_m07CE575512CBA9F5EEA96AB9836FFEE9B41E42C4 ();
// 0x00000429 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_Api::ConvertAsync(System.Int32,UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams)
extern void Api_ConvertAsync_m8F9E79E238770BD269614CE0BFB504C047C050CF ();
// 0x0000042A System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Api::NativeHandleValid(System.Int32)
extern void Api_NativeHandleValid_m701C5F499E36DF72B983AC977DF256CA85DADF3C ();
// 0x0000042B System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Api::TryGetAsyncRequestData(System.Int32,System.IntPtr&,System.Int32&)
extern void Api_TryGetAsyncRequestData_m508C75D22AD8855AEFA5C27DA303CC475F9B89D8 ();
// 0x0000042C System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Api::ConvertAsync(System.Int32,UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams,UnityEngine.XR.ARSubsystems.XRCpuImage_Api_OnImageRequestCompleteDelegate,System.IntPtr)
extern void Api_ConvertAsync_m9B704468536CA59F5639710895CE9AAEFF614C34 ();
// 0x0000042D System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Api::DisposeImage(System.Int32)
extern void Api_DisposeImage_m1B11F698C036CAF86F0890B8D471BA522A1E3C49 ();
// 0x0000042E System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Api::DisposeAsyncRequest(System.Int32)
extern void Api_DisposeAsyncRequest_mEE2E850FC016E274831E20CC8D9E86A603B8B46C ();
// 0x0000042F UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversionStatus UnityEngine.XR.ARSubsystems.XRCpuImage_Api::GetAsyncRequestStatus(System.Int32)
extern void Api_GetAsyncRequestStatus_m43BB62DA43105C042FFBBEB7FC7ED8132D524D89 ();
// 0x00000430 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Api::FormatSupported(UnityEngine.XR.ARSubsystems.XRCpuImage,UnityEngine.TextureFormat)
extern void Api_FormatSupported_m49584181A3FA7582BAD3AAA84EC798546F68B39A ();
// 0x00000431 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Api::.ctor()
extern void Api__ctor_m83BE4DB9E9BE1D814DB3AAF1834B4C8D09ED4AD9 ();
// 0x00000432 UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion::get_conversionParams()
extern void AsyncConversion_get_conversionParams_m0976435603CD82928943B73F9B8F97A17918C730_AdjustorThunk ();
// 0x00000433 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion::set_conversionParams(UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams)
extern void AsyncConversion_set_conversionParams_m34F73B62E8DB64467D17D086F47FFC8324E9DFF6_AdjustorThunk ();
// 0x00000434 UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversionStatus UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion::get_status()
extern void AsyncConversion_get_status_m8259C60C53F18039D0583A9A4A5DB96197424481_AdjustorThunk ();
// 0x00000435 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion::.ctor(UnityEngine.XR.ARSubsystems.XRCpuImage_Api,System.Int32,UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams)
extern void AsyncConversion__ctor_mBA54D876492EC5DC1BAE3B01B6860C9467994E0A_AdjustorThunk ();
// 0x00000436 Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion::GetData()
// 0x00000437 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion::Dispose()
extern void AsyncConversion_Dispose_mD9C8D177C56B0F184440379985FDFE3C80967B84_AdjustorThunk ();
// 0x00000438 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion::GetHashCode()
extern void AsyncConversion_GetHashCode_mF897267F14D4D94F2227C1D4172793F342A63343_AdjustorThunk ();
// 0x00000439 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion::Equals(System.Object)
extern void AsyncConversion_Equals_m99E225DBA74089B656EE74BD4B9B6A67222F66AC_AdjustorThunk ();
// 0x0000043A System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion::Equals(UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion)
extern void AsyncConversion_Equals_m8538BEACA275973D73D16422E475DA9D78CBC8B2_AdjustorThunk ();
// 0x0000043B System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion::op_Equality(UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion,UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion)
extern void AsyncConversion_op_Equality_m233B9C8841F13F7E5BC44528DE6229044148B5AC ();
// 0x0000043C System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion::op_Inequality(UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion,UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion)
extern void AsyncConversion_op_Inequality_m7317C7F778398EA1CCB85BCE878F66AF43C7BB90 ();
// 0x0000043D System.String UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversion::ToString()
extern void AsyncConversion_ToString_mA0AB15C1A772EBFF298A58B11F445B71C1B7F0C4_AdjustorThunk ();
// 0x0000043E UnityEngine.RectInt UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::get_inputRect()
extern void ConversionParams_get_inputRect_m045ABAD49308AAEFECE4DAF94CABFAACB53BD1D0_AdjustorThunk ();
// 0x0000043F System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::set_inputRect(UnityEngine.RectInt)
extern void ConversionParams_set_inputRect_mB13482D23EE76FBD8F12B9DFBAD925184C25AAB3_AdjustorThunk ();
// 0x00000440 UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::get_outputDimensions()
extern void ConversionParams_get_outputDimensions_m0D23770C75EC23B7D102457D91C2EC0CB2EF6459_AdjustorThunk ();
// 0x00000441 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::set_outputDimensions(UnityEngine.Vector2Int)
extern void ConversionParams_set_outputDimensions_mF716A8EB0CB6469CE580C340B52DD525654F0B24_AdjustorThunk ();
// 0x00000442 UnityEngine.TextureFormat UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::get_outputFormat()
extern void ConversionParams_get_outputFormat_mFAADF1A8ABD173F6C123F7C638E0F4476034CC5B_AdjustorThunk ();
// 0x00000443 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::set_outputFormat(UnityEngine.TextureFormat)
extern void ConversionParams_set_outputFormat_m9CF571FF5292BBEFB3325275BA42902FCF90958C_AdjustorThunk ();
// 0x00000444 UnityEngine.XR.ARSubsystems.XRCpuImage_Transformation UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::get_transformation()
extern void ConversionParams_get_transformation_m994D08D1B5A0D5BB83C3798CB6EB3AF6C3C8B8A3_AdjustorThunk ();
// 0x00000445 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::set_transformation(UnityEngine.XR.ARSubsystems.XRCpuImage_Transformation)
extern void ConversionParams_set_transformation_mCC2BD22B567479DCB1E2130D4A5EDACD179580B7_AdjustorThunk ();
// 0x00000446 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::.ctor(UnityEngine.XR.ARSubsystems.XRCpuImage,UnityEngine.TextureFormat,UnityEngine.XR.ARSubsystems.XRCpuImage_Transformation)
extern void ConversionParams__ctor_m9C2749383F583F06C1BD86F82406AF0140869486_AdjustorThunk ();
// 0x00000447 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::GetHashCode()
extern void ConversionParams_GetHashCode_m9EF42E4869E2FAC3DDE6E573B9256FBB58A178CF_AdjustorThunk ();
// 0x00000448 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::Equals(UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams)
extern void ConversionParams_Equals_mB0565EFE0C5F12FF86A1E1770A7C14C1DAA4DE23_AdjustorThunk ();
// 0x00000449 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::Equals(System.Object)
extern void ConversionParams_Equals_mA5EDAE5A24DDB8C623E204CFD5D862C2465693F8_AdjustorThunk ();
// 0x0000044A System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::op_Equality(UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams,UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams)
extern void ConversionParams_op_Equality_m79F1BC601A8C332E432DC993754E7FEAE40FDC5F ();
// 0x0000044B System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::op_Inequality(UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams,UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams)
extern void ConversionParams_op_Inequality_m85E4B82A5A388A2B09BA0E11396AB4E7C3CD3384 ();
// 0x0000044C System.String UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams::ToString()
extern void ConversionParams_ToString_m32588C7E28744CB4E7F5639CAE06DFE5202B8011_AdjustorThunk ();
// 0x0000044D System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::get_rowStride()
extern void Plane_get_rowStride_m4BAE42C59A73306C1BB1273DC9E2620E0A917510_AdjustorThunk ();
// 0x0000044E System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::set_rowStride(System.Int32)
extern void Plane_set_rowStride_m6B356A9FFBD8ABCDD1FCF3059E896D8EB495705E_AdjustorThunk ();
// 0x0000044F System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::get_pixelStride()
extern void Plane_get_pixelStride_m77F6BF761236739DADCB33DB1E09A1281F9D96DD_AdjustorThunk ();
// 0x00000450 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::set_pixelStride(System.Int32)
extern void Plane_set_pixelStride_mD255E466170F89A9759FAD9322D39C6751C67AD0_AdjustorThunk ();
// 0x00000451 Unity.Collections.NativeArray`1<System.Byte> UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::get_data()
extern void Plane_get_data_mA92994670020F432B255BCB11460D7905498CDF2_AdjustorThunk ();
// 0x00000452 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::set_data(Unity.Collections.NativeArray`1<System.Byte>)
extern void Plane_set_data_m6F2C130A5DDE7CC3D1E484E03C86BCB8547470E0_AdjustorThunk ();
// 0x00000453 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::.ctor(System.Int32,System.Int32,Unity.Collections.NativeArray`1<System.Byte>)
extern void Plane__ctor_m99EBC727E6617DC223BDA39654D9005F75FDEA9C_AdjustorThunk ();
// 0x00000454 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::GetHashCode()
extern void Plane_GetHashCode_m67EB14E1A37770F9890A94DC933503EE623DA8E8_AdjustorThunk ();
// 0x00000455 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::Equals(System.Object)
extern void Plane_Equals_m781BBA0BD1BA4A6DD71BDA29F5D279E38D01E4A2_AdjustorThunk ();
// 0x00000456 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::Equals(UnityEngine.XR.ARSubsystems.XRCpuImage_Plane)
extern void Plane_Equals_mA6FA62324DE2553D518056C9E52D4E3474FF5578_AdjustorThunk ();
// 0x00000457 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::op_Equality(UnityEngine.XR.ARSubsystems.XRCpuImage_Plane,UnityEngine.XR.ARSubsystems.XRCpuImage_Plane)
extern void Plane_op_Equality_mB8D39D33D386C35D166FD172F2DFDF1E1CD5D7E9 ();
// 0x00000458 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::op_Inequality(UnityEngine.XR.ARSubsystems.XRCpuImage_Plane,UnityEngine.XR.ARSubsystems.XRCpuImage_Plane)
extern void Plane_op_Inequality_m55DC7145F310462CDFE2CDFA26BE052EBF88FAF1 ();
// 0x00000459 System.String UnityEngine.XR.ARSubsystems.XRCpuImage_Plane::ToString()
extern void Plane_ToString_mC2EC1F10E79405DCAED387A0E982059795E794D8_AdjustorThunk ();
// 0x0000045A System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo::get_nativeHandle()
extern void Cinfo_get_nativeHandle_mF7E4A4B3DF3627E6BF0502B758F2A19B15C6B55E_AdjustorThunk ();
// 0x0000045B UnityEngine.Vector2Int UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo::get_dimensions()
extern void Cinfo_get_dimensions_m2CF1ED609BFB2D6BE94123B49DFCE7A9A9297815_AdjustorThunk ();
// 0x0000045C System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo::get_planeCount()
extern void Cinfo_get_planeCount_m1C35DDAB469099A985BD4C9D0364DEE9C58C2FDB_AdjustorThunk ();
// 0x0000045D System.Double UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo::get_timestamp()
extern void Cinfo_get_timestamp_m5EADD7F92B48CF9CECE80291B14E2F7F758A54AE_AdjustorThunk ();
// 0x0000045E UnityEngine.XR.ARSubsystems.XRCpuImage_Format UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo::get_format()
extern void Cinfo_get_format_mB465EA4362E0035D6BBF4628DB563923C6C13FE7_AdjustorThunk ();
// 0x0000045F System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo::.ctor(System.Int32,UnityEngine.Vector2Int,System.Int32,System.Double,UnityEngine.XR.ARSubsystems.XRCpuImage_Format)
extern void Cinfo__ctor_m3DC67D1C506B09C2E79DD1E2CE6FCC2E8254E5F8_AdjustorThunk ();
// 0x00000460 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo)
extern void Cinfo_Equals_m8DD26ADB239B2BA1B623FEAC22649E21F3F83D64_AdjustorThunk ();
// 0x00000461 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_mA9FFEAF14ADEB5C4B57C0F8D792907E368B54A7E_AdjustorThunk ();
// 0x00000462 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo,UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo)
extern void Cinfo_op_Equality_m807D3763B3DD93C56B2375C323C65B6E70DADB20 ();
// 0x00000463 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo,UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo)
extern void Cinfo_op_Inequality_m5D556CF78F2332C5853FE1C3B6BA96929557C36D ();
// 0x00000464 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m7D8BC06648D7CADE45E3ECB7044F5CC426706A3B_AdjustorThunk ();
// 0x00000465 System.String UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo::ToString()
extern void Cinfo_ToString_mFE83F0C0B9A3E1BB036DEEB947CC1E2CA64D2D02_AdjustorThunk ();
// 0x00000466 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider::Start()
extern void Provider_Start_m5FE902BE2D3439448C3A5B3FF4926CE1F7767A49 ();
// 0x00000467 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider::Stop()
extern void Provider_Stop_mC6F7D719BF33D225CF30A3AF6935D04B79131D6B ();
// 0x00000468 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider::Destroy()
extern void Provider_Destroy_mF22AE9359D3DB52341035F86AF040BF560F4751A ();
// 0x00000469 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud> UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRPointCloud,Unity.Collections.Allocator)
// 0x0000046A UnityEngine.XR.ARSubsystems.XRPointCloudData UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider::GetPointCloudData(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator)
// 0x0000046B System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystem_Provider::.ctor()
extern void Provider__ctor_mE05BCBAE62243FDB4D9505D37B6559AD5453F504 ();
// 0x0000046C System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::get_supportsFeaturePoints()
extern void Cinfo_get_supportsFeaturePoints_m9ABB1B99DDF90F76567CF1D40D5FCAB6C26415B5_AdjustorThunk ();
// 0x0000046D System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::set_supportsFeaturePoints(System.Boolean)
extern void Cinfo_set_supportsFeaturePoints_m05831EFF5A03CCC424A2DB1E8C8460E54385E798_AdjustorThunk ();
// 0x0000046E System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::get_supportsConfidence()
extern void Cinfo_get_supportsConfidence_m99E2A9D06072DD88955F84D667CDA0E029E59453_AdjustorThunk ();
// 0x0000046F System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::set_supportsConfidence(System.Boolean)
extern void Cinfo_set_supportsConfidence_m2FCA1C93FDE8DC5DC4A553F9EF32E399844C0F79_AdjustorThunk ();
// 0x00000470 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::get_supportsUniqueIds()
extern void Cinfo_get_supportsUniqueIds_m5A834E8536CABCA693F0B13C0A742C8A76E05C42_AdjustorThunk ();
// 0x00000471 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::set_supportsUniqueIds(System.Boolean)
extern void Cinfo_set_supportsUniqueIds_mE8AA1B05D64ABF1690D4B30E5C245B9A923A9116_AdjustorThunk ();
// 0x00000472 UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Capabilities UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::get_capabilities()
extern void Cinfo_get_capabilities_m6B6CF2B89609939E9E7313A3AAD56C543E4445AB_AdjustorThunk ();
// 0x00000473 System.Void UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::set_capabilities(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Capabilities)
extern void Cinfo_set_capabilities_mEF50340ACAFF47D30E8270D45453909F64E2541E_AdjustorThunk ();
// 0x00000474 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_m11C179BADC0B35FB12263C3A7D3410897618FEDE_AdjustorThunk ();
// 0x00000475 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_mD7C8B1C1CA730E2AC1B15E61F563503E77DA05CF_AdjustorThunk ();
// 0x00000476 System.Int32 UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_mB134492A755E3A215541BF574B1E8449CFDA47D7_AdjustorThunk ();
// 0x00000477 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_mB0D0E72FCAE2987433EA598970459E7A028D681F ();
// 0x00000478 System.Boolean UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_mA0865AAD9BBD14C16B9D7CEF2906FAE715E519DC ();
// 0x00000479 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::Start()
extern void Provider_Start_mF4B8E1109EA3D6641B7BBC6D030DB3CC31EDEB86 ();
// 0x0000047A System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::Stop()
extern void Provider_Stop_mD261C689F310117F4E3E9BDAD86ED85E40A0A1E7 ();
// 0x0000047B System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::Destroy()
extern void Provider_Destroy_m663FE695F3D6F792185B1DA8038FE49827602925 ();
// 0x0000047C System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::get_automaticPlacementRequested()
extern void Provider_get_automaticPlacementRequested_m9B5004655119A447E62D41C67E080AFE47948D0E ();
// 0x0000047D System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::set_automaticPlacementRequested(System.Boolean)
extern void Provider_set_automaticPlacementRequested_m44104AA9F0658054A418F6C63D62542E425D9F35 ();
// 0x0000047E System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::get_automaticPlacementEnabled()
extern void Provider_get_automaticPlacementEnabled_mB5334AFCB345E05EDD9BD3E2B6BB20BAAEFAB5B3 ();
// 0x0000047F System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::get_environmentTextureHDRRequested()
extern void Provider_get_environmentTextureHDRRequested_m617730B6C4565085184D2312488F5E5DE2E49BDA ();
// 0x00000480 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::set_environmentTextureHDRRequested(System.Boolean)
extern void Provider_set_environmentTextureHDRRequested_mA8B32D4D17D116112F639EBCC804885C81339C06 ();
// 0x00000481 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::get_environmentTextureHDREnabled()
extern void Provider_get_environmentTextureHDREnabled_mE0FE8C839DEF777561E3E35F14043DFCACBD2790 ();
// 0x00000482 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::TryAddEnvironmentProbe(UnityEngine.Pose,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.XR.ARSubsystems.XREnvironmentProbe&)
extern void Provider_TryAddEnvironmentProbe_m24898D8AA6188C532001CFFF37ED24AF487AF88A ();
// 0x00000483 System.Boolean UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::RemoveEnvironmentProbe(UnityEngine.XR.ARSubsystems.TrackableId)
extern void Provider_RemoveEnvironmentProbe_m99508DA9076D76663A2522E4F4E71DE0085159CB ();
// 0x00000484 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbe> UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XREnvironmentProbe,Unity.Collections.Allocator)
// 0x00000485 System.Void UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem_Provider::.ctor()
extern void Provider__ctor_mC53B619920A3DCA6D544EE8C2CB291BA654AE955 ();
// 0x00000486 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::Start()
extern void Provider_Start_m01229A7C89ACBF0622DCBABE00BBC51073BBF7FD ();
// 0x00000487 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::Stop()
extern void Provider_Stop_m93A00BA28BEAE45559E8E8AEA487970AD14336E0 ();
// 0x00000488 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::Destroy()
extern void Provider_Destroy_mE5F773C1A80C498037F11654F01C87789C1E5F9D ();
// 0x00000489 System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::GetFaceMesh(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,UnityEngine.XR.ARSubsystems.XRFaceMesh&)
extern void Provider_GetFaceMesh_m0E2EF1C274E371DF9C18BF2CD3A4929BB8782234 ();
// 0x0000048A UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace> UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRFace,Unity.Collections.Allocator)
// 0x0000048B System.Int32 UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::get_supportedFaceCount()
extern void Provider_get_supportedFaceCount_mEEBE86DDF219DE28323F6F57FBF23C75567195C1 ();
// 0x0000048C System.Int32 UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::get_requestedMaximumFaceCount()
extern void Provider_get_requestedMaximumFaceCount_m7A870B83987B1C30FC08DF75579CC455E87CE2CA ();
// 0x0000048D System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::set_requestedMaximumFaceCount(System.Int32)
extern void Provider_set_requestedMaximumFaceCount_mD52CFD9F2A73AA0D8C995D979A9287F31CBB81AD ();
// 0x0000048E System.Int32 UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::get_currentMaximumFaceCount()
extern void Provider_get_currentMaximumFaceCount_m29EC2E2946AB373C642E4EA8F35AEFAE3C3594E5 ();
// 0x0000048F System.Void UnityEngine.XR.ARSubsystems.XRFaceSubsystem_Provider::.ctor()
extern void Provider__ctor_m166848C609D6D2087DF29FA51BF6D15D0CDEE8E9 ();
// 0x00000490 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::Start()
extern void Provider_Start_m39EA57D18B1D961D8F46393D460F8C49EB1C2CC4 ();
// 0x00000491 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::Stop()
extern void Provider_Stop_m60C55970E0B4333B559254762A03A86B8B8CCE25 ();
// 0x00000492 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::Destroy()
extern void Provider_Destroy_m2FA5194EC4B55B0C74169BDD17E102FFD3D74BDD ();
// 0x00000493 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::get_pose2DRequested()
extern void Provider_get_pose2DRequested_m5C1FB8E7FABE069C4606E884078DE7BEC283C6F1 ();
// 0x00000494 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::set_pose2DRequested(System.Boolean)
extern void Provider_set_pose2DRequested_m825FA8836E965804FFD5E9DC86AFD262ED2C751F ();
// 0x00000495 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::get_pose2DEnabled()
extern void Provider_get_pose2DEnabled_mFF8908B8606D9B6F72EA1CA4B11D2F48B942CB68 ();
// 0x00000496 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::get_pose3DRequested()
extern void Provider_get_pose3DRequested_m472D66466C39778B8EA1AC437A4544D29CBE14D3 ();
// 0x00000497 System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::set_pose3DRequested(System.Boolean)
extern void Provider_set_pose3DRequested_m6632DCF009B525457D0A31817960C31D424E638B ();
// 0x00000498 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::get_pose3DEnabled()
extern void Provider_get_pose3DEnabled_m2F1FDA9E6A059CC9DDE60EDE9F716973D7AB0982 ();
// 0x00000499 System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::get_pose3DScaleEstimationRequested()
extern void Provider_get_pose3DScaleEstimationRequested_mC809B773BC1537F61FE85AA7ECC7B3D671DB986F ();
// 0x0000049A System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::set_pose3DScaleEstimationRequested(System.Boolean)
extern void Provider_set_pose3DScaleEstimationRequested_m185A2389C93346F73270FC5976DF7283A3207114 ();
// 0x0000049B System.Boolean UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::get_pose3DScaleEstimationEnabled()
extern void Provider_get_pose3DScaleEstimationEnabled_m1B35E3CA7AE5361860B24A691504A7CE841921F0 ();
// 0x0000049C UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody> UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRHumanBody,Unity.Collections.Allocator)
// 0x0000049D System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::GetSkeleton(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBodyJoint>&)
extern void Provider_GetSkeleton_m33CDE833C6F892F96C3EBDCC2E7808E1E563786A ();
// 0x0000049E Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint> UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::GetHumanBodyPose2DJoints(UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint,System.Int32,System.Int32,UnityEngine.ScreenOrientation,Unity.Collections.Allocator)
extern void Provider_GetHumanBodyPose2DJoints_mE6C0A7C2BBE59E674CE44A199EC1B6EBD80BCF85 ();
// 0x0000049F System.Void UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem_Provider::.ctor()
extern void Provider__ctor_m2DD638AC530CF994612A6E9CB426633013097379 ();
// 0x000004A0 System.Void UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::.ctor(UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary)
extern void Enumerator__ctor_mE0E83E8BE30FC3338D7CA9F0AA641B71EB21AE42_AdjustorThunk ();
// 0x000004A1 System.Boolean UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::MoveNext()
extern void Enumerator_MoveNext_m3F8A455818B6DE64979CD24653C260C86A1A9F8F_AdjustorThunk ();
// 0x000004A2 UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::get_Current()
extern void Enumerator_get_Current_mB2B2A92CE85E0846FEDDE76130EE8D6CD32A799C_AdjustorThunk ();
// 0x000004A3 System.Void UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::Dispose()
extern void Enumerator_Dispose_m111C536A1B2BF00B4AA92904D8549298AB3DE701_AdjustorThunk ();
// 0x000004A4 System.Int32 UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::GetHashCode()
extern void Enumerator_GetHashCode_mF5FBE6E01F47937059A2A365DAF03C0F987A2EE4_AdjustorThunk ();
// 0x000004A5 System.Boolean UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::Equals(System.Object)
extern void Enumerator_Equals_m63765D51882404C6EFABE2122DDF3C0BEA9C900C_AdjustorThunk ();
// 0x000004A6 System.Boolean UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::Equals(UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator)
extern void Enumerator_Equals_m92192638599D8F40E62775D2D0D3FBD625A7BA2B_AdjustorThunk ();
// 0x000004A7 System.Boolean UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::op_Equality(UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator,UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator)
extern void Enumerator_op_Equality_mE77ACF3611768439424275772001826634CA5FA5 ();
// 0x000004A8 System.Boolean UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator::op_Inequality(UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator,UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary_Enumerator)
extern void Enumerator_op_Inequality_m5E0EEC0D10DBAC36CA9B467BD582B9A43C0086E3 ();
// 0x000004A9 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::Destroy()
extern void Provider_Destroy_m11964F4221D5BA9C0743E2F5BC9292A88087263E ();
// 0x000004AA UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage> UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRTrackedImage,Unity.Collections.Allocator)
// 0x000004AB System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::set_imageLibrary(UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary)
// 0x000004AC UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::CreateRuntimeLibrary(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
// 0x000004AD System.Int32 UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::get_requestedMaxNumberOfMovingImages()
extern void Provider_get_requestedMaxNumberOfMovingImages_mE2AF1D82D1093DE329B65D0211F59B17CE33B854 ();
// 0x000004AE System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::set_requestedMaxNumberOfMovingImages(System.Int32)
extern void Provider_set_requestedMaxNumberOfMovingImages_m75CCCB9B1161AB7EE57BF810ED34962260EADB82 ();
// 0x000004AF System.Int32 UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::get_currentMaxNumberOfMovingImages()
extern void Provider_get_currentMaxNumberOfMovingImages_mF1D6CCED9859E909E2E1C5CF755216FE3365A423 ();
// 0x000004B0 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_Provider::.ctor()
extern void Provider__ctor_m5C4C95F3D3E533CE14E842815064740F9A796B3D ();
// 0x000004B1 System.String UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::get_id()
extern void Cinfo_get_id_mE1765ABB412D25FC37DF2545917CDE39A25EA0F6_AdjustorThunk ();
// 0x000004B2 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::set_id(System.String)
extern void Cinfo_set_id_mF18B67F52DD34A8CD8A718ED36CB4651873B3EE0_AdjustorThunk ();
// 0x000004B3 System.Type UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_m32E4C78E8FE23C4B720FA8109F3FC500270A7976_AdjustorThunk ();
// 0x000004B4 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_m9045C99491613E201755F5A363F9CC5978740E59_AdjustorThunk ();
// 0x000004B5 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::get_supportsMovingImages()
extern void Cinfo_get_supportsMovingImages_mAFEA78B5C515F6198E9374823D7339A022627395_AdjustorThunk ();
// 0x000004B6 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::set_supportsMovingImages(System.Boolean)
extern void Cinfo_set_supportsMovingImages_m7045441CA2E8CC03F2CD60BA03C8066095684F96_AdjustorThunk ();
// 0x000004B7 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::get_requiresPhysicalImageDimensions()
extern void Cinfo_get_requiresPhysicalImageDimensions_m3355CD140DD153A14A948B5BD6DB1408E8A0901A_AdjustorThunk ();
// 0x000004B8 System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::set_requiresPhysicalImageDimensions(System.Boolean)
extern void Cinfo_set_requiresPhysicalImageDimensions_m9D03A815EA42EA80166E3B86133BE942EA43DA73_AdjustorThunk ();
// 0x000004B9 System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::get_supportsMutableLibrary()
extern void Cinfo_get_supportsMutableLibrary_mA0084B2031B77E3D57A1F9EC386D4F792AA81351_AdjustorThunk ();
// 0x000004BA System.Void UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::set_supportsMutableLibrary(System.Boolean)
extern void Cinfo_set_supportsMutableLibrary_mEC0925834ED74911F8F74DAC0B900FE755B2046F_AdjustorThunk ();
// 0x000004BB System.Int32 UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_mF3E717F0CC69CC001EFA348AC61CF901A303DFA8_AdjustorThunk ();
// 0x000004BC System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_m4E86F97DB5221B36E4E56964D652F231705D1CB9_AdjustorThunk ();
// 0x000004BD System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m091DB48FBB609CDAEC3CCEA410AA31F2535C2416_AdjustorThunk ();
// 0x000004BE System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_m58B48DF606C7865C18F85D32A9B8F367405C1328 ();
// 0x000004BF System.Boolean UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_mE579E93B5464711D0BF03E164E3744FFFDC43A12 ();
// 0x000004C0 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject> UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRTrackedObject,Unity.Collections.Allocator)
// 0x000004C1 System.Void UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem_Provider::set_library(UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary)
extern void Provider_set_library_m6F2877F7FA8A553EE928E6645704D8B221EDDFD7 ();
// 0x000004C2 System.Void UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem_Provider::Destroy()
extern void Provider_Destroy_mE1069B245ED2B4BDEC06EC8B0E9E5FBFB5B9800B ();
// 0x000004C3 System.Void UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem_Provider::.ctor()
extern void Provider__ctor_mB5F3534CE2168FA542BD9F930D94F8A2EB9552AF ();
// 0x000004C4 System.Boolean UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities::Equals(UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities)
extern void Capabilities_Equals_mAAFDCBAE52F3C1B309C45E48BD808DA78C7DC564_AdjustorThunk ();
// 0x000004C5 System.Boolean UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities::Equals(System.Object)
extern void Capabilities_Equals_mB45A6FB7362E73CC4755B2472B41C5B2A712A919_AdjustorThunk ();
// 0x000004C6 System.Int32 UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities::GetHashCode()
extern void Capabilities_GetHashCode_m3991EA229599E5B7AA537CE032DE18B460516A36_AdjustorThunk ();
// 0x000004C7 System.Boolean UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities::op_Equality(UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities)
extern void Capabilities_op_Equality_mA0E6EE3679220598BB92754B15236E0038DDAF73 ();
// 0x000004C8 System.Boolean UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities::op_Inequality(UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor_Capabilities)
extern void Capabilities_op_Inequality_m6D0E893644F1BD640DE1CC718DCD4B448722C1FC ();
// 0x000004C9 System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::Start()
extern void Provider_Start_mD50F13CC06EE1729EAE110ABD255FAC63793EE24 ();
// 0x000004CA System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::Stop()
extern void Provider_Stop_m25975AC9B019910481B63C255848CB770CFD3C29 ();
// 0x000004CB System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::Destroy()
extern void Provider_Destroy_m31B32525D0C7699CCD06CE773206BB418B4E0A26 ();
// 0x000004CC UnityEngine.XR.ARSubsystems.HumanSegmentationStencilMode UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::get_requestedHumanStencilMode()
extern void Provider_get_requestedHumanStencilMode_m18256398D8FE8F0542FCC4BB65C9EA3514FABAE0 ();
// 0x000004CD System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::set_requestedHumanStencilMode(UnityEngine.XR.ARSubsystems.HumanSegmentationStencilMode)
extern void Provider_set_requestedHumanStencilMode_m00780B697E372E9B41698CEEF37878EA0C4F60E4 ();
// 0x000004CE UnityEngine.XR.ARSubsystems.HumanSegmentationStencilMode UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::get_currentHumanStencilMode()
extern void Provider_get_currentHumanStencilMode_m5D6E29969695702B94EDB769874EC2D24A382548 ();
// 0x000004CF UnityEngine.XR.ARSubsystems.HumanSegmentationDepthMode UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::get_requestedHumanDepthMode()
extern void Provider_get_requestedHumanDepthMode_m15169D772194CA27D20C9550D195906501E2A8D7 ();
// 0x000004D0 System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::set_requestedHumanDepthMode(UnityEngine.XR.ARSubsystems.HumanSegmentationDepthMode)
extern void Provider_set_requestedHumanDepthMode_m13E63E8CF1595A488707D006DED37F11FB1A3DF8 ();
// 0x000004D1 UnityEngine.XR.ARSubsystems.HumanSegmentationDepthMode UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::get_currentHumanDepthMode()
extern void Provider_get_currentHumanDepthMode_m74F79DB0B223D85FB98369AEA1025D6163A23A1A ();
// 0x000004D2 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::TryGetHumanStencil(UnityEngine.XR.ARSubsystems.XRTextureDescriptor&)
extern void Provider_TryGetHumanStencil_m7C5C0D51BAA6B7856607DD089AF19859DA24EA6D ();
// 0x000004D3 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::TryAcquireHumanStencilCpuImage(UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo&)
extern void Provider_TryAcquireHumanStencilCpuImage_m0A0F53372BDC5692117AA6D266E8C4BB16AE2911 ();
// 0x000004D4 UnityEngine.XR.ARSubsystems.XRCpuImage_Api UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::get_humanStencilCpuImageApi()
extern void Provider_get_humanStencilCpuImageApi_m5FC7EE8966D1E76BAE1135170222934A1F610B80 ();
// 0x000004D5 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::TryGetHumanDepth(UnityEngine.XR.ARSubsystems.XRTextureDescriptor&)
extern void Provider_TryGetHumanDepth_m2375CEC3B42856A637D62DFB471C8A5F4C7F0A98 ();
// 0x000004D6 System.Boolean UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::TryAcquireHumanDepthCpuImage(UnityEngine.XR.ARSubsystems.XRCpuImage_Cinfo&)
extern void Provider_TryAcquireHumanDepthCpuImage_m42F193F062F56FF3BBDE3C65B0D75F3C1EC18C5E ();
// 0x000004D7 UnityEngine.XR.ARSubsystems.XRCpuImage_Api UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::get_humanDepthCpuImageApi()
extern void Provider_get_humanDepthCpuImageApi_m2E34052E2595468EE99BA0717E472460C8F6A28B ();
// 0x000004D8 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTextureDescriptor> UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::GetTextureDescriptors(UnityEngine.XR.ARSubsystems.XRTextureDescriptor,Unity.Collections.Allocator)
extern void Provider_GetTextureDescriptors_m6C6D5BF23FE555624A90F4D58D51E18924E1C77E ();
// 0x000004D9 System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::GetMaterialKeywords(System.Collections.Generic.List`1<System.String>&,System.Collections.Generic.List`1<System.String>&)
extern void Provider_GetMaterialKeywords_m3384CA3D7122556953D02D84FD79D1C2899C3DF6 ();
// 0x000004DA System.Void UnityEngine.XR.ARSubsystems.XROcclusionSubsystem_Provider::.ctor()
extern void Provider__ctor_mA606A7F882679DD5F680A8129A3E720240F92751 ();
// 0x000004DB System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider::Start()
extern void Provider_Start_m3C7BEE1FE7D772CDFA9F85954E99BE5221E0C619 ();
// 0x000004DC System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider::Stop()
extern void Provider_Stop_m1B77B544FB83D259FB48E593F6D9588606988E0F ();
// 0x000004DD System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider::Destroy()
extern void Provider_Destroy_m44B1A8963DD124B74CC43086FE505E152A410FCD ();
// 0x000004DE UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRParticipant> UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRParticipant,Unity.Collections.Allocator)
// 0x000004DF System.Void UnityEngine.XR.ARSubsystems.XRParticipantSubsystem_Provider::.ctor()
extern void Provider__ctor_m6001DA63BA8C0083F1108DA94BD0560F3328ED84 ();
// 0x000004E0 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::Start()
extern void Provider_Start_m7AE9B6E2114F33DEC961029A0FAC48D34E31D41F ();
// 0x000004E1 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::Stop()
extern void Provider_Stop_m69077CC0D1E396DAAA7BFFD006B77BA84F6F995D ();
// 0x000004E2 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::Destroy()
extern void Provider_Destroy_mC8502382CF0665866814C2099DDA6B1A6E0C23B0 ();
// 0x000004E3 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::GetBoundary(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<UnityEngine.Vector2>&)
extern void Provider_GetBoundary_m2C4BE79AE5170DC51ABD28DA0C27401ABD8F051C ();
// 0x000004E4 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.BoundedPlane> UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.BoundedPlane,Unity.Collections.Allocator)
// 0x000004E5 UnityEngine.XR.ARSubsystems.PlaneDetectionMode UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::get_requestedPlaneDetectionMode()
extern void Provider_get_requestedPlaneDetectionMode_m062ADF77A2CD37DD74A04C75892B71E7F3DFDF6F ();
// 0x000004E6 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::set_requestedPlaneDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void Provider_set_requestedPlaneDetectionMode_m81E255515CBA075567F35CCF75E58E3F04CC66F8 ();
// 0x000004E7 UnityEngine.XR.ARSubsystems.PlaneDetectionMode UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::get_currentPlaneDetectionMode()
extern void Provider_get_currentPlaneDetectionMode_mD04D0393449872D56C83A519B63BA5E9805A7EDE ();
// 0x000004E8 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_Provider::.ctor()
extern void Provider__ctor_m7AD4BD2425F46D263463BCFE2237CC592391C60C ();
// 0x000004E9 System.String UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_id()
extern void Cinfo_get_id_m2403BAC8C8F4BF231808C7B49F4C577E0856726F_AdjustorThunk ();
// 0x000004EA System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_id(System.String)
extern void Cinfo_set_id_mB3D68A1AEE80E5941EBE7DEFFFB34AC44AB92A8E_AdjustorThunk ();
// 0x000004EB System.Type UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_m0B590545FD801FEF5F7F41CBDCCB1FD1A02D191D_AdjustorThunk ();
// 0x000004EC System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_mDACCFA76DE27D24DAD241496ABCE1606EA51E97C_AdjustorThunk ();
// 0x000004ED System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_supportsHorizontalPlaneDetection()
extern void Cinfo_get_supportsHorizontalPlaneDetection_m63C8441C4FC4ABC35300ADE1F54AB2888A1B39C7_AdjustorThunk ();
// 0x000004EE System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_supportsHorizontalPlaneDetection(System.Boolean)
extern void Cinfo_set_supportsHorizontalPlaneDetection_m03E0099B1BAC214B26A2F4D476DA2A7D5AE01EF9_AdjustorThunk ();
// 0x000004EF System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_supportsVerticalPlaneDetection()
extern void Cinfo_get_supportsVerticalPlaneDetection_mCCE776E1BB11FB03839F91E120320DCFBD6E1884_AdjustorThunk ();
// 0x000004F0 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_supportsVerticalPlaneDetection(System.Boolean)
extern void Cinfo_set_supportsVerticalPlaneDetection_m3F6EDFE99022E0D214BEAD727F1AB7A77370DBF4_AdjustorThunk ();
// 0x000004F1 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_supportsArbitraryPlaneDetection()
extern void Cinfo_get_supportsArbitraryPlaneDetection_mD7F89437FBB000DA51ACDD62370FC94D74E0FD99_AdjustorThunk ();
// 0x000004F2 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_supportsArbitraryPlaneDetection(System.Boolean)
extern void Cinfo_set_supportsArbitraryPlaneDetection_m26BE6DBE028AC98927B6E9349599DAF9E4213D18_AdjustorThunk ();
// 0x000004F3 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_supportsBoundaryVertices()
extern void Cinfo_get_supportsBoundaryVertices_m7CE566F0032F95077CB95A1C366D0CD42F32AE49_AdjustorThunk ();
// 0x000004F4 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_supportsBoundaryVertices(System.Boolean)
extern void Cinfo_set_supportsBoundaryVertices_m8B0526794B4738A739CD44345ACBA74639B0F1F5_AdjustorThunk ();
// 0x000004F5 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::get_supportsClassification()
extern void Cinfo_get_supportsClassification_m14F960125988547CFF0E554530B66B20BE3E691F_AdjustorThunk ();
// 0x000004F6 System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::set_supportsClassification(System.Boolean)
extern void Cinfo_set_supportsClassification_m6C3B970AD60C50F76244036E320FB7327A0493D4_AdjustorThunk ();
// 0x000004F7 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_m936E7CA82DA297BC58F64EDF44B95E773814201F_AdjustorThunk ();
// 0x000004F8 System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_mE47F4CD38B3E6F3814A12A842B633064533C2A1B_AdjustorThunk ();
// 0x000004F9 System.Int32 UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m3F8925D6A3131763126D23787186F7696FD5C63B_AdjustorThunk ();
// 0x000004FA System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_mED6DCEBD3B06F1031BEB3A9391C4F25DDBAE0850 ();
// 0x000004FB System.Boolean UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_mCB1CE860BB90AD58801FB9AA8618BF780E235FE3 ();
// 0x000004FC System.Void UnityEngine.XR.ARSubsystems.Promise`1_ImmediatePromise::OnKeepWaiting()
// 0x000004FD System.Void UnityEngine.XR.ARSubsystems.Promise`1_ImmediatePromise::.ctor(T)
// 0x000004FE System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::Start()
extern void Provider_Start_mA565B894F858E9176BD59D05BCA2656E89E68184 ();
// 0x000004FF System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::Stop()
extern void Provider_Stop_mADDEF920450D92ACE94E849F5179B62487BEABE2 ();
// 0x00000500 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::Destroy()
extern void Provider_Destroy_mF35AF8F2635ED60E06B102E0AB210D350D0A77D3 ();
// 0x00000501 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::TryAddRaycast(UnityEngine.Vector2,System.Single,UnityEngine.XR.ARSubsystems.XRRaycast&)
extern void Provider_TryAddRaycast_m0C6021D62486AB3DEDFC3271F0E870E6164F02A8 ();
// 0x00000502 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::TryAddRaycast(UnityEngine.Ray,System.Single,UnityEngine.XR.ARSubsystems.XRRaycast&)
extern void Provider_TryAddRaycast_m41C77266A25B9178723890A5F425FF6C3740F79E ();
// 0x00000503 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::RemoveRaycast(UnityEngine.XR.ARSubsystems.TrackableId)
extern void Provider_RemoveRaycast_mCE8AD69CF81315A58D0B693502C87760E7510FFB ();
// 0x00000504 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRRaycast> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRRaycast,Unity.Collections.Allocator)
extern void Provider_GetChanges_m60D267A3A9EC93BE9769C31F17B45CB954F8CE3F ();
// 0x00000505 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::Raycast(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void Provider_Raycast_m6063A859AC10ACF7F27A4AE0AAD82C3A0DF831AE ();
// 0x00000506 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::Raycast(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void Provider_Raycast_mF885EFF5FFD12196F53C9FE405EC523020C35CE2 ();
// 0x00000507 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_Provider::.ctor()
extern void Provider__ctor_m1A46FEF4E5C93F21E2C802E59B99507E79C0F2C1 ();
// 0x00000508 System.String UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::get_id()
extern void Cinfo_get_id_m0418C70EB8FBC60BB9B83053AA5175AEFE31CAF8_AdjustorThunk ();
// 0x00000509 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::set_id(System.String)
extern void Cinfo_set_id_mFFFBB447D6D0DF4A27428D414FE23BCCB16D78D1_AdjustorThunk ();
// 0x0000050A System.Type UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_m0E0FEE226FD08939BF83B7B2644BEC6362BA157B_AdjustorThunk ();
// 0x0000050B System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_m00E3D77F5C33C670222169A514C9804886D4FD72_AdjustorThunk ();
// 0x0000050C System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::get_supportsViewportBasedRaycast()
extern void Cinfo_get_supportsViewportBasedRaycast_m323FE06DA2E4222E6BF4CE89541DB4630CB254B3_AdjustorThunk ();
// 0x0000050D System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::set_supportsViewportBasedRaycast(System.Boolean)
extern void Cinfo_set_supportsViewportBasedRaycast_mBA63D727FCB8E5FE2EF1BD5CA2192B54768DEF0D_AdjustorThunk ();
// 0x0000050E System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::get_supportsWorldBasedRaycast()
extern void Cinfo_get_supportsWorldBasedRaycast_mFBB112B068EE22D519CAC45E35255D6FDACCAE74_AdjustorThunk ();
// 0x0000050F System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::set_supportsWorldBasedRaycast(System.Boolean)
extern void Cinfo_set_supportsWorldBasedRaycast_mDDA30FB5ADF2800F44467B727F19668AAE05AFEF_AdjustorThunk ();
// 0x00000510 UnityEngine.XR.ARSubsystems.TrackableType UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::get_supportedTrackableTypes()
extern void Cinfo_get_supportedTrackableTypes_mCC57E28DFCE93ECA772B1DE2E3E49AD030D79424_AdjustorThunk ();
// 0x00000511 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::set_supportedTrackableTypes(UnityEngine.XR.ARSubsystems.TrackableType)
extern void Cinfo_set_supportedTrackableTypes_m91638341F04B3BC3688660AFFE66308C19B13C6B_AdjustorThunk ();
// 0x00000512 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::get_supportsTrackedRaycasts()
extern void Cinfo_get_supportsTrackedRaycasts_m7259F48B08EA9AEEE4E0966758AB3600F9E62928_AdjustorThunk ();
// 0x00000513 System.Void UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::set_supportsTrackedRaycasts(System.Boolean)
extern void Cinfo_set_supportsTrackedRaycasts_mF82AA23E5BE9FB36864FD268E7B46A2E043E75EB_AdjustorThunk ();
// 0x00000514 System.Int32 UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_mCC56E718130099F9809415C6C1CE9DC981F06211_AdjustorThunk ();
// 0x00000515 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_mA57BDAF996011C56A0017EFBBB805683CFA03452_AdjustorThunk ();
// 0x00000516 System.String UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::ToString()
extern void Cinfo_ToString_m9E094799A4E711569550478753C87EE9FC40DC24_AdjustorThunk ();
// 0x00000517 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_mC668E44130D6114FFD62DA8470840EE5E39DBBB5_AdjustorThunk ();
// 0x00000518 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_mB012A524026CD2F05A29AB06C6E364D2B2BBD773 ();
// 0x00000519 System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_mE9E256858FF976A18E836CDE86AA6AF00DC20828 ();
// 0x0000051A System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::Resume()
extern void Provider_Resume_m8CAB34CF062DD4D2BE34F606AA14F9F78BB6904E ();
// 0x0000051B System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::Pause()
extern void Provider_Pause_m29AA9017C4F53BEC8B24467A7C469512F421A97A ();
// 0x0000051C System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void Provider_Update_m3C7FA8783EB5ED76A0A022CB87D840921D94941E ();
// 0x0000051D System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams,UnityEngine.XR.ARSubsystems.Configuration)
extern void Provider_Update_m1E2645995751B235BB20D6E7CD413AD66CEF396A ();
// 0x0000051E UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_requestedFeatures()
extern void Provider_get_requestedFeatures_m86F0B383BB349C56A01C6D745DD270B4FD02DA96 ();
// 0x0000051F UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_requestedTrackingMode()
extern void Provider_get_requestedTrackingMode_mA3745867DA98C76877E1BCA45271D9B05BE16B62 ();
// 0x00000520 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::set_requestedTrackingMode(UnityEngine.XR.ARSubsystems.Feature)
extern void Provider_set_requestedTrackingMode_mCF43B7949FAF18EA2BB8B5F77A7B12F8669BB087 ();
// 0x00000521 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_currentTrackingMode()
extern void Provider_get_currentTrackingMode_m4052ECEBABD1313C1E889CCC12D9D7C6F71812F3 ();
// 0x00000522 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.ConfigurationDescriptor> UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::GetConfigurationDescriptors(Unity.Collections.Allocator)
extern void Provider_GetConfigurationDescriptors_m1D29E783FF6BB21848ED664E649A5C388E27240B ();
// 0x00000523 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::Destroy()
extern void Provider_Destroy_m0BFF3C770F8D3DDEDB8BBE1E3E540B33722CF8AC ();
// 0x00000524 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::Reset()
extern void Provider_Reset_m43A334CBFFF6A9D9FC404A0FD5DC05A41CE9CAE1 ();
// 0x00000525 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::OnApplicationPause()
extern void Provider_OnApplicationPause_m02759BAEDA12223C11B002BE7CEAA14C3308C4AD ();
// 0x00000526 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::OnApplicationResume()
extern void Provider_OnApplicationResume_mD415E31314285E9277FF8C9B5F3B39E34417B736 ();
// 0x00000527 System.IntPtr UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_nativePtr()
extern void Provider_get_nativePtr_mEEB293C5FF1BBA91207EABA24752E9DF7A715609 ();
// 0x00000528 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionAvailability> UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::GetAvailabilityAsync()
extern void Provider_GetAvailabilityAsync_m9CC6F74169601931E94DA1177C34A543048B0A01 ();
// 0x00000529 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionInstallationStatus> UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::InstallAsync()
extern void Provider_InstallAsync_m074282442B17B260C246A17F8D7E1C4E947A6017 ();
// 0x0000052A UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_trackingState()
extern void Provider_get_trackingState_m9E8D77D2BA6BD8F3508CFA530482C6D790DCFF35 ();
// 0x0000052B UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_notTrackingReason()
extern void Provider_get_notTrackingReason_m3EF060E8F7A23B6CD196E1B7FCF2F34457B065AA ();
// 0x0000052C System.Guid UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_sessionId()
extern void Provider_get_sessionId_mD3AED2746B5F6920BD6BCC4234D6B9189CCF4BFA ();
// 0x0000052D System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_matchFrameRateEnabled()
extern void Provider_get_matchFrameRateEnabled_mF335F53CADFD3D8BC3FC07BE5EAEBC9DB77788BB ();
// 0x0000052E System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_matchFrameRateRequested()
extern void Provider_get_matchFrameRateRequested_mE1E128BE57DC7CE46CD39CE4C196136D1CA0D41D ();
// 0x0000052F System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::set_matchFrameRateRequested(System.Boolean)
extern void Provider_set_matchFrameRateRequested_mE5B0D38037143DC102C7B2729ECA3534D9E99DAE ();
// 0x00000530 System.Int32 UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::get_frameRate()
extern void Provider_get_frameRate_m55D5E1C5FDF9ADAB43DE7E3F74C7A463ECA63F33 ();
// 0x00000531 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystem_Provider::.ctor()
extern void Provider__ctor_m890EE1FF005D8F1C10E3F1F4B0B9C37D9ECD29DA ();
// 0x00000532 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::get_supportsInstall()
extern void Cinfo_get_supportsInstall_m5FBFD4D2F10A6A46F66F4EFBC61EA14DE0FEED99_AdjustorThunk ();
// 0x00000533 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::set_supportsInstall(System.Boolean)
extern void Cinfo_set_supportsInstall_mD74EB42C503AC5393E630A56E3AE579FE1558660_AdjustorThunk ();
// 0x00000534 System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::get_supportsMatchFrameRate()
extern void Cinfo_get_supportsMatchFrameRate_mE591F09F87EA8F4E5563039C47A6331E6AF31895_AdjustorThunk ();
// 0x00000535 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::set_supportsMatchFrameRate(System.Boolean)
extern void Cinfo_set_supportsMatchFrameRate_m2B92004D3F2E01EA5DDFBF5F928C5604E68B8D21_AdjustorThunk ();
// 0x00000536 System.String UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::get_id()
extern void Cinfo_get_id_m998EB4BD213159391A0A66FD6C002C1CE5CD14E8_AdjustorThunk ();
// 0x00000537 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::set_id(System.String)
extern void Cinfo_set_id_mC4FF3C524E18065C55B5142D58FBD58A66479A41_AdjustorThunk ();
// 0x00000538 System.Type UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::get_subsystemImplementationType()
extern void Cinfo_get_subsystemImplementationType_m63942F2B0929DF14EB7885E7553A9970FBD3E108_AdjustorThunk ();
// 0x00000539 System.Void UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::set_subsystemImplementationType(System.Type)
extern void Cinfo_set_subsystemImplementationType_m3C759AEC2943DE059B20AA7F5A5B932B7432473C_AdjustorThunk ();
// 0x0000053A System.Int32 UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m06E1060A5995A0C346AB7C0E56CCAC4BEC758A6E_AdjustorThunk ();
// 0x0000053B System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo)
extern void Cinfo_Equals_mC919333F29E857CC3F929451A080AC4A38385E01_AdjustorThunk ();
// 0x0000053C System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m2118FE6BBF6355D645E72BDD6662CF313B8E94EB_AdjustorThunk ();
// 0x0000053D System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Equality_mCDF32E8CB0B505E476F92FD9A3FAAD6C46BF20DA ();
// 0x0000053E System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo,UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor_Cinfo)
extern void Cinfo_op_Inequality_mA173B6B47123F27CE69E7C18FECEC6871CB08A99 ();
// 0x0000053F System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Api_OnImageRequestCompleteDelegate::.ctor(System.Object,System.IntPtr)
extern void OnImageRequestCompleteDelegate__ctor_mD43059752F7624FFA94F613205E495EC605E7D51 ();
// 0x00000540 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Api_OnImageRequestCompleteDelegate::Invoke(UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversionStatus,UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams,System.IntPtr,System.Int32,System.IntPtr)
extern void OnImageRequestCompleteDelegate_Invoke_m33E9D6285A745EA2D0F9E06424DD40AEA7B2CC04 ();
// 0x00000541 System.IAsyncResult UnityEngine.XR.ARSubsystems.XRCpuImage_Api_OnImageRequestCompleteDelegate::BeginInvoke(UnityEngine.XR.ARSubsystems.XRCpuImage_AsyncConversionStatus,UnityEngine.XR.ARSubsystems.XRCpuImage_ConversionParams,System.IntPtr,System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern void OnImageRequestCompleteDelegate_BeginInvoke_m0EAA6B016398F7313486563F2F010843FB6A0553 ();
// 0x00000542 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Api_OnImageRequestCompleteDelegate::EndInvoke(System.IAsyncResult)
extern void OnImageRequestCompleteDelegate_EndInvoke_m68F9AA715E179A2D6E7262A667F63B6F555490B0 ();
// 0x00000543 System.IntPtr UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo::get_dataPtr()
extern void Cinfo_get_dataPtr_m860037B7497AD33D5165C395EB3AFFAEA0F6BADC_AdjustorThunk ();
// 0x00000544 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo::get_dataLength()
extern void Cinfo_get_dataLength_mFBEFD08EFEE2017356874B6E8AE6303597AD3D36_AdjustorThunk ();
// 0x00000545 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo::get_rowStride()
extern void Cinfo_get_rowStride_m6CE40AD415A5A2F2476C3B91D25E93099A52102E_AdjustorThunk ();
// 0x00000546 System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo::get_pixelStride()
extern void Cinfo_get_pixelStride_m0D02EC6E5A6B35E13DE45915714A05A15BEFE59C_AdjustorThunk ();
// 0x00000547 System.Void UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo::.ctor(System.IntPtr,System.Int32,System.Int32,System.Int32)
extern void Cinfo__ctor_mF63E5ED8C476C4959A3BD447AC19EAD8B5179F3E_AdjustorThunk ();
// 0x00000548 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo::Equals(UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo)
extern void Cinfo_Equals_mAC6C6B00AE4DD3426FAD7123B24AB255D468C12B_AdjustorThunk ();
// 0x00000549 System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m7AB4E01E78BD52CE783E6C327A2534FF76C572A1_AdjustorThunk ();
// 0x0000054A System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo::op_Equality(UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo,UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo)
extern void Cinfo_op_Equality_m1ABC4C273CBEF7D200EBE5CD30342A252C55E19F ();
// 0x0000054B System.Boolean UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo::op_Inequality(UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo,UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo)
extern void Cinfo_op_Inequality_m429704A6D661F16D034470AD32CA75E29101814A ();
// 0x0000054C System.Int32 UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_m6BB3D041840DCC39ED4E94BE8F415BECC722EBEB_AdjustorThunk ();
// 0x0000054D System.String UnityEngine.XR.ARSubsystems.XRCpuImage_Plane_Cinfo::ToString()
extern void Cinfo_ToString_m207241F57F3BCD62CA497E9746D67B5DCE5028E4_AdjustorThunk ();
static Il2CppMethodPointer s_methodPointers[1357] = 
{
	XRAnchor_get_defaultValue_m457A914338467F05B7928AF1657C2447DDD38B96,
	XRAnchor__ctor_m590E9A5E27E9C84C8C2AD35012271FE9C10A005E_AdjustorThunk,
	XRAnchor__ctor_m49CDF2A63F1027B771BC697C7A0CDB742C9DE739_AdjustorThunk,
	XRAnchor_get_trackableId_m7BD89E3F1664C126D09D8DD141EA18E8A9933711_AdjustorThunk,
	XRAnchor_get_pose_m7CA50F0FCB9FE7A6FB60C6AFD33B62AF4BE0CB1A_AdjustorThunk,
	XRAnchor_get_trackingState_m6C73E20FCB9A2E33666BD07816D04099D61EF3EF_AdjustorThunk,
	XRAnchor_get_nativePtr_m19C61EBCF7D12C860A76C60CAFF2E7B0FBDFF137_AdjustorThunk,
	XRAnchor_get_sessionId_m3545F898B9A294B7D70434F25F439A1891D29FEE_AdjustorThunk,
	XRAnchor_GetHashCode_m593C577C80CA85C9354B112F494F4934E17EA369_AdjustorThunk,
	XRAnchor_Equals_m89DED9F057036C85F1987E32A651A9D59D60AAD7_AdjustorThunk,
	XRAnchor_Equals_m0EFEDC085E8C3080D6868D1540B933426F72A4A2_AdjustorThunk,
	XRAnchor_op_Equality_m21E2BB8C91610B5AF27BC05A58705E74F1498800,
	XRAnchor_op_Inequality_m01B9F6BCF82A24457752761541A482983357982E,
	XRAnchor__cctor_mC3E7FD4E2A9B8EF79EC58A30B00CF8DC481CB3FA,
	XRAnchorSubsystem__ctor_mB1BE6B896975D4F735B5CBBB3F30B7E350435A16,
	XRAnchorSubsystem_OnStart_m120BD231AFCC0B894C3B3BFF9A7343F0B5F53BB7,
	XRAnchorSubsystem_OnStop_m60282819F70508F3788AB414E64174B07C9DA935,
	XRAnchorSubsystem_OnDestroyed_m4D13D1C9010C7CECAECDF8BE3342D06782CDE172,
	XRAnchorSubsystem_GetChanges_mA9F45640A583B5E123F075733A25637AC4444D25,
	XRAnchorSubsystem_TryAddAnchor_m9F1AF4816CB4E057B7865296416B5F82AB40E381,
	XRAnchorSubsystem_TryAttachAnchor_m1D49B360C92C7448A369A63A28DE509DDFC8051C,
	XRAnchorSubsystem_TryRemoveAnchor_m95FFE9DECD2CA03F5B9C4C2B1DA58891725C43EE,
	NULL,
	XRAnchorSubsystemDescriptor_get_supportsTrackableAttachments_mC606DA1BE171F5FDFE670DD382FADA7A9E457C2A,
	XRAnchorSubsystemDescriptor_set_supportsTrackableAttachments_m6D69CEBBEAEFE86FAC483223DC138F53D926C21E,
	XRAnchorSubsystemDescriptor_Create_m1E770B2B67D3E34E14C84C952F4EFF171B020F1A,
	XRAnchorSubsystemDescriptor__ctor_m8EE0138294476EBE73B28511776747A3E16021F6,
	XRReferencePoint_get_defaultValue_mCCFAF4140E24AC2FDF1C8D19043E57B6BFEAC0AD,
	XRReferencePoint__ctor_mF823D8DC414C0B110A0C29F28668C1F3DA2B2997_AdjustorThunk,
	XRReferencePoint__ctor_m816965A70CDD2827DE0808A49B135E411E8532BB_AdjustorThunk,
	XRReferencePoint_get_trackableId_m6D53542802F2444CE58861B8868274F9A8296D88_AdjustorThunk,
	XRReferencePoint_get_pose_mA4320629B8C7AE23D97FCD8E2C5FB9C9FB6AED9C_AdjustorThunk,
	XRReferencePoint_get_trackingState_mBA0DB4050B734039D22D0ECF69CD6E8896DF52B8_AdjustorThunk,
	XRReferencePoint_get_nativePtr_mE9EC85AD0E4976145CB0EDC4A74AA5BB076C5789_AdjustorThunk,
	XRReferencePoint_get_sessionId_m5DCAF1725B8A29481940252D80634C99A3C2F0D0_AdjustorThunk,
	XRReferencePoint_GetHashCode_mD7BC968C92D3CC25E7D06502570A94B104F9E32C_AdjustorThunk,
	XRReferencePoint_Equals_mA58F0C1C266D740037A7D6700857A5E739160AF8_AdjustorThunk,
	XRReferencePoint_Equals_mD22BFD6609737E5CC6A31D2C1B519CD5207C89BC_AdjustorThunk,
	XRReferencePoint_op_Equality_mBE34F72FA0469B0D2B97620A8C7F53C01E85A8BE,
	XRReferencePoint_op_Inequality_m675724084220C79C2F9A35E8D8462DD13146DDD0,
	XRReferencePoint__cctor_m3A7635582CA05369AD6537861329B761A106DB82,
	XRReferencePointSubsystem__ctor_mD93381DE24CA18A7BA022014E77BEBA9AA2CD6E9,
	XRReferencePointSubsystem_OnStart_m60B74FB6CD375125B7623A7D0D8D4B7602B53AD8,
	XRReferencePointSubsystem_OnStop_mB170405D1F702B112F844EF70F71B5AA54C345B3,
	XRReferencePointSubsystem_OnDestroyed_mCC9C916A1FA37AA028305374BF13E962E2260260,
	XRReferencePointSubsystem_GetChanges_mE0EC8049CED1EA604A751066DB97430E803BE487,
	XRReferencePointSubsystem_TryAddReferencePoint_m55C922BC7F9943136A05B7E883D044CFBD5E4B87,
	XRReferencePointSubsystem_TryAttachReferencePoint_mFC09929BC0AF19465D22E30685A9C213BE434B8E,
	XRReferencePointSubsystem_TryRemoveReferencePoint_m3F404A6F9DD63129EE44497554CC59CE8396DC22,
	NULL,
	XRReferencePointSubsystemDescriptor_get_supportsTrackableAttachments_mA94E2928B96D7F5CC69B8D225F8FADDCDC80922D,
	XRReferencePointSubsystemDescriptor_set_supportsTrackableAttachments_mA4F7709D4C170D414F862490ABBF0090DE46A8AB,
	XRReferencePointSubsystemDescriptor_Create_m4E9D9DF5FCE2FE3F8672653BC733F87D3A4327D0,
	XRReferencePointSubsystemDescriptor__ctor_mB5AD6FF2521FF148612D7662DF93BD6CA68069B2,
	XRCameraConfiguration_get_width_m8ECF3F57F94FC3F97FC1FE9CAAE4D1DCAD39F067_AdjustorThunk,
	XRCameraConfiguration_get_height_m92B6D4553AE4900BDF258E6F224ECACD06E30C40_AdjustorThunk,
	XRCameraConfiguration_get_resolution_mDED625C9D21911EF0D05C49DCBC589FE7915C2B2_AdjustorThunk,
	XRCameraConfiguration_get_framerate_m76E136DC6045EB254C009C56A3BC667D7EBB3C77_AdjustorThunk,
	XRCameraConfiguration_get_nativeConfigurationHandle_mEB607EED8AC9829EDAE9D969F066062FB6B2C300_AdjustorThunk,
	XRCameraConfiguration__ctor_mC618794BE9A0EEE69A332A6FD53A8F39997FE372_AdjustorThunk,
	XRCameraConfiguration__ctor_m4BC0ECCE825EEBD919A474A5D1E0E68A318A2237_AdjustorThunk,
	XRCameraConfiguration_ToString_m702D1FD8278A19B797B0DDA0C37C08D4F41D4535_AdjustorThunk,
	XRCameraConfiguration_GetHashCode_m05CDBC4B6384E712DF61BB1E387014DE4FEDA04C_AdjustorThunk,
	XRCameraConfiguration_Equals_m63FEF3901B78AE9D56217FE49666AC7BCECD4C38_AdjustorThunk,
	XRCameraConfiguration_Equals_mB4BD44CDAA94CFC33E88A0205EAEF098220F1E97_AdjustorThunk,
	XRCameraConfiguration_op_Equality_mD226534E63EB64C9AE11264609C249004FC843DA,
	XRCameraConfiguration_op_Inequality_mFCEDF0D1D7723413124B15E044F9B542400BB3FD,
	XRCameraFrame_get_timestampNs_m0FAE10EDEEDF94C0892E8800E8CB693F64B83B14_AdjustorThunk,
	XRCameraFrame_get_averageBrightness_m153B54A25E5013B090D737BB3BC2DCF300C88E92_AdjustorThunk,
	XRCameraFrame_get_averageColorTemperature_m18EFBA25B4D6580D16CE859CECD1A0767CFBA006_AdjustorThunk,
	XRCameraFrame_get_colorCorrection_m21EA821139C87463A369E9D308D3794A398D7A2C_AdjustorThunk,
	XRCameraFrame_get_projectionMatrix_m1AFF19568E4F29BDC16B943B34A8C988CA8382A1_AdjustorThunk,
	XRCameraFrame_get_displayMatrix_mE7C0D233A017D72EA36CC4879F49BC872D49B2A5_AdjustorThunk,
	XRCameraFrame_get_trackingState_mB45FB220A1EDE8A180217EC6F710A9651E61682A_AdjustorThunk,
	XRCameraFrame_get_nativePtr_mA8681A34CB2D48614EBF236A61AD0649C6D11E7E_AdjustorThunk,
	XRCameraFrame_get_properties_mE2BF04FF350BC7B1FF375CB5C4E703211133DEF5_AdjustorThunk,
	XRCameraFrame_get_averageIntensityInLumens_mD12108393415DB6E8CF9E8F4787B5BEFC526D460_AdjustorThunk,
	XRCameraFrame_get_exposureDuration_mDEE7FB820A5F0D72393A15B920C0491CB6949070_AdjustorThunk,
	XRCameraFrame_get_exposureOffset_m3CECB3069D57AA892A6D5B9FD9B78CBDC6A712F0_AdjustorThunk,
	XRCameraFrame_get_mainLightIntensityLumens_m6D3A8EE7DB14B2E02CE989F62818D3CF07286ABF_AdjustorThunk,
	XRCameraFrame_get_mainLightColor_mEE9EEAC1AC653DCEFD91EB190B762F427B2EB698_AdjustorThunk,
	XRCameraFrame_get_mainLightDirection_mCB3295F9A3FE677D53B8AF5C71DF76A3174B4072_AdjustorThunk,
	XRCameraFrame_get_ambientSphericalHarmonics_m34B37F0029843DB7846400F943C84157AFB7F043_AdjustorThunk,
	XRCameraFrame_get_cameraGrain_m72F990B7526591FE40C70083779B0B4F1CE14E35_AdjustorThunk,
	XRCameraFrame_get_noiseIntensity_mD42D8F6D6374209752F61E8A63DD98D6CA5CAEC2_AdjustorThunk,
	XRCameraFrame_get_hasTimestamp_m08EAA6466145202B9E9E612895F452F35CB6F673_AdjustorThunk,
	XRCameraFrame_get_hasAverageBrightness_m08A69E2E7D6D477EB44CBBB282F82476149DA292_AdjustorThunk,
	XRCameraFrame_get_hasAverageColorTemperature_m434C9B70523C3ED04A3BD3C5DCCE25199387168F_AdjustorThunk,
	XRCameraFrame_get_hasColorCorrection_mE00653B8C09496E3FD15089827105AB65B2C108F_AdjustorThunk,
	XRCameraFrame_get_hasProjectionMatrix_m614664027EA1D284BE2C5B5FD5ED2D84E88BB39A_AdjustorThunk,
	XRCameraFrame_get_hasDisplayMatrix_m545C335B1B2849139A11439858E4FEE82741CCA9_AdjustorThunk,
	XRCameraFrame_get_hasAverageIntensityInLumens_mFFFFCBBC3BBB69F1C1F7FA47DBECF72D3F988813_AdjustorThunk,
	XRCameraFrame_get_hasExposureDuration_m8ACE9F1BCE652BE74152A7641B6AE15C94BAAE56_AdjustorThunk,
	XRCameraFrame_get_hasExposureOffset_m1D09B799890FF7214EA2F3C881E4B2673E43992A_AdjustorThunk,
	XRCameraFrame_get_hasMainLightIntensityLumens_m28BBB2A9DDA43CD6EB2D872F045E1AA41CB2B016_AdjustorThunk,
	XRCameraFrame_get_hasMainLightColor_mA219350CE0AC141BD931021A46A9862A9340F7C5_AdjustorThunk,
	XRCameraFrame_get_hasMainLightDirection_m985FBD9C1BFBAF5C97B67BBFDB25FDA8B5231280_AdjustorThunk,
	XRCameraFrame_get_hasAmbientSphericalHarmonics_m21AB7CA0BC5E522208735F1D670053518D407D08_AdjustorThunk,
	XRCameraFrame_get_hasCameraGrain_m2AB571C0D95AEE9F87E609BF40F44A005AD73B20_AdjustorThunk,
	XRCameraFrame_get_hasNoiseIntensity_m0959F24C2CE910180B80489C8B43C211EE6E9ED7_AdjustorThunk,
	XRCameraFrame_TryGetTimestamp_m0930188029B9A820EE23C68EE9FA3A45BFD76928_AdjustorThunk,
	XRCameraFrame_TryGetAverageBrightness_mC1F49F26D2531DE89DAC1529E7D8C1506ED78C1C_AdjustorThunk,
	XRCameraFrame_TryGetAverageColorTemperature_m0A7E61255CE92478AB82FE5F013ED04089C857F2_AdjustorThunk,
	XRCameraFrame_TryGetProjectionMatrix_mA846AEFFB2CDA29D49756C5396A4F6FB17EFF5A3_AdjustorThunk,
	XRCameraFrame_TryGetDisplayMatrix_m3634FB652629917956DB5EA25F37E53F84BF9E4F_AdjustorThunk,
	XRCameraFrame_TryGetAverageIntensityInLumens_m6F0AA92D0F6E0FD01D2B2F342B8F080B11C424D0_AdjustorThunk,
	XRCameraFrame_Equals_m5795BA83EB6809C67D23D58FA2D9BCF8F7664EA8_AdjustorThunk,
	XRCameraFrame_Equals_m72CE1B12ABF7FAB123A2898E4DCBFFB5EE088777_AdjustorThunk,
	XRCameraFrame_op_Equality_m39912ADC47C063E96C2E98E7FF5F9E16E1BE7069,
	XRCameraFrame_op_Inequality_m03D8D007CD75133E461110920866206D695740D3,
	XRCameraFrame_GetHashCode_mB1F696089EB4E7E021E3383006178C023928D124_AdjustorThunk,
	XRCameraFrame_ToString_mBFD4510AB5312CCAF0C115734AF91E8BF6C480AC_AdjustorThunk,
	XRCameraIntrinsics_get_focalLength_m9D090B0B207598F353860CB5735B85B78827C93F_AdjustorThunk,
	XRCameraIntrinsics_get_principalPoint_mC66F07CA90FAA4A8A94BDF2A62641196C9DD6DEC_AdjustorThunk,
	XRCameraIntrinsics_get_resolution_m6572536639CC3A6F1A1E1DE50971522B2933355D_AdjustorThunk,
	XRCameraIntrinsics__ctor_m40F0632FD31F48C9FFB073ED2C5516D94D172633_AdjustorThunk,
	XRCameraIntrinsics_Equals_mF8BFEADD1C696A2DC28CCA8F07620B0376967EB1_AdjustorThunk,
	XRCameraIntrinsics_Equals_m9BE2EE15CDEC43C62816925AF33D02974592CFA2_AdjustorThunk,
	XRCameraIntrinsics_op_Equality_mB198DA468A2DEB3F6D497DA72851ECF39740874D,
	XRCameraIntrinsics_op_Inequality_mB74D8A6DCDE30C21E121941C206767C8507E6817,
	XRCameraIntrinsics_GetHashCode_m3A63BB112EB34BF80692DEF316C220C129041F26_AdjustorThunk,
	XRCameraIntrinsics_ToString_mDFEFA74E34AB9AAAC4221614F3BBBFD19D454E37_AdjustorThunk,
	XRCameraParams_get_zNear_mD3785EC9C402C69AAFAF24AB16BA946963509F04_AdjustorThunk,
	XRCameraParams_set_zNear_m6DD4FC25349D4D0805EB2BE360CBE4954EADB951_AdjustorThunk,
	XRCameraParams_get_zFar_m91AE073EEFBFA27403EF2D07A00AB814A0E9E767_AdjustorThunk,
	XRCameraParams_set_zFar_mC5FDDD40DD4FAF1E73C8A45042E14810233A06C8_AdjustorThunk,
	XRCameraParams_get_screenWidth_m284D5554D15C661CB76315320E728900F234B8FC_AdjustorThunk,
	XRCameraParams_set_screenWidth_m9047176614CF36F4D0D7D7B509FF8748BB87FC50_AdjustorThunk,
	XRCameraParams_get_screenHeight_mBAE5A3EEA77FD635ABCA56D62B80270356F350A4_AdjustorThunk,
	XRCameraParams_set_screenHeight_m16D3F7A4907E471B3433D965A96E7E74B3956D3B_AdjustorThunk,
	XRCameraParams_get_screenOrientation_mF47570D9A01E6E868BD0A77E89942B47B9A5A86B_AdjustorThunk,
	XRCameraParams_set_screenOrientation_m3539BE3608BE3DFCA6890DF6F6C38DE34D244CB6_AdjustorThunk,
	XRCameraParams_Equals_m13F7C4A8684FAE1B19033C4D4F173A203BD31B6C_AdjustorThunk,
	XRCameraParams_Equals_m5C141227483E4A8477B429232F18168069A2A871_AdjustorThunk,
	XRCameraParams_op_Equality_m5631553B4B12F4A44972CD37522F1AE449FFF2C5,
	XRCameraParams_op_Inequality_m3CBEF158E25B5B1B51E946BC72EFBD1CBEA20072,
	XRCameraParams_GetHashCode_m4B13187F7D2BDEF05AEE01B1C08BF7BE743450D3_AdjustorThunk,
	XRCameraParams_ToString_mDF369BABC469E9F0FD2F675CA51762D43B658A95_AdjustorThunk,
	XRCameraSubsystem__ctor_m494821143D51BD2028BDC82ED7FCFACAD928B92D,
	XRCameraSubsystem_get_currentCamera_m282FFC69B0ADD4A2E95D7B7BEA183B642264140C,
	XRCameraSubsystem_get_requestedCamera_mDEEADE48A7A0CCC1ADAA9EB8E71C9A5B6D16A1C4,
	XRCameraSubsystem_set_requestedCamera_m65F790F389D3F872FD4C667C07E77645F009BD02,
	XRCameraSubsystem_get_autoFocusEnabled_m2169E884DF44269DD6EAB5EDA16E199E3BE6DD1D,
	XRCameraSubsystem_get_autoFocusRequested_mB8C597786A8E139BDF3C8C4C2E37BAE48D281423,
	XRCameraSubsystem_set_autoFocusRequested_m9FE78AF905C22EF72760868A91FD164CE83DC29B,
	XRCameraSubsystem_get_currentLightEstimation_mB75AD8B5DCDF179B3339D982D5B5D4EDA9DF4826,
	XRCameraSubsystem_get_requestedLightEstimation_mB9F4B02D8C27819A36B09B7FD57E70854942E0C8,
	XRCameraSubsystem_set_requestedLightEstimation_m0D5FC93E6EC1921CB83EAF535C6AA1EC5C4EA694,
	XRCameraSubsystem_OnStart_m20BD1C2BFE2BD80BEDC645782953875B923D1D2E,
	XRCameraSubsystem_OnStop_m72C30EE6BA570645B8338043A45B8F1815E04833,
	XRCameraSubsystem_OnDestroyed_mC2BAABF53993142A81A6F010D91C022EE15D1954,
	XRCameraSubsystem_GetTextureDescriptors_mD7CFEDA2DDB138789A1E96CE71672F9C12FE4D21,
	XRCameraSubsystem_get_cameraMaterial_m5B8CE90E2D4F8AF83D5D8134B1CB57D38065EBF5,
	XRCameraSubsystem_OnBeforeBackgroundRender_m6D6CBD308D7E538974A268019054AAE46BFC2FBC,
	XRCameraSubsystem_TryGetIntrinsics_m7002E115FF03D6FD7131F11918579C79613CD3B6,
	XRCameraSubsystem_GetConfigurations_m89E371017BA4612ECF7B4D472A0F71D290738BFA,
	XRCameraSubsystem_get_currentConfiguration_m3AB42FB62192A5C598C668FB84BEE5B1D371278D,
	XRCameraSubsystem_set_currentConfiguration_m7B434D9F5A4E5A324D74E5ECF655937F746CA166,
	XRCameraSubsystem_get_invertCulling_mEA7FB5A83FC369DCB02B408585E21CDD6005899D,
	NULL,
	XRCameraSubsystem_TryGetLatestFrame_m89FA346C38CE80B4D7069CB8C8C6F4952537B771,
	XRCameraSubsystem_get_permissionGranted_m47E72ACE1D8264D73CCEE9341DCE766C3038056C,
	XRCameraSubsystem_GetMaterialKeywords_mA71C43409E4766D3350FC61C2403A896F461F8EF,
	XRCameraSubsystem_TryGetLatestImage_m6AD5F6C38BFF2E73745A37F5868055D6F79750F2,
	XRCameraSubsystem_TryAcquireLatestCpuImage_mDE6925F131B1FA9E0A87CBB3D06B8D8245003EDC,
	XRCameraSubsystem_Register_m703AA08482A5B9AFE8DCCCC33CF7D65BA4B56508,
	XRCameraSubsystemCinfo_get_id_m1DB8669B5D86333CEB72F4B933CCA95503921A5F_AdjustorThunk,
	XRCameraSubsystemCinfo_set_id_m559DBD38CBD75958E02AF1F62D676E431F661520_AdjustorThunk,
	XRCameraSubsystemCinfo_get_implementationType_m6240AB836CC4FDF45C40F018932181137A9F4EFC_AdjustorThunk,
	XRCameraSubsystemCinfo_set_implementationType_m49F9345BCD43251FFDC4DD68E1C006AFB98E3634_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsAverageBrightness_m435847F6BEF656D51B758CE3A345ED340348D19C_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsAverageBrightness_m2F3DA568B12203F175FEAF777DA8753394F4780B_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsAverageColorTemperature_mEB9C8EA9865557B03879002D49702DABC898FFEA_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsAverageColorTemperature_m697272008DF9F4DA4287992BF6968433A46FD977_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsColorCorrection_m750008A02F360F9B08CFA09A56041CC9F322D4D7_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsColorCorrection_m021F469C4BE1CF91484A2C483102FED9D650EB49_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsDisplayMatrix_m4FB943DF5BAD950FC1E8F207C43E1423CE8DACB4_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsDisplayMatrix_m15E696085A5B470BC086F59B6EB2B4ED67E6BC6E_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsProjectionMatrix_m6264020FBBB0BE11F965A3806FE976C477A85029_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsProjectionMatrix_m392F1AA0BE94C4F9BE41B4B501373769BC32BBE3_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsTimestamp_mA412069D5F8BE727C5697AE3DECB293EEDD5C2CC_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsTimestamp_mC0811CD8D662F79DA2C8144617A3C4B3F57475CD_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsCameraConfigurations_mC9E1DDCB24429931986DF787B70AE1B26155C968_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsCameraConfigurations_mE52728D3658A16E910A5290A1F13A3B49716A86E_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsCameraImage_m0F0C52A74FE18C235A3A45A3112A41049CFF21CB_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsCameraImage_mAB3557E5539DCFAEE89775A5A7A768EF9AFC18B2_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsAverageIntensityInLumens_mAA76CDE43EF6429034235FA62E9EA906C017DABA_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsAverageIntensityInLumens_m4FFC89CEF155CC9CAEE5B26B7B511EFB56401A77_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsFaceTrackingAmbientIntensityLightEstimation_m23AA159827223BD1F7C74A9F277EA3A11EA884A6_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsFaceTrackingAmbientIntensityLightEstimation_m20E48DF12ECC198A544B85B4E6E827D5C44B0F8F_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsFaceTrackingHDRLightEstimation_m097C130B703C858853BDA995D3E92003DFFAC98B_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsFaceTrackingHDRLightEstimation_m88310BAC5F76E8BB7E43421B136782D26CDF52B9_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsWorldTrackingAmbientIntensityLightEstimation_m8AC57089BDA75C88D09190067A03184659811A32_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsWorldTrackingAmbientIntensityLightEstimation_mB569E126D3D68FB5FFD6A9FBB8BD8A5B9EA2C68F_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsWorldTrackingHDRLightEstimation_mC47D81A504E58D9835032082113BA08900121E10_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsWorldTrackingHDRLightEstimation_mBBFB21875B3BE98CADDC0945FD09162AB5A4D4D3_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsFocusModes_m16EB0068BED9439C2054345D4AF8C377FA3AE165_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsFocusModes_m799DEE56E25682F95D399627085512849CB183F0_AdjustorThunk,
	XRCameraSubsystemCinfo_get_supportsCameraGrain_m824EC72A0311C4F7ABA41EBF3F1D512256364074_AdjustorThunk,
	XRCameraSubsystemCinfo_set_supportsCameraGrain_m7A9DDA176272DAE0056462DA008F808C69FFB382_AdjustorThunk,
	XRCameraSubsystemCinfo_Equals_mFE87F930B521AF5DE45B11505D902F7D54132D7E_AdjustorThunk,
	XRCameraSubsystemCinfo_Equals_mC1C672F751A190D738D675E92E08A9BD51CDFD17_AdjustorThunk,
	XRCameraSubsystemCinfo_op_Equality_m35BEFC61F3D89C60D502DE1D7EDFF866CDCF03DA,
	XRCameraSubsystemCinfo_op_Inequality_mF793DEE0C5451A47EF9BC048CF55C19B691C6AAA,
	XRCameraSubsystemCinfo_GetHashCode_mD73BDB766ED1A08FD01FE5B5DC4659F5C6AA46D2_AdjustorThunk,
	XRCameraSubsystemDescriptor__ctor_m3CEE22F997A70853919BFA5D77F4E42B641FA5A8,
	XRCameraSubsystemDescriptor_get_supportsAverageBrightness_m982955584D39B97CD69E4E72FA65FAD92D6CCA82,
	XRCameraSubsystemDescriptor_set_supportsAverageBrightness_m035A437122E761A414CAEB39CBBB811B2BE5E5A1,
	XRCameraSubsystemDescriptor_get_supportsAverageColorTemperature_mF1DC55FBCDA326B859182DDA33EE3832B11151D6,
	XRCameraSubsystemDescriptor_set_supportsAverageColorTemperature_mC2E6DAE5DEA38CC15FB9BF2BC8D0D74D6EAF074F,
	XRCameraSubsystemDescriptor_get_supportsDisplayMatrix_m9AC0B144D6812FC25D67F4F1B13EFF53428B6462,
	XRCameraSubsystemDescriptor_set_supportsDisplayMatrix_m6AA3AD2B2169BEFEC0E25AF133954C105ACF7A57,
	XRCameraSubsystemDescriptor_get_supportsProjectionMatrix_m52F7C1EDC42DAC2863A6F01538CFEE89DD6FA668,
	XRCameraSubsystemDescriptor_set_supportsProjectionMatrix_mF08048B92AAC06A3E1AD27FAAE706307E8330B13,
	XRCameraSubsystemDescriptor_get_supportsTimestamp_m78C4A64340FC54929EC0D23F34F7893F11999370,
	XRCameraSubsystemDescriptor_set_supportsTimestamp_mCB9A7611C018D10F589A5614C639D43BD68EEAC1,
	XRCameraSubsystemDescriptor_get_supportsCameraConfigurations_m878013E1778D4D8ED8E20D61EFA80B0676A7688B,
	XRCameraSubsystemDescriptor_set_supportsCameraConfigurations_mD83859D14421E469C44251BD9BB485DE452F69DA,
	XRCameraSubsystemDescriptor_get_supportsCameraImage_mFA18FCE5C49B17187AD3ACA02435903D255A0093,
	XRCameraSubsystemDescriptor_set_supportsCameraImage_mA00EF890A0FA5562BAF9FF4C7D576DC6A587C444,
	XRCameraSubsystemDescriptor_get_supportsAverageIntensityInLumens_mE38B4C4D2479E91D44C522A3CAA4C617ABCE2309,
	XRCameraSubsystemDescriptor_set_supportsAverageIntensityInLumens_mA3B28B90284C27EE9FADB0C32FBA590EAC33A6AB,
	XRCameraSubsystemDescriptor_get_supportsFocusModes_m65F7A70A6965CEB2D4B8D42FE35A443B04562F9B,
	XRCameraSubsystemDescriptor_set_supportsFocusModes_mC0614C2F8149C084D9ECCDC6E240BAAE6913734E,
	XRCameraSubsystemDescriptor_get_supportsFaceTrackingAmbientIntensityLightEstimation_m448DBB0CC704A29454243ED6022701F2FFB7D19F,
	XRCameraSubsystemDescriptor_set_supportsFaceTrackingAmbientIntensityLightEstimation_m4F005F802752C51D36461E3E80220FF4B0777463,
	XRCameraSubsystemDescriptor_get_supportsFaceTrackingHDRLightEstimation_mC0E15E39B702A849E7AB91F3B2A5BC950B1C0C8C,
	XRCameraSubsystemDescriptor_set_supportsFaceTrackingHDRLightEstimation_m7328E20EC1237AED5871608A27021AB566E28898,
	XRCameraSubsystemDescriptor_get_supportsWorldTrackingAmbientIntensityLightEstimation_m917A561ABD1E474A48D8784B361360D930042778,
	XRCameraSubsystemDescriptor_set_supportsWorldTrackingAmbientIntensityLightEstimation_m0867CE9108F76FBEB341DEE537717BD609860A41,
	XRCameraSubsystemDescriptor_get_supportsWorldTrackingHDRLightEstimation_m4E113282BC0476FA0673F95D741C390F4076E4A8,
	XRCameraSubsystemDescriptor_set_supportsWorldTrackingHDRLightEstimation_m9146770DDEEF2023163D25C87784FD44AE1FCF44,
	XRCameraSubsystemDescriptor_get_supportsCameraGrain_m384B5321E12E7E7AE907D1665C75017A3911E5F3,
	XRCameraSubsystemDescriptor_set_supportsCameraGrain_m830D2705CB8327CF8E9C881BFEDA1E547871A7BA,
	XRCameraSubsystemDescriptor_Create_mD1CD2F7DDCCF8702EEDE082BAFCAFAC8ECE3DEA3,
	Configuration_get_descriptor_m8E1A59E0CDBA65733F1E89153C4CAEDBDE4BF5CD_AdjustorThunk,
	Configuration_set_descriptor_m3D755E0F4483B99A7038B27DEE762780C92033BF_AdjustorThunk,
	Configuration_get_features_m8DB48A18EE1E9DDC73A6FAAE1F671621E4D9C3FF_AdjustorThunk,
	Configuration_set_features_m6355432613E049A1E2316EE8B95BE7DFD08C564E_AdjustorThunk,
	Configuration__ctor_m6DCC3415FCA98D74490598E6B333220002F83B9F_AdjustorThunk,
	Configuration_GetHashCode_mC1034C38DC7D77D3314C85529794198AE7414D28_AdjustorThunk,
	Configuration_Equals_mDDA107F00E66E0E4E80051E78A69FAAABC440311_AdjustorThunk,
	Configuration_Equals_m9D01331F4C4217610EBF65953C40A621A696F6FC_AdjustorThunk,
	Configuration_op_Equality_m56BB883CDDC773E9417AB7E77C70570D54AA4F6D,
	Configuration_op_Inequality_mBC68393FE19F9726E36CD002D51A29D049D03546,
	NULL,
	ConfigurationChooser__ctor_m293981265D9F9F94392ED1EDAB6AB40C633CDEB5,
	ConfigurationDescriptor_get_identifier_m8C2119C732D9203F795339D70278341A09717114_AdjustorThunk,
	ConfigurationDescriptor_get_capabilities_m1C841BC2128C90CBE3966A3885AA3444BFBCF87F_AdjustorThunk,
	ConfigurationDescriptor_get_rank_m16DB30BCB6D532A72D4DC437289651C11B72DAA7_AdjustorThunk,
	ConfigurationDescriptor__ctor_m9FEDA4546BDA2EE188D8C44D4FD5EBC8BF23B244_AdjustorThunk,
	ConfigurationDescriptor_HexString_mD1D5021B1B8160927E4D52057DD8BB1F3A405573_AdjustorThunk,
	ConfigurationDescriptor_ToString_m8C9B3F12F1B3998DE1CA034FB04A699013D437AA_AdjustorThunk,
	ConfigurationDescriptor_GetHashCode_m5568ACF729A487DD99C555182524736ADA2E8F07_AdjustorThunk,
	ConfigurationDescriptor_Equals_m20A0629D5EB248930415E499619C746E246CCC0D_AdjustorThunk,
	ConfigurationDescriptor_Equals_m63E4470800DF34C13B14CCD8879A45D03C8E28C4_AdjustorThunk,
	ConfigurationDescriptor_op_Equality_m3DD670C71C323823087D717EE530978642956637,
	ConfigurationDescriptor_op_Inequality_m54778CDE499216C9034DF6BA616C5583D348300C,
	DefaultConfigurationChooser_ChooseConfiguration_m5C0D67889CBF390B4BC805FACCB19082487A105D,
	DefaultConfigurationChooser__ctor_mA2DD6EAD824E737D3FA2F722E023ADEFEA79C268,
	FeatureExtensions_Any_m9A551424EE459226756825C4204862B7DD1FFE5D,
	FeatureExtensions_All_m28EA1884048677E672FCB7DB0333FAB23F00A556,
	FeatureExtensions_None_m6C758CA7C15EF85929E49C2955B1E0302EA4939E,
	FeatureExtensions_Union_mEEFBCF0FA10823B7A65625E4F4DE4784D12B4EB1,
	FeatureExtensions_Intersection_m79C38D66FB2BD6E622A3E6F5A7B838E7EC61F0A9,
	FeatureExtensions_SetDifference_mE8B2C6885F24E4E29A52F51A57AF8F57B3F5E980,
	FeatureExtensions_SymmetricDifference_m664AA718B00BFEF9108230DF90DFD269D42919D5,
	FeatureExtensions_SetEnabled_m8BB3223239E1259F0057CEBF2CBD23192860FF06,
	FeatureExtensions_Cameras_m1BECB29268A0BF510D6C311CBF5CB54254E03A15,
	FeatureExtensions_TrackingModes_mDE0FC3B938A8A3F22854D58DCAD264915C064843,
	FeatureExtensions_LightEstimation_m9CD1E28D53E5DED2E4E4E3FDBF72369972217877,
	FeatureExtensions_WithoutCameraOrTracking_m30F447B3A3619A4D5ACE7B2CC168EE9DE8713668,
	FeatureExtensions_LowestBit_mB1DB2BC2E74BD3F8C262F504FF6CCDF35F20F151,
	FeatureExtensions_ToStringList_mE85CF7C790356F522E273D136328B745F0C4F149,
	FeatureExtensions_Count_m65FE7DAEFA72D88B6BD55B6EA93D59975EF90DEB,
	XRCpuImage_get_dimensions_m21142AD3EFE33129CBBD8ACC604D1398121FB0CE_AdjustorThunk,
	XRCpuImage_set_dimensions_m992E0A3FE1FE423D86A046987F251FD7D3D63E89_AdjustorThunk,
	XRCpuImage_get_width_mB2BFCC0E20A9C86B6BD2AAE14B80EF44EA4327C7_AdjustorThunk,
	XRCpuImage_get_height_m5D71C1DCDCC58501BBB0568B66E9259C60700AA7_AdjustorThunk,
	XRCpuImage_get_planeCount_mB545D3D7E24A0E186EFD1992A749B01A04AB096D_AdjustorThunk,
	XRCpuImage_set_planeCount_m5737E3E280F5F49311540F98C9BB025716DCD977_AdjustorThunk,
	XRCpuImage_get_format_mD0A34A79B5F05E264452DCE9901DD2F43F527880_AdjustorThunk,
	XRCpuImage_set_format_m8629082CD757705A7BD147BADAB20A85562E7AB0_AdjustorThunk,
	XRCpuImage_get_timestamp_m8BD98D270345A7FB70D7AC06A4D9C6BB2A170923_AdjustorThunk,
	XRCpuImage_set_timestamp_m8F93F34F608F2A1A095EBC756D619B6F304208B9_AdjustorThunk,
	XRCpuImage_get_valid_m74F130657887888264BB9A93150F39CB5077DDBB_AdjustorThunk,
	XRCpuImage__ctor_mA38DE21566F85C757C12AF8CD750EE8F10C1AEFB_AdjustorThunk,
	XRCpuImage_FormatSupported_m3CD1E19616F7FAD3F2E2901EC26E0E3E33CD09BA_AdjustorThunk,
	XRCpuImage_GetPlane_mCF396EE57D114577EF10F97D7425EE950D60D8E8_AdjustorThunk,
	XRCpuImage_GetConvertedDataSize_m467C94A6671214C85E47BF77B8BC82FF2F0790D2_AdjustorThunk,
	XRCpuImage_GetConvertedDataSize_m5A64E2913C09E6120C9358D80EBFA6AA2B57BB5B_AdjustorThunk,
	XRCpuImage_Convert_m440FC298D000D4ABED5CDBA38FB22A5CD0BB61F5_AdjustorThunk,
	XRCpuImage_Convert_mD5EF9358113B8C9543795F7568FC0F9FAE7F8435_AdjustorThunk,
	XRCpuImage_ConvertAsync_m1486B5A2C9147AD0F0143A1A00CE24E308109F15_AdjustorThunk,
	XRCpuImage_ConvertAsync_m7804259655BF42F77E739615633C728527E3E97D_AdjustorThunk,
	XRCpuImage_OnAsyncConversionComplete_mE1632D9BCA9BB444DBB3283CBFE5567609FF98D0,
	XRCpuImage_ValidateNativeHandleAndThrow_mCCE2D922A560FEA868503AC78BBA9C10B7FC8F25_AdjustorThunk,
	XRCpuImage_ValidateConversionParamsAndThrow_m910D903A78C38AE0FE83278C9C8143ACF29D4351_AdjustorThunk,
	XRCpuImage_Dispose_m2E0EDC3DCC4EC7820D895586CD406593AFB70E0B_AdjustorThunk,
	XRCpuImage_GetHashCode_mC3A63D5C67E64A34967C0D766BF07F32A946C4C2_AdjustorThunk,
	XRCpuImage_Equals_m4AE6685AE4EE997352BC72A2D2B2F704502C0D20_AdjustorThunk,
	XRCpuImage_Equals_mA327280B4F60BF824A15F67F2FF0AD608DD5A07A_AdjustorThunk,
	XRCpuImage_op_Equality_m2804851877FA4BF36BE91DFFEB11B74A9332F3E0,
	XRCpuImage_op_Inequality_m447487CD14D74E28B660B0C0F0C0870E8D0A9DC1,
	XRCpuImage_ToString_mCC4B5407E5B32F13D6F563B4012EDBABBF1548F0_AdjustorThunk,
	XRCpuImage__cctor_mC7990C4BC0490F405725B4287151E407D36B83F4,
	XRCpuImageFormatExtensions_AsTextureFormat_m99960EBA0D4425DD05F8A50ED43D1474008C522A,
	XRDepthSubsystem__ctor_m1374F0D0EDEA230229CC5ADFF30647B3D2D100D3,
	XRDepthSubsystem_OnStart_m0C7E81B5E231E3909F8494F8A9706F37B4B78FDC,
	XRDepthSubsystem_OnDestroyed_m3AF3ED89CA1E5D4E00F9681600BBA028E5A62429,
	XRDepthSubsystem_OnStop_m8FD775CD57E1FF5809A58B0960E3117A76C5C4A7,
	XRDepthSubsystem_GetChanges_m7B42781E43AFE126FBE6DFE2C8A3AF76DC53EEB3,
	XRDepthSubsystem_GetPointCloudData_m49BEF4047DEED6FC3E885AF893387ED347971BB8,
	NULL,
	XRDepthSubsystemDescriptor__ctor_m350CD434BA44A06555D7522D9511963FA95437A5,
	XRDepthSubsystemDescriptor_get_supportsFeaturePoints_m85CDF3E45EEAFC5B98302006B1782376C929B6C6,
	XRDepthSubsystemDescriptor_set_supportsFeaturePoints_m372ABA744A1FC28D1ACAACEB5890BB1D156A515F,
	XRDepthSubsystemDescriptor_get_supportsUniqueIds_m3C75A183E8BAA7D6019A3E0941D43BA337C19049,
	XRDepthSubsystemDescriptor_set_supportsUniqueIds_m4E7BCEF119F81892B7AA739EEBC6072A15E67957,
	XRDepthSubsystemDescriptor_get_supportsConfidence_m8FCFA9A0F8537516D883BEEB63D045373DADC92E,
	XRDepthSubsystemDescriptor_set_supportsConfidence_m513D6A5FCE31FD37260DDE662A08100F81FAC3EE,
	XRDepthSubsystemDescriptor_RegisterDescriptor_m53237B806014993075B559C44D9A743DB447154F,
	XRPointCloud_get_defaultValue_m71EFAD95365CFFB007E85B39F6CCEB2182FCEEDC,
	XRPointCloud__ctor_m0F992F42C621E29D49C0B27DD514B62FA5A7A655_AdjustorThunk,
	XRPointCloud_get_trackableId_mA394197EAD026665FC02A1118CBBB46FF6873EF1_AdjustorThunk,
	XRPointCloud_get_pose_m09C2DF1AD7F1220B547BD2EBCCA6E35F85A87EB0_AdjustorThunk,
	XRPointCloud_get_trackingState_m0CE633649849B59E4AEA7875F62F0B34CA61FC96_AdjustorThunk,
	XRPointCloud_get_nativePtr_m313F72EB3D0E3A439691D4A4AF84A61EE08FE371_AdjustorThunk,
	XRPointCloud_GetHashCode_m171B58B8F5EB316F2E7746BFF30205A9724B11F7_AdjustorThunk,
	XRPointCloud_Equals_m38B177BD481DFAAA66F4B66BE336A98AA4C4DCC6_AdjustorThunk,
	XRPointCloud_Equals_m01C5EBB7AC6017B014186EC74AC1CD637A7D56E9_AdjustorThunk,
	XRPointCloud_op_Equality_m0AD50ADAE436F5A4D5DC68D67E0CCEF37E2783C0,
	XRPointCloud_op_Inequality_mD012C95636F2CFABAB524548362726BD2A190A48,
	XRPointCloud__cctor_mF51225F68978906B6C48B574FB42B90511C76130,
	XRPointCloudData_get_positions_m2BDA572054D639DB35E9FDA3D15AEF3B7B39D40C_AdjustorThunk,
	XRPointCloudData_set_positions_m78BB0E1E2A5860DAC6F60D6C9A6A37544FF9880E_AdjustorThunk,
	XRPointCloudData_get_confidenceValues_m156073A1640F58477DBCBAC6BDA05C3BF866ACE6_AdjustorThunk,
	XRPointCloudData_set_confidenceValues_mC837DE6B63BF8CBD8F2480C1A4ED3247AABCB861_AdjustorThunk,
	XRPointCloudData_get_identifiers_m6AA5FE2F151300371A1F5A8310A49A0D4A35BD23_AdjustorThunk,
	XRPointCloudData_set_identifiers_m550B2B8C6EF821D5BBD47C066FF6C961EF0CA562_AdjustorThunk,
	XRPointCloudData_Dispose_mDF78595F088472E60327A1D366AA787C68A3EDE3_AdjustorThunk,
	XRPointCloudData_GetHashCode_mA67A28CD8661AAE597D4466135AF1750D2569409_AdjustorThunk,
	XRPointCloudData_Equals_mC870B135A9697D1A2AFB40892E70D7D403590E2A_AdjustorThunk,
	XRPointCloudData_ToString_m3790E45AE87D2C6F63D664EF736F530B7A4FCB4D_AdjustorThunk,
	XRPointCloudData_Equals_mDE5097D689526E5461CBBC48C36E6221F71F7598_AdjustorThunk,
	XRPointCloudData_op_Equality_m96CE37D4B2F213B20AE0352C787DBE13BE339ECE,
	XRPointCloudData_op_Inequality_mB515ACB6DD311FB5AF3CA608B7078581AFEDD25B,
	XREnvironmentProbe_get_defaultValue_m14B351BF8F54FCFCDA803FA3C29D1590BC0148E4,
	XREnvironmentProbe_get_trackableId_m2F7F8DCE954C099E60807742B6A4B27DA2F30085_AdjustorThunk,
	XREnvironmentProbe_set_trackableId_m02C924524F96E6A0A434C30A08F251F9AF407453_AdjustorThunk,
	XREnvironmentProbe_get_scale_m2346CDBC2FEF9CC85761BF11829E10D2992E0004_AdjustorThunk,
	XREnvironmentProbe_set_scale_m122476D077C3E65524BDF5A662B6D4FC01FC1954_AdjustorThunk,
	XREnvironmentProbe_get_pose_m2CF6BF7E554B1225E99947B620D2C029499E7996_AdjustorThunk,
	XREnvironmentProbe_set_pose_m3BE983E4E465A1DC6D4E829788A0FE217250BE28_AdjustorThunk,
	XREnvironmentProbe_get_size_mCFA1F9B8C8BE0138585D2BE4C2059E0153133C11_AdjustorThunk,
	XREnvironmentProbe_set_size_m0F087E604DCEC21E2CBFCC6BB1AE557C684B4428_AdjustorThunk,
	XREnvironmentProbe_get_textureDescriptor_mA926C0701B8D774E5C0A1F51DC54546CB71ECFFA_AdjustorThunk,
	XREnvironmentProbe_set_textureDescriptor_m104A7E7448BDBA6BB45B540AE3DDA8D37150BF82_AdjustorThunk,
	XREnvironmentProbe_get_trackingState_m48BAAE58BF028382D00D5F49BA1F6023E53B0AF9_AdjustorThunk,
	XREnvironmentProbe_set_trackingState_m1E2728289637E522527BC24ED51E97CAAB7E7E4C_AdjustorThunk,
	XREnvironmentProbe_get_nativePtr_m3F1EB67BA31BFA57D741EC97267A851EA376E8D5_AdjustorThunk,
	XREnvironmentProbe_set_nativePtr_mE1643DBD64E1DF09A9B62F7DFD5342C90F00E010_AdjustorThunk,
	XREnvironmentProbe_Equals_mD54F2132909E56F471109994AB0D41C708BE8C91_AdjustorThunk,
	XREnvironmentProbe_Equals_mF133292903D42FC122E0F88D73FE918A3F0D6722_AdjustorThunk,
	XREnvironmentProbe_op_Equality_mB488DFEB2BC8DB08F90D3B88DE96A6C274745779,
	XREnvironmentProbe_op_Inequality_mC044BE07ABC3F68B8BE3A59DAF9D86F6FB2F7C87,
	XREnvironmentProbe_GetHashCode_m0171139D9AB03DE5465347053AEF443E482DEA83_AdjustorThunk,
	XREnvironmentProbe_ToString_m6F495FF5C04959B29C49C6E45DCD15FD3E6F153F_AdjustorThunk,
	XREnvironmentProbe_ToString_mAADD3CBE24607D1FDE563A639744E7A6657EF7DB_AdjustorThunk,
	XREnvironmentProbe__cctor_m833E25DAF76FD20D0DDC939A61A75E4457896AF9,
	XREnvironmentProbeSubsystem__ctor_mD5858B70AAE45F8F825E8C0D6E980C8DCD8A7E33,
	XREnvironmentProbeSubsystem_get_automaticPlacementRequested_mCC6780B1B0DE13EBC0460D8471D744D3E1FF3059,
	XREnvironmentProbeSubsystem_set_automaticPlacementRequested_mD346F82B7985B210050CA290587B387CC1AF1494,
	XREnvironmentProbeSubsystem_get_subsystemDescriptor_mB3F44E9126C4222121677B889A82A8765515528E,
	XREnvironmentProbeSubsystem_get_automaticPlacementEnabled_m8652E1BE78F0049A3821862C791F899F24A13834,
	XREnvironmentProbeSubsystem_get_environmentTextureHDRRequested_mC65FBA739FBDA6B4111308DEE949DE2317AFA36B,
	XREnvironmentProbeSubsystem_set_environmentTextureHDRRequested_m21BF6D56B29E88354A9263C2FC88A2D05B59C873,
	XREnvironmentProbeSubsystem_get_environmentTextureHDREnabled_mEA9B5F383052BC8DDD43E168A0228FB31ABC4318,
	XREnvironmentProbeSubsystem_GetChanges_mA100F4697822F10AAAD8506683629996239BD7C5,
	XREnvironmentProbeSubsystem_OnStart_m6043B9D1DE6190686DA46A5D4DFD284C4E144166,
	XREnvironmentProbeSubsystem_OnStop_mEC24AA84C449BD8D8EA6DBDA7598A2375D2C012C,
	XREnvironmentProbeSubsystem_OnDestroyed_m2D6E9339398E952F73B8C6E951386B05D648E3BA,
	XREnvironmentProbeSubsystem_TryAddEnvironmentProbe_m20C670C360A17973331846037D1C075351EB399B,
	XREnvironmentProbeSubsystem_RemoveEnvironmentProbe_m402226077D7D17C3CD67CCD60CAEC91697AB9FB9,
	NULL,
	XREnvironmentProbeSubsystem_Register_m0701E0648B078B75B6DC47AED942AEA83B986E11,
	XREnvironmentProbeSubsystemCinfo_get_id_m35112EDFEC749952B475247292216B73C159BBDF_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_id_mC04469DE2280532DA98230C42D293A40E73870EE_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_implementationType_m4092117AA12E3175B4DBCA30FC7E035E785058E0_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_implementationType_mC5DFD8692500C7513DE3AC48BDE90AB5CE9D1AF9_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_supportsManualPlacement_m9F8E25BF94BD0D089D9BFE4EFE4F4F5349EE8F00_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_supportsManualPlacement_m82BD1C3FA4706E2EC81E6B8CF2D9804FA75BD391_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfManual_mEF312990150F85B0CF01436677E5F795BF9BD61E_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfManual_m99273CA85107749AF9914F38B1B944660E5C0860_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_supportsAutomaticPlacement_m65A5F29D5CB748401E6F039C82332012BA2EC61A_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_supportsAutomaticPlacement_mCF993333DFF9094801678045C101EAC59D6A0A55_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_supportsRemovalOfAutomatic_mE0EA584480EB5DA00FF26D3129926FD60FD36CD9_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_supportsRemovalOfAutomatic_mB19AC789CAA508C4AB003E575CDD69692B7974D8_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_supportsEnvironmentTexture_m4B973F04E1F4698C0C001A88463A1C1DE9CD0C95_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_supportsEnvironmentTexture_m6DCBFAB057A1C98762332AB4E661C056ECD574FC_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_get_supportsEnvironmentTextureHDR_m79C96A18C168AE457BD4E1F996F63BAD23DC8149_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_set_supportsEnvironmentTextureHDR_m37EB940E817F6792E0085944543D9D41F7BF0058_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_Equals_m5515D71C2B24DE3277CCB1CFE7B900F6FB867679_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_Equals_m9054174CEA9A5167D68DE43F4861EC5EF9F797AA_AdjustorThunk,
	XREnvironmentProbeSubsystemCinfo_op_Equality_m069BD1A34F40D78B3A2C189DF0C6DBF9F02524E1,
	XREnvironmentProbeSubsystemCinfo_op_Inequality_mD521BAED92A48BD4F24DD2693A3075D8B0DF3B3F,
	XREnvironmentProbeSubsystemCinfo_GetHashCode_m455133A5C39919DE0737C8B2EFD39B29331B08D0_AdjustorThunk,
	XREnvironmentProbeSubsystemDescriptor__ctor_mB4A6964DAD6E27CDA6E2FD747C31F45FAFCBFDBE,
	XREnvironmentProbeSubsystemDescriptor_get_supportsManualPlacement_m5AD7DF2F4755AF9D77E0543BB886EFBA3B9F819D,
	XREnvironmentProbeSubsystemDescriptor_set_supportsManualPlacement_m5F6FA0792E3DDDF3F050BB7359BC4A5C0990B624,
	XREnvironmentProbeSubsystemDescriptor_get_supportsRemovalOfManual_m5E310D3807CEA59342DC63BFC92C236EBEE9B050,
	XREnvironmentProbeSubsystemDescriptor_set_supportsRemovalOfManual_mF8D9A8DABBD292211E67EFCA1060D6F37B70D329,
	XREnvironmentProbeSubsystemDescriptor_get_supportsAutomaticPlacement_mE81C20404E1B5E438640209D2AEAD344B960E83A,
	XREnvironmentProbeSubsystemDescriptor_set_supportsAutomaticPlacement_m0F393A7ADFA798824A9D0EDB586D879BDE79B117,
	XREnvironmentProbeSubsystemDescriptor_get_supportsRemovalOfAutomatic_mF2E0D44C620A28668F575A2FFDB85DD0DFA33A40,
	XREnvironmentProbeSubsystemDescriptor_set_supportsRemovalOfAutomatic_mB92E4298805A5F9640139D53BFBF72CC28B006BE,
	XREnvironmentProbeSubsystemDescriptor_get_supportsEnvironmentTexture_m012FA38EC2000019C5DA5359C048AF6F645FD6BA,
	XREnvironmentProbeSubsystemDescriptor_set_supportsEnvironmentTexture_mC3622C706E9C8AFA8FB574B8096568C6484D61A9,
	XREnvironmentProbeSubsystemDescriptor_get_supportsEnvironmentTextureHDR_m5028FEAD3D411E08D66884DA61AE117126FECC30,
	XREnvironmentProbeSubsystemDescriptor_set_supportsEnvironmentTextureHDR_m77896178EBFF4F9BA49E428611E6AFBD0CFE09A7,
	XREnvironmentProbeSubsystemDescriptor_Create_mB7EBDC47BFF343F6B7B621B5D5E5EE9600EF767E,
	XRFace_get_defaultValue_m10A49DFCC1786C0E8F3244200F1AC9696C16AD34,
	XRFace_get_trackableId_mC7AA3B622C6B16A9E8B5A3BEA524C7ED54A6188D_AdjustorThunk,
	XRFace_get_pose_m3792AF11CBB24361529B7291ED46B9DD2970AC54_AdjustorThunk,
	XRFace_get_trackingState_m29CA0D89354ACC65EF8A18C09201BCBB3F732E3B_AdjustorThunk,
	XRFace_get_nativePtr_mAB11BBF883F193C7A8224F78D8AC3F814E5CFF24_AdjustorThunk,
	XRFace_get_leftEyePose_mB6508142768ACD1B9C5EA05224DEF9E690C7F0F1_AdjustorThunk,
	XRFace_get_rightEyePose_m8A78A727975AA070F197E566C8135B2CA45E4996_AdjustorThunk,
	XRFace_get_fixationPoint_mBC16DB0D6E29A8DCAEB022097B502398BF106405_AdjustorThunk,
	XRFace_Equals_m9070A1CAB9F7539BB46EF54457FF63D77F49AB2F_AdjustorThunk,
	XRFace_GetHashCode_m4BD8F265F67DE7593A58B15EBEBCC826788C0C16_AdjustorThunk,
	XRFace_op_Equality_m869853751F92BFF53F76B82C59DA0E3EB34ACEC3,
	XRFace_op_Inequality_mD49925C1F7492D95AF42C8C6F79C78556520035B,
	XRFace_Equals_mFD60266E20097CC26C9C7C24F81A56B40468424F_AdjustorThunk,
	XRFace__cctor_m75F3E710F4D3A5809652FBB5237C2FB241EB8D89,
	XRFaceMesh_Resize_m278CBD449E198430D20CCC37897C7F254A94D65E_AdjustorThunk,
	XRFaceMesh_get_vertices_mD6CE3C180851CA10DE10A7B6C7BF0817E73609E9_AdjustorThunk,
	XRFaceMesh_get_normals_m8CF3D0395943001F6A681154DF7A75FA045CE6AE_AdjustorThunk,
	XRFaceMesh_get_indices_m4355D06541511C7724AA543FB0D66BE69F261F11_AdjustorThunk,
	XRFaceMesh_get_uvs_mECB939F9E262D22AED47311D6985116FC0CEEA9B_AdjustorThunk,
	XRFaceMesh_Dispose_m99786F0191BAD0D7E369911BEC69528BF8EEEDBC_AdjustorThunk,
	XRFaceMesh_GetHashCode_m1DAD63B36571737E153BF2B79C78E7C70C0E7CDA_AdjustorThunk,
	XRFaceMesh_Equals_m2D832F4D965FF78E5B5AEB89D8DD66A622F473E4_AdjustorThunk,
	XRFaceMesh_ToString_m037407F8735C9CF13EEA1CC1C9F216CA930F3ED4_AdjustorThunk,
	XRFaceMesh_Equals_m4560A69CCA817DD121CF283E87330937C98B233A_AdjustorThunk,
	XRFaceMesh_op_Equality_m26FDDF515E062E95ED2BE3A9E0059567A01B24F3,
	XRFaceMesh_op_Inequality_m2EE5ED9174FB59B8B721D52E57B94B96AEA3A516,
	NULL,
	XRFaceSubsystem__ctor_m848121C59971872F188E502DC2228B2F888C3B39,
	XRFaceSubsystem_OnStart_m156F96C39D26A1F4787FD202FBC7B75F668249C1,
	XRFaceSubsystem_OnDestroyed_m6A339572B90043E82921A77EF62497005DA78151,
	XRFaceSubsystem_OnStop_mF9AE8B1760B8EB324DCC41AC2BDF2A6CA5237080,
	XRFaceSubsystem_get_requestedMaximumFaceCount_mCDBADE73C31C3FCECA03C16974C0BA626AF5A678,
	XRFaceSubsystem_set_requestedMaximumFaceCount_mF74DBC2F6E1532AFA44B006A9B2E593DB041C80D,
	XRFaceSubsystem_get_currentMaximumFaceCount_m7C73145C5EB8CFED720755F1DE40C55A60BA2896,
	XRFaceSubsystem_get_supportedFaceCount_m98AC6844E4919284379E0B7D17DE393D2DEAC182,
	XRFaceSubsystem_GetChanges_m7A0FF036C2DF93EBD2B39EB65AA87335601C9AA7,
	XRFaceSubsystem_GetFaceMesh_m347A4FE63D6AB092FCBA7EB94542F9B3DBFDFA52,
	NULL,
	FaceSubsystemParams_get_id_mAEEA8292B68FD9CDC00CDB3F54650CE9B8DFFD55_AdjustorThunk,
	FaceSubsystemParams_set_id_m3DB203D7778C049F34D33B8B621CA63C26C50173_AdjustorThunk,
	FaceSubsystemParams_get_subsystemImplementationType_m07845AB046188D52ADBE505597D94AEE5443BBC6_AdjustorThunk,
	FaceSubsystemParams_set_subsystemImplementationType_mEB4B5EA266E818DA6DFA71FCD85E00C22F7D36F4_AdjustorThunk,
	FaceSubsystemParams_get_supportsFacePose_m1F7E0D0F6964B4DF7C97155FC8EE013FFFA4BC6B_AdjustorThunk,
	FaceSubsystemParams_set_supportsFacePose_mAEC183F6C4DEB3146FD8A336540851FB500295D5_AdjustorThunk,
	FaceSubsystemParams_get_supportsFaceMeshVerticesAndIndices_m18D1BB926039B3D8BD2541FF395632C24023E22E_AdjustorThunk,
	FaceSubsystemParams_set_supportsFaceMeshVerticesAndIndices_m508C74CE5B60402069FE4A2E6FA373531A139D78_AdjustorThunk,
	FaceSubsystemParams_get_supportsFaceMeshUVs_m35C7FCE40E7C59CBF05291DF7266A12A6D75A217_AdjustorThunk,
	FaceSubsystemParams_set_supportsFaceMeshUVs_mC4CC736CE3F53B8C04F3399DFCAA1727CF4928A9_AdjustorThunk,
	FaceSubsystemParams_get_supportsFaceMeshNormals_mDDB3AB2B685A51D5D36C9A5D5C403F0F4C8DFEA5_AdjustorThunk,
	FaceSubsystemParams_set_supportsFaceMeshNormals_m100AA9A06836AF23CE936AA2BDA6403E584E4B3C_AdjustorThunk,
	FaceSubsystemParams_get_supportsEyeTracking_m141415010676F58F488D1CB30163E16A275C177D_AdjustorThunk,
	FaceSubsystemParams_set_supportsEyeTracking_m06BFB3B0F58946DD993C9AEA99D3B617F15AFBB6_AdjustorThunk,
	FaceSubsystemParams_Equals_m1C189CDCD693681575267133354C41E5F4CA158C_AdjustorThunk,
	FaceSubsystemParams_Equals_m319C11F36441455D498041CCAC4762F4D7393F03_AdjustorThunk,
	FaceSubsystemParams_GetHashCode_mEDB28F47A58BE6CC8307751C6D73BE5C5801DE77_AdjustorThunk,
	FaceSubsystemParams_op_Equality_mCA3B5779A790E6B0846E36A6FA895A1450F5957C,
	FaceSubsystemParams_op_Inequality_m75983FBFBAE50E14DEB5E01BA48CA9E9B2B018E6,
	XRFaceSubsystemDescriptor__ctor_mBD859B2AB42BFCD523BB02010B0048D03C4FEB3D,
	XRFaceSubsystemDescriptor_get_supportsFacePose_m68F807E50DD826B46C3E13D44E426ED7430F42A3,
	XRFaceSubsystemDescriptor_get_supportsFaceMeshVerticesAndIndices_m70FD2CA774BC71CA1CBE56FFF48FBC157A34E444,
	XRFaceSubsystemDescriptor_get_supportsFaceMeshUVs_m29254875989386F1B592D9C20164E0CE7AFC96CB,
	XRFaceSubsystemDescriptor_get_supportsFaceMeshNormals_mBD374FCF46BEA94D10073D70780AA8104A018CE0,
	XRFaceSubsystemDescriptor_get_supportsEyeTracking_m25F30879E205470D26A93B900CFE3B6EA33EB618,
	XRFaceSubsystemDescriptor_Create_m68C75BA860D9E4EB66A232D86C3BFB2B435FEE74,
	GuidUtil_Compose_mF0A2DF31C9F5E45DC7786601C82B926546B021D4,
	HashCode_Combine_m59AE2A453CEA5122839942FAD800C7E591DCC83D,
	HashCode_ReferenceHash_mBCF52B8AB75E5AF77B211169F8A9029E3A4AE89D,
	HashCode_Combine_m8B2DF6BE8079E68942F69FD736B474339C199588,
	HashCode_Combine_m43F67900AD70583B21B1477145A5490CB3FD6DB2,
	HashCode_Combine_m7C576EEF662F6F46F0D8E59F304B21A3DDA3FFF5,
	HashCode_Combine_m015E8EDB3690FB78DCEE2E810749C9D37C10AD5E,
	HashCode_Combine_mD5158D70E4D3C96633A68F4B1C417A357CA570FF,
	HashCode_Combine_m9476B5F623B72DB1FBC776B0E37B79F052DFB8F1,
	XRHumanBody_get_trackableId_m6932327AA835FDFFA3A8AC2C11C45E2491E998AA_AdjustorThunk,
	XRHumanBody_set_trackableId_m9D91D37BF9C59410D9D15B428F009E448CE2E12A_AdjustorThunk,
	XRHumanBody_get_pose_m3E48843E383A32DF5ED22BFD89FB52C9C7AD1E5B_AdjustorThunk,
	XRHumanBody_set_pose_m95CBE9AE820F6CA2C3AA06F8201B226BABF189D8_AdjustorThunk,
	XRHumanBody_get_estimatedHeightScaleFactor_m42EC00C0BA5064C9F02DF146FBF06AF12F987E20_AdjustorThunk,
	XRHumanBody_set_estimatedHeightScaleFactor_m5CBA4FE2D7F35F03F6D5A2521F71F5760E4E47A2_AdjustorThunk,
	XRHumanBody_get_trackingState_m313FDD56C02437850F6BD0B1C4F735F1B06AC3CA_AdjustorThunk,
	XRHumanBody_set_trackingState_mC53B5E2436E49F115A3AF0BE79175C7FB8E6F496_AdjustorThunk,
	XRHumanBody_get_nativePtr_m322717DC0CFB9C7E2AFB064BCC2A233E99B713D8_AdjustorThunk,
	XRHumanBody_set_nativePtr_m73E59C4802F71BD054295CB53587C50D28926B12_AdjustorThunk,
	XRHumanBody_get_defaultValue_m93BC6C94804B68DB3D5C99631A36E0BACACEF20E,
	XRHumanBody_Equals_mCD4170D2041DF3892DD5185DBE2F78F154CA4F5E_AdjustorThunk,
	XRHumanBody_Equals_m0F6EB11CB87E2809A62F29438C47424676EB9601_AdjustorThunk,
	XRHumanBody_op_Equality_mA345FE946AC61C9EEF4F959AFF0B7D0F2E0E38EA,
	XRHumanBody_op_Inequality_m5ECB9E3AA85696BA74A8BDDB2F9B152818C4DC43,
	XRHumanBody_GetHashCode_m88DD5A5B057659E907F3715926F9CFDC9B1F2E27_AdjustorThunk,
	XRHumanBody__cctor_m8F0780BA6E9EADBA6D4C5250FAF95EBC80F67ED9,
	XRHumanBodyJoint_get_index_m2FF98E6DAD602BF81A289D8617423DABA5C9E1BE_AdjustorThunk,
	XRHumanBodyJoint_get_parentIndex_m44F1F92C071C6D0C398826086906EBE196D10E5E_AdjustorThunk,
	XRHumanBodyJoint_get_localScale_m8C6B243F80A342FAE843FEC72E0551CA35F94CEC_AdjustorThunk,
	XRHumanBodyJoint_get_localPose_m99D151B828994BCC94134CF7C1F99D197D701924_AdjustorThunk,
	XRHumanBodyJoint_get_anchorScale_m93774112A48834FF193F64C20A932144AF5F29A4_AdjustorThunk,
	XRHumanBodyJoint_get_anchorPose_m68590EE4ECF7790861446409AC93D29728955491_AdjustorThunk,
	XRHumanBodyJoint_get_tracked_mE08747C37E23CE4041B1508F80CBAC759D1E463A_AdjustorThunk,
	XRHumanBodyJoint__ctor_m7DD8554A3DB1E94EAFF47584054D2156B9A8AC75_AdjustorThunk,
	XRHumanBodyJoint_Equals_m5B9B34C9174921998E796152C84C3B50DAD8403E_AdjustorThunk,
	XRHumanBodyJoint_Equals_m300514EDD81E0CB83FFECCA87BD4F46283F72262_AdjustorThunk,
	XRHumanBodyJoint_op_Equality_mCFE279F68500F5560EAFBAC78221137E30968A9B,
	XRHumanBodyJoint_op_Inequality_m9DF662438BD450D7AA85E4314EB114292EF3707F,
	XRHumanBodyJoint_GetHashCode_mACD20EC1EB6727E1CCF55520D5C1B3A89D1AFDFB_AdjustorThunk,
	XRHumanBodyJoint_ToString_mBABBE34A2903E956E30E312B54D77EFF25377D8B_AdjustorThunk,
	XRHumanBodyJoint_ToString_m6BFD692ABEAA12E3E5A646857A1E820B2F3ADC11_AdjustorThunk,
	XRHumanBodyPose2DJoint_get_index_mD9FDCC24C68D17796A05B98EFF9CADF0246FD7EC_AdjustorThunk,
	XRHumanBodyPose2DJoint_get_parentIndex_m0C4A19B0FB8293184E647A0A8EC03A7022EF9FEA_AdjustorThunk,
	XRHumanBodyPose2DJoint_get_position_m0BCAA6689F1121EDFD38D334B926D4F54ECE605E_AdjustorThunk,
	XRHumanBodyPose2DJoint_get_tracked_m111E89A5838439234E91BB8628DA8311B51FAB85_AdjustorThunk,
	XRHumanBodyPose2DJoint__ctor_mD27CC086E2B589B0E2BFD7B95DDD331E73E83A5B_AdjustorThunk,
	XRHumanBodyPose2DJoint_Equals_mEC56CED7627342B791C31D08FB59EECE6383B8A5_AdjustorThunk,
	XRHumanBodyPose2DJoint_Equals_m24B4310DA6326E8106C760B34B2FE1BABF3F0363_AdjustorThunk,
	XRHumanBodyPose2DJoint_op_Equality_m10919647DEBA6C480E7CDD365619606690127006,
	XRHumanBodyPose2DJoint_op_Inequality_m401A52CF6A7AD6C8A2FC7E57E4A586E1E9BB0BAC,
	XRHumanBodyPose2DJoint_GetHashCode_m567FCBE39D79FE81B59DA5C379FAAFCBB0349C21_AdjustorThunk,
	XRHumanBodyPose2DJoint_ToString_mE0D6AB26ECEE026021EB07111142AAC530A917B4_AdjustorThunk,
	XRHumanBodyPose2DJoint_ToString_m401CCCD51037D42EEA6B6E79EE4621E716ECF529_AdjustorThunk,
	XRHumanBodySubsystem_get_pose2DRequested_mDB41813111380F9D8195A1F5FE8B0DE55F06098B,
	XRHumanBodySubsystem_set_pose2DRequested_m3BF999A23600C191381ECDD6EB66373FDA46CD86,
	XRHumanBodySubsystem_get_pose2DEnabled_m893C824E4AE179F04F45C754DD64404087A3E78B,
	XRHumanBodySubsystem_get_pose3DRequested_mC73DEF74952ABB92741874CFA27AA7C1B7A9CB28,
	XRHumanBodySubsystem_set_pose3DRequested_mB0DA2A9AAAD7297597F7817D7855F31F967D69EE,
	XRHumanBodySubsystem_get_pose3DEnabled_m2EAD056DCD50B75F4E87A3AAED98ABA2AA403C7F,
	XRHumanBodySubsystem_get_pose3DScaleEstimationRequested_mB109E27892E4EE7D82C20C31CD006FF75CEDE4E2,
	XRHumanBodySubsystem_set_pose3DScaleEstimationRequested_m2CB0FC8D225000B64C84708A3C63E2931FE59D09,
	XRHumanBodySubsystem_get_pose3DScaleEstimationEnabled_mEFB4713A2B4E308CD8B0A37D77A7D49482DC7E01,
	XRHumanBodySubsystem__ctor_m2AC16B13B82E955D9601CBDFE93D6316FEE22497,
	XRHumanBodySubsystem_OnStart_m54AAE040A476EE3857350E562E63DCE74D623D37,
	XRHumanBodySubsystem_OnStop_mC566D8BD9E4A61C4CBBC604681E56771211DCC5D,
	XRHumanBodySubsystem_OnDestroyed_m89C8064BB65A1BBE3E72FEE8C92393022A7D547A,
	XRHumanBodySubsystem_GetChanges_mDD19CA22B2DCD5F318F24A80232FF251716B67C8,
	XRHumanBodySubsystem_GetSkeleton_mB6924CE63569B2151553227515A51F241C3581C1,
	XRHumanBodySubsystem_GetHumanBodyPose2DJoints_m2CC03CB9773A7992B394080F3D97D80A3287F96C,
	NULL,
	XRHumanBodySubsystem_Register_m7B79583DD1CBF6AD5ABECC95B63E001E4FE82342,
	XRHumanBodySubsystemCinfo_get_id_m2351F8702B67461C381E99A2BEBB44E6E70C656C_AdjustorThunk,
	XRHumanBodySubsystemCinfo_set_id_m2F5A213DA264A18243B72F18268A963780C599A0_AdjustorThunk,
	XRHumanBodySubsystemCinfo_get_implementationType_m801524FD9F24E9A4E4DE6A442313F957F17B43F3_AdjustorThunk,
	XRHumanBodySubsystemCinfo_set_implementationType_mF7AEC0C8173DD69CB5C99EDB5C3771B1C8416BE3_AdjustorThunk,
	XRHumanBodySubsystemCinfo_get_supportsHumanBody2D_m3A1954180F31774465457140AED37F85D5530AEC_AdjustorThunk,
	XRHumanBodySubsystemCinfo_set_supportsHumanBody2D_m0FB82F19C615B7912E7D07B9B7BE06FF9E8661E5_AdjustorThunk,
	XRHumanBodySubsystemCinfo_get_supportsHumanBody3D_m4C2A07E0A796F98705E3E7B5187F4E24EA06744F_AdjustorThunk,
	XRHumanBodySubsystemCinfo_set_supportsHumanBody3D_mB9FE42BA975ABA23027E454E52E89B0CE44C814F_AdjustorThunk,
	XRHumanBodySubsystemCinfo_get_supportsHumanBody3DScaleEstimation_mBBA1279F9F6108A7C64F4C2828C9522585EA22E7_AdjustorThunk,
	XRHumanBodySubsystemCinfo_set_supportsHumanBody3DScaleEstimation_mE16BC101154FE97C200A79196CACD9176246928A_AdjustorThunk,
	XRHumanBodySubsystemCinfo_Equals_m57792C7AE10CE3DA0E80743AFF2DA808D308839B_AdjustorThunk,
	XRHumanBodySubsystemCinfo_Equals_m491D4271ACD9CF310EC5E9B6886F68B1A0E24D47_AdjustorThunk,
	XRHumanBodySubsystemCinfo_op_Equality_mD23BCE60F13DECC8FF29F4A7E60963A367BA5B46,
	XRHumanBodySubsystemCinfo_op_Inequality_mB943F987D91BFF9760FA1AC4CD47EC78F745F663,
	XRHumanBodySubsystemCinfo_GetHashCode_m7B445703AA6EA9C3CE69B45494F5443A5EA4C7C1_AdjustorThunk,
	XRHumanBodySubsystemDescriptor__ctor_m93C7493D80D6F74451A6CB6ECEFEF0EF3CC4A781,
	XRHumanBodySubsystemDescriptor_get_supportsHumanBody2D_mC77D7E93FC0A45CB78FADA03690D74EFE6A220B0,
	XRHumanBodySubsystemDescriptor_set_supportsHumanBody2D_m3547E6D95ADA18366B57CCAB05D1F69A47F18457,
	XRHumanBodySubsystemDescriptor_get_supportsHumanBody3D_m1B38E725F577DF2B1A7113C21757A27C0FD3FCC1,
	XRHumanBodySubsystemDescriptor_set_supportsHumanBody3D_m29383BA5B2AE8244ED5E2504887909AAFD61F5E0,
	XRHumanBodySubsystemDescriptor_get_supportsHumanBody3DScaleEstimation_m2A321A79DC0F16972CC81576BC0B62F31CF9F395,
	XRHumanBodySubsystemDescriptor_set_supportsHumanBody3DScaleEstimation_m658F556331D0FF423A9CD3765DC477A494091B9C,
	XRHumanBodySubsystemDescriptor_Create_m2E42215B4BA4999BAEBEE9C1CF30615B3936C4EF,
	NULL,
	NULL,
	NULL,
	MutableRuntimeReferenceImageLibrary_ScheduleAddImageJob_mA55DD943A67604A04A45611E1C508C45212178E3,
	NULL,
	MutableRuntimeReferenceImageLibrary_GetSupportedTextureFormatAt_mD0BD6E3C3B979C5946BB4E2B8F30B57511602DF5,
	NULL,
	MutableRuntimeReferenceImageLibrary_IsTextureFormatSupported_m196AF8CC46F8D19600F1D02E5EC7F2F73DA745AA,
	MutableRuntimeReferenceImageLibrary_GetEnumerator_mF0AD39CFBE88F86EC1F8D6D905C8AAB4E22F6349,
	MutableRuntimeReferenceImageLibrary_GenerateNewGuid_m9E2A885618C9EDD289C6891D181B6BF8002C6732,
	MutableRuntimeReferenceImageLibrary__ctor_m6C12CDC2B47B9C510CC95F349BF1403A256B41D5,
	RuntimeReferenceImageLibrary_get_Item_mFC00AA9544BAF5762E53450DB26CE3B950E135F2,
	NULL,
	NULL,
	RuntimeReferenceImageLibrary__ctor_m5D95CE2FFF53CB65745F1273F6C24A804C4DF5E9,
	XRImageTrackingSubsystem__ctor_m32B2EFB936DCAB4D5B7DC15C0A993C1F1736A4EF,
	XRImageTrackingSubsystem_OnStart_m171D82E548A07407640CF28589ACAA4EE6066209,
	XRImageTrackingSubsystem_OnStop_m297891ECD05100E676DDD27FAEA4388DBF2C4521,
	XRImageTrackingSubsystem_OnDestroyed_mDBA0F73C00C6790E6DDF2E060F5E26CE470F18BC,
	XRImageTrackingSubsystem_get_imageLibrary_mCFF108F9559826539F0EEC73BA0092F7544537B8,
	XRImageTrackingSubsystem_set_imageLibrary_m02F82B9AA311E96B2EFEF1808E18E881832008EE,
	XRImageTrackingSubsystem_CreateRuntimeLibrary_m26FE1D40056DEED2BE3D7A090B375184076792AE,
	XRImageTrackingSubsystem_GetChanges_m84DB25AC8DB44AE84050A755C823960BABC2CFA0,
	XRImageTrackingSubsystem_get_requestedMaxNumberOfMovingImages_mA7FD5D9BF9D702AF6A97F8BA75189403012612BB,
	XRImageTrackingSubsystem_set_requestedMaxNumberOfMovingImages_mFFF6F4BA02B92B7A68E324C78746DA41E5C93F3B,
	XRImageTrackingSubsystem_get_currentMaxNumberOfMovingImages_m337416CE69762F2494BA01E4ECED7826809F6FE7,
	NULL,
	XRImageTrackingSubsystemDescriptor_get_supportsMovingImages_m7B4BB427E6EBD7D250ED4F8C523DB9E5B9EFD293,
	XRImageTrackingSubsystemDescriptor_set_supportsMovingImages_m49B9418C9FD793F56A7137D7F5F15835493D2676,
	XRImageTrackingSubsystemDescriptor_get_requiresPhysicalImageDimensions_mA8297F529194125C38FA13B46C83E04C0B786599,
	XRImageTrackingSubsystemDescriptor_set_requiresPhysicalImageDimensions_m2FA9112497A113B4D27FA66EBE1FD506260BB763,
	XRImageTrackingSubsystemDescriptor_get_supportsMutableLibrary_m5533B99BFE6C3887A3308628413326C791F28212,
	XRImageTrackingSubsystemDescriptor_set_supportsMutableLibrary_mB25E82BDAFDB47FE8DBEA218309593AD51401E3F,
	XRImageTrackingSubsystemDescriptor_Create_mADB63232DFEE95AA44F52496B7289C8469E118B9,
	XRImageTrackingSubsystemDescriptor__ctor_m5D86FBB12C3EA031B9DEC71C6A2CC48F4A176711,
	XRReferenceImage__ctor_m6B1ECFC5354FC9FDC73635BF5E693DA33DE02A4B_AdjustorThunk,
	XRReferenceImage_get_guid_mEFF96705B63F80C7C38125D170F7E62B784AEED2_AdjustorThunk,
	XRReferenceImage_get_textureGuid_m818FF686F0FAB8AE85630A62E3FA74C2C81C2AC0_AdjustorThunk,
	XRReferenceImage_get_specifySize_m5792CAE7DC7ADB8591E770B1F32D71BCCE9F0597_AdjustorThunk,
	XRReferenceImage_get_size_m29A6DA526141F214BE2949524305EFE91C07FA32_AdjustorThunk,
	XRReferenceImage_get_width_m52157ED5E292633BA8BA0CFE7F97A756B6985DB8_AdjustorThunk,
	XRReferenceImage_get_height_mA55A287CAA626B1817465DA78D946D3EB82E0D8D_AdjustorThunk,
	XRReferenceImage_get_name_m6DD83ECED755444645A14816918747E2EEAE92A9_AdjustorThunk,
	XRReferenceImage_get_texture_m97887B57DD747DCE051484D1C97F1240B673FE16_AdjustorThunk,
	XRReferenceImage_ToString_m37745D8B95903B29F917CE6E0A141E79E6F4B937_AdjustorThunk,
	XRReferenceImage_GetHashCode_mFBF4D8E0A33B3EFDCB6D6E5A944AB4CF52AAB334_AdjustorThunk,
	XRReferenceImage_Equals_m90DF560ECB15F4363E40AF3C3A2F82F5E0FD147D_AdjustorThunk,
	XRReferenceImage_Equals_m5FBBB7CFF96645894AA08221795ABC2A98E4DEF5_AdjustorThunk,
	XRReferenceImage_op_Equality_mD0355E3DAD36502F8CF26DDDCCBFB5440D41A171,
	XRReferenceImage_op_Inequality_mDDDE4B2F7C0270E4FE4837E693B36C72892C4297,
	XRReferenceImageLibrary_get_count_m031E8D7C22B586EE44B0256912F688867E9DEB84,
	XRReferenceImageLibrary_GetEnumerator_m7F1F01CC0BD4EF373F5B3766D2D1476901C4DCFD,
	XRReferenceImageLibrary_get_Item_m9FE52A96359701129EC85E3AFB382E08E7E3B799,
	XRReferenceImageLibrary_indexOf_m6E722FBAD970A7FB4C9415EDF6F46BAA25B89116,
	XRReferenceImageLibrary_get_guid_mAE3BC056A0B6817FD14E09D150B561CB468EFCDC,
	XRReferenceImageLibrary__ctor_m783C31BF895E269078A6F82966CE008024A5450D,
	XRTrackedImage__ctor_mF31D86D7A523FD9EE7F4166A9ABB04272E93436B_AdjustorThunk,
	XRTrackedImage_get_defaultValue_mC27C0C8BAC99DFBD1900C92FBA0D4940D86468EE,
	XRTrackedImage_get_trackableId_m6EB6DBACC95E5EE2AFEE3CE421F4C123F32E9CB8_AdjustorThunk,
	XRTrackedImage_get_sourceImageId_m402089FA779BB9821B50B23F79579466D895939B_AdjustorThunk,
	XRTrackedImage_get_pose_m0566E087CA2DC99DF749E80277510C61DCF13186_AdjustorThunk,
	XRTrackedImage_get_size_m746034D0E2FD28C9E48A90965E4FCD9137988906_AdjustorThunk,
	XRTrackedImage_get_trackingState_mA7177B042E8F9F9B584582970BC5FF0377CE94DB_AdjustorThunk,
	XRTrackedImage_get_nativePtr_mB44BA43B02762B89091D56F254221F0741808629_AdjustorThunk,
	XRTrackedImage_GetHashCode_mC1A5AB6C756498852952CB1B9F4F69D1177A02A6_AdjustorThunk,
	XRTrackedImage_Equals_m12A588942242306FC770FD88421D00750F22A141_AdjustorThunk,
	XRTrackedImage_Equals_m7C7F0B2FC7A6818276C2BC763CF0465333453B9C_AdjustorThunk,
	XRTrackedImage_op_Equality_m8C52E2C73BB01445DA64A954189A25E1C6B162AA,
	XRTrackedImage_op_Inequality_mDC56A7B7605F26C5D1D049FE1D68D4463155F847,
	XRTrackedImage__cctor_m4D42652FA025B44DA4EEAF27F15B77E11DAF4614,
	NULL,
	NULL,
	NULL,
	NULL,
	XRObjectTrackingSubsystem__ctor_m76F90C34C8D522DB4C6F2943BF5B74076DFC464A,
	XRObjectTrackingSubsystem_OnStart_m2CE4CCEBC38F4E78EFDCC5E5E1A42B31BC1877DA,
	XRObjectTrackingSubsystem_get_library_m475F269C1D4EEA78B1415CE53B705EEC59973DBD,
	XRObjectTrackingSubsystem_set_library_mE75E8CCF9DF2E39914DF5BE5D26F2A07C01637EF,
	XRObjectTrackingSubsystem_OnDestroyed_m4EE74EF86BC5E6596F0BD15C2750F93A04355069,
	XRObjectTrackingSubsystem_OnStop_m05729B1B5EBBF4F8338DC755819BA21D7F1DFB16,
	XRObjectTrackingSubsystem_GetChanges_m8FCC51540E8197A8B79F50CC2E9B665B7D3FF205,
	NULL,
	XRObjectTrackingSubsystemDescriptor_get_capabilities_m76851EA2A5BC61D9B216AC74F2E95BF26748BD77,
	XRObjectTrackingSubsystemDescriptor_set_capabilities_m5F8A4A904B5B3FCB93E512CD5C03261672EE9462,
	XRObjectTrackingSubsystemDescriptor__ctor_m20F2C0E95938A87ABE2AD12DE31D4C70D1C5EC12,
	XRReferenceObject_get_name_mB9D4C5BF34D4FF064180412CCE6585867BA98718_AdjustorThunk,
	XRReferenceObject_get_guid_mA7BD0F3F54EABC39D19355113087CD4DFF94BE57_AdjustorThunk,
	NULL,
	XRReferenceObject_FindEntry_mBCEBCEF4265B7D210FFA15179493BF8BDBB70C94_AdjustorThunk,
	XRReferenceObject_Equals_m504798221B2E8A72005FD241A5C5E2A063FF37A2_AdjustorThunk,
	XRReferenceObject_GetHashCode_mE918BE08147EE066AF8CC5971E4F5A9221881678_AdjustorThunk,
	XRReferenceObject_Equals_mC83DEBBA89CBF9EF334B79634C9CA099B166DBC9_AdjustorThunk,
	XRReferenceObject_op_Equality_mA352611067E59B117AB607BAE3CD182706C462D0,
	XRReferenceObject_op_Inequality_m14C3E4A6DDD3BD64A168898642FAC1741D542155,
	XRReferenceObjectEntry__ctor_mB617B601F6FA34FA4DAB5E9AE50925FCBFB1B6FE,
	XRReferenceObjectLibrary_get_count_mAB2D34091CFC65B152FD3FD7095480FF5D6CC99C,
	XRReferenceObjectLibrary_GetEnumerator_m7ECEFBA77B4669ADD3EDCE0841E4FF9C79C3312D,
	XRReferenceObjectLibrary_get_Item_m09F2CB2A14D830F37053B8B6E02E9659F3527996,
	XRReferenceObjectLibrary_get_guid_mC10138A0A17DF18E104F14079063BA1C12A3DF87,
	XRReferenceObjectLibrary_indexOf_mBF56521C68737F5FBBA77684AD76A78E19C5B1F0,
	XRReferenceObjectLibrary__ctor_mA11977E1ED613E7E7EF79C3F2494E243F619C442,
	XRTrackedObject_get_defaultValue_m4623361129019EE5722A95C580171705EA1F3901,
	XRTrackedObject_get_trackableId_mB720981791DE599B20879640517A33BE2FE2D84D_AdjustorThunk,
	XRTrackedObject_get_pose_mF865EAF61AE8767D6A0CCF59494A51F2D670F603_AdjustorThunk,
	XRTrackedObject_get_trackingState_m0BD1D36132D7B57151A4CAE07B94238B2AEF3DED_AdjustorThunk,
	XRTrackedObject_get_nativePtr_mD654B09F24E79E99FA2A6B1A95C4EAFDF09C639F_AdjustorThunk,
	XRTrackedObject_get_referenceObjectGuid_m09514BB6AD9782AF342076F85BB28631C458BDC8_AdjustorThunk,
	XRTrackedObject__ctor_m81B6436D0E3BA4E73E1445074972DB81E3D27275_AdjustorThunk,
	XRTrackedObject_Equals_mF0CA07E970C48D514F2B9BBEC0FE44F46429C524_AdjustorThunk,
	XRTrackedObject_GetHashCode_m2F1509AA89026BB34BFFE2C07529AAB3B5B0A429_AdjustorThunk,
	XRTrackedObject_op_Equality_m06220676F2AB319883E5895019E7010622DE9583,
	XRTrackedObject_op_Inequality_m05EF7C266FC336DCCA28A984954021CE67818E40,
	XRTrackedObject_Equals_m925ED652F271F772E282C3621290411A259CBEEE_AdjustorThunk,
	XRTrackedObject__cctor_mF6797A036790C2B6133B8B8A44C64B49FDBFF296,
	SegmentationDepthModeExtension_Enabled_mC45FEDDFD11308BA5CFB5C2240A88635895E9187,
	SegmentationStencilModeExtension_Enabled_mA5776A4254B52494C3EE2307E50B98CFE3AD84E9,
	XROcclusionSubsystem_get_requestedHumanStencilMode_mAD9070FFC7B5D0AFD32F66D1E19781000EA4FBE5,
	XROcclusionSubsystem_set_requestedHumanStencilMode_m78A1E8C64FA3FE165307CB4F01D79A78BE28D240,
	XROcclusionSubsystem_get_currentHumanStencilMode_m9849AFC9E475F1FAD6E72C470ADB280BD07B6D7C,
	XROcclusionSubsystem_get_requestedHumanDepthMode_mFCE0752D4D52962C5661FE062875E2651B553749,
	XROcclusionSubsystem_set_requestedHumanDepthMode_mB6EF9CA558CACAF18BEF9C5B314BAE5E3E8C96AD,
	XROcclusionSubsystem_get_currentHumanDepthMode_mB0AEA01C806ABCECFEE120CE1C55266069F9AE93,
	XROcclusionSubsystem__ctor_m45621B004D781EE9D13D4019637E8F40066F3635,
	XROcclusionSubsystem_OnStart_mA0B7A6DA08D403017F81E20C618C480619243AFB,
	XROcclusionSubsystem_OnStop_m44A69643260CD3780DC4FA320EB9FD504BF63D67,
	XROcclusionSubsystem_OnDestroyed_mBACD7ABA75543E8CE322990B7A824AD231E42F30,
	XROcclusionSubsystem_TryGetHumanStencil_mD77A6EC8BAABEEBCB9A7A94595D8100BF25653EF,
	XROcclusionSubsystem_TryAcquireHumanStencilCpuImage_m43ADAED8657329954D85AA2B27052D09F5A79578,
	XROcclusionSubsystem_TryAcquireHumanDepthCpuImage_mFA2D132F84E9EC2E1EA7DFD9948BAC4ED57F8175,
	XROcclusionSubsystem_TryGetHumanDepth_mCD193E94A35335AA62E4A59E45DDF2FE1C32BC17,
	XROcclusionSubsystem_GetTextureDescriptors_mCA503BE6370F557B008A9B00BA3A2AB52CB91321,
	XROcclusionSubsystem_GetMaterialKeywords_mDD300FD3B92701C39A5B55737737A23C9687AB4B,
	NULL,
	XROcclusionSubsystem_Register_mB185B88C9B33B36B35798EBB7BAA81298C32653F,
	XROcclusionSubsystemCinfo_get_id_m96A0EF17FED72227D3ED1136F891B8AD462A22C3_AdjustorThunk,
	XROcclusionSubsystemCinfo_set_id_mA0AA0AA870676C64796A472C2A628497172F0458_AdjustorThunk,
	XROcclusionSubsystemCinfo_get_implementationType_m375A67B86D9AED5EA754A97869FB74624164313E_AdjustorThunk,
	XROcclusionSubsystemCinfo_set_implementationType_m9E73CDE4355BF6B44269FCC147E95E2BB9AB4FC4_AdjustorThunk,
	XROcclusionSubsystemCinfo_get_supportsHumanSegmentationStencilImage_m04B46A2EC45A5664843ED06C3A797CA984170E6C_AdjustorThunk,
	XROcclusionSubsystemCinfo_set_supportsHumanSegmentationStencilImage_mC57AB832FA71CD2607744CB7595C4CB75D3C8618_AdjustorThunk,
	XROcclusionSubsystemCinfo_get_supportsHumanSegmentationDepthImage_m4111410B7452236F3C676F152C49A1E50D6C59AF_AdjustorThunk,
	XROcclusionSubsystemCinfo_set_supportsHumanSegmentationDepthImage_mC6477C08869651350B069C0F82A15498FF3EEFDD_AdjustorThunk,
	XROcclusionSubsystemCinfo_Equals_mB73AB4777C08BBB6A84453DB250F0A3C7743439D_AdjustorThunk,
	XROcclusionSubsystemCinfo_Equals_m6F0BDC6E02CACCACA97BCBA852EF23B4E4639BC3_AdjustorThunk,
	XROcclusionSubsystemCinfo_op_Equality_m95B41F2A31BB59670EBD254BDFAF32846959FEB4,
	XROcclusionSubsystemCinfo_op_Inequality_mEA92EBF18577F8EB4C2101B3D9AF0F52CAF94AAC,
	XROcclusionSubsystemCinfo_GetHashCode_mBBD1B91B4917F2B46FE0D174B10EBFD54F264F52_AdjustorThunk,
	XROcclusionSubsystemDescriptor__ctor_mC66EEC804A37FC13398BF862C35B2210A956AB5C,
	XROcclusionSubsystemDescriptor_get_supportsHumanSegmentationStencilImage_m2ABDF70773231A0A0EDA5357739B7254AA531708,
	XROcclusionSubsystemDescriptor_set_supportsHumanSegmentationStencilImage_m8BD2C081B31FC942AE2EC96770FCCDBCAF04226F,
	XROcclusionSubsystemDescriptor_get_supportsHumanSegmentationDepthImage_mCA9CABD2BFD1ACC9012903F7858C3B438E3D1EDD,
	XROcclusionSubsystemDescriptor_set_supportsHumanSegmentationDepthImage_mD53709C1291F48491604CBD4C6571621BD924122,
	XROcclusionSubsystemDescriptor_Create_mE8F6EB04566638A7E69EC8F42305163B90A3DA85,
	XRParticipant__ctor_mAA44F8E88C7CD88220CCFC72140293C6A87C3936_AdjustorThunk,
	XRParticipant_get_defaultParticipant_m1ACE083807AB7AC1D3C5C6B008B21D6235E0C90D,
	XRParticipant_get_trackableId_mAF0DAE2613E96C830102678EA49DA306402C7700_AdjustorThunk,
	XRParticipant_get_pose_m9FDF90F628DF1FC812226F06F196A113644C1717_AdjustorThunk,
	XRParticipant_get_trackingState_m759EEC47B61486F19F9312FBBD6B29DD2F0C46FB_AdjustorThunk,
	XRParticipant_get_nativePtr_m564BDF8BE2F0111A797DC8444537B79017E67CFF_AdjustorThunk,
	XRParticipant_get_sessionId_m5938006D21673E09D6CFCA25D59838C96D6F4104_AdjustorThunk,
	XRParticipant_GetHashCode_mC57DBF0AB32B41BEFF5BACDF6491A31922C1BECF_AdjustorThunk,
	XRParticipant_Equals_mDD39066DDA04071F6A1BE956D8C0C8CCF4FC2F2E_AdjustorThunk,
	XRParticipant_Equals_mDA735125F2F48F7EAE29E9F89DA73AF80D667E3B_AdjustorThunk,
	XRParticipant_op_Equality_m487121B72916A790BBC6650532F5E87DC42C10E6,
	XRParticipant_op_Inequality_mAAC7AF1F23488D31EA335A672F26577305E7E777,
	XRParticipant__cctor_m08A417AEC969464B03C91EB03652D93A5F5A620C,
	XRParticipantSubsystem__ctor_m2A0AF9EDA5FA38728983C7B80CD1528333744028,
	XRParticipantSubsystem_OnStart_m418621832F927A7D135F1A859DEC11844D859BE8,
	XRParticipantSubsystem_OnStop_m7D9596D15D2D034F3E3B3C828EB39BDA8B3A0312,
	XRParticipantSubsystem_OnDestroyed_m1FC0F22A84096B22BEC61922444455428F2AA480,
	XRParticipantSubsystem_GetChanges_mAFC1419DAF4C1E3FEDD4B14C2561C4413AFA47BA,
	NULL,
	XRParticipantSubsystemDescriptor_get_capabilities_mC77B52797D0A552DE7073B23C3BB84682B700B21,
	XRParticipantSubsystemDescriptor_set_capabilities_m59EB7EA3C05BEE15CAB691BCCA3B3DD7B2D5BDA6,
	NULL,
	XRParticipantSubsystemDescriptor__ctor_m4D10C6AEDF54E4B11618837A7989EE5A696A4368,
	BoundedPlane_get_defaultValue_mD9C5DCC9919CFB735B2D62B8F4BEF1DAEBA37E89,
	BoundedPlane__ctor_m6669034B2D75285B18BB5F4AB225FFF405E12896_AdjustorThunk,
	BoundedPlane_get_trackableId_mD2A3DCDA61898E637ACAE0A0F4A832931378071A_AdjustorThunk,
	BoundedPlane_get_subsumedById_m77895AF3F9E056F4816CB9177209586E98967889_AdjustorThunk,
	BoundedPlane_get_pose_m30B084D1F307CB46894BE4FAD448788001A0F0DF_AdjustorThunk,
	BoundedPlane_get_center_m1AB8ED2CBDC8F4A85358F98A8DD832436818505F_AdjustorThunk,
	BoundedPlane_get_extents_mC0F005CBFDA55C68EF9CC52FBC8C0FC07854CBBB_AdjustorThunk,
	BoundedPlane_get_size_mC49BFA58CD7CE92C2470C82F30EA6F8A4F38B854_AdjustorThunk,
	BoundedPlane_get_alignment_m85C46D81426435F0B9399ABAF80AC5342A2F7137_AdjustorThunk,
	BoundedPlane_get_trackingState_m9B6467E433564ABE4BED9328F7B23AB05905B008_AdjustorThunk,
	BoundedPlane_get_nativePtr_m715E5D9C20481ED26B239C0865C9A934EB114E8F_AdjustorThunk,
	BoundedPlane_get_classification_mBC7152460D4441EE38BE0A9ACC26F31AC810C373_AdjustorThunk,
	BoundedPlane_get_width_m0A50EE1F2895796227CD25F82B867DBEDE310E0A_AdjustorThunk,
	BoundedPlane_get_height_m142AC62B93F60D6C445DFAB1380EE4CDDE852DC7_AdjustorThunk,
	BoundedPlane_get_normal_m1DBB621B1447071A5C7C5F2966A90459B9481078_AdjustorThunk,
	BoundedPlane_get_plane_mB634B619F93280612D0D395F8BD42B8533CEA787_AdjustorThunk,
	BoundedPlane_GetCorners_mC7C70E4A661E0AC9DCD3E53C11C3CFE6885A7D91_AdjustorThunk,
	BoundedPlane_ToString_mC00A85440C02CCFFD015A56BC18154E709DF4646_AdjustorThunk,
	BoundedPlane_Equals_mE1F35325F340F3CCC6662E73296FFDF8B0436C4D_AdjustorThunk,
	BoundedPlane_GetHashCode_mE003D802E745B7D4891C72388C0E80E9F8FA45DD_AdjustorThunk,
	BoundedPlane_op_Equality_mF251CF6307424910EBF752DC6BECC935C8B5BE34,
	BoundedPlane_op_Inequality_m6BEAF4DF01C450C1C4F1491967959CA3223114EE,
	BoundedPlane_Equals_m45A1269EAC68DE7B82FDC42D04073236B2FD333C_AdjustorThunk,
	BoundedPlane__cctor_m86C83D139BFE0A6444E9D93CCFD7EC5749BED3E4,
	PlaneAlignmentExtensions_IsHorizontal_m1FE7018A4BEB30AC79E56AD6E4EB7F1F306343D9,
	PlaneAlignmentExtensions_IsVertical_mBE82213A0D92EAC23478A636A344498733536717,
	XRPlaneSubsystem__ctor_mD446FA374A919AF2EDD1A1D7C47913E621CBF809,
	XRPlaneSubsystem_OnStart_mE7DE5789EA25815D532CAC0E888F4018DFA8DDC7,
	XRPlaneSubsystem_OnDestroyed_m092E6A86005959CBA29F6F8328250B3A87D78058,
	XRPlaneSubsystem_OnStop_m9E81F5B23F8D0AE818DA72270A43927E5597291C,
	XRPlaneSubsystem_get_requestedPlaneDetectionMode_mB79EA6EDEA868D978E61B63D6D2C2ECA342E957A,
	XRPlaneSubsystem_set_requestedPlaneDetectionMode_m31D455F45A2E087A0BEA96AB76B4F3F8FC77E8F9,
	XRPlaneSubsystem_get_currentPlaneDetectionMode_mAFE5F30973EC4F786ED19B179C0DCE45272F1316,
	XRPlaneSubsystem_GetChanges_m0487B4AE994BA3CE0DD7D9FA365856F4C9F5710B,
	XRPlaneSubsystem_GetBoundary_m8E8F373A0147BF4BB2E51B8EB12CBD85244A598F,
	NULL,
	NULL,
	XRPlaneSubsystemDescriptor_get_supportsHorizontalPlaneDetection_mCE1A453A7A8232F5F853C3DE97D050E1FBF1EF4B,
	XRPlaneSubsystemDescriptor_set_supportsHorizontalPlaneDetection_m9461E5C2D8FFB4D448B3D67D86466AD37F9D4701,
	XRPlaneSubsystemDescriptor_get_supportsVerticalPlaneDetection_m40C0375B4213105E598B98B1FA139209924B8787,
	XRPlaneSubsystemDescriptor_set_supportsVerticalPlaneDetection_m8A4D26E382738EAE8C04FE1A25A0C6F361A94868,
	XRPlaneSubsystemDescriptor_get_supportsArbitraryPlaneDetection_m167EFA5CEDBF49EB1221D7E4FD594183EBF38A92,
	XRPlaneSubsystemDescriptor_set_supportsArbitraryPlaneDetection_m33CAB48781AA7EF4032C40320B761A2F192432C1,
	XRPlaneSubsystemDescriptor_get_supportsBoundaryVertices_m566A87DC259E6BFF79DA388547F9A7AC73A71849,
	XRPlaneSubsystemDescriptor_set_supportsBoundaryVertices_mD484E051C4FA3399B9BAF6E47260D58806AB4993,
	XRPlaneSubsystemDescriptor_get_supportsClassification_mB068F96C31ACDA86254500C653F34FF21BD78219,
	XRPlaneSubsystemDescriptor_set_supportsClassification_m51D7D13704B7109545D8555E299E5F12525B0143,
	XRPlaneSubsystemDescriptor_Create_m3C5EB6950F78DFA4DEF405243ED67F0CBE2110CA,
	XRPlaneSubsystemDescriptor__ctor_mF240C9165052F15A60A6055E0F880629D3B8B5F3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	XRRaycast_get_defaultValue_mD62060B6A68161E3B3A2F929E772C1B681306CC0,
	XRRaycast_get_trackableId_m6DBE200F60327FBBD8C1852FD50F5881AFDEE90B_AdjustorThunk,
	XRRaycast_get_pose_m6EAC1A67DCD90871104B13EE918B1F19C9B8083A_AdjustorThunk,
	XRRaycast_get_trackingState_m78D3C1216CFEC8374CC3B84540DDF6B9FD94ECAB_AdjustorThunk,
	XRRaycast_get_nativePtr_m2BF1942CDEE019895049665F903277F290B436DA_AdjustorThunk,
	XRRaycast_get_distance_m2AFA9CBDBDA989D5EA183DC81EE867910960616B_AdjustorThunk,
	XRRaycast_get_hitTrackableId_m4E477515193C2CE62EF964D6E26E1BE6DB48F5F3_AdjustorThunk,
	XRRaycast__ctor_m7D756FF576B0D1C307B2DE2807129ED2176EEBE7_AdjustorThunk,
	XRRaycast_GetHashCode_m24DCE228B4EA497D7F9CC62D27271B1A72156C35_AdjustorThunk,
	XRRaycast_Equals_mD29FED6CD5F7C75D4005856BE7FE35107F84A81D_AdjustorThunk,
	XRRaycast_Equals_mBB5E88F0CE73CBE2F93483A95A810FFB9575F495_AdjustorThunk,
	XRRaycast_op_Equality_m078A221E3D496F3A19339788052561E0ECFC7EF5,
	XRRaycast_op_Inequality_m3A2D9FD239BE87C78BD26004A802272654CC35F5,
	XRRaycast__cctor_mE6BA22D64A43ECC5E386DB7ADC0461CAA3E0AD9B,
	XRRaycastHit_get_defaultValue_m17AEBDAC971A56C3FC4C7C4E2E14ECC357658DFA,
	XRRaycastHit_get_trackableId_mAECCB1BE08FB0B5A48CB27D955250FE2068492CF_AdjustorThunk,
	XRRaycastHit_set_trackableId_mD5381CB555237421AA3A1A4F42BDBA66C2CEE77F_AdjustorThunk,
	XRRaycastHit_get_pose_mE0B0A754E818C6FF3675A41CA95185A3E608C8C3_AdjustorThunk,
	XRRaycastHit_set_pose_m3E6F13DE1371303DD66CD9D9E8B86500C24C5516_AdjustorThunk,
	XRRaycastHit_get_distance_mC748DE6ED96B0C735DCA4AD320FA0BF522246D19_AdjustorThunk,
	XRRaycastHit_set_distance_m53218D1A8CBD8F632F988C439D5F98633A050815_AdjustorThunk,
	XRRaycastHit_get_hitType_m52BBF5DBDE1B3E7E01571EE029F68EB29E240DA6_AdjustorThunk,
	XRRaycastHit_set_hitType_m776B39B9226EB310D47FB6A10BA78844AEC4EE58_AdjustorThunk,
	XRRaycastHit__ctor_m522E98C4B6FD85F8386911C5BC497DE4968E3961_AdjustorThunk,
	XRRaycastHit_GetHashCode_m74F67C2F858CF9669399A90DC761E0E763C0827F_AdjustorThunk,
	XRRaycastHit_Equals_m0A24B5C58B6CA930CDD05F6F17F54FD60DA10DE5_AdjustorThunk,
	XRRaycastHit_Equals_m2E3F746F63AC5ED95DF5E79AB43C2DE8A8E42E60_AdjustorThunk,
	XRRaycastHit_op_Equality_m1543C9C16776653F7665569024203E1F54305FC7,
	XRRaycastHit_op_Inequality_m5533730BFA8AD45F27384E867981EF82A0AB0862,
	XRRaycastHit__cctor_m304374EB65F3AE9EFA5D8418B9CF3CE8A90B752B,
	XRRaycastSubsystem__ctor_mE8BD2BFB3AFD44403F3A663CA5D5AAC707419506,
	XRRaycastSubsystem_OnStart_m7B2A704BD7A5EA9FAF5E80D67C83AC90A2B7E0E7,
	XRRaycastSubsystem_OnStop_mB922B7F9B65FF7EC41D3042D161476F5E491573F,
	XRRaycastSubsystem_OnDestroyed_mD476DFCB51E552D3BB2C841CAB96B532D9D77BCC,
	XRRaycastSubsystem_GetChanges_mE85AF5735862EDDABEF7C2F955D2B4C1CB1A80DA,
	XRRaycastSubsystem_TryAddRaycast_m04F51D27F5AFFA539549FA123B7D41A63E3A71AC,
	XRRaycastSubsystem_TryAddRaycast_mFA57974A0D00FC2A6F752468BE4FF8A9A78BB828,
	XRRaycastSubsystem_RemoveRaycast_m979A4F09D73F43CD9D405D9595673CA5DFBE2F2D,
	XRRaycastSubsystem_Raycast_mD6335AB75E7AD15295138215F593EAB71754E6FA,
	XRRaycastSubsystem_Raycast_m46598C4ACA7D6AC6B6DA53A92ED1349F327EC6BF,
	NULL,
	XRRaycastSubsystemDescriptor_get_supportsViewportBasedRaycast_mD71BE0D71A6B3B48DDAB480114F930293C40DF26,
	XRRaycastSubsystemDescriptor_set_supportsViewportBasedRaycast_m7A6EBE60F40966D0314C378B1441C7DB41C0720D,
	XRRaycastSubsystemDescriptor_get_supportsWorldBasedRaycast_m7CDCA8DFD75903B7169A59254A31EBCB1D1962BD,
	XRRaycastSubsystemDescriptor_set_supportsWorldBasedRaycast_m23E91C1C3684B0FE2AF37D3BE2A79B0D88BFC7B3,
	XRRaycastSubsystemDescriptor_get_supportedTrackableTypes_m02F17127CFA033A9D6D84C7F0D53D0BA3FE379C4,
	XRRaycastSubsystemDescriptor_set_supportedTrackableTypes_mA429421E574C9261CFC271AC43A521E43B990DCD,
	XRRaycastSubsystemDescriptor_get_supportsTrackedRaycasts_mCFC70DB9283F92245EB14474CD4E996005E252FC,
	XRRaycastSubsystemDescriptor_set_supportsTrackedRaycasts_m5945AF718E03677402F4007F25462DC1BC631524,
	XRRaycastSubsystemDescriptor_RegisterDescriptor_mFA32B9879B902AA46943CF8809094299062A41DB,
	XRRaycastSubsystemDescriptor__ctor_m449AFD6137C639FB21F17C456B20BC954875BC9E,
	ScopedProfiler__ctor_mC6576AB1ED762DB2335436C4C63121FE04BBF264_AdjustorThunk,
	ScopedProfiler__ctor_m0EFC86CF601B63CCAC679AF8DE9BE02A9A2635AB_AdjustorThunk,
	ScopedProfiler_Dispose_m9330643F81D6C1961371A3D1436A53EFCB232887_AdjustorThunk,
	SerializableGuid__ctor_m67FFC2270F5BF1783DE5E4C4F85A214315DE46BB_AdjustorThunk,
	SerializableGuid_get_empty_mCDC698E4D3EE9F3B311588C6FC1EE7CC9E820892,
	SerializableGuid_get_guid_mDD1F60EF61B262769627D5F48F8840285E1986A0_AdjustorThunk,
	SerializableGuid_GetHashCode_m0C64440E27DBCFCF12B8E0E0FF92AB5B15344BC0_AdjustorThunk,
	SerializableGuid_Equals_m6D1C338E1D6985165503EBB3B369A7A7A58442D9_AdjustorThunk,
	SerializableGuid_ToString_m4D5204E4E13A13718A1450AABFC59A192DDEF80D_AdjustorThunk,
	SerializableGuid_ToString_m2B1D0D590302829D2CB6A8419F715D79AEBF0780_AdjustorThunk,
	SerializableGuid_ToString_m0B633F3089883432FF2F63F82D49F0DF86ABF77F_AdjustorThunk,
	SerializableGuid_Equals_m22017F6AF109B89F27E01D9E99014B0E95D6649E_AdjustorThunk,
	SerializableGuid_op_Equality_m0324F1D0AF69531DE06866CD1DD802E9C3CD40CB,
	SerializableGuid_op_Inequality_m262FFAE2823E3735F070BA7C168A505365B64B52,
	SerializableGuid__cctor_m7370C5A2AF171A415261C9CB3A55593963A6A721,
	SessionAvailabilityExtensions_IsSupported_mA0E371DBCB2DB69E066EA4DCADC76721AAD04ABA,
	SessionAvailabilityExtensions_IsInstalled_m30AE248D1BBF1C669EE32FE5976A35C16152DC9C,
	XRSessionSubsystem_get_nativePtr_m0F00EE85A23E2FBE08AE83393F4C7DC97C22366B,
	XRSessionSubsystem_get_sessionId_m830EF72639051E3486DA85FE6E4EF1C9AD3481E1,
	XRSessionSubsystem_GetAvailabilityAsync_mE1444BD33C0A1EAD4982FC0AE64D1251635487ED,
	XRSessionSubsystem_InstallAsync_m35E08EF7130491F2E498C990109FA7323A2ABCCC,
	XRSessionSubsystem__ctor_m2817E6FDD974187708CFD270DE4C6D3132774648,
	XRSessionSubsystem_OnStart_mCE65AF851F73EEE76E058AD66DA6E3E355C3FFCB,
	XRSessionSubsystem_Reset_mA6596EEA8C670E2561986B54BB34E0F15A0D5836,
	XRSessionSubsystem_OnStop_mC995FD70B27E089A98FEE62DAFB6642521F90F33,
	XRSessionSubsystem_OnDestroyed_mE818CF27CDBBA6E22713A186B50C7BED4A77CBCA,
	XRSessionSubsystem_DetermineConfiguration_mF4235A223A27B21F222E70E23FCD211F606F91AB,
	XRSessionSubsystem_Update_m168E0641976ED5CD2084BD32692044A2D361B945,
	XRSessionSubsystem_get_currentConfiguration_mD51168E864094B29123D44E2284844EFD08F9218,
	XRSessionSubsystem_set_currentConfiguration_m173B5C54D28FA42528D57FC6C36665712239704E,
	XRSessionSubsystem_get_requestedFeatures_m918BFAED674C970093F4D15BA4425E39AEF2E72B,
	XRSessionSubsystem_GetConfigurationDescriptors_mD9F32AE5997EFAA780BCA50998D79494673C7207,
	XRSessionSubsystem_OnApplicationPause_mE53A32290C253F808E5B14A11B7917286E86B08A,
	XRSessionSubsystem_OnApplicationResume_m4103D0866A4152C9A52E52A306D85EBAD8405F2F,
	XRSessionSubsystem_get_trackingState_m6CEDC16CB9B224A0302A83BC2C22FC4C0905EB30,
	XRSessionSubsystem_get_requestedTrackingMode_m5C0B4035A44E91732BAB0BA1CD8D6B84ED0A3DC4,
	XRSessionSubsystem_set_requestedTrackingMode_mE6950872428B9C4FD3A11BF3C2B8273345644339,
	XRSessionSubsystem_get_currentTrackingMode_m7707AA9874ED16C9E8908EB50B270F5067EB0CD2,
	XRSessionSubsystem_get_configurationChooser_mEBF33BD6BF7567261EC97270477B88BC20E00E0A,
	XRSessionSubsystem_set_configurationChooser_m3BED2362E1E27943DF11DF7474A02CDB137050CF,
	XRSessionSubsystem_get_notTrackingReason_m2425113BCCDD44CEF92AA9A045C002CAF981B6D7,
	XRSessionSubsystem_get_matchFrameRateEnabled_mD95D36035F4CF3A1606234CC4092B78CD7DF6671,
	XRSessionSubsystem_get_matchFrameRateRequested_m55D71F6FB285EC98FB7FEFF73D2B17738E6FD251,
	XRSessionSubsystem_set_matchFrameRateRequested_m6AEBB720BEB2954B383907B5191DEFFD03606824,
	XRSessionSubsystem_get_frameRate_m9C029A08839E039C3459DF5CE4A5E5CFAD3DC7DE,
	NULL,
	XRSessionSubsystem_get_subsystemDescriptor_mEEA810642D61C1E226C8A8C4F6133FF085B3DB35,
	XRSessionSubsystemDescriptor_get_supportsInstall_m2AA89682007FE1D8BB811FD152DE326FF7BB5A99,
	XRSessionSubsystemDescriptor_set_supportsInstall_m010EE3F0CB4B143A90B93C1F10F063FB12546920,
	XRSessionSubsystemDescriptor_get_supportsMatchFrameRate_m66DA7D5EE88322AF2EE5FC3B1BF8203115C2CA8F,
	XRSessionSubsystemDescriptor_set_supportsMatchFrameRate_mC2B0189D51BF3B64026D01DD6A088052C5D74BFC,
	XRSessionSubsystemDescriptor_RegisterDescriptor_m7A9F84E8A57323CDB5DC415BA05E72D6A72025E4,
	XRSessionSubsystemDescriptor__ctor_mF2A65C6A814FB2D22D5ED1608E5EFD5B0CD9A6E2,
	XRSessionUpdateParams_get_screenOrientation_m111C145EA6A683F025DF48C6EA355E37D8974183_AdjustorThunk,
	XRSessionUpdateParams_set_screenOrientation_m7C20FD52988E0F21604700B5CDA93FBA63DD28C6_AdjustorThunk,
	XRSessionUpdateParams_get_screenDimensions_m61A9722E272D6292B9C7C093BF7792FB007BF21E_AdjustorThunk,
	XRSessionUpdateParams_set_screenDimensions_m41570268847916BA02DD2427BDDB08B3D466A905_AdjustorThunk,
	XRSessionUpdateParams_GetHashCode_m3E0C208F41FAC84F879A073F85FB9DC0F1C09520_AdjustorThunk,
	XRSessionUpdateParams_Equals_m415AB0E24C9CF0C013872ED16C571B65DACF24B1_AdjustorThunk,
	XRSessionUpdateParams_ToString_m7150FEAE08C59544392C3D47B3CB5AC318B82F4A_AdjustorThunk,
	XRSessionUpdateParams_Equals_mFE8BAF000FDC02612C5D563960EB974E510DEAB3_AdjustorThunk,
	XRSessionUpdateParams_op_Equality_mEB57CF7E4D66886BF9EE3FF1BBF7D1B73E63608B,
	XRSessionUpdateParams_op_Inequality_mD00004C24603E13FC7D4F0239F812446EE21FF75,
	TrackableId_get_invalidId_mBE9FA1EC8F2EC1575C1B31666EA928A3382DF1CD,
	TrackableId_get_subId1_mF453A72AB194301098CEE0A9CED682524CFB30BD_AdjustorThunk,
	TrackableId_set_subId1_m43A33DBEA409BEF994296301506511538AFD96DB_AdjustorThunk,
	TrackableId_get_subId2_m2738106507454E9F70ADF4E3A74DB37BAD0E912F_AdjustorThunk,
	TrackableId_set_subId2_m0C75B44985527D97E859E98B0512FD3BE65FB539_AdjustorThunk,
	TrackableId__ctor_m497D3C74C918FDE476EA168A431DAE4E135E88B4_AdjustorThunk,
	TrackableId__ctor_m9DD30A8FE5DB5798D50C6F121E8FFD324F4DE4D6_AdjustorThunk,
	TrackableId_ToString_mBA49191865E57697F4279D2781B182590726A215_AdjustorThunk,
	TrackableId_GetHashCode_m89E7236D11700A1FAF335918CA65CDEB1BF4D973_AdjustorThunk,
	TrackableId_Equals_mBB92F3933E2215399757A70A3704E580DD32406C_AdjustorThunk,
	TrackableId_Equals_mCE458E0FDCDD6E339FCC1926EE88EB7B3D45F943_AdjustorThunk,
	TrackableId_op_Equality_m3171D96F331BC92756A7B171C85CD627442E7873,
	TrackableId_op_Inequality_mE90FE883749A9959B2B3F85FB12456646ACE6B93,
	TrackableId__cctor_m4EBC45AC6693CBEFB8B222F0EE6D8DAEC841264E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	XRCpuImageAsyncConversionStatusExtensions_IsDone_mBCD5B446791EE8D22AC506686C479B584AF6F6AA,
	XRCpuImageAsyncConversionStatusExtensions_IsError_mC4BC17F0DC4216F3D3537C92958FBAA2C980AE14,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	XRTextureDescriptor_get_nativeTexture_m83CAA03353C203B7D38618C1963C715F052081F8_AdjustorThunk,
	XRTextureDescriptor_set_nativeTexture_mEF92A3E263840B8F428C314323C20A11F896D907_AdjustorThunk,
	XRTextureDescriptor_get_width_m158B2CEE4A0F56DF263BB642F5E4A3C3CF339E0B_AdjustorThunk,
	XRTextureDescriptor_set_width_m59E159F83238991BAD9838C5835A07E44F6A163E_AdjustorThunk,
	XRTextureDescriptor_get_height_mCE50370000BCF4A70B95344A0731A771401C0894_AdjustorThunk,
	XRTextureDescriptor_set_height_mE690E293BE1FE8009052CC87FA454FB79DE9DF0E_AdjustorThunk,
	XRTextureDescriptor_get_mipmapCount_m491B149D8BBF148B2030214818E237A28D9B6CC4_AdjustorThunk,
	XRTextureDescriptor_set_mipmapCount_m8CC98FD1B188CA92DE7C1C430BF71E11E7AD7858_AdjustorThunk,
	XRTextureDescriptor_get_format_mA2DA22DC1DEBCAD27A9C69F3374D614DF1C3FA2B_AdjustorThunk,
	XRTextureDescriptor_set_format_m2BEDFB4C31E590B2C2AAE7145AEAE714491E0EA6_AdjustorThunk,
	XRTextureDescriptor_get_propertyNameId_mA3A29036B96A64D1C4F147678E60E2BFCAAAAFF0_AdjustorThunk,
	XRTextureDescriptor_set_propertyNameId_m87654C29B3CEFA71D22E9F1323058334E8338B4F_AdjustorThunk,
	XRTextureDescriptor_get_valid_mF060565C5E24FDF97771F6FDA3235562DF01977B_AdjustorThunk,
	XRTextureDescriptor_get_depth_m753CFA3697D1A98ABFA8331BDA0F37C8D156ABA9_AdjustorThunk,
	XRTextureDescriptor_set_depth_mB7F0D2390736CBDF0325186F9D3DFD1831C067DF_AdjustorThunk,
	XRTextureDescriptor_get_dimension_m580C5254C35EE0208427909D7DA2CED82BF8835F_AdjustorThunk,
	XRTextureDescriptor_set_dimension_mA4C6202E8028775EEE873185090FC8FB0847F371_AdjustorThunk,
	XRTextureDescriptor_hasIdenticalTextureMetadata_mD9C2A76A8B680BB7B2742F82235E40977CD098AE_AdjustorThunk,
	XRTextureDescriptor_Reset_m64A787FBD1F11161369A72A7D61763DDF8D74EBC_AdjustorThunk,
	XRTextureDescriptor_Equals_m124C4B8E0370717E0714FB2D28493A77034C6E38_AdjustorThunk,
	XRTextureDescriptor_Equals_m8D2E1A6303E60A653F70870CBD04845414F6A0A5_AdjustorThunk,
	XRTextureDescriptor_op_Equality_mBEE6E663B93B3648626DAACC5D0AD1F2C0B76847,
	XRTextureDescriptor_op_Inequality_m1373FCEBF131F54B4A61398DE7034853861E9EAE,
	XRTextureDescriptor_GetHashCode_mE61628A57D74C31744B57EBFBE8E8EDFA673B65F_AdjustorThunk,
	XRTextureDescriptor_ToString_mA7C17125D54876E04397C7022031B6A346CA9A7F_AdjustorThunk,
	Provider_Start_mF4586E88E223BF0A1F8B875E37ED8E956025F7DC,
	Provider_Stop_m7670689E746816D6B1D5D3AA7D79CD83AF463D20,
	Provider_Destroy_m67293D02BB308F9F682704C9674ED6BF0F9106CD,
	NULL,
	Provider_TryAddAnchor_mF724411302D13377F72BAD790C4D6841A3B8D94C,
	Provider_TryAttachAnchor_m82C5EF065E0549465D43C8768478DCED4DAB432A,
	Provider_TryRemoveAnchor_m3E507AA8C15B4B13787D2FECC6D22E6442A52D20,
	Provider__ctor_mF658D6D83E2D1075FAEAF7AF0AABEE1A3B8CDD87,
	Cinfo_get_id_mAB9FA0AEB8F01DFBEFD37A79A0A76523FDC5EA97_AdjustorThunk,
	Cinfo_set_id_m07E3333D64F89961070832339B11E5BCAA3923E1_AdjustorThunk,
	Cinfo_get_subsystemImplementationType_mDD1BA48FC9C3C388B5DF95EA240F84A4862AB497_AdjustorThunk,
	Cinfo_set_subsystemImplementationType_m8222C7E7310436CA41B9C74D28C4C487B1D02DAD_AdjustorThunk,
	Cinfo_get_supportsTrackableAttachments_m266173570E6C947C78F79960EE9C7C7E120048B5_AdjustorThunk,
	Cinfo_set_supportsTrackableAttachments_mF43F0A8FF39724C929A1AE638719068470F01160_AdjustorThunk,
	Cinfo_GetHashCode_m384C61B0C35F73A23C9D5B627245E6F7B3ACB610_AdjustorThunk,
	Cinfo_Equals_m670F61358AE4F7A8BD0CD8A1DFC52800519D75E5_AdjustorThunk,
	Cinfo_Equals_m1C2F4C09DC5A8A145F507FA0383E9BB9536515FD_AdjustorThunk,
	Cinfo_op_Equality_mF2CE3F3A33DDD2B27855774E43DC9076775BC98C,
	Cinfo_op_Inequality_m31844DA84D4A263CA3B6BA5965DF214511E50B95,
	Provider_Start_m5E039CE52C6D7873CAE86F45CC8CCDDD10CFA738,
	Provider_Stop_m00D52CC50FC71C24B3BFC370A592A4BDC3A2E805,
	Provider_Destroy_m181F91A509C877EEDAA0CDBCA51A03123BFA5DEF,
	NULL,
	Provider_TryAddReferencePoint_m24758900098738D227C197B63C5BB1A4D6E48599,
	Provider_TryAttachReferencePoint_m220B48C6D25FCB8C834B21CB58D2C45C87212180,
	Provider_TryRemoveReferencePoint_m1DE094DA1EBC860FB65C6B200F2CA79940239A80,
	Provider__ctor_mCAD1B3B1509E0232266D304B2CAB429D16C3359E,
	Cinfo_get_id_mB1E35C0B52EEAA8EB934C4D3F02465CF8A752015_AdjustorThunk,
	Cinfo_set_id_m9CAE75E21B0DAE38A8619D1B04D17EDEC81E97D7_AdjustorThunk,
	Cinfo_get_subsystemImplementationType_m764697AF3D79BDFA6010287A8B542F9323693096_AdjustorThunk,
	Cinfo_set_subsystemImplementationType_m9D5112215C6766E6561E42DA858B7F3D72F0005E_AdjustorThunk,
	Cinfo_get_supportsTrackableAttachments_m041B030B1BD0114D8FCE9A9F804CFF5984FB07BD_AdjustorThunk,
	Cinfo_set_supportsTrackableAttachments_mC0A061EDA609485B0E83FC7857E8573C93F38FD7_AdjustorThunk,
	Cinfo_GetHashCode_mA1002FD6C9DDB0C39442B7692A7CDB5562C61086_AdjustorThunk,
	Cinfo_Equals_mC210DE78D45CD980BF7B5A186241CFC5CC243D2E_AdjustorThunk,
	Cinfo_Equals_m7D9C657E628D2DFC390587FBE0BDFEB1D5CDC92D_AdjustorThunk,
	Cinfo_op_Equality_mE1F2D9B59D8CE1464461320ED675AF909582610A,
	Cinfo_op_Inequality_m434589D57A438F397CD363A5E2CB491095725A53,
	Provider_get_cpuImageApi_m20E5F2315BA1B77750BCFC6AB528084A0FA02987,
	Provider_get_cameraMaterial_m82824265BAB7CF990BA7F189A7DBAC583B06D7C4,
	Provider_get_permissionGranted_mA6995C1A907115576EE47F20298835116FDC8D37,
	Provider_get_invertCulling_m4D45573FD037E1BD3A4E5B90B8B4226AC0302A02,
	Provider_get_currentCamera_m299A8E1A074CB725165D1A0691D723EFECAD6D07,
	Provider_get_requestedCamera_m14F1AFE7227DA3B140BF1F0018CF16A79B2735B0,
	Provider_set_requestedCamera_m5C2C333F32E753D08C2FE311BE46E936646DB351,
	Provider_Start_m3D11C46FF20C01CFCFE6EDAA7E3343BB76CA60A1,
	Provider_Stop_m496FD02A62F7B5C2690BFB8883150846011EE681,
	Provider_Destroy_m1B756AB089B69831AE9FDA27C06464D90640D3B7,
	Provider_TryGetFrame_mA84121E1F987E6A61FA76E4730F66546CE8D4185,
	Provider_get_autoFocusEnabled_m3947CAE5A0D98C752EDB0257F1B3CD79896132A3,
	Provider_get_autoFocusRequested_m7B38D80EDC1E28E0E5C14F2B6157D9B399ECCC17,
	Provider_set_autoFocusRequested_m2C390A2C14C33F75C8E650EA23A522A20686CCC7,
	Provider_get_currentLightEstimation_mFC6A9C54C1A59510DF8CB6991A5D8BFC735EC642,
	Provider_get_requestedLightEstimation_mA711479359B827B01C37AB9306A0AFB786E12D48,
	Provider_set_requestedLightEstimation_m6CC82728BFE90FE5939780452447E3BB7BA552E4,
	Provider_TryGetIntrinsics_m881C1076A92682F37AB6D83C9FF420DACBFCB1ED,
	Provider_GetConfigurations_m9FD5BBC0B7C6270E7371BC88EC5EB0B38CBEACE2,
	Provider_get_currentConfiguration_m2A1DE1BFD6EA2F2F5E4130AA941F91FCB5672185,
	Provider_set_currentConfiguration_m5BA7EA99BF9DA46C010D6F62BEAD46B4BC0B1F3B,
	Provider_GetTextureDescriptors_mFC6F6C5A3B399ACAD7A8E78A93E73ABA8197F4D3,
	Provider_GetMaterialKeywords_m5BD9801B59F20AE0090BB12574A4CB4E69E466A6,
	Provider_TryAcquireLatestCpuImage_mAADAAE7BD5FFDB7A3132232990CAEFCEE7834B59,
	Provider_CreateCameraMaterial_m62FBA6BFEA66D3EE8265E8C53A66AF4C403E14B4,
	Provider_OnBeforeBackgroundRender_mBF9C737F3E774D8D51DEFF2E11F33A5DD88395A7,
	Provider__ctor_mCC7EE094D3B2DA61C91D5E78E8E62F1F58DD47FE,
	Api_TryGetPlane_m9D64816098EE052F9D610E1EF0DE7F77CB8E02BE,
	Api_TryGetConvertedDataSize_mC457255D5B5BB9201905A929880F80842DF8E2BB,
	Api_TryConvert_m07CE575512CBA9F5EEA96AB9836FFEE9B41E42C4,
	Api_ConvertAsync_m8F9E79E238770BD269614CE0BFB504C047C050CF,
	Api_NativeHandleValid_m701C5F499E36DF72B983AC977DF256CA85DADF3C,
	Api_TryGetAsyncRequestData_m508C75D22AD8855AEFA5C27DA303CC475F9B89D8,
	Api_ConvertAsync_m9B704468536CA59F5639710895CE9AAEFF614C34,
	Api_DisposeImage_m1B11F698C036CAF86F0890B8D471BA522A1E3C49,
	Api_DisposeAsyncRequest_mEE2E850FC016E274831E20CC8D9E86A603B8B46C,
	Api_GetAsyncRequestStatus_m43BB62DA43105C042FFBBEB7FC7ED8132D524D89,
	Api_FormatSupported_m49584181A3FA7582BAD3AAA84EC798546F68B39A,
	Api__ctor_m83BE4DB9E9BE1D814DB3AAF1834B4C8D09ED4AD9,
	AsyncConversion_get_conversionParams_m0976435603CD82928943B73F9B8F97A17918C730_AdjustorThunk,
	AsyncConversion_set_conversionParams_m34F73B62E8DB64467D17D086F47FFC8324E9DFF6_AdjustorThunk,
	AsyncConversion_get_status_m8259C60C53F18039D0583A9A4A5DB96197424481_AdjustorThunk,
	AsyncConversion__ctor_mBA54D876492EC5DC1BAE3B01B6860C9467994E0A_AdjustorThunk,
	NULL,
	AsyncConversion_Dispose_mD9C8D177C56B0F184440379985FDFE3C80967B84_AdjustorThunk,
	AsyncConversion_GetHashCode_mF897267F14D4D94F2227C1D4172793F342A63343_AdjustorThunk,
	AsyncConversion_Equals_m99E225DBA74089B656EE74BD4B9B6A67222F66AC_AdjustorThunk,
	AsyncConversion_Equals_m8538BEACA275973D73D16422E475DA9D78CBC8B2_AdjustorThunk,
	AsyncConversion_op_Equality_m233B9C8841F13F7E5BC44528DE6229044148B5AC,
	AsyncConversion_op_Inequality_m7317C7F778398EA1CCB85BCE878F66AF43C7BB90,
	AsyncConversion_ToString_mA0AB15C1A772EBFF298A58B11F445B71C1B7F0C4_AdjustorThunk,
	ConversionParams_get_inputRect_m045ABAD49308AAEFECE4DAF94CABFAACB53BD1D0_AdjustorThunk,
	ConversionParams_set_inputRect_mB13482D23EE76FBD8F12B9DFBAD925184C25AAB3_AdjustorThunk,
	ConversionParams_get_outputDimensions_m0D23770C75EC23B7D102457D91C2EC0CB2EF6459_AdjustorThunk,
	ConversionParams_set_outputDimensions_mF716A8EB0CB6469CE580C340B52DD525654F0B24_AdjustorThunk,
	ConversionParams_get_outputFormat_mFAADF1A8ABD173F6C123F7C638E0F4476034CC5B_AdjustorThunk,
	ConversionParams_set_outputFormat_m9CF571FF5292BBEFB3325275BA42902FCF90958C_AdjustorThunk,
	ConversionParams_get_transformation_m994D08D1B5A0D5BB83C3798CB6EB3AF6C3C8B8A3_AdjustorThunk,
	ConversionParams_set_transformation_mCC2BD22B567479DCB1E2130D4A5EDACD179580B7_AdjustorThunk,
	ConversionParams__ctor_m9C2749383F583F06C1BD86F82406AF0140869486_AdjustorThunk,
	ConversionParams_GetHashCode_m9EF42E4869E2FAC3DDE6E573B9256FBB58A178CF_AdjustorThunk,
	ConversionParams_Equals_mB0565EFE0C5F12FF86A1E1770A7C14C1DAA4DE23_AdjustorThunk,
	ConversionParams_Equals_mA5EDAE5A24DDB8C623E204CFD5D862C2465693F8_AdjustorThunk,
	ConversionParams_op_Equality_m79F1BC601A8C332E432DC993754E7FEAE40FDC5F,
	ConversionParams_op_Inequality_m85E4B82A5A388A2B09BA0E11396AB4E7C3CD3384,
	ConversionParams_ToString_m32588C7E28744CB4E7F5639CAE06DFE5202B8011_AdjustorThunk,
	Plane_get_rowStride_m4BAE42C59A73306C1BB1273DC9E2620E0A917510_AdjustorThunk,
	Plane_set_rowStride_m6B356A9FFBD8ABCDD1FCF3059E896D8EB495705E_AdjustorThunk,
	Plane_get_pixelStride_m77F6BF761236739DADCB33DB1E09A1281F9D96DD_AdjustorThunk,
	Plane_set_pixelStride_mD255E466170F89A9759FAD9322D39C6751C67AD0_AdjustorThunk,
	Plane_get_data_mA92994670020F432B255BCB11460D7905498CDF2_AdjustorThunk,
	Plane_set_data_m6F2C130A5DDE7CC3D1E484E03C86BCB8547470E0_AdjustorThunk,
	Plane__ctor_m99EBC727E6617DC223BDA39654D9005F75FDEA9C_AdjustorThunk,
	Plane_GetHashCode_m67EB14E1A37770F9890A94DC933503EE623DA8E8_AdjustorThunk,
	Plane_Equals_m781BBA0BD1BA4A6DD71BDA29F5D279E38D01E4A2_AdjustorThunk,
	Plane_Equals_mA6FA62324DE2553D518056C9E52D4E3474FF5578_AdjustorThunk,
	Plane_op_Equality_mB8D39D33D386C35D166FD172F2DFDF1E1CD5D7E9,
	Plane_op_Inequality_m55DC7145F310462CDFE2CDFA26BE052EBF88FAF1,
	Plane_ToString_mC2EC1F10E79405DCAED387A0E982059795E794D8_AdjustorThunk,
	Cinfo_get_nativeHandle_mF7E4A4B3DF3627E6BF0502B758F2A19B15C6B55E_AdjustorThunk,
	Cinfo_get_dimensions_m2CF1ED609BFB2D6BE94123B49DFCE7A9A9297815_AdjustorThunk,
	Cinfo_get_planeCount_m1C35DDAB469099A985BD4C9D0364DEE9C58C2FDB_AdjustorThunk,
	Cinfo_get_timestamp_m5EADD7F92B48CF9CECE80291B14E2F7F758A54AE_AdjustorThunk,
	Cinfo_get_format_mB465EA4362E0035D6BBF4628DB563923C6C13FE7_AdjustorThunk,
	Cinfo__ctor_m3DC67D1C506B09C2E79DD1E2CE6FCC2E8254E5F8_AdjustorThunk,
	Cinfo_Equals_m8DD26ADB239B2BA1B623FEAC22649E21F3F83D64_AdjustorThunk,
	Cinfo_Equals_mA9FFEAF14ADEB5C4B57C0F8D792907E368B54A7E_AdjustorThunk,
	Cinfo_op_Equality_m807D3763B3DD93C56B2375C323C65B6E70DADB20,
	Cinfo_op_Inequality_m5D556CF78F2332C5853FE1C3B6BA96929557C36D,
	Cinfo_GetHashCode_m7D8BC06648D7CADE45E3ECB7044F5CC426706A3B_AdjustorThunk,
	Cinfo_ToString_mFE83F0C0B9A3E1BB036DEEB947CC1E2CA64D2D02_AdjustorThunk,
	Provider_Start_m5FE902BE2D3439448C3A5B3FF4926CE1F7767A49,
	Provider_Stop_mC6F7D719BF33D225CF30A3AF6935D04B79131D6B,
	Provider_Destroy_mF22AE9359D3DB52341035F86AF040BF560F4751A,
	NULL,
	NULL,
	Provider__ctor_mE05BCBAE62243FDB4D9505D37B6559AD5453F504,
	Cinfo_get_supportsFeaturePoints_m9ABB1B99DDF90F76567CF1D40D5FCAB6C26415B5_AdjustorThunk,
	Cinfo_set_supportsFeaturePoints_m05831EFF5A03CCC424A2DB1E8C8460E54385E798_AdjustorThunk,
	Cinfo_get_supportsConfidence_m99E2A9D06072DD88955F84D667CDA0E029E59453_AdjustorThunk,
	Cinfo_set_supportsConfidence_m2FCA1C93FDE8DC5DC4A553F9EF32E399844C0F79_AdjustorThunk,
	Cinfo_get_supportsUniqueIds_m5A834E8536CABCA693F0B13C0A742C8A76E05C42_AdjustorThunk,
	Cinfo_set_supportsUniqueIds_mE8AA1B05D64ABF1690D4B30E5C245B9A923A9116_AdjustorThunk,
	Cinfo_get_capabilities_m6B6CF2B89609939E9E7313A3AAD56C543E4445AB_AdjustorThunk,
	Cinfo_set_capabilities_mEF50340ACAFF47D30E8270D45453909F64E2541E_AdjustorThunk,
	Cinfo_Equals_m11C179BADC0B35FB12263C3A7D3410897618FEDE_AdjustorThunk,
	Cinfo_Equals_mD7C8B1C1CA730E2AC1B15E61F563503E77DA05CF_AdjustorThunk,
	Cinfo_GetHashCode_mB134492A755E3A215541BF574B1E8449CFDA47D7_AdjustorThunk,
	Cinfo_op_Equality_mB0D0E72FCAE2987433EA598970459E7A028D681F,
	Cinfo_op_Inequality_mA0865AAD9BBD14C16B9D7CEF2906FAE715E519DC,
	Provider_Start_mF4B8E1109EA3D6641B7BBC6D030DB3CC31EDEB86,
	Provider_Stop_mD261C689F310117F4E3E9BDAD86ED85E40A0A1E7,
	Provider_Destroy_m663FE695F3D6F792185B1DA8038FE49827602925,
	Provider_get_automaticPlacementRequested_m9B5004655119A447E62D41C67E080AFE47948D0E,
	Provider_set_automaticPlacementRequested_m44104AA9F0658054A418F6C63D62542E425D9F35,
	Provider_get_automaticPlacementEnabled_mB5334AFCB345E05EDD9BD3E2B6BB20BAAEFAB5B3,
	Provider_get_environmentTextureHDRRequested_m617730B6C4565085184D2312488F5E5DE2E49BDA,
	Provider_set_environmentTextureHDRRequested_mA8B32D4D17D116112F639EBCC804885C81339C06,
	Provider_get_environmentTextureHDREnabled_mE0FE8C839DEF777561E3E35F14043DFCACBD2790,
	Provider_TryAddEnvironmentProbe_m24898D8AA6188C532001CFFF37ED24AF487AF88A,
	Provider_RemoveEnvironmentProbe_m99508DA9076D76663A2522E4F4E71DE0085159CB,
	NULL,
	Provider__ctor_mC53B619920A3DCA6D544EE8C2CB291BA654AE955,
	Provider_Start_m01229A7C89ACBF0622DCBABE00BBC51073BBF7FD,
	Provider_Stop_m93A00BA28BEAE45559E8E8AEA487970AD14336E0,
	Provider_Destroy_mE5F773C1A80C498037F11654F01C87789C1E5F9D,
	Provider_GetFaceMesh_m0E2EF1C274E371DF9C18BF2CD3A4929BB8782234,
	NULL,
	Provider_get_supportedFaceCount_mEEBE86DDF219DE28323F6F57FBF23C75567195C1,
	Provider_get_requestedMaximumFaceCount_m7A870B83987B1C30FC08DF75579CC455E87CE2CA,
	Provider_set_requestedMaximumFaceCount_mD52CFD9F2A73AA0D8C995D979A9287F31CBB81AD,
	Provider_get_currentMaximumFaceCount_m29EC2E2946AB373C642E4EA8F35AEFAE3C3594E5,
	Provider__ctor_m166848C609D6D2087DF29FA51BF6D15D0CDEE8E9,
	Provider_Start_m39EA57D18B1D961D8F46393D460F8C49EB1C2CC4,
	Provider_Stop_m60C55970E0B4333B559254762A03A86B8B8CCE25,
	Provider_Destroy_m2FA5194EC4B55B0C74169BDD17E102FFD3D74BDD,
	Provider_get_pose2DRequested_m5C1FB8E7FABE069C4606E884078DE7BEC283C6F1,
	Provider_set_pose2DRequested_m825FA8836E965804FFD5E9DC86AFD262ED2C751F,
	Provider_get_pose2DEnabled_mFF8908B8606D9B6F72EA1CA4B11D2F48B942CB68,
	Provider_get_pose3DRequested_m472D66466C39778B8EA1AC437A4544D29CBE14D3,
	Provider_set_pose3DRequested_m6632DCF009B525457D0A31817960C31D424E638B,
	Provider_get_pose3DEnabled_m2F1FDA9E6A059CC9DDE60EDE9F716973D7AB0982,
	Provider_get_pose3DScaleEstimationRequested_mC809B773BC1537F61FE85AA7ECC7B3D671DB986F,
	Provider_set_pose3DScaleEstimationRequested_m185A2389C93346F73270FC5976DF7283A3207114,
	Provider_get_pose3DScaleEstimationEnabled_m1B35E3CA7AE5361860B24A691504A7CE841921F0,
	NULL,
	Provider_GetSkeleton_m33CDE833C6F892F96C3EBDCC2E7808E1E563786A,
	Provider_GetHumanBodyPose2DJoints_mE6C0A7C2BBE59E674CE44A199EC1B6EBD80BCF85,
	Provider__ctor_m2DD638AC530CF994612A6E9CB426633013097379,
	Enumerator__ctor_mE0E83E8BE30FC3338D7CA9F0AA641B71EB21AE42_AdjustorThunk,
	Enumerator_MoveNext_m3F8A455818B6DE64979CD24653C260C86A1A9F8F_AdjustorThunk,
	Enumerator_get_Current_mB2B2A92CE85E0846FEDDE76130EE8D6CD32A799C_AdjustorThunk,
	Enumerator_Dispose_m111C536A1B2BF00B4AA92904D8549298AB3DE701_AdjustorThunk,
	Enumerator_GetHashCode_mF5FBE6E01F47937059A2A365DAF03C0F987A2EE4_AdjustorThunk,
	Enumerator_Equals_m63765D51882404C6EFABE2122DDF3C0BEA9C900C_AdjustorThunk,
	Enumerator_Equals_m92192638599D8F40E62775D2D0D3FBD625A7BA2B_AdjustorThunk,
	Enumerator_op_Equality_mE77ACF3611768439424275772001826634CA5FA5,
	Enumerator_op_Inequality_m5E0EEC0D10DBAC36CA9B467BD582B9A43C0086E3,
	Provider_Destroy_m11964F4221D5BA9C0743E2F5BC9292A88087263E,
	NULL,
	NULL,
	NULL,
	Provider_get_requestedMaxNumberOfMovingImages_mE2AF1D82D1093DE329B65D0211F59B17CE33B854,
	Provider_set_requestedMaxNumberOfMovingImages_m75CCCB9B1161AB7EE57BF810ED34962260EADB82,
	Provider_get_currentMaxNumberOfMovingImages_mF1D6CCED9859E909E2E1C5CF755216FE3365A423,
	Provider__ctor_m5C4C95F3D3E533CE14E842815064740F9A796B3D,
	Cinfo_get_id_mE1765ABB412D25FC37DF2545917CDE39A25EA0F6_AdjustorThunk,
	Cinfo_set_id_mF18B67F52DD34A8CD8A718ED36CB4651873B3EE0_AdjustorThunk,
	Cinfo_get_subsystemImplementationType_m32E4C78E8FE23C4B720FA8109F3FC500270A7976_AdjustorThunk,
	Cinfo_set_subsystemImplementationType_m9045C99491613E201755F5A363F9CC5978740E59_AdjustorThunk,
	Cinfo_get_supportsMovingImages_mAFEA78B5C515F6198E9374823D7339A022627395_AdjustorThunk,
	Cinfo_set_supportsMovingImages_m7045441CA2E8CC03F2CD60BA03C8066095684F96_AdjustorThunk,
	Cinfo_get_requiresPhysicalImageDimensions_m3355CD140DD153A14A948B5BD6DB1408E8A0901A_AdjustorThunk,
	Cinfo_set_requiresPhysicalImageDimensions_m9D03A815EA42EA80166E3B86133BE942EA43DA73_AdjustorThunk,
	Cinfo_get_supportsMutableLibrary_mA0084B2031B77E3D57A1F9EC386D4F792AA81351_AdjustorThunk,
	Cinfo_set_supportsMutableLibrary_mEC0925834ED74911F8F74DAC0B900FE755B2046F_AdjustorThunk,
	Cinfo_GetHashCode_mF3E717F0CC69CC001EFA348AC61CF901A303DFA8_AdjustorThunk,
	Cinfo_Equals_m4E86F97DB5221B36E4E56964D652F231705D1CB9_AdjustorThunk,
	Cinfo_Equals_m091DB48FBB609CDAEC3CCEA410AA31F2535C2416_AdjustorThunk,
	Cinfo_op_Equality_m58B48DF606C7865C18F85D32A9B8F367405C1328,
	Cinfo_op_Inequality_mE579E93B5464711D0BF03E164E3744FFFDC43A12,
	NULL,
	Provider_set_library_m6F2877F7FA8A553EE928E6645704D8B221EDDFD7,
	Provider_Destroy_mE1069B245ED2B4BDEC06EC8B0E9E5FBFB5B9800B,
	Provider__ctor_mB5F3534CE2168FA542BD9F930D94F8A2EB9552AF,
	Capabilities_Equals_mAAFDCBAE52F3C1B309C45E48BD808DA78C7DC564_AdjustorThunk,
	Capabilities_Equals_mB45A6FB7362E73CC4755B2472B41C5B2A712A919_AdjustorThunk,
	Capabilities_GetHashCode_m3991EA229599E5B7AA537CE032DE18B460516A36_AdjustorThunk,
	Capabilities_op_Equality_mA0E6EE3679220598BB92754B15236E0038DDAF73,
	Capabilities_op_Inequality_m6D0E893644F1BD640DE1CC718DCD4B448722C1FC,
	Provider_Start_mD50F13CC06EE1729EAE110ABD255FAC63793EE24,
	Provider_Stop_m25975AC9B019910481B63C255848CB770CFD3C29,
	Provider_Destroy_m31B32525D0C7699CCD06CE773206BB418B4E0A26,
	Provider_get_requestedHumanStencilMode_m18256398D8FE8F0542FCC4BB65C9EA3514FABAE0,
	Provider_set_requestedHumanStencilMode_m00780B697E372E9B41698CEEF37878EA0C4F60E4,
	Provider_get_currentHumanStencilMode_m5D6E29969695702B94EDB769874EC2D24A382548,
	Provider_get_requestedHumanDepthMode_m15169D772194CA27D20C9550D195906501E2A8D7,
	Provider_set_requestedHumanDepthMode_m13E63E8CF1595A488707D006DED37F11FB1A3DF8,
	Provider_get_currentHumanDepthMode_m74F79DB0B223D85FB98369AEA1025D6163A23A1A,
	Provider_TryGetHumanStencil_m7C5C0D51BAA6B7856607DD089AF19859DA24EA6D,
	Provider_TryAcquireHumanStencilCpuImage_m0A0F53372BDC5692117AA6D266E8C4BB16AE2911,
	Provider_get_humanStencilCpuImageApi_m5FC7EE8966D1E76BAE1135170222934A1F610B80,
	Provider_TryGetHumanDepth_m2375CEC3B42856A637D62DFB471C8A5F4C7F0A98,
	Provider_TryAcquireHumanDepthCpuImage_m42F193F062F56FF3BBDE3C65B0D75F3C1EC18C5E,
	Provider_get_humanDepthCpuImageApi_m2E34052E2595468EE99BA0717E472460C8F6A28B,
	Provider_GetTextureDescriptors_m6C6D5BF23FE555624A90F4D58D51E18924E1C77E,
	Provider_GetMaterialKeywords_m3384CA3D7122556953D02D84FD79D1C2899C3DF6,
	Provider__ctor_mA606A7F882679DD5F680A8129A3E720240F92751,
	Provider_Start_m3C7BEE1FE7D772CDFA9F85954E99BE5221E0C619,
	Provider_Stop_m1B77B544FB83D259FB48E593F6D9588606988E0F,
	Provider_Destroy_m44B1A8963DD124B74CC43086FE505E152A410FCD,
	NULL,
	Provider__ctor_m6001DA63BA8C0083F1108DA94BD0560F3328ED84,
	Provider_Start_m7AE9B6E2114F33DEC961029A0FAC48D34E31D41F,
	Provider_Stop_m69077CC0D1E396DAAA7BFFD006B77BA84F6F995D,
	Provider_Destroy_mC8502382CF0665866814C2099DDA6B1A6E0C23B0,
	Provider_GetBoundary_m2C4BE79AE5170DC51ABD28DA0C27401ABD8F051C,
	NULL,
	Provider_get_requestedPlaneDetectionMode_m062ADF77A2CD37DD74A04C75892B71E7F3DFDF6F,
	Provider_set_requestedPlaneDetectionMode_m81E255515CBA075567F35CCF75E58E3F04CC66F8,
	Provider_get_currentPlaneDetectionMode_mD04D0393449872D56C83A519B63BA5E9805A7EDE,
	Provider__ctor_m7AD4BD2425F46D263463BCFE2237CC592391C60C,
	Cinfo_get_id_m2403BAC8C8F4BF231808C7B49F4C577E0856726F_AdjustorThunk,
	Cinfo_set_id_mB3D68A1AEE80E5941EBE7DEFFFB34AC44AB92A8E_AdjustorThunk,
	Cinfo_get_subsystemImplementationType_m0B590545FD801FEF5F7F41CBDCCB1FD1A02D191D_AdjustorThunk,
	Cinfo_set_subsystemImplementationType_mDACCFA76DE27D24DAD241496ABCE1606EA51E97C_AdjustorThunk,
	Cinfo_get_supportsHorizontalPlaneDetection_m63C8441C4FC4ABC35300ADE1F54AB2888A1B39C7_AdjustorThunk,
	Cinfo_set_supportsHorizontalPlaneDetection_m03E0099B1BAC214B26A2F4D476DA2A7D5AE01EF9_AdjustorThunk,
	Cinfo_get_supportsVerticalPlaneDetection_mCCE776E1BB11FB03839F91E120320DCFBD6E1884_AdjustorThunk,
	Cinfo_set_supportsVerticalPlaneDetection_m3F6EDFE99022E0D214BEAD727F1AB7A77370DBF4_AdjustorThunk,
	Cinfo_get_supportsArbitraryPlaneDetection_mD7F89437FBB000DA51ACDD62370FC94D74E0FD99_AdjustorThunk,
	Cinfo_set_supportsArbitraryPlaneDetection_m26BE6DBE028AC98927B6E9349599DAF9E4213D18_AdjustorThunk,
	Cinfo_get_supportsBoundaryVertices_m7CE566F0032F95077CB95A1C366D0CD42F32AE49_AdjustorThunk,
	Cinfo_set_supportsBoundaryVertices_m8B0526794B4738A739CD44345ACBA74639B0F1F5_AdjustorThunk,
	Cinfo_get_supportsClassification_m14F960125988547CFF0E554530B66B20BE3E691F_AdjustorThunk,
	Cinfo_set_supportsClassification_m6C3B970AD60C50F76244036E320FB7327A0493D4_AdjustorThunk,
	Cinfo_Equals_m936E7CA82DA297BC58F64EDF44B95E773814201F_AdjustorThunk,
	Cinfo_Equals_mE47F4CD38B3E6F3814A12A842B633064533C2A1B_AdjustorThunk,
	Cinfo_GetHashCode_m3F8925D6A3131763126D23787186F7696FD5C63B_AdjustorThunk,
	Cinfo_op_Equality_mED6DCEBD3B06F1031BEB3A9391C4F25DDBAE0850,
	Cinfo_op_Inequality_mCB1CE860BB90AD58801FB9AA8618BF780E235FE3,
	NULL,
	NULL,
	Provider_Start_mA565B894F858E9176BD59D05BCA2656E89E68184,
	Provider_Stop_mADDEF920450D92ACE94E849F5179B62487BEABE2,
	Provider_Destroy_mF35AF8F2635ED60E06B102E0AB210D350D0A77D3,
	Provider_TryAddRaycast_m0C6021D62486AB3DEDFC3271F0E870E6164F02A8,
	Provider_TryAddRaycast_m41C77266A25B9178723890A5F425FF6C3740F79E,
	Provider_RemoveRaycast_mCE8AD69CF81315A58D0B693502C87760E7510FFB,
	Provider_GetChanges_m60D267A3A9EC93BE9769C31F17B45CB954F8CE3F,
	Provider_Raycast_m6063A859AC10ACF7F27A4AE0AAD82C3A0DF831AE,
	Provider_Raycast_mF885EFF5FFD12196F53C9FE405EC523020C35CE2,
	Provider__ctor_m1A46FEF4E5C93F21E2C802E59B99507E79C0F2C1,
	Cinfo_get_id_m0418C70EB8FBC60BB9B83053AA5175AEFE31CAF8_AdjustorThunk,
	Cinfo_set_id_mFFFBB447D6D0DF4A27428D414FE23BCCB16D78D1_AdjustorThunk,
	Cinfo_get_subsystemImplementationType_m0E0FEE226FD08939BF83B7B2644BEC6362BA157B_AdjustorThunk,
	Cinfo_set_subsystemImplementationType_m00E3D77F5C33C670222169A514C9804886D4FD72_AdjustorThunk,
	Cinfo_get_supportsViewportBasedRaycast_m323FE06DA2E4222E6BF4CE89541DB4630CB254B3_AdjustorThunk,
	Cinfo_set_supportsViewportBasedRaycast_mBA63D727FCB8E5FE2EF1BD5CA2192B54768DEF0D_AdjustorThunk,
	Cinfo_get_supportsWorldBasedRaycast_mFBB112B068EE22D519CAC45E35255D6FDACCAE74_AdjustorThunk,
	Cinfo_set_supportsWorldBasedRaycast_mDDA30FB5ADF2800F44467B727F19668AAE05AFEF_AdjustorThunk,
	Cinfo_get_supportedTrackableTypes_mCC57E28DFCE93ECA772B1DE2E3E49AD030D79424_AdjustorThunk,
	Cinfo_set_supportedTrackableTypes_m91638341F04B3BC3688660AFFE66308C19B13C6B_AdjustorThunk,
	Cinfo_get_supportsTrackedRaycasts_m7259F48B08EA9AEEE4E0966758AB3600F9E62928_AdjustorThunk,
	Cinfo_set_supportsTrackedRaycasts_mF82AA23E5BE9FB36864FD268E7B46A2E043E75EB_AdjustorThunk,
	Cinfo_GetHashCode_mCC56E718130099F9809415C6C1CE9DC981F06211_AdjustorThunk,
	Cinfo_Equals_mA57BDAF996011C56A0017EFBBB805683CFA03452_AdjustorThunk,
	Cinfo_ToString_m9E094799A4E711569550478753C87EE9FC40DC24_AdjustorThunk,
	Cinfo_Equals_mC668E44130D6114FFD62DA8470840EE5E39DBBB5_AdjustorThunk,
	Cinfo_op_Equality_mB012A524026CD2F05A29AB06C6E364D2B2BBD773,
	Cinfo_op_Inequality_mE9E256858FF976A18E836CDE86AA6AF00DC20828,
	Provider_Resume_m8CAB34CF062DD4D2BE34F606AA14F9F78BB6904E,
	Provider_Pause_m29AA9017C4F53BEC8B24467A7C469512F421A97A,
	Provider_Update_m3C7FA8783EB5ED76A0A022CB87D840921D94941E,
	Provider_Update_m1E2645995751B235BB20D6E7CD413AD66CEF396A,
	Provider_get_requestedFeatures_m86F0B383BB349C56A01C6D745DD270B4FD02DA96,
	Provider_get_requestedTrackingMode_mA3745867DA98C76877E1BCA45271D9B05BE16B62,
	Provider_set_requestedTrackingMode_mCF43B7949FAF18EA2BB8B5F77A7B12F8669BB087,
	Provider_get_currentTrackingMode_m4052ECEBABD1313C1E889CCC12D9D7C6F71812F3,
	Provider_GetConfigurationDescriptors_m1D29E783FF6BB21848ED664E649A5C388E27240B,
	Provider_Destroy_m0BFF3C770F8D3DDEDB8BBE1E3E540B33722CF8AC,
	Provider_Reset_m43A334CBFFF6A9D9FC404A0FD5DC05A41CE9CAE1,
	Provider_OnApplicationPause_m02759BAEDA12223C11B002BE7CEAA14C3308C4AD,
	Provider_OnApplicationResume_mD415E31314285E9277FF8C9B5F3B39E34417B736,
	Provider_get_nativePtr_mEEB293C5FF1BBA91207EABA24752E9DF7A715609,
	Provider_GetAvailabilityAsync_m9CC6F74169601931E94DA1177C34A543048B0A01,
	Provider_InstallAsync_m074282442B17B260C246A17F8D7E1C4E947A6017,
	Provider_get_trackingState_m9E8D77D2BA6BD8F3508CFA530482C6D790DCFF35,
	Provider_get_notTrackingReason_m3EF060E8F7A23B6CD196E1B7FCF2F34457B065AA,
	Provider_get_sessionId_mD3AED2746B5F6920BD6BCC4234D6B9189CCF4BFA,
	Provider_get_matchFrameRateEnabled_mF335F53CADFD3D8BC3FC07BE5EAEBC9DB77788BB,
	Provider_get_matchFrameRateRequested_mE1E128BE57DC7CE46CD39CE4C196136D1CA0D41D,
	Provider_set_matchFrameRateRequested_mE5B0D38037143DC102C7B2729ECA3534D9E99DAE,
	Provider_get_frameRate_m55D5E1C5FDF9ADAB43DE7E3F74C7A463ECA63F33,
	Provider__ctor_m890EE1FF005D8F1C10E3F1F4B0B9C37D9ECD29DA,
	Cinfo_get_supportsInstall_m5FBFD4D2F10A6A46F66F4EFBC61EA14DE0FEED99_AdjustorThunk,
	Cinfo_set_supportsInstall_mD74EB42C503AC5393E630A56E3AE579FE1558660_AdjustorThunk,
	Cinfo_get_supportsMatchFrameRate_mE591F09F87EA8F4E5563039C47A6331E6AF31895_AdjustorThunk,
	Cinfo_set_supportsMatchFrameRate_m2B92004D3F2E01EA5DDFBF5F928C5604E68B8D21_AdjustorThunk,
	Cinfo_get_id_m998EB4BD213159391A0A66FD6C002C1CE5CD14E8_AdjustorThunk,
	Cinfo_set_id_mC4FF3C524E18065C55B5142D58FBD58A66479A41_AdjustorThunk,
	Cinfo_get_subsystemImplementationType_m63942F2B0929DF14EB7885E7553A9970FBD3E108_AdjustorThunk,
	Cinfo_set_subsystemImplementationType_m3C759AEC2943DE059B20AA7F5A5B932B7432473C_AdjustorThunk,
	Cinfo_GetHashCode_m06E1060A5995A0C346AB7C0E56CCAC4BEC758A6E_AdjustorThunk,
	Cinfo_Equals_mC919333F29E857CC3F929451A080AC4A38385E01_AdjustorThunk,
	Cinfo_Equals_m2118FE6BBF6355D645E72BDD6662CF313B8E94EB_AdjustorThunk,
	Cinfo_op_Equality_mCDF32E8CB0B505E476F92FD9A3FAAD6C46BF20DA,
	Cinfo_op_Inequality_mA173B6B47123F27CE69E7C18FECEC6871CB08A99,
	OnImageRequestCompleteDelegate__ctor_mD43059752F7624FFA94F613205E495EC605E7D51,
	OnImageRequestCompleteDelegate_Invoke_m33E9D6285A745EA2D0F9E06424DD40AEA7B2CC04,
	OnImageRequestCompleteDelegate_BeginInvoke_m0EAA6B016398F7313486563F2F010843FB6A0553,
	OnImageRequestCompleteDelegate_EndInvoke_m68F9AA715E179A2D6E7262A667F63B6F555490B0,
	Cinfo_get_dataPtr_m860037B7497AD33D5165C395EB3AFFAEA0F6BADC_AdjustorThunk,
	Cinfo_get_dataLength_mFBEFD08EFEE2017356874B6E8AE6303597AD3D36_AdjustorThunk,
	Cinfo_get_rowStride_m6CE40AD415A5A2F2476C3B91D25E93099A52102E_AdjustorThunk,
	Cinfo_get_pixelStride_m0D02EC6E5A6B35E13DE45915714A05A15BEFE59C_AdjustorThunk,
	Cinfo__ctor_mF63E5ED8C476C4959A3BD447AC19EAD8B5179F3E_AdjustorThunk,
	Cinfo_Equals_mAC6C6B00AE4DD3426FAD7123B24AB255D468C12B_AdjustorThunk,
	Cinfo_Equals_m7AB4E01E78BD52CE783E6C327A2534FF76C572A1_AdjustorThunk,
	Cinfo_op_Equality_m1ABC4C273CBEF7D200EBE5CD30342A252C55E19F,
	Cinfo_op_Inequality_m429704A6D661F16D034470AD32CA75E29101814A,
	Cinfo_GetHashCode_m6BB3D041840DCC39ED4E94BE8F415BECC722EBEB_AdjustorThunk,
	Cinfo_ToString_m207241F57F3BCD62CA497E9746D67B5DCE5028E4_AdjustorThunk,
};
static const int32_t s_InvokerIndices[1357] = 
{
	2008,
	2009,
	2010,
	2011,
	2012,
	10,
	15,
	722,
	10,
	2013,
	9,
	2014,
	2014,
	3,
	23,
	23,
	23,
	23,
	2015,
	2016,
	2017,
	2018,
	14,
	89,
	31,
	2020,
	2021,
	2024,
	2009,
	2010,
	2011,
	2012,
	10,
	15,
	722,
	10,
	2025,
	9,
	2026,
	2026,
	3,
	23,
	23,
	23,
	23,
	2027,
	2016,
	2017,
	2018,
	14,
	89,
	31,
	2029,
	2030,
	10,
	10,
	2033,
	780,
	15,
	2034,
	2035,
	14,
	10,
	9,
	2036,
	2037,
	2037,
	181,
	733,
	733,
	1557,
	1505,
	1505,
	10,
	15,
	10,
	733,
	464,
	733,
	733,
	1557,
	1516,
	2038,
	2039,
	733,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	843,
	843,
	843,
	843,
	843,
	843,
	2040,
	9,
	2041,
	2041,
	10,
	14,
	1532,
	1532,
	2033,
	2042,
	2043,
	9,
	2044,
	2044,
	10,
	14,
	733,
	338,
	733,
	338,
	733,
	338,
	733,
	338,
	10,
	32,
	2045,
	9,
	2046,
	2046,
	10,
	14,
	23,
	181,
	181,
	209,
	89,
	89,
	31,
	181,
	181,
	209,
	23,
	23,
	23,
	2047,
	14,
	32,
	843,
	2048,
	2049,
	2050,
	89,
	14,
	2051,
	89,
	576,
	843,
	843,
	2052,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	2055,
	9,
	2056,
	2056,
	10,
	2057,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	2058,
	2059,
	2060,
	181,
	209,
	2061,
	10,
	2062,
	9,
	2063,
	2063,
	2064,
	23,
	15,
	181,
	10,
	2065,
	996,
	14,
	10,
	2066,
	9,
	2067,
	2067,
	2064,
	23,
	1339,
	1339,
	1339,
	446,
	446,
	446,
	446,
	2068,
	269,
	269,
	269,
	269,
	269,
	294,
	144,
	2033,
	2069,
	10,
	10,
	10,
	32,
	10,
	32,
	464,
	339,
	89,
	2070,
	30,
	2071,
	2072,
	2073,
	2074,
	2075,
	2076,
	2077,
	2078,
	23,
	2079,
	23,
	10,
	9,
	2080,
	2081,
	2081,
	14,
	3,
	21,
	23,
	23,
	23,
	23,
	2108,
	2109,
	14,
	2111,
	89,
	31,
	89,
	31,
	89,
	31,
	2112,
	2115,
	2009,
	2011,
	2012,
	10,
	15,
	10,
	2116,
	9,
	2117,
	2117,
	3,
	2118,
	2119,
	2120,
	2121,
	2122,
	2123,
	23,
	10,
	9,
	14,
	2124,
	2125,
	2125,
	2126,
	2011,
	2127,
	1516,
	1517,
	2012,
	2128,
	1516,
	1517,
	2039,
	2129,
	10,
	32,
	15,
	7,
	2130,
	9,
	2131,
	2131,
	10,
	14,
	28,
	3,
	23,
	89,
	31,
	14,
	89,
	89,
	31,
	89,
	2132,
	23,
	23,
	23,
	2133,
	2018,
	14,
	2134,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	2136,
	9,
	2137,
	2137,
	10,
	2138,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	2139,
	2140,
	2011,
	2012,
	10,
	15,
	2012,
	2012,
	1516,
	9,
	10,
	2141,
	2141,
	2142,
	3,
	342,
	2118,
	2118,
	2143,
	2144,
	23,
	10,
	9,
	14,
	2145,
	2146,
	2146,
	-1,
	23,
	23,
	23,
	23,
	10,
	32,
	10,
	10,
	2147,
	2148,
	14,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	2150,
	9,
	10,
	2151,
	2151,
	2152,
	89,
	89,
	89,
	89,
	89,
	2153,
	2154,
	177,
	94,
	207,
	208,
	2155,
	2156,
	2157,
	2158,
	2011,
	2127,
	2012,
	2128,
	733,
	338,
	10,
	32,
	15,
	7,
	2159,
	2160,
	9,
	2161,
	2161,
	10,
	3,
	10,
	10,
	1516,
	2012,
	1516,
	2012,
	89,
	2162,
	2163,
	9,
	2164,
	2164,
	10,
	14,
	28,
	10,
	10,
	1532,
	89,
	2165,
	2166,
	9,
	2167,
	2167,
	10,
	14,
	28,
	89,
	31,
	89,
	89,
	31,
	89,
	89,
	31,
	89,
	23,
	23,
	23,
	23,
	2168,
	2148,
	2169,
	14,
	2170,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	2173,
	9,
	2174,
	2174,
	10,
	2175,
	89,
	31,
	89,
	31,
	89,
	31,
	2176,
	10,
	2177,
	2178,
	2178,
	10,
	37,
	37,
	30,
	2179,
	2180,
	23,
	2177,
	10,
	2177,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	28,
	2184,
	10,
	32,
	10,
	14,
	89,
	31,
	89,
	31,
	89,
	31,
	2186,
	2187,
	2190,
	722,
	722,
	89,
	1532,
	733,
	733,
	14,
	14,
	14,
	10,
	9,
	2191,
	2192,
	2192,
	10,
	2193,
	2177,
	2194,
	722,
	23,
	2195,
	2196,
	2011,
	722,
	2012,
	1532,
	10,
	15,
	10,
	2197,
	9,
	2198,
	2198,
	3,
	-1,
	-1,
	-1,
	14,
	23,
	23,
	14,
	26,
	23,
	23,
	2199,
	-1,
	2201,
	2202,
	2203,
	14,
	722,
	-1,
	28,
	2206,
	10,
	9,
	2207,
	2207,
	23,
	10,
	2208,
	2209,
	722,
	2210,
	23,
	2211,
	2011,
	2012,
	10,
	15,
	722,
	2010,
	9,
	10,
	2212,
	2212,
	2213,
	3,
	46,
	46,
	10,
	32,
	10,
	10,
	32,
	10,
	23,
	23,
	23,
	23,
	843,
	843,
	843,
	843,
	2047,
	576,
	14,
	2214,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	2215,
	9,
	2216,
	2216,
	10,
	2217,
	89,
	31,
	89,
	31,
	2218,
	2010,
	2219,
	2011,
	2012,
	10,
	15,
	722,
	10,
	2220,
	9,
	2221,
	2221,
	3,
	23,
	23,
	23,
	23,
	2222,
	14,
	10,
	32,
	-1,
	115,
	2224,
	2225,
	2011,
	2011,
	2012,
	1532,
	1532,
	1532,
	10,
	10,
	15,
	10,
	733,
	733,
	1516,
	2226,
	2227,
	14,
	9,
	10,
	2228,
	2228,
	2229,
	3,
	46,
	46,
	23,
	23,
	23,
	23,
	10,
	32,
	10,
	2230,
	2148,
	14,
	-1,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	2232,
	2233,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2236,
	2011,
	2012,
	10,
	15,
	733,
	2011,
	2237,
	10,
	9,
	2238,
	2239,
	2239,
	3,
	2240,
	2011,
	2127,
	2012,
	2128,
	733,
	338,
	10,
	32,
	2241,
	10,
	9,
	2242,
	2243,
	2243,
	3,
	23,
	23,
	23,
	23,
	2244,
	2245,
	2246,
	2127,
	2247,
	2248,
	14,
	89,
	31,
	89,
	31,
	10,
	32,
	89,
	31,
	2252,
	2253,
	26,
	27,
	23,
	977,
	2180,
	722,
	10,
	9,
	14,
	28,
	105,
	2256,
	2257,
	2257,
	3,
	46,
	46,
	15,
	722,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	2258,
	2259,
	2260,
	2261,
	181,
	2262,
	23,
	23,
	10,
	181,
	209,
	181,
	14,
	26,
	10,
	89,
	89,
	31,
	10,
	14,
	14,
	89,
	31,
	89,
	31,
	2264,
	2265,
	10,
	32,
	2033,
	2069,
	10,
	9,
	14,
	2268,
	2269,
	2269,
	2270,
	181,
	209,
	181,
	209,
	977,
	26,
	14,
	10,
	9,
	2018,
	2271,
	2271,
	3,
	2011,
	2012,
	10,
	15,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	46,
	46,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	15,
	7,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	89,
	10,
	32,
	10,
	32,
	2272,
	23,
	2272,
	9,
	2273,
	2273,
	10,
	14,
	23,
	23,
	23,
	2019,
	2016,
	2017,
	2018,
	23,
	14,
	26,
	14,
	26,
	89,
	31,
	10,
	9,
	2022,
	2023,
	2023,
	23,
	23,
	23,
	2028,
	2016,
	2017,
	2018,
	23,
	14,
	26,
	14,
	26,
	89,
	31,
	10,
	9,
	2031,
	2032,
	2032,
	14,
	14,
	89,
	89,
	181,
	181,
	209,
	23,
	23,
	23,
	2051,
	89,
	89,
	31,
	181,
	181,
	209,
	843,
	2053,
	2049,
	2050,
	2054,
	576,
	843,
	28,
	32,
	23,
	883,
	2082,
	2083,
	2084,
	30,
	1138,
	2085,
	32,
	32,
	37,
	2086,
	23,
	2089,
	2079,
	10,
	2090,
	-1,
	23,
	10,
	9,
	2091,
	2092,
	2092,
	14,
	2093,
	2094,
	2033,
	2069,
	10,
	32,
	10,
	32,
	2095,
	10,
	2096,
	9,
	2097,
	2097,
	14,
	10,
	32,
	10,
	32,
	2098,
	1699,
	2099,
	10,
	9,
	2100,
	2101,
	2101,
	14,
	10,
	2033,
	10,
	464,
	10,
	2105,
	2106,
	9,
	2107,
	2107,
	10,
	14,
	23,
	23,
	23,
	2110,
	2109,
	23,
	89,
	31,
	89,
	31,
	89,
	31,
	10,
	32,
	2113,
	9,
	10,
	2114,
	2114,
	23,
	23,
	23,
	89,
	31,
	89,
	89,
	31,
	89,
	2133,
	2018,
	2135,
	23,
	23,
	23,
	23,
	2148,
	2149,
	10,
	10,
	32,
	10,
	23,
	23,
	23,
	23,
	89,
	31,
	89,
	89,
	31,
	89,
	89,
	31,
	89,
	2171,
	2148,
	2172,
	23,
	26,
	89,
	2181,
	23,
	10,
	9,
	2182,
	2183,
	2183,
	23,
	2185,
	26,
	28,
	10,
	32,
	10,
	23,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	10,
	2188,
	9,
	2189,
	2189,
	2200,
	26,
	23,
	23,
	2204,
	9,
	10,
	2205,
	2205,
	23,
	23,
	23,
	10,
	32,
	10,
	10,
	32,
	10,
	843,
	843,
	14,
	843,
	843,
	14,
	2054,
	576,
	23,
	23,
	23,
	23,
	2223,
	23,
	23,
	23,
	23,
	2148,
	2231,
	10,
	32,
	10,
	23,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	2234,
	9,
	10,
	2235,
	2235,
	-1,
	-1,
	23,
	23,
	23,
	2245,
	2246,
	2127,
	2249,
	2250,
	2251,
	23,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	10,
	32,
	89,
	31,
	10,
	9,
	14,
	2254,
	2255,
	2255,
	23,
	23,
	2259,
	2263,
	181,
	181,
	209,
	181,
	2262,
	23,
	23,
	23,
	23,
	15,
	14,
	14,
	10,
	10,
	722,
	89,
	89,
	31,
	10,
	23,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	26,
	10,
	2266,
	9,
	2267,
	2267,
	124,
	2087,
	2088,
	26,
	15,
	10,
	10,
	10,
	2102,
	2103,
	9,
	2104,
	2104,
	10,
	14,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[1] = 
{
	{ 0x06000128, 8,  (void**)&XRCpuImage_OnAsyncConversionComplete_mE1632D9BCA9BB444DBB3283CBFE5567609FF98D0_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[15] = 
{
	{ 0x02000051, { 26, 4 } },
	{ 0x02000062, { 33, 11 } },
	{ 0x02000063, { 44, 3 } },
	{ 0x02000064, { 47, 12 } },
	{ 0x02000066, { 59, 5 } },
	{ 0x02000086, { 30, 3 } },
	{ 0x060001C0, { 2, 5 } },
	{ 0x0600028A, { 7, 3 } },
	{ 0x0600028B, { 10, 4 } },
	{ 0x0600028C, { 14, 3 } },
	{ 0x06000295, { 17, 1 } },
	{ 0x0600029B, { 18, 2 } },
	{ 0x060002F2, { 20, 1 } },
	{ 0x06000318, { 21, 5 } },
	{ 0x06000436, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[64] = 
{
	{ (Il2CppRGCTXDataType)3, 24592 },
	{ (Il2CppRGCTXDataType)3, 24593 },
	{ (Il2CppRGCTXDataType)3, 24594 },
	{ (Il2CppRGCTXDataType)3, 24595 },
	{ (Il2CppRGCTXDataType)3, 24596 },
	{ (Il2CppRGCTXDataType)2, 27213 },
	{ (Il2CppRGCTXDataType)3, 24597 },
	{ (Il2CppRGCTXDataType)3, 24598 },
	{ (Il2CppRGCTXDataType)3, 24599 },
	{ (Il2CppRGCTXDataType)3, 24600 },
	{ (Il2CppRGCTXDataType)3, 24601 },
	{ (Il2CppRGCTXDataType)3, 24602 },
	{ (Il2CppRGCTXDataType)3, 24603 },
	{ (Il2CppRGCTXDataType)3, 24604 },
	{ (Il2CppRGCTXDataType)2, 27275 },
	{ (Il2CppRGCTXDataType)3, 24605 },
	{ (Il2CppRGCTXDataType)3, 24606 },
	{ (Il2CppRGCTXDataType)1, 31691 },
	{ (Il2CppRGCTXDataType)1, 27289 },
	{ (Il2CppRGCTXDataType)2, 27289 },
	{ (Il2CppRGCTXDataType)1, 31692 },
	{ (Il2CppRGCTXDataType)3, 24607 },
	{ (Il2CppRGCTXDataType)3, 24608 },
	{ (Il2CppRGCTXDataType)3, 24609 },
	{ (Il2CppRGCTXDataType)2, 27346 },
	{ (Il2CppRGCTXDataType)3, 24610 },
	{ (Il2CppRGCTXDataType)3, 24611 },
	{ (Il2CppRGCTXDataType)2, 31693 },
	{ (Il2CppRGCTXDataType)3, 24612 },
	{ (Il2CppRGCTXDataType)3, 24613 },
	{ (Il2CppRGCTXDataType)3, 24614 },
	{ (Il2CppRGCTXDataType)2, 27359 },
	{ (Il2CppRGCTXDataType)3, 24615 },
	{ (Il2CppRGCTXDataType)2, 27403 },
	{ (Il2CppRGCTXDataType)3, 24616 },
	{ (Il2CppRGCTXDataType)3, 24617 },
	{ (Il2CppRGCTXDataType)3, 24618 },
	{ (Il2CppRGCTXDataType)3, 24619 },
	{ (Il2CppRGCTXDataType)3, 24620 },
	{ (Il2CppRGCTXDataType)3, 24621 },
	{ (Il2CppRGCTXDataType)2, 27405 },
	{ (Il2CppRGCTXDataType)3, 24622 },
	{ (Il2CppRGCTXDataType)3, 24623 },
	{ (Il2CppRGCTXDataType)3, 24624 },
	{ (Il2CppRGCTXDataType)3, 24625 },
	{ (Il2CppRGCTXDataType)2, 27410 },
	{ (Il2CppRGCTXDataType)2, 31694 },
	{ (Il2CppRGCTXDataType)2, 31695 },
	{ (Il2CppRGCTXDataType)3, 24626 },
	{ (Il2CppRGCTXDataType)3, 24627 },
	{ (Il2CppRGCTXDataType)3, 24628 },
	{ (Il2CppRGCTXDataType)2, 27417 },
	{ (Il2CppRGCTXDataType)3, 24629 },
	{ (Il2CppRGCTXDataType)3, 24630 },
	{ (Il2CppRGCTXDataType)2, 31696 },
	{ (Il2CppRGCTXDataType)3, 24631 },
	{ (Il2CppRGCTXDataType)3, 24632 },
	{ (Il2CppRGCTXDataType)3, 24633 },
	{ (Il2CppRGCTXDataType)3, 24634 },
	{ (Il2CppRGCTXDataType)3, 24635 },
	{ (Il2CppRGCTXDataType)3, 24636 },
	{ (Il2CppRGCTXDataType)3, 24637 },
	{ (Il2CppRGCTXDataType)3, 24638 },
	{ (Il2CppRGCTXDataType)2, 27424 },
};
extern const Il2CppCodeGenModule g_Unity_XR_ARSubsystemsCodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_ARSubsystemsCodeGenModule = 
{
	"Unity.XR.ARSubsystems.dll",
	1357,
	s_methodPointers,
	s_InvokerIndices,
	1,
	s_reversePInvokeIndices,
	15,
	s_rgctxIndices,
	64,
	s_rgctxValues,
	NULL,
};
