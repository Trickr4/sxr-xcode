﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String UnityEngine.WWW::EscapeURL(System.String)
extern void WWW_EscapeURL_m380EF432DAC12ED77405BDF7C4C98797AF226161 ();
// 0x00000002 System.String UnityEngine.WWW::EscapeURL(System.String,System.Text.Encoding)
extern void WWW_EscapeURL_m0647BBF9E8044A60534D2139A91DA4A788D91420 ();
// 0x00000003 System.Boolean UnityEngine.WWW::get_keepWaiting()
extern void WWW_get_keepWaiting_m2C52E54F48964EFD711C55A514E738CDF5D2EEB4 ();
static Il2CppMethodPointer s_methodPointers[3] = 
{
	WWW_EscapeURL_m380EF432DAC12ED77405BDF7C4C98797AF226161,
	WWW_EscapeURL_m0647BBF9E8044A60534D2139A91DA4A788D91420,
	WWW_get_keepWaiting_m2C52E54F48964EFD711C55A514E738CDF5D2EEB4,
};
static const int32_t s_InvokerIndices[3] = 
{
	0,
	1,
	89,
};
extern const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule = 
{
	"UnityEngine.UnityWebRequestWWWModule.dll",
	3,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
