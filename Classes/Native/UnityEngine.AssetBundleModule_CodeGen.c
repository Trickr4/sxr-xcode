﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.AssetBundle::.ctor()
extern void AssetBundle__ctor_mF8A0A863455A7DF958A966C86BE58E07535CD3A4 ();
// 0x00000002 System.Void UnityEngine.AssetBundle::UnloadAllAssetBundles(System.Boolean)
extern void AssetBundle_UnloadAllAssetBundles_mB49AAB07A97A3383897CF55F81EC096620969363 ();
// 0x00000003 UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromFile_Internal(System.String,System.UInt32,System.UInt64)
extern void AssetBundle_LoadFromFile_Internal_m684F1E7201F297DBCDF4654DFCA2159F61ABFA50 ();
// 0x00000004 UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromFile(System.String)
extern void AssetBundle_LoadFromFile_m9D7CD085CD2B41301BDDA86BB040A2479B227AF9 ();
// 0x00000005 System.Boolean UnityEngine.AssetBundle::Contains(System.String)
extern void AssetBundle_Contains_mF604436762C9B5242AB73A4BEE853298B9054519 ();
// 0x00000006 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String)
extern void AssetBundle_LoadAsset_m5D5EFC8B7A27AE31CCDA12F652664513F7BF8A29 ();
// 0x00000007 T UnityEngine.AssetBundle::LoadAsset(System.String)
// 0x00000008 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String,System.Type)
extern void AssetBundle_LoadAsset_m36BA6CCFE773DFCF4490EB4910022B74BB6BF283 ();
// 0x00000009 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)
extern void AssetBundle_LoadAsset_Internal_m7B24CA02317176FEADA41DE03CFF849D92B2392A ();
// 0x0000000A UnityEngine.Object[] UnityEngine.AssetBundle::LoadAllAssets()
extern void AssetBundle_LoadAllAssets_m66CEC478D6E4BEE3CCA9411310556928D075B7A4 ();
// 0x0000000B UnityEngine.Object[] UnityEngine.AssetBundle::LoadAllAssets(System.Type)
extern void AssetBundle_LoadAllAssets_m1F7E39416EBCC192881058CBDCA490A8019E13EA ();
// 0x0000000C System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern void AssetBundle_Unload_m5B7DE41516A059545F8C493EC459EA0A9166EBB3 ();
// 0x0000000D System.String[] UnityEngine.AssetBundle::GetAllAssetNames()
extern void AssetBundle_GetAllAssetNames_m0E14E731A20685CECDA32C33E44D7710E5194C26 ();
// 0x0000000E System.String[] UnityEngine.AssetBundle::GetAllScenePaths()
extern void AssetBundle_GetAllScenePaths_m94680159EEF4C07626DA7A8ED1C889849EEAC45D ();
// 0x0000000F UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)
extern void AssetBundle_LoadAssetWithSubAssets_Internal_m576FBCE3D60FC5E7E4462E6EF55DF8E5F49072B4 ();
// 0x00000010 System.String[] UnityEngine.AssetBundleManifest::GetAllAssetBundles()
extern void AssetBundleManifest_GetAllAssetBundles_mA1CBE54E5AD5BA69F8E0CE33F87A2055FCF1B599 ();
static Il2CppMethodPointer s_methodPointers[16] = 
{
	AssetBundle__ctor_mF8A0A863455A7DF958A966C86BE58E07535CD3A4,
	AssetBundle_UnloadAllAssetBundles_mB49AAB07A97A3383897CF55F81EC096620969363,
	AssetBundle_LoadFromFile_Internal_m684F1E7201F297DBCDF4654DFCA2159F61ABFA50,
	AssetBundle_LoadFromFile_m9D7CD085CD2B41301BDDA86BB040A2479B227AF9,
	AssetBundle_Contains_mF604436762C9B5242AB73A4BEE853298B9054519,
	AssetBundle_LoadAsset_m5D5EFC8B7A27AE31CCDA12F652664513F7BF8A29,
	NULL,
	AssetBundle_LoadAsset_m36BA6CCFE773DFCF4490EB4910022B74BB6BF283,
	AssetBundle_LoadAsset_Internal_m7B24CA02317176FEADA41DE03CFF849D92B2392A,
	AssetBundle_LoadAllAssets_m66CEC478D6E4BEE3CCA9411310556928D075B7A4,
	AssetBundle_LoadAllAssets_m1F7E39416EBCC192881058CBDCA490A8019E13EA,
	AssetBundle_Unload_m5B7DE41516A059545F8C493EC459EA0A9166EBB3,
	AssetBundle_GetAllAssetNames_m0E14E731A20685CECDA32C33E44D7710E5194C26,
	AssetBundle_GetAllScenePaths_m94680159EEF4C07626DA7A8ED1C889849EEAC45D,
	AssetBundle_LoadAssetWithSubAssets_Internal_m576FBCE3D60FC5E7E4462E6EF55DF8E5F49072B4,
	AssetBundleManifest_GetAllAssetBundles_mA1CBE54E5AD5BA69F8E0CE33F87A2055FCF1B599,
};
static const int32_t s_InvokerIndices[16] = 
{
	23,
	851,
	1768,
	0,
	9,
	28,
	-1,
	105,
	105,
	14,
	28,
	31,
	14,
	14,
	105,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000007, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)1, 26367 },
	{ (Il2CppRGCTXDataType)2, 26367 },
};
extern const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule = 
{
	"UnityEngine.AssetBundleModule.dll",
	16,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
};
