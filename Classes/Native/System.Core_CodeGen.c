﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B ();
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 ();
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 ();
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E ();
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 ();
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 ();
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC ();
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 ();
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 ();
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA ();
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 ();
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 ();
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 ();
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD ();
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 ();
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 ();
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 ();
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED ();
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC ();
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 ();
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E ();
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC ();
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 ();
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 ();
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 ();
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F ();
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C ();
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A ();
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 ();
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA ();
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B ();
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F ();
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F ();
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA ();
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 ();
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 ();
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 ();
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA ();
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F ();
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD ();
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA ();
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 ();
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA ();
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 ();
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 ();
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000031 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000032 System.Exception System.Linq.Error::NoMatch()
extern void Error_NoMatch_m96B9371C94C28A7C23CC8B8D25CC7B50734E1B14 ();
// 0x00000033 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000035 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000036 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000039 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenByDescending(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Concat(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ConcatIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000040 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000041 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfType(System.Collections.IEnumerable)
// 0x00000043 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfTypeIterator(System.Collections.IEnumerable)
// 0x00000044 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000045 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000046 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000047 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000048 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000049 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004A TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004B TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004C TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000004D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x0000004E System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004F System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000050 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000051 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000052 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000053 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000054 TAccumulate System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
// 0x00000055 System.Single System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<System.Single>)
extern void Enumerable_Max_mBE99D67A0AF1147E3C46DCF7D489129134248262 ();
// 0x00000056 System.Single System.Linq.Enumerable::Average(System.Collections.Generic.IEnumerable`1<System.Single>)
extern void Enumerable_Average_m48238E2FDB29EE702DAD6BBAD1D019D8D4550FFF ();
// 0x00000057 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000058 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000059 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000005A System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000005B System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000005C System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000005D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000005F System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000060 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000061 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000062 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000063 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000064 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000065 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000066 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000067 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000068 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000069 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000006A System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000006B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000006C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006D System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000006E System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000006F System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000070 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000071 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000072 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000073 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000074 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000075 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000076 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000077 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000078 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000079 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x0000007A System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x0000007B System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000007C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007D System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000007E System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x0000007F System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000080 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000081 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000082 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000083 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000084 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000085 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000086 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x00000087 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x00000088 System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x00000089 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x0000008A System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x0000008B TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000008C System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x0000008D System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x0000008E System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000008F System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000090 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::.ctor(System.Int32)
// 0x00000091 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::System.IDisposable.Dispose()
// 0x00000092 System.Boolean System.Linq.Enumerable_<ConcatIterator>d__59`1::MoveNext()
// 0x00000093 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::<>m__Finally1()
// 0x00000094 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::<>m__Finally2()
// 0x00000095 TSource System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000096 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerator.Reset()
// 0x00000097 System.Object System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerator.get_Current()
// 0x00000098 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000099 System.Collections.IEnumerator System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009A System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x0000009B System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x0000009C System.Boolean System.Linq.Enumerable_<DistinctIterator>d__68`1::MoveNext()
// 0x0000009D System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::<>m__Finally1()
// 0x0000009E TSource System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000009F System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x000000A0 System.Object System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x000000A1 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000A2 System.Collections.IEnumerator System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A3 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::.ctor(System.Int32)
// 0x000000A4 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.IDisposable.Dispose()
// 0x000000A5 System.Boolean System.Linq.Enumerable_<OfTypeIterator>d__97`1::MoveNext()
// 0x000000A6 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::<>m__Finally1()
// 0x000000A7 TResult System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000A8 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.Reset()
// 0x000000A9 System.Object System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.get_Current()
// 0x000000AA System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000AB System.Collections.IEnumerator System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AC System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x000000AD System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x000000AE System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x000000AF System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x000000B0 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000B1 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x000000B2 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x000000B3 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000B4 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B5 System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x000000B6 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000B7 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000B8 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000B9 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000BA System.Void System.Linq.Set`1::Resize()
// 0x000000BB System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000BC System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000BD System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000BE System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BF System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000C0 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000C1 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000C2 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000C3 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x000000C4 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000C5 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000C6 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000C7 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000C8 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000C9 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000CA System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000CB System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000CC System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000CD System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000CE System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000CF System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000D0 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000D1 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000D2 TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000D3 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000D4 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000D5 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000D6 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000D7 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000D8 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000D9 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000DA System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000DB System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000DC System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000DD System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000DE System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000DF System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000E0 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000E1 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000E2 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000E3 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000E4 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000E5 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000E6 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000E7 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000E8 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000E9 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000EA System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000EB System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000EC System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000ED T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000EE System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000EF System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[239] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NoMatch_m96B9371C94C28A7C23CC8B8D25CC7B50734E1B14,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Max_mBE99D67A0AF1147E3C46DCF7D489129134248262,
	Enumerable_Average_m48238E2FDB29EE702DAD6BBAD1D019D8D4550FFF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[239] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	105,
	14,
	105,
	31,
	23,
	23,
	23,
	23,
	23,
	105,
	105,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	917,
	27,
	37,
	206,
	206,
	3,
	0,
	0,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1163,
	1163,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[65] = 
{
	{ 0x02000008, { 100, 4 } },
	{ 0x02000009, { 104, 9 } },
	{ 0x0200000A, { 115, 7 } },
	{ 0x0200000B, { 124, 10 } },
	{ 0x0200000C, { 136, 11 } },
	{ 0x0200000D, { 150, 9 } },
	{ 0x0200000E, { 162, 12 } },
	{ 0x0200000F, { 177, 1 } },
	{ 0x02000010, { 178, 2 } },
	{ 0x02000011, { 180, 12 } },
	{ 0x02000012, { 192, 9 } },
	{ 0x02000013, { 201, 11 } },
	{ 0x02000014, { 212, 6 } },
	{ 0x02000015, { 218, 6 } },
	{ 0x02000016, { 224, 2 } },
	{ 0x02000018, { 226, 8 } },
	{ 0x0200001A, { 234, 3 } },
	{ 0x0200001B, { 239, 5 } },
	{ 0x0200001C, { 244, 7 } },
	{ 0x0200001D, { 251, 3 } },
	{ 0x0200001E, { 254, 7 } },
	{ 0x0200001F, { 261, 4 } },
	{ 0x02000020, { 265, 21 } },
	{ 0x02000022, { 286, 2 } },
	{ 0x06000033, { 0, 10 } },
	{ 0x06000034, { 10, 10 } },
	{ 0x06000035, { 20, 5 } },
	{ 0x06000036, { 25, 5 } },
	{ 0x06000037, { 30, 1 } },
	{ 0x06000038, { 31, 2 } },
	{ 0x06000039, { 33, 2 } },
	{ 0x0600003A, { 35, 2 } },
	{ 0x0600003B, { 37, 1 } },
	{ 0x0600003C, { 38, 1 } },
	{ 0x0600003D, { 39, 2 } },
	{ 0x0600003E, { 41, 1 } },
	{ 0x0600003F, { 42, 2 } },
	{ 0x06000040, { 44, 3 } },
	{ 0x06000041, { 47, 2 } },
	{ 0x06000042, { 49, 1 } },
	{ 0x06000043, { 50, 2 } },
	{ 0x06000044, { 52, 2 } },
	{ 0x06000045, { 54, 2 } },
	{ 0x06000046, { 56, 4 } },
	{ 0x06000047, { 60, 3 } },
	{ 0x06000048, { 63, 4 } },
	{ 0x06000049, { 67, 3 } },
	{ 0x0600004A, { 70, 4 } },
	{ 0x0600004B, { 74, 3 } },
	{ 0x0600004C, { 77, 3 } },
	{ 0x0600004D, { 80, 1 } },
	{ 0x0600004E, { 81, 1 } },
	{ 0x0600004F, { 82, 3 } },
	{ 0x06000050, { 85, 2 } },
	{ 0x06000051, { 87, 3 } },
	{ 0x06000052, { 90, 2 } },
	{ 0x06000053, { 92, 5 } },
	{ 0x06000054, { 97, 3 } },
	{ 0x06000066, { 113, 2 } },
	{ 0x0600006B, { 122, 2 } },
	{ 0x06000070, { 134, 2 } },
	{ 0x06000076, { 147, 3 } },
	{ 0x0600007B, { 159, 3 } },
	{ 0x06000080, { 174, 3 } },
	{ 0x060000BF, { 237, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[288] = 
{
	{ (Il2CppRGCTXDataType)2, 31579 },
	{ (Il2CppRGCTXDataType)3, 24340 },
	{ (Il2CppRGCTXDataType)2, 31580 },
	{ (Il2CppRGCTXDataType)2, 31581 },
	{ (Il2CppRGCTXDataType)3, 24341 },
	{ (Il2CppRGCTXDataType)2, 31582 },
	{ (Il2CppRGCTXDataType)2, 31583 },
	{ (Il2CppRGCTXDataType)3, 24342 },
	{ (Il2CppRGCTXDataType)2, 31584 },
	{ (Il2CppRGCTXDataType)3, 24343 },
	{ (Il2CppRGCTXDataType)2, 31585 },
	{ (Il2CppRGCTXDataType)3, 24344 },
	{ (Il2CppRGCTXDataType)2, 31586 },
	{ (Il2CppRGCTXDataType)2, 31587 },
	{ (Il2CppRGCTXDataType)3, 24345 },
	{ (Il2CppRGCTXDataType)2, 31588 },
	{ (Il2CppRGCTXDataType)2, 31589 },
	{ (Il2CppRGCTXDataType)3, 24346 },
	{ (Il2CppRGCTXDataType)2, 31590 },
	{ (Il2CppRGCTXDataType)3, 24347 },
	{ (Il2CppRGCTXDataType)2, 31591 },
	{ (Il2CppRGCTXDataType)3, 24348 },
	{ (Il2CppRGCTXDataType)3, 24349 },
	{ (Il2CppRGCTXDataType)2, 24602 },
	{ (Il2CppRGCTXDataType)3, 24350 },
	{ (Il2CppRGCTXDataType)2, 31592 },
	{ (Il2CppRGCTXDataType)3, 24351 },
	{ (Il2CppRGCTXDataType)3, 24352 },
	{ (Il2CppRGCTXDataType)2, 24609 },
	{ (Il2CppRGCTXDataType)3, 24353 },
	{ (Il2CppRGCTXDataType)3, 24354 },
	{ (Il2CppRGCTXDataType)2, 31593 },
	{ (Il2CppRGCTXDataType)3, 24355 },
	{ (Il2CppRGCTXDataType)2, 31594 },
	{ (Il2CppRGCTXDataType)3, 24356 },
	{ (Il2CppRGCTXDataType)2, 31595 },
	{ (Il2CppRGCTXDataType)3, 24357 },
	{ (Il2CppRGCTXDataType)3, 24358 },
	{ (Il2CppRGCTXDataType)3, 24359 },
	{ (Il2CppRGCTXDataType)2, 31596 },
	{ (Il2CppRGCTXDataType)3, 24360 },
	{ (Il2CppRGCTXDataType)3, 24361 },
	{ (Il2CppRGCTXDataType)2, 31597 },
	{ (Il2CppRGCTXDataType)3, 24362 },
	{ (Il2CppRGCTXDataType)2, 31598 },
	{ (Il2CppRGCTXDataType)3, 24363 },
	{ (Il2CppRGCTXDataType)3, 24364 },
	{ (Il2CppRGCTXDataType)2, 24648 },
	{ (Il2CppRGCTXDataType)3, 24365 },
	{ (Il2CppRGCTXDataType)3, 24366 },
	{ (Il2CppRGCTXDataType)2, 31599 },
	{ (Il2CppRGCTXDataType)3, 24367 },
	{ (Il2CppRGCTXDataType)2, 24653 },
	{ (Il2CppRGCTXDataType)3, 24368 },
	{ (Il2CppRGCTXDataType)2, 31600 },
	{ (Il2CppRGCTXDataType)3, 24369 },
	{ (Il2CppRGCTXDataType)2, 31601 },
	{ (Il2CppRGCTXDataType)2, 31602 },
	{ (Il2CppRGCTXDataType)2, 24657 },
	{ (Il2CppRGCTXDataType)2, 31603 },
	{ (Il2CppRGCTXDataType)2, 24659 },
	{ (Il2CppRGCTXDataType)2, 31604 },
	{ (Il2CppRGCTXDataType)3, 24370 },
	{ (Il2CppRGCTXDataType)2, 31605 },
	{ (Il2CppRGCTXDataType)2, 31606 },
	{ (Il2CppRGCTXDataType)2, 24662 },
	{ (Il2CppRGCTXDataType)2, 31607 },
	{ (Il2CppRGCTXDataType)2, 24664 },
	{ (Il2CppRGCTXDataType)2, 31608 },
	{ (Il2CppRGCTXDataType)3, 24371 },
	{ (Il2CppRGCTXDataType)2, 31609 },
	{ (Il2CppRGCTXDataType)2, 31610 },
	{ (Il2CppRGCTXDataType)2, 24667 },
	{ (Il2CppRGCTXDataType)2, 31611 },
	{ (Il2CppRGCTXDataType)2, 24669 },
	{ (Il2CppRGCTXDataType)2, 31612 },
	{ (Il2CppRGCTXDataType)3, 24372 },
	{ (Il2CppRGCTXDataType)2, 31613 },
	{ (Il2CppRGCTXDataType)2, 24672 },
	{ (Il2CppRGCTXDataType)2, 31614 },
	{ (Il2CppRGCTXDataType)2, 31615 },
	{ (Il2CppRGCTXDataType)2, 24676 },
	{ (Il2CppRGCTXDataType)2, 24678 },
	{ (Il2CppRGCTXDataType)2, 31616 },
	{ (Il2CppRGCTXDataType)3, 24373 },
	{ (Il2CppRGCTXDataType)2, 31617 },
	{ (Il2CppRGCTXDataType)2, 24681 },
	{ (Il2CppRGCTXDataType)2, 24683 },
	{ (Il2CppRGCTXDataType)2, 31618 },
	{ (Il2CppRGCTXDataType)3, 24374 },
	{ (Il2CppRGCTXDataType)2, 31619 },
	{ (Il2CppRGCTXDataType)3, 24375 },
	{ (Il2CppRGCTXDataType)3, 24376 },
	{ (Il2CppRGCTXDataType)2, 31620 },
	{ (Il2CppRGCTXDataType)2, 24688 },
	{ (Il2CppRGCTXDataType)2, 31621 },
	{ (Il2CppRGCTXDataType)2, 24690 },
	{ (Il2CppRGCTXDataType)2, 24691 },
	{ (Il2CppRGCTXDataType)2, 31622 },
	{ (Il2CppRGCTXDataType)3, 24377 },
	{ (Il2CppRGCTXDataType)3, 24378 },
	{ (Il2CppRGCTXDataType)3, 24379 },
	{ (Il2CppRGCTXDataType)2, 24697 },
	{ (Il2CppRGCTXDataType)3, 24380 },
	{ (Il2CppRGCTXDataType)3, 24381 },
	{ (Il2CppRGCTXDataType)2, 24709 },
	{ (Il2CppRGCTXDataType)2, 31623 },
	{ (Il2CppRGCTXDataType)3, 24382 },
	{ (Il2CppRGCTXDataType)3, 24383 },
	{ (Il2CppRGCTXDataType)2, 24711 },
	{ (Il2CppRGCTXDataType)2, 31451 },
	{ (Il2CppRGCTXDataType)3, 24384 },
	{ (Il2CppRGCTXDataType)3, 24385 },
	{ (Il2CppRGCTXDataType)2, 31624 },
	{ (Il2CppRGCTXDataType)3, 24386 },
	{ (Il2CppRGCTXDataType)3, 24387 },
	{ (Il2CppRGCTXDataType)2, 24721 },
	{ (Il2CppRGCTXDataType)2, 31625 },
	{ (Il2CppRGCTXDataType)3, 24388 },
	{ (Il2CppRGCTXDataType)3, 24389 },
	{ (Il2CppRGCTXDataType)3, 23553 },
	{ (Il2CppRGCTXDataType)3, 24390 },
	{ (Il2CppRGCTXDataType)2, 31626 },
	{ (Il2CppRGCTXDataType)3, 24391 },
	{ (Il2CppRGCTXDataType)3, 24392 },
	{ (Il2CppRGCTXDataType)2, 24733 },
	{ (Il2CppRGCTXDataType)2, 31627 },
	{ (Il2CppRGCTXDataType)3, 24393 },
	{ (Il2CppRGCTXDataType)3, 24394 },
	{ (Il2CppRGCTXDataType)3, 24395 },
	{ (Il2CppRGCTXDataType)3, 24396 },
	{ (Il2CppRGCTXDataType)3, 24397 },
	{ (Il2CppRGCTXDataType)3, 23559 },
	{ (Il2CppRGCTXDataType)3, 24398 },
	{ (Il2CppRGCTXDataType)2, 31628 },
	{ (Il2CppRGCTXDataType)3, 24399 },
	{ (Il2CppRGCTXDataType)3, 24400 },
	{ (Il2CppRGCTXDataType)2, 24746 },
	{ (Il2CppRGCTXDataType)2, 31629 },
	{ (Il2CppRGCTXDataType)3, 24401 },
	{ (Il2CppRGCTXDataType)3, 24402 },
	{ (Il2CppRGCTXDataType)2, 24748 },
	{ (Il2CppRGCTXDataType)2, 31630 },
	{ (Il2CppRGCTXDataType)3, 24403 },
	{ (Il2CppRGCTXDataType)3, 24404 },
	{ (Il2CppRGCTXDataType)2, 31631 },
	{ (Il2CppRGCTXDataType)3, 24405 },
	{ (Il2CppRGCTXDataType)3, 24406 },
	{ (Il2CppRGCTXDataType)2, 31632 },
	{ (Il2CppRGCTXDataType)3, 24407 },
	{ (Il2CppRGCTXDataType)3, 24408 },
	{ (Il2CppRGCTXDataType)2, 24763 },
	{ (Il2CppRGCTXDataType)2, 31633 },
	{ (Il2CppRGCTXDataType)3, 24409 },
	{ (Il2CppRGCTXDataType)3, 24410 },
	{ (Il2CppRGCTXDataType)3, 24411 },
	{ (Il2CppRGCTXDataType)3, 23570 },
	{ (Il2CppRGCTXDataType)2, 31634 },
	{ (Il2CppRGCTXDataType)3, 24412 },
	{ (Il2CppRGCTXDataType)3, 24413 },
	{ (Il2CppRGCTXDataType)2, 31635 },
	{ (Il2CppRGCTXDataType)3, 24414 },
	{ (Il2CppRGCTXDataType)3, 24415 },
	{ (Il2CppRGCTXDataType)2, 24779 },
	{ (Il2CppRGCTXDataType)2, 31636 },
	{ (Il2CppRGCTXDataType)3, 24416 },
	{ (Il2CppRGCTXDataType)3, 24417 },
	{ (Il2CppRGCTXDataType)3, 24418 },
	{ (Il2CppRGCTXDataType)3, 24419 },
	{ (Il2CppRGCTXDataType)3, 24420 },
	{ (Il2CppRGCTXDataType)3, 24421 },
	{ (Il2CppRGCTXDataType)3, 23576 },
	{ (Il2CppRGCTXDataType)2, 31637 },
	{ (Il2CppRGCTXDataType)3, 24422 },
	{ (Il2CppRGCTXDataType)3, 24423 },
	{ (Il2CppRGCTXDataType)2, 31638 },
	{ (Il2CppRGCTXDataType)3, 24424 },
	{ (Il2CppRGCTXDataType)3, 24425 },
	{ (Il2CppRGCTXDataType)3, 24426 },
	{ (Il2CppRGCTXDataType)3, 24427 },
	{ (Il2CppRGCTXDataType)3, 24428 },
	{ (Il2CppRGCTXDataType)3, 24429 },
	{ (Il2CppRGCTXDataType)2, 31639 },
	{ (Il2CppRGCTXDataType)2, 31640 },
	{ (Il2CppRGCTXDataType)3, 24430 },
	{ (Il2CppRGCTXDataType)2, 24814 },
	{ (Il2CppRGCTXDataType)2, 24808 },
	{ (Il2CppRGCTXDataType)3, 24431 },
	{ (Il2CppRGCTXDataType)2, 24807 },
	{ (Il2CppRGCTXDataType)2, 31641 },
	{ (Il2CppRGCTXDataType)3, 24432 },
	{ (Il2CppRGCTXDataType)3, 24433 },
	{ (Il2CppRGCTXDataType)3, 24434 },
	{ (Il2CppRGCTXDataType)3, 24435 },
	{ (Il2CppRGCTXDataType)2, 24827 },
	{ (Il2CppRGCTXDataType)2, 24822 },
	{ (Il2CppRGCTXDataType)3, 24436 },
	{ (Il2CppRGCTXDataType)2, 24821 },
	{ (Il2CppRGCTXDataType)2, 31642 },
	{ (Il2CppRGCTXDataType)3, 24437 },
	{ (Il2CppRGCTXDataType)3, 24438 },
	{ (Il2CppRGCTXDataType)3, 24439 },
	{ (Il2CppRGCTXDataType)2, 31643 },
	{ (Il2CppRGCTXDataType)3, 24440 },
	{ (Il2CppRGCTXDataType)2, 24840 },
	{ (Il2CppRGCTXDataType)2, 24832 },
	{ (Il2CppRGCTXDataType)3, 24441 },
	{ (Il2CppRGCTXDataType)3, 24442 },
	{ (Il2CppRGCTXDataType)2, 24831 },
	{ (Il2CppRGCTXDataType)2, 31644 },
	{ (Il2CppRGCTXDataType)3, 24443 },
	{ (Il2CppRGCTXDataType)3, 24444 },
	{ (Il2CppRGCTXDataType)3, 24445 },
	{ (Il2CppRGCTXDataType)2, 24844 },
	{ (Il2CppRGCTXDataType)3, 24446 },
	{ (Il2CppRGCTXDataType)2, 31645 },
	{ (Il2CppRGCTXDataType)3, 24447 },
	{ (Il2CppRGCTXDataType)3, 24448 },
	{ (Il2CppRGCTXDataType)3, 24449 },
	{ (Il2CppRGCTXDataType)2, 24852 },
	{ (Il2CppRGCTXDataType)3, 24450 },
	{ (Il2CppRGCTXDataType)2, 31646 },
	{ (Il2CppRGCTXDataType)3, 24451 },
	{ (Il2CppRGCTXDataType)3, 24452 },
	{ (Il2CppRGCTXDataType)2, 31647 },
	{ (Il2CppRGCTXDataType)2, 31648 },
	{ (Il2CppRGCTXDataType)3, 24453 },
	{ (Il2CppRGCTXDataType)2, 31649 },
	{ (Il2CppRGCTXDataType)2, 31650 },
	{ (Il2CppRGCTXDataType)3, 24454 },
	{ (Il2CppRGCTXDataType)3, 24455 },
	{ (Il2CppRGCTXDataType)2, 24871 },
	{ (Il2CppRGCTXDataType)3, 24456 },
	{ (Il2CppRGCTXDataType)2, 24872 },
	{ (Il2CppRGCTXDataType)2, 31651 },
	{ (Il2CppRGCTXDataType)3, 24457 },
	{ (Il2CppRGCTXDataType)3, 24458 },
	{ (Il2CppRGCTXDataType)2, 31652 },
	{ (Il2CppRGCTXDataType)3, 24459 },
	{ (Il2CppRGCTXDataType)2, 31653 },
	{ (Il2CppRGCTXDataType)3, 24460 },
	{ (Il2CppRGCTXDataType)3, 24461 },
	{ (Il2CppRGCTXDataType)3, 24462 },
	{ (Il2CppRGCTXDataType)2, 24891 },
	{ (Il2CppRGCTXDataType)3, 24463 },
	{ (Il2CppRGCTXDataType)2, 24899 },
	{ (Il2CppRGCTXDataType)3, 24464 },
	{ (Il2CppRGCTXDataType)2, 31654 },
	{ (Il2CppRGCTXDataType)2, 31655 },
	{ (Il2CppRGCTXDataType)3, 24465 },
	{ (Il2CppRGCTXDataType)3, 24466 },
	{ (Il2CppRGCTXDataType)3, 24467 },
	{ (Il2CppRGCTXDataType)3, 24468 },
	{ (Il2CppRGCTXDataType)3, 24469 },
	{ (Il2CppRGCTXDataType)3, 24470 },
	{ (Il2CppRGCTXDataType)2, 24915 },
	{ (Il2CppRGCTXDataType)2, 31656 },
	{ (Il2CppRGCTXDataType)3, 24471 },
	{ (Il2CppRGCTXDataType)3, 24472 },
	{ (Il2CppRGCTXDataType)2, 24919 },
	{ (Il2CppRGCTXDataType)3, 24473 },
	{ (Il2CppRGCTXDataType)2, 31657 },
	{ (Il2CppRGCTXDataType)2, 24929 },
	{ (Il2CppRGCTXDataType)2, 24927 },
	{ (Il2CppRGCTXDataType)2, 31658 },
	{ (Il2CppRGCTXDataType)3, 24474 },
	{ (Il2CppRGCTXDataType)2, 31659 },
	{ (Il2CppRGCTXDataType)3, 24475 },
	{ (Il2CppRGCTXDataType)3, 24476 },
	{ (Il2CppRGCTXDataType)3, 24477 },
	{ (Il2CppRGCTXDataType)2, 24933 },
	{ (Il2CppRGCTXDataType)3, 24478 },
	{ (Il2CppRGCTXDataType)3, 24479 },
	{ (Il2CppRGCTXDataType)2, 24936 },
	{ (Il2CppRGCTXDataType)3, 24480 },
	{ (Il2CppRGCTXDataType)1, 31660 },
	{ (Il2CppRGCTXDataType)2, 24935 },
	{ (Il2CppRGCTXDataType)3, 24481 },
	{ (Il2CppRGCTXDataType)1, 24935 },
	{ (Il2CppRGCTXDataType)1, 24933 },
	{ (Il2CppRGCTXDataType)2, 31661 },
	{ (Il2CppRGCTXDataType)2, 24935 },
	{ (Il2CppRGCTXDataType)3, 24482 },
	{ (Il2CppRGCTXDataType)3, 24483 },
	{ (Il2CppRGCTXDataType)3, 24484 },
	{ (Il2CppRGCTXDataType)2, 24934 },
	{ (Il2CppRGCTXDataType)3, 24485 },
	{ (Il2CppRGCTXDataType)2, 24947 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	239,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	65,
	s_rgctxIndices,
	288,
	s_rgctxValues,
	NULL,
};
