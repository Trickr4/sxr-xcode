﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAssetBundle::GetAssetBundle(System.String,System.UInt32)
extern void UnityWebRequestAssetBundle_GetAssetBundle_m7B84478DA8C074ACED6A71C3A8E434335E7D803C ();
// 0x00000002 System.IntPtr UnityEngine.Networking.DownloadHandlerAssetBundle::Create(UnityEngine.Networking.DownloadHandlerAssetBundle,System.String,System.UInt32)
extern void DownloadHandlerAssetBundle_Create_mF2BE2489505217F7017337C5D8D1BB379C99E172 ();
// 0x00000003 System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::InternalCreateAssetBundle(System.String,System.UInt32)
extern void DownloadHandlerAssetBundle_InternalCreateAssetBundle_m4A8DF4FEFAA7A192CAA269AC3EC7D801FC949B4F ();
// 0x00000004 System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::.ctor(System.String,System.UInt32)
extern void DownloadHandlerAssetBundle__ctor_m1B3026A98DBBB87D338779CE3FB78F0882D3F833 ();
// 0x00000005 System.Byte[] UnityEngine.Networking.DownloadHandlerAssetBundle::GetData()
extern void DownloadHandlerAssetBundle_GetData_mEE15F9F5235CA56D352A42360DBB97F4FC991F79 ();
// 0x00000006 System.String UnityEngine.Networking.DownloadHandlerAssetBundle::GetText()
extern void DownloadHandlerAssetBundle_GetText_m0144E0707A55ADBBC66C19DC5D6221D3223F84A6 ();
// 0x00000007 UnityEngine.AssetBundle UnityEngine.Networking.DownloadHandlerAssetBundle::get_assetBundle()
extern void DownloadHandlerAssetBundle_get_assetBundle_mC9081BC84B18ED108B8AA06C104985F62AFBD589 ();
// 0x00000008 UnityEngine.AssetBundle UnityEngine.Networking.DownloadHandlerAssetBundle::GetContent(UnityEngine.Networking.UnityWebRequest)
extern void DownloadHandlerAssetBundle_GetContent_mA39FD9C402592881680017B080D72337F708A287 ();
static Il2CppMethodPointer s_methodPointers[8] = 
{
	UnityWebRequestAssetBundle_GetAssetBundle_m7B84478DA8C074ACED6A71C3A8E434335E7D803C,
	DownloadHandlerAssetBundle_Create_mF2BE2489505217F7017337C5D8D1BB379C99E172,
	DownloadHandlerAssetBundle_InternalCreateAssetBundle_m4A8DF4FEFAA7A192CAA269AC3EC7D801FC949B4F,
	DownloadHandlerAssetBundle__ctor_m1B3026A98DBBB87D338779CE3FB78F0882D3F833,
	DownloadHandlerAssetBundle_GetData_mEE15F9F5235CA56D352A42360DBB97F4FC991F79,
	DownloadHandlerAssetBundle_GetText_m0144E0707A55ADBBC66C19DC5D6221D3223F84A6,
	DownloadHandlerAssetBundle_get_assetBundle_mC9081BC84B18ED108B8AA06C104985F62AFBD589,
	DownloadHandlerAssetBundle_GetContent_mA39FD9C402592881680017B080D72337F708A287,
};
static const int32_t s_InvokerIndices[8] = 
{
	119,
	1830,
	130,
	130,
	14,
	14,
	14,
	0,
};
extern const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestAssetBundleModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestAssetBundleModuleCodeGenModule = 
{
	"UnityEngine.UnityWebRequestAssetBundleModule.dll",
	8,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
