﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Video.VideoClipPlayable::GetHandle()
extern void VideoClipPlayable_GetHandle_m5B2EFD8CFE93DB4D2EE29A600E598233471DF242_AdjustorThunk ();
// 0x00000002 System.Boolean UnityEngine.Experimental.Video.VideoClipPlayable::Equals(UnityEngine.Experimental.Video.VideoClipPlayable)
extern void VideoClipPlayable_Equals_m2F07E6EBA96043274F2384F18E43B66035058BC2_AdjustorThunk ();
// 0x00000003 System.Void UnityEngine.Video.VideoPlayer::set_source(UnityEngine.Video.VideoSource)
extern void VideoPlayer_set_source_mE27652C749468B4A44FE3557BA809C503C849ABE ();
// 0x00000004 System.String UnityEngine.Video.VideoPlayer::get_url()
extern void VideoPlayer_get_url_mC8F4EC64F3938721095E741897A83F4A35741203 ();
// 0x00000005 System.Void UnityEngine.Video.VideoPlayer::set_url(System.String)
extern void VideoPlayer_set_url_m076BC425E754574E8DFCDFA7AC2A37A3EFD5AF21 ();
// 0x00000006 System.Void UnityEngine.Video.VideoPlayer::set_clip(UnityEngine.Video.VideoClip)
extern void VideoPlayer_set_clip_mD8D5AA8642010DFACD0B88F38FAF7CBBB8DD4E64 ();
// 0x00000007 System.Void UnityEngine.Video.VideoPlayer::set_renderMode(UnityEngine.Video.VideoRenderMode)
extern void VideoPlayer_set_renderMode_m9DC3CFC4B99F66FA6E6116379D8C7F64D798CB63 ();
// 0x00000008 System.Void UnityEngine.Video.VideoPlayer::set_targetCamera(UnityEngine.Camera)
extern void VideoPlayer_set_targetCamera_mAD892B74820C093D33A8808C6CA4BFF7935773C4 ();
// 0x00000009 UnityEngine.RenderTexture UnityEngine.Video.VideoPlayer::get_targetTexture()
extern void VideoPlayer_get_targetTexture_mDFF8E4B60BDFE26F5535E5D15CAAA8546DB939E3 ();
// 0x0000000A System.Void UnityEngine.Video.VideoPlayer::set_targetTexture(UnityEngine.RenderTexture)
extern void VideoPlayer_set_targetTexture_m4088B6058A001D48C911DC71023AC723BD3C8252 ();
// 0x0000000B System.Void UnityEngine.Video.VideoPlayer::set_aspectRatio(UnityEngine.Video.VideoAspectRatio)
extern void VideoPlayer_set_aspectRatio_mEA5D88F04CF46AE08D472146D0A0274DA5E97065 ();
// 0x0000000C System.Void UnityEngine.Video.VideoPlayer::Prepare()
extern void VideoPlayer_Prepare_m32AB43745A92A6762D570E60975AD69DB8FFF566 ();
// 0x0000000D System.Boolean UnityEngine.Video.VideoPlayer::get_isPrepared()
extern void VideoPlayer_get_isPrepared_mF0DC157BD0B5E8FF26A27EA7ABE4BEDCBE963694 ();
// 0x0000000E System.Void UnityEngine.Video.VideoPlayer::set_playOnAwake(System.Boolean)
extern void VideoPlayer_set_playOnAwake_m3109BDD49A2981518F062FACBF0ECF1DB8BFE207 ();
// 0x0000000F System.Void UnityEngine.Video.VideoPlayer::Play()
extern void VideoPlayer_Play_m2BCD775F42A36AC291C7B32D9E4D934EF0B91257 ();
// 0x00000010 System.Void UnityEngine.Video.VideoPlayer::Pause()
extern void VideoPlayer_Pause_m62EE5660CFA287D78BB3FE815CA5649242509B93 ();
// 0x00000011 System.Double UnityEngine.Video.VideoPlayer::get_time()
extern void VideoPlayer_get_time_mF842FC1E9A1FD2333C7C9D13338D3B178665564A ();
// 0x00000012 System.Void UnityEngine.Video.VideoPlayer::set_time(System.Double)
extern void VideoPlayer_set_time_m474F47BCE704FFCB3C6FFAAE6414A9ED89678514 ();
// 0x00000013 System.Void UnityEngine.Video.VideoPlayer::set_skipOnDrop(System.Boolean)
extern void VideoPlayer_set_skipOnDrop_mBAA06EE0F8E6E41AA3CC43F0791E76BF2C14C0C8 ();
// 0x00000014 System.Double UnityEngine.Video.VideoPlayer::get_length()
extern void VideoPlayer_get_length_m593F8FA891C7B1D76BFAF6F1DFFAE2BB85FEDDC3 ();
// 0x00000015 System.UInt16 UnityEngine.Video.VideoPlayer::get_controlledAudioTrackMaxCount()
extern void VideoPlayer_get_controlledAudioTrackMaxCount_m5CD07FAE01ADA002F88F9E7B3432993AB52DAAF1 ();
// 0x00000016 System.Void UnityEngine.Video.VideoPlayer::set_controlledAudioTrackCount(System.UInt16)
extern void VideoPlayer_set_controlledAudioTrackCount_mE67D147B78D4DB81593E6E29631D475D98E7DA3B ();
// 0x00000017 System.Void UnityEngine.Video.VideoPlayer::SetControlledAudioTrackCount(System.UInt16)
extern void VideoPlayer_SetControlledAudioTrackCount_m4E37B42737D9CA58DD795F30DC42FF48CB0E1069 ();
// 0x00000018 System.Void UnityEngine.Video.VideoPlayer::EnableAudioTrack(System.UInt16,System.Boolean)
extern void VideoPlayer_EnableAudioTrack_m30901BF68B20BB8397E246FDDE699E5D513AAA5B ();
// 0x00000019 System.Void UnityEngine.Video.VideoPlayer::set_audioOutputMode(UnityEngine.Video.VideoAudioOutputMode)
extern void VideoPlayer_set_audioOutputMode_m95A8791FE87A490EAEF1959E7597E0CC1659227F ();
// 0x0000001A System.Void UnityEngine.Video.VideoPlayer::SetTargetAudioSource(System.UInt16,UnityEngine.AudioSource)
extern void VideoPlayer_SetTargetAudioSource_m98880F0925C0E7DDFCB946563A23613CFFC494FE ();
// 0x0000001B System.Void UnityEngine.Video.VideoPlayer::InvokePrepareCompletedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokePrepareCompletedCallback_Internal_m4CFD7054C97BE95CAC055CF18466E90D060E9B53 ();
// 0x0000001C System.Void UnityEngine.Video.VideoPlayer::InvokeFrameReadyCallback_Internal(UnityEngine.Video.VideoPlayer,System.Int64)
extern void VideoPlayer_InvokeFrameReadyCallback_Internal_m4F62FC3695CFC72045E3C90503D541AE6E023EE8 ();
// 0x0000001D System.Void UnityEngine.Video.VideoPlayer::InvokeLoopPointReachedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokeLoopPointReachedCallback_Internal_m1A07EB382FD3CE673FF171A11047BEC54A6BB9AB ();
// 0x0000001E System.Void UnityEngine.Video.VideoPlayer::InvokeStartedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokeStartedCallback_Internal_m28D5BF153FC37959C225292BE859135FF778C8BB ();
// 0x0000001F System.Void UnityEngine.Video.VideoPlayer::InvokeFrameDroppedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokeFrameDroppedCallback_Internal_m669EAEF06893B53351EE81C3858BD62661228C58 ();
// 0x00000020 System.Void UnityEngine.Video.VideoPlayer::InvokeErrorReceivedCallback_Internal(UnityEngine.Video.VideoPlayer,System.String)
extern void VideoPlayer_InvokeErrorReceivedCallback_Internal_mF7849030756F4A2B5226437C165ECDFE6E52E385 ();
// 0x00000021 System.Void UnityEngine.Video.VideoPlayer::InvokeSeekCompletedCallback_Internal(UnityEngine.Video.VideoPlayer)
extern void VideoPlayer_InvokeSeekCompletedCallback_Internal_mFFB686F5BF044F61495CCF2EFB3517857F98C078 ();
// 0x00000022 System.Void UnityEngine.Video.VideoPlayer::InvokeClockResyncOccurredCallback_Internal(UnityEngine.Video.VideoPlayer,System.Double)
extern void VideoPlayer_InvokeClockResyncOccurredCallback_Internal_m21D2B32DDE5ED48F6D6F2ECA9B7A0A2724AF9D6F ();
// 0x00000023 System.Void UnityEngine.Video.VideoPlayer_EventHandler::.ctor(System.Object,System.IntPtr)
extern void EventHandler__ctor_mA31DCA369A8B7C473F6CE19F6B53D6F3FAF7D6A7 ();
// 0x00000024 System.Void UnityEngine.Video.VideoPlayer_EventHandler::Invoke(UnityEngine.Video.VideoPlayer)
extern void EventHandler_Invoke_m137A7D976F198147AD939AEF51E157107A3B1FBC ();
// 0x00000025 System.IAsyncResult UnityEngine.Video.VideoPlayer_EventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.AsyncCallback,System.Object)
extern void EventHandler_BeginInvoke_mCA1B5193B15F3D56BCB40A8DDBA703724040F348 ();
// 0x00000026 System.Void UnityEngine.Video.VideoPlayer_EventHandler::EndInvoke(System.IAsyncResult)
extern void EventHandler_EndInvoke_m95A975D9455F92F836E7E19BDB85538B1EBF4067 ();
// 0x00000027 System.Void UnityEngine.Video.VideoPlayer_ErrorEventHandler::.ctor(System.Object,System.IntPtr)
extern void ErrorEventHandler__ctor_m9E9B3A7A439858703258976491E29057CB17F534 ();
// 0x00000028 System.Void UnityEngine.Video.VideoPlayer_ErrorEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.String)
extern void ErrorEventHandler_Invoke_m0A812811B673439792D99C125EE4FFE5E358EF6C ();
// 0x00000029 System.IAsyncResult UnityEngine.Video.VideoPlayer_ErrorEventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.String,System.AsyncCallback,System.Object)
extern void ErrorEventHandler_BeginInvoke_mD4C6F60629C221D7702E40D460818B90032FBAA7 ();
// 0x0000002A System.Void UnityEngine.Video.VideoPlayer_ErrorEventHandler::EndInvoke(System.IAsyncResult)
extern void ErrorEventHandler_EndInvoke_m7ABF3F8E15D2EF4AE6961324B66208A7FD127295 ();
// 0x0000002B System.Void UnityEngine.Video.VideoPlayer_FrameReadyEventHandler::.ctor(System.Object,System.IntPtr)
extern void FrameReadyEventHandler__ctor_m7DFDBF9203E8F9FC1093E1655C5E2695623D7E3D ();
// 0x0000002C System.Void UnityEngine.Video.VideoPlayer_FrameReadyEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.Int64)
extern void FrameReadyEventHandler_Invoke_m88D0AC1BED08D66B6CFA18DA23C58D10795DDA70 ();
// 0x0000002D System.IAsyncResult UnityEngine.Video.VideoPlayer_FrameReadyEventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.Int64,System.AsyncCallback,System.Object)
extern void FrameReadyEventHandler_BeginInvoke_m5DA99DFE61C78E158FF79535447F7649FC09E5F1 ();
// 0x0000002E System.Void UnityEngine.Video.VideoPlayer_FrameReadyEventHandler::EndInvoke(System.IAsyncResult)
extern void FrameReadyEventHandler_EndInvoke_mC54DCEDB2F8CB30CBC6CD1590E2C08150E3E0CFF ();
// 0x0000002F System.Void UnityEngine.Video.VideoPlayer_TimeEventHandler::.ctor(System.Object,System.IntPtr)
extern void TimeEventHandler__ctor_mF41715E69B793B1C7DCA3A619CFB05097466523F ();
// 0x00000030 System.Void UnityEngine.Video.VideoPlayer_TimeEventHandler::Invoke(UnityEngine.Video.VideoPlayer,System.Double)
extern void TimeEventHandler_Invoke_m278E51F2838EC435606BE1CB3AD0E881505FAE10 ();
// 0x00000031 System.IAsyncResult UnityEngine.Video.VideoPlayer_TimeEventHandler::BeginInvoke(UnityEngine.Video.VideoPlayer,System.Double,System.AsyncCallback,System.Object)
extern void TimeEventHandler_BeginInvoke_m184CF1FDB1D643F00FE3C60982ED62EC4888F21D ();
// 0x00000032 System.Void UnityEngine.Video.VideoPlayer_TimeEventHandler::EndInvoke(System.IAsyncResult)
extern void TimeEventHandler_EndInvoke_m2759449ABAAE5711D31E414D5906AB042725F7AA ();
static Il2CppMethodPointer s_methodPointers[50] = 
{
	VideoClipPlayable_GetHandle_m5B2EFD8CFE93DB4D2EE29A600E598233471DF242_AdjustorThunk,
	VideoClipPlayable_Equals_m2F07E6EBA96043274F2384F18E43B66035058BC2_AdjustorThunk,
	VideoPlayer_set_source_mE27652C749468B4A44FE3557BA809C503C849ABE,
	VideoPlayer_get_url_mC8F4EC64F3938721095E741897A83F4A35741203,
	VideoPlayer_set_url_m076BC425E754574E8DFCDFA7AC2A37A3EFD5AF21,
	VideoPlayer_set_clip_mD8D5AA8642010DFACD0B88F38FAF7CBBB8DD4E64,
	VideoPlayer_set_renderMode_m9DC3CFC4B99F66FA6E6116379D8C7F64D798CB63,
	VideoPlayer_set_targetCamera_mAD892B74820C093D33A8808C6CA4BFF7935773C4,
	VideoPlayer_get_targetTexture_mDFF8E4B60BDFE26F5535E5D15CAAA8546DB939E3,
	VideoPlayer_set_targetTexture_m4088B6058A001D48C911DC71023AC723BD3C8252,
	VideoPlayer_set_aspectRatio_mEA5D88F04CF46AE08D472146D0A0274DA5E97065,
	VideoPlayer_Prepare_m32AB43745A92A6762D570E60975AD69DB8FFF566,
	VideoPlayer_get_isPrepared_mF0DC157BD0B5E8FF26A27EA7ABE4BEDCBE963694,
	VideoPlayer_set_playOnAwake_m3109BDD49A2981518F062FACBF0ECF1DB8BFE207,
	VideoPlayer_Play_m2BCD775F42A36AC291C7B32D9E4D934EF0B91257,
	VideoPlayer_Pause_m62EE5660CFA287D78BB3FE815CA5649242509B93,
	VideoPlayer_get_time_mF842FC1E9A1FD2333C7C9D13338D3B178665564A,
	VideoPlayer_set_time_m474F47BCE704FFCB3C6FFAAE6414A9ED89678514,
	VideoPlayer_set_skipOnDrop_mBAA06EE0F8E6E41AA3CC43F0791E76BF2C14C0C8,
	VideoPlayer_get_length_m593F8FA891C7B1D76BFAF6F1DFFAE2BB85FEDDC3,
	VideoPlayer_get_controlledAudioTrackMaxCount_m5CD07FAE01ADA002F88F9E7B3432993AB52DAAF1,
	VideoPlayer_set_controlledAudioTrackCount_mE67D147B78D4DB81593E6E29631D475D98E7DA3B,
	VideoPlayer_SetControlledAudioTrackCount_m4E37B42737D9CA58DD795F30DC42FF48CB0E1069,
	VideoPlayer_EnableAudioTrack_m30901BF68B20BB8397E246FDDE699E5D513AAA5B,
	VideoPlayer_set_audioOutputMode_m95A8791FE87A490EAEF1959E7597E0CC1659227F,
	VideoPlayer_SetTargetAudioSource_m98880F0925C0E7DDFCB946563A23613CFFC494FE,
	VideoPlayer_InvokePrepareCompletedCallback_Internal_m4CFD7054C97BE95CAC055CF18466E90D060E9B53,
	VideoPlayer_InvokeFrameReadyCallback_Internal_m4F62FC3695CFC72045E3C90503D541AE6E023EE8,
	VideoPlayer_InvokeLoopPointReachedCallback_Internal_m1A07EB382FD3CE673FF171A11047BEC54A6BB9AB,
	VideoPlayer_InvokeStartedCallback_Internal_m28D5BF153FC37959C225292BE859135FF778C8BB,
	VideoPlayer_InvokeFrameDroppedCallback_Internal_m669EAEF06893B53351EE81C3858BD62661228C58,
	VideoPlayer_InvokeErrorReceivedCallback_Internal_mF7849030756F4A2B5226437C165ECDFE6E52E385,
	VideoPlayer_InvokeSeekCompletedCallback_Internal_mFFB686F5BF044F61495CCF2EFB3517857F98C078,
	VideoPlayer_InvokeClockResyncOccurredCallback_Internal_m21D2B32DDE5ED48F6D6F2ECA9B7A0A2724AF9D6F,
	EventHandler__ctor_mA31DCA369A8B7C473F6CE19F6B53D6F3FAF7D6A7,
	EventHandler_Invoke_m137A7D976F198147AD939AEF51E157107A3B1FBC,
	EventHandler_BeginInvoke_mCA1B5193B15F3D56BCB40A8DDBA703724040F348,
	EventHandler_EndInvoke_m95A975D9455F92F836E7E19BDB85538B1EBF4067,
	ErrorEventHandler__ctor_m9E9B3A7A439858703258976491E29057CB17F534,
	ErrorEventHandler_Invoke_m0A812811B673439792D99C125EE4FFE5E358EF6C,
	ErrorEventHandler_BeginInvoke_mD4C6F60629C221D7702E40D460818B90032FBAA7,
	ErrorEventHandler_EndInvoke_m7ABF3F8E15D2EF4AE6961324B66208A7FD127295,
	FrameReadyEventHandler__ctor_m7DFDBF9203E8F9FC1093E1655C5E2695623D7E3D,
	FrameReadyEventHandler_Invoke_m88D0AC1BED08D66B6CFA18DA23C58D10795DDA70,
	FrameReadyEventHandler_BeginInvoke_m5DA99DFE61C78E158FF79535447F7649FC09E5F1,
	FrameReadyEventHandler_EndInvoke_mC54DCEDB2F8CB30CBC6CD1590E2C08150E3E0CFF,
	TimeEventHandler__ctor_mF41715E69B793B1C7DCA3A619CFB05097466523F,
	TimeEventHandler_Invoke_m278E51F2838EC435606BE1CB3AD0E881505FAE10,
	TimeEventHandler_BeginInvoke_m184CF1FDB1D643F00FE3C60982ED62EC4888F21D,
	TimeEventHandler_EndInvoke_m2759449ABAAE5711D31E414D5906AB042725F7AA,
};
static const int32_t s_InvokerIndices[50] = 
{
	1744,
	1987,
	32,
	14,
	26,
	26,
	32,
	26,
	14,
	26,
	32,
	23,
	89,
	31,
	23,
	23,
	464,
	339,
	31,
	464,
	771,
	610,
	610,
	1139,
	32,
	1087,
	163,
	161,
	163,
	163,
	163,
	137,
	163,
	1988,
	124,
	26,
	214,
	26,
	124,
	27,
	125,
	26,
	124,
	180,
	1989,
	26,
	124,
	1214,
	1990,
	26,
};
extern const Il2CppCodeGenModule g_UnityEngine_VideoModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VideoModuleCodeGenModule = 
{
	"UnityEngine.VideoModule.dll",
	50,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
